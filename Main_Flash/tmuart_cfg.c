
#include "_fr.h"
#include "extern.h"

#include "tmtypes.h"
#include "tmuart1_private.h"

static tm_uart_t g_Uart1;

extern int uart1rxevent;
extern int uart1txevent;

tm_u8_t tr_data[]={'T','R','A','K','M','A','T','E','\r','\n'};  /* transmission data area */
tm_u8_t txbuf[64];
//unsigned char tr_data[]={0xAA, 0x00,0x55,0x00};  /* transmission data area */
tm_u8_t re_data[20];
tm_u8_t num1 = 0;
tm_u8_t num0 = 0;
tm_u8_t tmp;
tm_u16_t txlen;

#pragma section CODE=CODE_MYAPP, attr=CODE
void Asynch_UART1_Init(void)
{

  /* UART1 mode setting */
  IO_MFS1.UART.SCR.bit.UPCL = 1;	/* UART Initial */

  IO_MFS1.UART.SMR.byte = 0x00;	/* Asynchronous serial mode */
  IO_MFS1.UART.SMR.bit.SOE = 1;	/* Serial Data output enable */

  /* baund rate 115.2kbps (16MHz) */
  IO_MFS1.UART.BGR = 138;			/* (16x1000000)/(138+1) = 115107 */

  /* UART control resister setting */
  IO_MFS1.UART.SCR.byte = 0x00;

  /* UART ESCR resister setting */
  IO_MFS1.UART.ESCR.byte = 0x00;

  /* Serial Transmission enable */
  IO_MFS1.UART.SCR.bit.TIE =1;	/* Transmit int enable */
  IO_MFS1.UART.SCR.bit.TXE = 1;	/* Transmit enable */

  IO_MFS1.UART.SCR.bit.RIE = 1;	/* Receive int enable */
  IO_MFS1.UART.SCR.bit.RXE = 1;	/* Receive enable */

}

void InitCfgUART(void)
{
  /* Srilal I/F Output Enable */
  IO_KEYCDR = 0x0E84;
  IO_KEYCDR = 0x4E84;
  IO_KEYCDR = 0x8E84;
  IO_KEYCDR = 0xCE84;
  //IO_EPFR35.byte = 0xE5;  /* SIN0_1,SCK0_1 */
  IO_EPFR36.byte = 0xF8;  /* SOT0_1,SCK0_1 */

  /* Peripheral terminal setting */
  IO_KEYCDR = 0x0E20;
  IO_KEYCDR = 0x4E20;
  IO_KEYCDR = 0x8E20;
  IO_KEYCDR = 0xCE20;
  IO_PFR00.byte = 0x02; /* ch8 SIN8_1 */

  KEYCDR = 0x0F40;
  KEYCDR = 0x4F40;
  KEYCDR = 0x8F40;
  KEYCDR = 0xcF40;
  IO_PORTEN.byte = 0xFD;

  KEYCDR = 0x24AC;
  KEYCDR = 0x64AC;
  KEYCDR = 0xA4AC;
  KEYCDR = 0xE4AC;
  ADERH0 = 0x0000;


  IO_KEYCDR = 0x0E2B;
  IO_KEYCDR = 0x4E2B;
  IO_KEYCDR = 0x8E2B;
  IO_KEYCDR = 0xCE2B;
  IO_PFR11.byte = 0x80;

  IO_KEYCDR = 0x0E0B;
  IO_KEYCDR = 0x4E0B;
  IO_KEYCDR = 0x8E0B;
  IO_KEYCDR = 0xCE0B;
  IO_DDR11.byte = 0x80;
  IO_PDR11.byte = 0x80;

  /* Interrupt level setting */
  IO_ICR[6].byte = 0x10;		/* Multi Func Serial RX0 */
  IO_ICR[7].byte = 0x11;		/* Multi Func Serial TX0 */

  txlen = 10;
  //Asynch_UART1_TX();
  //Asynch_UART1_RX();
  //
  memset((void *)&g_Uart1,0,sizeof(g_Uart1));

  g_Uart1.pRxBufferRdPtr = g_Uart1.RxBuffer;
  g_Uart1.pRxBufferWrPtr = g_Uart1.RxBuffer;

  g_Uart1.pTxQueueBufferWrPtr = g_Uart1.TxQueueBuffer;

  g_Uart1.QueueRdIndex = 0;
  g_Uart1.QueueWrIndex = 0;

  g_Uart1.pTxQueueBufferWrPtr = g_Uart1.TxQueueBuffer;
  g_Uart1.TxLen = 0;
  g_Uart1.bDataSent = TM_TRUE;
  g_Uart1.NbrOfBlocksInQueue = 0;

  Asynch_UART1_Init();

}

/* Multi Fuction Serial TX8 Int routine */
__interrupt void Multi_TX1_int(void)
{
  if(g_Uart1.TxLen != 0)
    IO_MFS1.CSIO.RDR.hword.RDR0 = *g_Uart1.pTx++;   /* transmission data set */
  else
  {
    IO_MFS1.UART.SCR.bit.TIE = 0; /* Transmit int disable to stop generating tx interrupts */
    g_Uart1.bDataSent = TM_TRUE;
    uart1txevent = TM_TRUE;
  }
  tmp = IO_MFS1.UART.SSR.byte;		/* dummy read */
  g_Uart1.TxLen--;
}

/* Multi Function Serial RX8 Int routine */
__interrupt void Multi_RX1_int(void)
{
  tm_u8_t ch;

  if(IO_MFS1.CSIO.SSR.bit.ORE == 1)
  {
    IO_MFS1.CSIO.SSR.bit.REC = 1;		/* error flag clear */
  }
  else
  {
    ch = IO_MFS1.CSIO.RDR.hword.RDR0; /* reception data read */
    tmp = IO_MFS1.UART.SSR.byte;      /* dummy read */
    *g_Uart1.pRxBufferWrPtr = ch;
    INCCIRCULARPTR(g_Uart1.pRxBufferWrPtr, g_Uart1.RxBuffer, RXBUFLEN);
    uart1rxevent = TM_TRUE;
  }
}

void UART1_BufferRead(tm_u8_t *pData, tm_u8_t *Len)
{

  *Len = 0;
  /* Diable interrupts */
  IO_MFS1.UART.SCR.bit.RIE = 0;

  /* Copy data */
  *Len = g_Uart1.pRxBufferWrPtr - g_Uart1.RxBuffer;
  memcpy(pData,g_Uart1.RxBuffer, *Len);
  g_Uart1.pRxBufferWrPtr = g_Uart1.RxBuffer;

  /* Enable Interrupts */
  IO_MFS1.UART.SCR.bit.RIE = 1;
}

void UART1_BufferWrite(tm_u8_t *pData, tm_u16_t Len)
{
  /* Copy data */
  g_Uart1.TxLen = Len;
  g_Uart1.pTx = pData;
  uart1txevent = TM_FALSE;

  /* Enable Interrupts */
  IO_MFS1.UART.SCR.bit.TIE = 1;
}

void UART1QueueData(tm_u8_t *pData, tm_u16_t Len)
{
  tm_u16_t tmpLen;
  /* Copy data to queue and update queue pts */

  tmpLen = TXQUEUEBUFLEN - (g_Uart1.pTxQueueBufferWrPtr - g_Uart1.TxQueueBuffer);

  if(tmpLen < Len)
  {
    /* Split data in to two queue entries */
    memcpy(g_Uart1.pTxQueueBufferWrPtr,pData,tmpLen);
    g_Uart1.Queue[g_Uart1.QueueWrIndex].pData = g_Uart1.pTxQueueBufferWrPtr;
    g_Uart1.Queue[g_Uart1.QueueWrIndex].Len = tmpLen;
    g_Uart1.Queue[g_Uart1.QueueWrIndex].bValid = TM_TRUE;
    INCCIRCULARINDEX(g_Uart1.QueueWrIndex, QUEUELEN);
    g_Uart1.NbrOfBlocksInQueue++;

    memcpy(g_Uart1.TxQueueBuffer,pData+tmpLen,Len - tmpLen);
    g_Uart1.Queue[g_Uart1.QueueWrIndex].pData = g_Uart1.TxQueueBuffer;
    g_Uart1.Queue[g_Uart1.QueueWrIndex].Len = Len - tmpLen;
    g_Uart1.Queue[g_Uart1.QueueWrIndex].bValid = TM_TRUE;
    INCCIRCULARINDEX(g_Uart1.QueueWrIndex, QUEUELEN);
    g_Uart1.pTxQueueBufferWrPtr = g_Uart1.TxQueueBuffer + Len - tmpLen;
    g_Uart1.NbrOfBlocksInQueue++;
  }
  else
  {
    memcpy(g_Uart1.pTxQueueBufferWrPtr,pData,Len);
    g_Uart1.Queue[g_Uart1.QueueWrIndex].pData = g_Uart1.pTxQueueBufferWrPtr;
    g_Uart1.Queue[g_Uart1.QueueWrIndex].Len = Len;
    g_Uart1.Queue[g_Uart1.QueueWrIndex].bValid = TM_TRUE;
    ADDCIRCULARPTR(g_Uart1.pTxQueueBufferWrPtr, Len, g_Uart1.TxQueueBuffer, TXQUEUEBUFLEN);
    INCCIRCULARINDEX(g_Uart1.QueueWrIndex, QUEUELEN);
    g_Uart1.NbrOfBlocksInQueue++;
  }
  if(g_Uart1.bDataSent == TM_TRUE)
  {
    UART1DeQueueData();
  }
}

void UART1DeQueueData(void)
{
  if(g_Uart1.NbrOfBlocksInQueue != 0)
  {
    g_Uart1.bDataSent = TM_FALSE;
    UART1_BufferWrite(g_Uart1.Queue[g_Uart1.QueueRdIndex].pData, g_Uart1.Queue[g_Uart1.QueueRdIndex].Len);
    INCCIRCULARINDEX(g_Uart1.QueueRdIndex, QUEUELEN);
    g_Uart1.NbrOfBlocksInQueue--;
  }
}

tm_bool_t UartQEmpty(void)
{
  if(g_Uart1.NbrOfBlocksInQueue == 0)
    return TM_TRUE;
  else
    return TM_FALSE;
}


