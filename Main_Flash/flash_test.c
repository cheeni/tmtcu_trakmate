/*******************************************************************************
* Copyright (C) 2013 Spansion LLC. All Rights Reserved. 
*
* This software is owned and published by: 
* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software contains source code for use with Spansion 
* components. This software is licensed by Spansion to be adapted only 
* for use in systems utilizing Spansion components. Spansion shall not be 
* responsible for misuse or illegal use of this software for devices not 
* supported herein.  Spansion is providing this software "AS IS" and will 
* not be responsible for issues arising from incorrect user implementation 
* of the software.  
*
* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE,
* REGARDING THE SOFTWARE (INCLUDING ANY ACOOMPANYING WRITTEN MATERIALS), 
* ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED USE, INCLUDING, 
* WITHOUT LIMITATION, THE IMPLIED WARRANTY OF MERCHANTABILITY, THE IMPLIED 
* WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE OR USE, AND THE IMPLIED 
* WARRANTY OF NONINFRINGEMENT.  
* SPANSION SHALL HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, 
* NEGLIGENCE OR OTHERWISE) FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT 
* LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, 
* LOSS OF BUSINESS INFORMATION, OR OTHER PECUNIARY LOSS) ARISING FROM USE OR 
* INABILITY TO USE THE SOFTWARE, INCLUDING, WITHOUT LIMITATION, ANY DIRECT, 
* INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, 
* SAVINGS OR PROFITS, 
* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 
* YOU ASSUME ALL RESPONSIBILITIES FOR SELECTION OF THE SOFTWARE TO ACHIEVE YOUR
* INTENDED RESULTS, AND FOR THE INSTALLATION OF, USE OF, AND RESULTS OBTAINED 
* FROM, THE SOFTWARE.  
*
* This software may be replicated in part or whole for the licensed use, 
* with the restriction that this Disclaimer and Copyright notice must be 
* included with each copy of this software, whether used in part or whole, 
* at all times.  
*****************************************************************************/
/**************************************************************************
 *  Date   :        2013/12/03
 *  PROJECT:        Main Flash
 *****************************************************************************/
#include "_fr.h"
#include "tmtypes.h"
#include "sectinfo.h"

/* Write Address of Main Flash (SA8) */
#define FLASH_ADR_WRITE	(volatile unsigned short *)(0x00080000)
#define FLASH_SIZE_HW 0x08
extern unsigned short write_data = 0x6363;
unsigned short tmp;

#pragma section CODE=CODE_MYAPP, attr=CODE
static  tm_sectinfo_t SectInfo;
void EraseSectors(void)
{
  SectInfo.sectaddr = FLASH_ADR_SEC2;
  SectInfo.a1 = FLASH_ADR2_3_COM1;
  SectInfo.a2 = FLASH_ADR2_3_COM2;
  SectInfo.a3 = FLASH_ADR2_3_COM1;
  SectInfo.a4 = FLASH_ADR2_3_COM1;
  SectInfo.a5 = FLASH_ADR2_3_COM2;

  __DI();
  EraseSector(&SectInfo);
  __EI();

  SectInfo.sectaddr = FLASH_ADR_SEC3;
  SectInfo.a1 = FLASH_ADR2_3_COM1;
  SectInfo.a2 = FLASH_ADR2_3_COM2;
  SectInfo.a3 = FLASH_ADR2_3_COM1;
  SectInfo.a4 = FLASH_ADR2_3_COM1;
  SectInfo.a5 = FLASH_ADR2_3_COM2;

  __DI();
  EraseSector(&SectInfo);
  __EI();

  SectInfo.sectaddr = FLASH_ADR_SEC4;
  SectInfo.a1 = FLASH_ADR4_5_COM1;
  SectInfo.a2 = FLASH_ADR4_5_COM2;
  SectInfo.a3 = FLASH_ADR4_5_COM1;
  SectInfo.a4 = FLASH_ADR4_5_COM1;
  SectInfo.a5 = FLASH_ADR4_5_COM2;

  __DI();
  EraseSector(&SectInfo);
  __EI();

  SectInfo.sectaddr = FLASH_ADR_SEC5;
  SectInfo.a1 = FLASH_ADR4_5_COM1;
  SectInfo.a2 = FLASH_ADR4_5_COM2;
  SectInfo.a3 = FLASH_ADR4_5_COM1;
  SectInfo.a4 = FLASH_ADR4_5_COM1;
  SectInfo.a5 = FLASH_ADR4_5_COM2;

  __DI();
  EraseSector(&SectInfo);
  __EI();


  SectInfo.sectaddr = FLASH_ADR_SEC6;
  SectInfo.a1 = FLASH_ADR6_7_COM1;
  SectInfo.a2 = FLASH_ADR6_7_COM2;
  SectInfo.a3 = FLASH_ADR6_7_COM1;
  SectInfo.a4 = FLASH_ADR6_7_COM1;
  SectInfo.a5 = FLASH_ADR6_7_COM2;

  __DI();
  EraseSector(&SectInfo);
  __EI();

  SectInfo.sectaddr = FLASH_ADR_SEC7;
  SectInfo.a1 = FLASH_ADR6_7_COM1;
  SectInfo.a2 = FLASH_ADR6_7_COM2;
  SectInfo.a3 = FLASH_ADR6_7_COM1;
  SectInfo.a4 = FLASH_ADR6_7_COM1;
  SectInfo.a5 = FLASH_ADR6_7_COM2;

  __DI();
  EraseSector(&SectInfo);
  __EI();

  SectInfo.sectaddr = FLASH_ADR_SEC8;
  SectInfo.a1 = FLASH_ADR8_9_COM1;
  SectInfo.a2 = FLASH_ADR8_9_COM2;
  SectInfo.a3 = FLASH_ADR8_9_COM1;
  SectInfo.a4 = FLASH_ADR8_9_COM1;
  SectInfo.a5 = FLASH_ADR8_9_COM2;

  __DI();
  EraseSector(&SectInfo);
  __EI();

  SectInfo.sectaddr = FLASH_ADR_SEC9;
  SectInfo.a1 = FLASH_ADR8_9_COM1;
  SectInfo.a2 = FLASH_ADR8_9_COM2;
  SectInfo.a3 = FLASH_ADR8_9_COM1;
  SectInfo.a4 = FLASH_ADR8_9_COM1;
  SectInfo.a5 = FLASH_ADR8_9_COM2;

  __DI();
  EraseSector(&SectInfo);
  __EI();

}
void EraseSector(tm_sectinfo_t *hSectorInfo)
{
  unsigned short tog0,tog1;

  IO_WDTCPR1 = 0xa5;	/* H/W WDT Clear */



  /******************************************************/
  /* Flash Sector Erase (SA8) */
  /******************************************************/
  *hSectorInfo->a1 = 0x00AA;
  *hSectorInfo->a2 = 0x0055;
  *hSectorInfo->a3 = 0x0080;
  *hSectorInfo->a4 = 0x00AA;
  *hSectorInfo->a5 = 0x0055;
  *hSectorInfo->sectaddr = 0x0030;

  /* During Erase operation */

  IO_WDTCPR1 = 0xa5;	/* H/W WDT Clear */

  tmp = *hSectorInfo->sectaddr;
  while(1){
    tog0 = (0x0040 & *hSectorInfo->sectaddr);
    tog1 = (0x0040 & *hSectorInfo->sectaddr);
    if(tog1 == tog0){
      break;			/* erase success */
    }
    else{
      if( 1 == (0x0020 & *hSectorInfo->sectaddr )){
        /* time limit exceed */
        /* Flash RESET COMMNAD */
        *hSectorInfo->sectaddr = 0x00F0;
        break;
      }
    }
    IO_WDTCPR1 = 0xa5;	/* H/W WDT Clear */
  }
  IO_WDTCPR1 = 0xa5;	/* H/W WDT Clear */
}
/*****************************************************************************/
/* FLASH Test */
/*****************************************************************************/
void FlashWriteWord(tm_u16_t *WriteAddr, tm_u16_t programword)
{
  unsigned short data_cnt;
  unsigned short tog0,tog1;


  IO_WDTCPR1 = 0xa5;	/* H/W WDT Clear */

  /***************************************************************/
  /* During Write operation */
  /***************************************************************/

  IO_WDTCPR1 = 0xa5;	/* H/W WDT Clear */

    IO_WDTCPR1 = 0xa5;	/* H/W WDT Clear */
    __wait_nop();
    __wait_nop();
    *FLASH_ADR8_9_COM1 = 0x00AA;
    __wait_nop();
    __wait_nop();
    *FLASH_ADR8_9_COM2 = 0x0055;
    __wait_nop();
    __wait_nop();
    *FLASH_ADR8_9_COM1 = 0x00A0;
    __wait_nop();
    __wait_nop();
    *(WriteAddr) = programword;

    /* Flash Write waitting */
    __wait_nop();
    __wait_nop();

    tmp = *(WriteAddr + data_cnt);
    while(1)
    {
      tog0 = 0x0040 & *(WriteAddr);
      tog1 = 0x0040 & *(WriteAddr);
      if(tog1 == tog0){
        break;			/* erase success */
      }
      else{
        if( 1 == (0x0020 & *(WriteAddr))){
          /* time limit exceed */
          /* Flash RESET COMMNAD */
          *(WriteAddr) = 0x00F0;
          break;
        }
      }
      IO_WDTCPR1 = 0xa5;	/* H/W WDT Clear */
    }
    //if(programword != *(WriteAddr))
      //break;	/* write fail */
  IO_WDTCPR1 = 0xa5;	/* H/W WDT Clear */

}
