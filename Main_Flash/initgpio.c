
#include "_fr.h"
#include "extern.h"

#pragma section CODE=CODE_MYAPP, attr=CODE
void InitGPIOs()
{

  /* Initialising P073 */
  /* Output configuration */
  KEYCDR = 0x24B2;
  KEYCDR = 0x64B2;
  KEYCDR = 0xA4B2;
  KEYCDR = 0xE4B2;
  ADERL1 = 0;

  IO_KEYCDR = 0x0E27;
  IO_KEYCDR = 0x4E27;
  IO_KEYCDR = 0x8E27;
  IO_KEYCDR = 0xCE27;
  IO_PFR07.byte = 0x00;

  IO_KEYCDR = 0x0E07;
  IO_KEYCDR = 0x4E07;
  IO_KEYCDR = 0x8E07;
  IO_KEYCDR = 0xCE07;
  IO_DDR07.byte = 0x08;

  IO_KEYCDR = 0x0E04;
  IO_KEYCDR = 0x4E04;
  IO_KEYCDR = 0x8E04;
  IO_KEYCDR = 0xCE04;
  IO_DDR04.byte = 0x18;

  IO_PDR04.byte = 0;

  IO_KEYCDR = 0x0E24;
  IO_KEYCDR = 0x4E24;
  IO_KEYCDR = 0x8E24;
  IO_KEYCDR = 0xCE24;
  IO_PFR04.byte = 0x18;

}

