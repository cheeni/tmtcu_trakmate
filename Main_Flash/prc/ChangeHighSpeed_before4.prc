#/*******************************************************************************
#* Copyright (C) 2013 Spansion LLC. All Rights Reserved. 
#*
#* This software is owned and published by: 
#* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
#*
#* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
#* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
#*
#* This software contains source code for use with Spansion 
#* components. This software is licensed by Spansion to be adapted only 
#* for use in systems utilizing Spansion components. Spansion shall not be 
#* responsible for misuse or illegal use of this software for devices not 
#* supported herein.  Spansion is providing this software "AS IS" and will 
#* not be responsible for issues arising from incorrect user implementation 
#* of the software.  
#*
#* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE,
#* REGARDING THE SOFTWARE (INCLUDING ANY ACOOMPANYING WRITTEN MATERIALS), 
#* ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED USE, INCLUDING, 
#* WITHOUT LIMITATION, THE IMPLIED WARRANTY OF MERCHANTABILITY, THE IMPLIED 
#* WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE OR USE, AND THE IMPLIED 
#* WARRANTY OF NONINFRINGEMENT.  
#* SPANSION SHALL HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, 
#* NEGLIGENCE OR OTHERWISE) FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT 
#* LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, 
#* LOSS OF BUSINESS INFORMATION, OR OTHER PECUNIARY LOSS) ARISING FROM USE OR 
#* INABILITY TO USE THE SOFTWARE, INCLUDING, WITHOUT LIMITATION, ANY DIRECT, 
#* INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, 
#* SAVINGS OR PROFITS, 
#* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 
#* YOU ASSUME ALL RESPONSIBILITIES FOR SELECTION OF THE SOFTWARE TO ACHIEVE YOUR
#* INTENDED RESULTS, AND FOR THE INSTALLATION OF, USE OF, AND RESULTS OBTAINED 
#* FROM, THE SOFTWARE.  
#*
#* This software may be replicated in part or whole for the licensed use, 
#* with the restriction that this Disclaimer and Copyright notice must be 
#* included with each copy of this software, whether used in part or whole, 
#* at all times.  
#*****************************************************************************/

#/** \file ChangeHighSpeed_before4.prc
# **
# ** Add description here...
# **
# ** History:
# **   - 2011-12-28  0.01  MKu  Preliminary version
# **   - 2012-03-09  0.02  MKu  Preliminary version(not released)
# **                            -Clock gear function addition
# **   - 2012-03-16  1.01  MKu  First edition
# **   - 2012-11-13  1.02  MKu  Parameter change in 4MHz clock setting
# **                            to match the limitation of PLL macro clock frequency
# *****************************************************************************/
CANCEL FREQUENCY /MAX		# disable high speed mode
reset

while 1				# check CMONR.MCRDY
  if %b(0x511)&0x20		# CMONR.MCRDY=1
    break;
  endif
endw

set mem 0x0510 = 0x20|(%b(0x510)&0xDF)
				# CSELR set clock source
				# SCEN(bit7) = 0 :enable sub clock
				# PCEN(bit6) = 0 :enable PLL clock
				#>MCEN(bit5) = 1 :enable main clock
				# CKS(bit1-0)= 2'b00 :clock select MCLK/2

while 1				# check CMONR.CKM
  if !(%b(0x511)&0x03)		# CMONR.CKM(bit1-0) = 2'b00
    break;
  endif
endw

				#------- set PLL clock(80MHz) -------
set mem/half 0x0514 = 0x00B0|(%h(0x514)&0xFF0F)
				# PLLCR
				#>POSW(bit7-4) = 4'b1101 (1024us)
				# PDS (bit3-0) = 4'b0000 (MCLK/1)

set mem 0x0520 = 0x00|(%b(0x520)&0xFE)
				# CCPSSELR : select PLL
				#>PCSEL(bit0) = 1'b0

set mem 0x0525 = 0x4F		# CCPLLFBR PLL macro clock = MCLK/1x80
				#>IDIV(bit6-0) = 7'b100_1111

set mem 0x0523 = 0x10		# CCPSDIVR PLL clock =  PLL macro clock / 4
				#>PODS(bit6-4) = 3'b001
				# SODS(bit2-0) = 3'b000

set mem 0x052D = 0x01|(%b(0x52D))
				# Clock Gear ON
				#>CCCGRCR0.GREN(bit0) = 1
set mem 0x052E = 0x00
				# CCCGRCR1 Step Set
				#>CCCGRCR1.GRSTP(bit7-6)=2'b00, CCCGRCR1.GRSTN(bit5-0) = 6'b00_0000,
set mem 0x052F = 0xFF
				# CCCGRCR2 Step Set
				#>CCCGRCR2.GRLP(bit7-0)=8'b1111_1111

set mem 0x0510 = 0x60|(%b(0x510)&0x9F)
				# CSELR set clock source
				# SCEN(bit7) = 0 :enable sub clock
				#>PCEN(bit6) = 1 :enable PLL clock
				#>MCEN(bit5) = 1 :enable main clock
				# CKS(bit1-0)= 2'b00 :clock select MCLK/2

while 1				# check CMONR.PCRDY
  if %b(0x511)&0x40		# CMONR.PCRDY=1
    break;
  endif
endw

set mem 0x0488 = 0x00|(%b(0x488)&0x1F)
				# DIVR0 : MCLK/1
				#>DIVB(bit7-5) = 3'b000

set mem 0x048a = 0x10|(%b(0x48a)&0x0F)
				# DIVR2 : PCLK1= 80/2 = 40MHz
				#>DIVP(bit7-4) = 4'b0001

set mem 0x1000 = 0x00|(%b(0x1000)&0xFE)
				# SACR : PCLK2 set
				#>M(bit0) = 0(sync)

set mem 0x0510 = 0x62|(%b(0x510)&0x9C)
				# CSELR set clock source
				# SCEN(bit7) = 0 :enable sub clock
				#>PCEN(bit6) = 1 :enable PLL clock
				#>MCEN(bit5) = 1 :enable main clock
				#>CKS(bit1-0)= 2'b10 :clock select PLL

while 1				# check SRCCLK==PLLSSCLK
  if %b(0x511)&0x42		# CMONR.PCRDY=1
    break;
  endif
endw

while 1				# check CCCGR0.GRSTS
  if !(%b(0x52D)&0xC0)		# CCCGR0.GRSTS(bit7-6)==2'b00
    break;
  endif
endw

set mem 0x052D = 0x02|(%b(0x52D))
				# Clock Gear Start
				#>CCCGRCR0.GSTR(bit1)=1

while 1				# check CCCGR0.GRSTS
  if %b(0x52D)&0x80		# CCCGR0.GRSTS==2'b10?
    break;
  endif
endw

while 1				# check Gear End
  if !(%b(0x52D)&0x02)		# CCCGRCR0.GSTR(bit1)==0?
    break;
  endif
endw

SET FREQUENCY /MAX 80		# enable high speed mode
SHOW SYSTEM
