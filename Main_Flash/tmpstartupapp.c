
#include "startup.h"

#define SYSCTL_RCGC2_R          (*((volatile uint32_t *)0x400FE108))
#define GPIO_PORTC_DIR_R        (*((volatile uint32_t *)0x40006400))
#define GPIO_PORTC_DEN_R        (*((volatile uint32_t *)0x4000651C))
#define GPIO_PORTC_DATA_R       (*((volatile uint32_t *)0x400063FC))
#define SYSCTL_RCC2_USERCC2     0x80000000  // Use RCC2
#define SYSCTL_RCC2_DIV400      0x40000000  // Divide PLL as 400 MHz vs. 200
#define SYSCTL_RCC2_SYSDIV2_M   0x1F800000  // System Clock Divisor 2

int uart0rxevent;
int uart0txevent;
int uart1rxevent;
int uart1txevent;
int uart6rxevent;
int uart6txevent;
int gsmcntrlevent;
int onesecevent;
int spirxdone;
int spitxdone;

char debugstr[128];
tm_startupapp_t App;
int printbc = 0;
#pragma section CODE=CODE_MYAPP, attr=CODE
int main()
{
  int eventNbr;

  /*
     Enable lazy stacking for interrupt handlers.  This allows floating-point
     instructions to be used within interrupt handlers, but at the expense of
     extra stack usage.
     */
  ROM_FPULazyStackingEnable();

  /* Set the clocking to run directly from the crystal. */
  //ROM_SysCtlClockSet(SYSCTL_SYSDIV_1 | SYSCTL_USE_OSC | SYSCTL_OSC_MAIN |
  //SYSCTL_XTAL_16MHZ);

  /* Set the clocking to run from internal clock and set system clock to 80 MHz*/
  ROM_SysCtlClockSet(SYSCTL_SYSDIV_2_5 | SYSCTL_OSC_INT |  SYSCTL_RCC2_USERCC2 | SYSCTL_RCC2_DIV400 );


  uart0rxevent = TM_FALSE;
  uart0txevent = TM_TRUE;
  spirxdone = TM_FALSE;

  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOH);
  ROM_GPIOPinTypeGPIOOutput(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5);
  ROM_GPIOPinTypeGPIOOutput(GPIO_PORTH_BASE, GPIO_PIN_3);

  ROM_GPIOPinWrite(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5, GPIO_PIN_4 | GPIO_PIN_5);
  ROM_GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_3);
  ROM_GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_0, 0);
  ROM_GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_3, 0);
  
  UART0Init(115200);
  InitFlash(&App.FlashTask.Flash);
  FlashTaskInit(&App.FlashTask);

  App.AppState = GETCURVERSION;

  //UART0QueueData((tm_u8_t *)"starting download\r\n",vts_strlen("starting download\r\n"));

#if 1
  FlashTaskGlobalUnprotect(&App.FlashTask);
  while(1)
  {
    /* Wait for events to occur */
    //eventNbr = WaitForEvents();
    ROM_IntMasterDisable();
    eventNbr = -1;
    if(spirxdone == TM_TRUE)
    {
      /* A response is found. Hanlde it */
      spirxdone = TM_FALSE;
      eventNbr = EVENT_FLASH_RX;
      goto eventfound1;
    }
eventfound1:
    ROM_IntMasterEnable();
    if(eventNbr != -1)
    {
        FlashCtrl(&App.FlashTask.Flash, TM_FALSE, TM_TRUE);
        /* FlashTask is called since flashtask dependes on EVENT_FLASH_RX */
        FlashTask(&App.FlashTask);
    }
        if(FlashStateGet(&App.FlashTask.Flash) == FLASH_IDLE)
          break;
  }
#endif
  //UART0QueueData((tm_u8_t *)"mainloop\r\n",vts_strlen("mainloop\r\n"));

  while(1)
  {
    ROM_IntMasterDisable();
    eventNbr = -1;
    if(spirxdone == TM_TRUE)
    {
      /* A response is found. Hanlde it */
      spirxdone = TM_FALSE;
      eventNbr = EVENT_FLASH_RX;
      goto eventfound;
    }
    if(uart0rxevent == TM_TRUE)
    {
      /* read uart bytes */
      uart0rxevent = TM_FALSE;
      eventNbr = EVENT_RX_CONFIG;
      goto eventfound;
    }
    if(uart0txevent == TM_TRUE)
    {
      uart0txevent = TM_FALSE;
      eventNbr = EVENT_TX_CONFIG;
      goto eventfound;
    }
eventfound:
    ROM_IntMasterEnable();
    if(UartQEmpty() == TM_TRUE)
    {
      if(MainLoop() == 1)
      {
         break;
      }
    }
        FlashTask(&App.FlashTask);
    switch(eventNbr)
    {
      case EVENT_FLASH_RX:
        FlashCtrl(&App.FlashTask.Flash, TM_FALSE, TM_TRUE);
        /* FlashTask is called since flashtask dependes on EVENT_FLASH_RX */
        FlashTask(&App.FlashTask);
        break;
      case EVENT_RX_CONFIG:
  ROM_GPIOPinWrite(GPIO_PORTC_BASE, GPIO_PIN_4, 0);
        UART0_BufferRead(App.UART0RxBuffer, &App.Len);
        break;
      case EVENT_TX_CONFIG:
        UART0DeQueueData();
        break;
    }
  }

  /* Clear all interrupts */
  /* Change vector table address*/
  HWREG(NVIC_VTABLE) = 0x2000;

  /* JUmp to main app*/
  __asm("  b 0x226c");

}

int MainLoop(void)
{
  uint32_t Addr;
  int i;
  unsigned char byte;
  int retval = 0;

  switch(App.AppState)
  {
    case IDLE:
      break;
    case GETCURVERSION:
      FlashTaskRead(&App.FlashTask, FLASHADDR_CURVER, 4, App.CurVer);
      App.AppState = GETOTAVERSION;
      break;
    case GETOTAVERSION:
      if(FlashStateGet(&App.FlashTask.Flash) == FLASH_IDLE)
      {
        FlashTaskRead(&App.FlashTask, FLASHADDR_OTAVER, 4, App.OTAVer);
        App.AppState = GETRUNVER;
      }
      break;
    case GETRUNVER:
      if(FlashStateGet(&App.FlashTask.Flash) == FLASH_IDLE)
      {
        FlashTaskRead(&App.FlashTask, FLASHADDR_RUNNINGVER, 4, App.RunVer);
        App.AppState = GETBYTECOUNT;
      }
      break;
    case GETBYTECOUNT:
      if(FlashStateGet(&App.FlashTask.Flash) == FLASH_IDLE)
      {
        App.CurVer[4] = '\0';
        App.OTAVer[4] = '\0';
        App.RunVer[4] = '\0';
        //UART0QueueData(App.OTAVer,4);
        //UART0QueueData("\r\n",2);
        //UART0QueueData(App.CurVer,4);
        //UART0QueueData("\r\n",2);
        //UART0QueueData(App.RunVer,4);
        //UART0QueueData("\r\n",2);
        //UART0QueueData("\r\n",2);
        //UART0QueueData("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",vts_strlen("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
        App.AppState = WAITFORUARTEMPTY;
      }
      break;
    case WAITFORUARTEMPTY:
      if(UartQEmpty() == TM_FALSE)
        break;
      else
      {
        if(CompareVersions(App.CurVer, App.OTAVer, App.RunVer) == 0)
        {
          retval = 1;
        }
        else
        {
          FlashTaskRead(&App.FlashTask, FLASHADDR_BYTECOUNT, 4, (tm_u8_t *)&App.TotalByteCount);
          App.AppState = ERASEMEMORY;
        }
      }
      break;
    case ERASEMEMORY:
      if(FlashStateGet(&App.FlashTask.Flash) == FLASH_IDLE)
      {
        //UART0QueueData((tm_u8_t *)"flash erased\r\n",vts_strlen("flash erased\r\n"));
        App.tmpTotalByteCount = App.TotalByteCount+4;
        /* Erase internal memory*/
        Addr = 0x2000;
        for(i = 0; i < 120; i++)
        {
          ROM_FlashErase(Addr);
          Addr += 1024;
        }
        App.WrAddr = 0x2000;
        App.RdAddr = FLASHADDR_SOFTWAREIMAGE;
        if(App.tmpTotalByteCount > 4)
        {
          FlashTaskRead(&App.FlashTask, App.RdAddr, 4, (tm_u8_t *)&App.ReadBuf);
          App.RdAddr += 4;
          App.tmpTotalByteCount -= 4;
        }
        else
        {
          FlashTaskRead(&App.FlashTask, App.RdAddr, App.tmpTotalByteCount, (tm_u8_t *)&App.ReadBuf);
          App.tmpTotalByteCount = 0;
          retval = 1;
        }
        App.AppState = WRITEDATA;
      }
      break;
    case WRITEDATA:
      if(FlashStateGet(&App.FlashTask.Flash) == FLASH_IDLE)
      {
#if 0
        for(i = 0; i < 4;i++)
        {
          vts_itoa(App.ReadBuf[i], buf);

        UART0QueueData(buf,vts_strlen(buf));
        UART0QueueData((tm_u8_t *)"\r\n",vts_strlen("\r\n"));
        }
#endif
        /* Pack bytes into 32bit word */
        App.ProgramWord = 0;
        for(i = 0; i < 4; i++)
        {
          byte = App.ReadBuf[i];
          App.ProgramWord |= (byte << (8*i));
        }
        if(App.tmpTotalByteCount == 0)
        {
          retval = 1;
          break;
        }
        //UART0QueueData((tm_u8_t *)"writing data\r\n",vts_strlen("writing data\r\n"));
        ROM_FlashProgram(&App.ProgramWord, App.WrAddr, 4);
        App.WrAddr += 4;
        if(App.tmpTotalByteCount > 4)
        {
          FlashTaskRead(&App.FlashTask, App.RdAddr, 4, (tm_u8_t *)&App.ReadBuf);
          App.RdAddr += 4;
          App.tmpTotalByteCount -= 4;
        }
        else
        {
          FlashTaskRead(&App.FlashTask, App.RdAddr, App.tmpTotalByteCount, (tm_u8_t *)&App.ReadBuf);
          App.tmpTotalByteCount = 0;
        }
      }
      break;
  }
  return retval;
}

/* Return 0 if image is not to be copied from SPI flash
 * else return 1 if image is to be copied from SPI flash */
int CompareVersions(tm_u8_t *CurVer, tm_u8_t *OTAVer, tm_u8_t *RunVer)
{
  if((OTAVer[0] == 0xFF) || (OTAVer[1] == 0xFF) || (OTAVer[2] == 0xFF) || (OTAVer[3] == 0xFF))
  {
    return 0;
  }
  else if((OTAVer[0] != CurVer[0]) || (OTAVer[1] != CurVer[1]) ||  (OTAVer[2] != CurVer[2]) || (OTAVer[3] != CurVer[3]))
  {
    if((RunVer[0] == 0xFF) && (RunVer[1] == 0xFF) && (RunVer[2] == 0xFF) && (RunVer[3] == 0xFF))
      return 1;
    else
      return 0;
  }
  else
    return 0;
}

#if 0
CurVer
New Ver

if(cur = 0xff)
  no upgrade
if(furever = 0xff)
  no upgrade
if(curver != futrever)
  check crc
  if (crc ok)
  upgrade

#endif
