#ifndef __TM_SECTINFO_H__
#define __TM_SECTINFO_H__

/* Main Flash Sector Start Address */
#define FLASH_ADR_SEC0	(volatile unsigned short *)(0x00070000)
#define FLASH_ADR_SEC1	(volatile unsigned short *)(0x00070004)
#define FLASH_ADR_SEC2	(volatile unsigned short *)(0x00074000)
#define FLASH_ADR_SEC3	(volatile unsigned short *)(0x00074004)
#define FLASH_ADR_SEC4	(volatile unsigned short *)(0x00078000)
#define FLASH_ADR_SEC5	(volatile unsigned short *)(0x00078004)
#define FLASH_ADR_SEC6	(volatile unsigned short *)(0x0007C000)
#define FLASH_ADR_SEC7	(volatile unsigned short *)(0x0007C004)
#define FLASH_ADR_SEC8	(volatile unsigned short *)(0x00080000)
#define FLASH_ADR_SEC9	(volatile unsigned short *)(0x00080004)
#define FLASH_ADR_SEC10	(volatile unsigned short *)(0x000A0000)
#define FLASH_ADR_SEC11	(volatile unsigned short *)(0x000A0004)
#define FLASH_ADR_SEC12	(volatile unsigned short *)(0x000C0000)
#define FLASH_ADR_SEC13	(volatile unsigned short *)(0x000C0004)
#define FLASH_ADR_SEC14	(volatile unsigned short *)(0x000E0000)
#define FLASH_ADR_SEC15	(volatile unsigned short *)(0x000E0004)

/* Main Flash Sector Command Address */
#define FLASH_ADR0_1_COM1	(volatile unsigned short *)(0x00071554)
#define FLASH_ADR0_1_COM2	(volatile unsigned short *)(0x00070AA8)
#define FLASH_ADR2_3_COM1	(volatile unsigned short *)(0x00075554)
#define FLASH_ADR2_3_COM2	(volatile unsigned short *)(0x00074AA8)
#define FLASH_ADR4_5_COM1	(volatile unsigned short *)(0x00079554)
#define FLASH_ADR4_5_COM2	(volatile unsigned short *)(0x00078AA8)
#define FLASH_ADR6_7_COM1	(volatile unsigned short *)(0x0007D554)
#define FLASH_ADR6_7_COM2	(volatile unsigned short *)(0x0007CAA8)
#define FLASH_ADR8_9_COM1	(volatile unsigned short *)(0x00081554)
#define FLASH_ADR8_9_COM2	(volatile unsigned short *)(0x00080AA8)
#define FLASH_ADR10_11_COM1	(volatile unsigned short *)(0x000A1554)
#define FLASH_ADR10_11_COM2	(volatile unsigned short *)(0x000A0AA8)
#define FLASH_ADR12_13_COM1	(volatile unsigned short *)(0x000C1554)
#define FLASH_ADR12_13_COM2	(volatile unsigned short *)(0x000C0AA8)
#define FLASH_ADR14_15_COM1	(volatile unsigned short *)(0x000E1554)
#define FLASH_ADR14_15_COM2	(volatile unsigned short *)(0x000E0AA8)

struct sectinfo
{
  volatile unsigned short *sectaddr;
  volatile unsigned short *a1;
  volatile unsigned short *a2;
  volatile unsigned short *a3;
  volatile unsigned short *a4;
  volatile unsigned short *a5;
};
typedef struct sectinfo tm_sectinfo_t;

void FlashWriteWord(tm_u16_t *,unsigned short programword);
void EraseSector(tm_sectinfo_t *hSectorInfo);
void EraseSectors(void);

#endif /*  __TM_SECTINFO_H__ */
