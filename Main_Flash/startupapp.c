#include "startupapp.h"

static tm_startupapp_t App;
static int eventNbr;

int tenmsecevent;
int uart1txevent;
int uart1rxevent;
int onesecevent;
int dmaevent;

tm_u8_t Len1;

#pragma section CODE=CODE_MYAPP, attr=CODE
void StartupApp(void)
{
  int LEDState = 0;
  /**************/
  //init_ADC();
  /**************/

  // Allow all interrupt levels
  memset(&App,0,sizeof(tm_startupapp_t));
  __set_il(0x1F); /* ILM = 31 */
  // Enable interrupts
  __EI();


/* Initailise peripherals */

  /* Initialise GPIOs */
  InitGPIOs();
  tenmsecevent = TM_FALSE;
  uart1txevent = TM_FALSE;
  uart1rxevent = TM_FALSE;
  onesecevent   = TM_FALSE;
  dmaevent = TM_FALSE;

  InitTimers();
  /* Initailise Timer */
  Init10msecTimer();

  /* Initialise configuration port */
  InitCfgUART();
  FlashTaskInit(&App.FlashTask);


  // Endless loop

  while(1)
  {
    /* Reset watchdog */
    WDTCPR1 = 0xa5;

    eventNbr = -1;
    __DI();
    /*TODO: Disable interrupts */
    if(dmaevent == TM_TRUE)
    {
      dmaevent = TM_FALSE;
      eventNbr = EVENT_DMA;
      goto eventfound;
    }
    if(uart1rxevent == TM_TRUE)
    {
      uart1rxevent = TM_FALSE;
      eventNbr = EVENT_RX_CONFIG;
      goto eventfound;
    }
    if(tenmsecevent == TM_TRUE)
    {
      tenmsecevent = TM_FALSE;
      eventNbr = EVENT_10MSEC;
      goto eventfound;
    }
    if(onesecevent == TM_TRUE)
    {
      onesecevent = TM_FALSE;
      eventNbr = EVENT_ONESEC;
      goto eventfound;
    }
    if(uart1txevent == TM_TRUE)
    {
      uart1txevent = TM_FALSE;
      eventNbr = EVENT_TX_CONFIG;
      goto eventfound;
    }
eventfound:
    /*TODO: Enable interrupts */
    __EI();
    WDTCPR1 = 0xa5;
    if(MainProcess(&App) == 1)
    {
  IO_ICR[2].byte = 31;  /* interrupt */
    __asm("  LDI #0x74000, R0");
    __asm("  JMP @R0");
      break;
    }
    switch(eventNbr)
    {
      case EVENT_TX_CONFIG:
        UART1DeQueueData();
        break;
      case EVENT_RX_CONFIG:
        UART1_BufferRead(App.UART1RxBuffer, &Len1);
        if(Len1 > 0)
        {
          //UART1QueueData(UART1RxBuffer, Len1);
          //UART8QueueData(UART1RxBuffer, Len1);
        }
        break;
      case EVENT_10MSEC:
        MainProcess(&App);
        FlashTaskCtrl(&App.FlashTask,TM_FALSE);
        break;
      case EVENT_ONESEC:
        CheckTimers();
        //PrintAdcVal();
        if(LEDState == 0)
        {
          LEDState = 1;
          IO_PDR07.byte = 0x09;
        }
        else
        {
          LEDState = 0;
          IO_PDR07.byte = 1;
        }
        break;
      case EVENT_DMA:
        FlashTaskCtrl(&App.FlashTask,TM_TRUE);
        break;
    }
    WDTCPR1 = 0xa5;
  }
}

int MainProcess(tm_startupapp_t *hApp)
{
  tm_u32_t Addr;
  int retval = 0;
  int i;
  tm_u8_t byte;
  tm_u16_t tmp;

  switch(hApp->AppState)
  {
     case GETCURVERSION:
        FlashTaskRead(&hApp->FlashTask, APP_CURVER_ADDR, 4, hApp->CurVer);
        hApp->AppState = GETNEWVERSION;
      break;
     case GETNEWVERSION:
      if(FlashTaskStateGet(&hApp->FlashTask) == FLASHTASK_IDLE)
      {
        FlashTaskRead(&hApp->FlashTask, APP_NEWVER_ADDR, 4, hApp->NewVer);
        hApp->AppState = GETRUNVERSION;
      }
      break;
    case GETRUNVERSION:
      if(FlashTaskStateGet(&hApp->FlashTask) == FLASHTASK_IDLE)
      {
        FlashTaskRead(&hApp->FlashTask, APP_RUNVER_ADDR, 4, hApp->RunVer);
        hApp->AppState = GETBYTECOUNT;
      }
      break;
    case GETBYTECOUNT:
      if(FlashTaskStateGet(&hApp->FlashTask) == FLASHTASK_IDLE)
      {
        hApp->CurVer[4] = '\0';
        hApp->NewVer[4] = '\0';
        hApp->RunVer[4] = '\0';
        //UART0QueueData(App.NewVer,4);
        //UART0QueueData("\r\n",2);
        //UART0QueueData(App.CurVer,4);
        //UART0QueueData("\r\n",2);
        //UART0QueueData(App.RunVer,4);
        //UART0QueueData("\r\n",2);
        //UART0QueueData("\r\n",2);
        //UART0QueueData("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",vts_strlen("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
        hApp->AppState = WAITFORUARTEMPTY;
      }
      break;
    case WAITFORUARTEMPTY:
      if(FlashTaskStateGet(&hApp->FlashTask) == FLASHTASK_IDLE)
      {
        if(CompareVersions(App.CurVer, App.NewVer, App.RunVer) == 0)
        {
          retval = 1;
        }
        else
        {
          FlashTaskRead(&hApp->FlashTask, APP_BYTECOUNTADDR, 4, (tm_u8_t *)&hApp->TotalByteCount);
          App.AppState = ERASEMEMORY;
          tmp = IO_FCTLR.hword;		/* Dammy Read */
          IO_FCTLR.hword = 0xC900;	/* Flash Write valid, FLASH */
          tmp = IO_FCTLR.hword;		/* Dammy Read */
        }
      }
      break;
    case ERASEMEMORY:
      if(FlashTaskStateGet(&hApp->FlashTask) == FLASHTASK_IDLE)
      {
        EraseSectors();
        hApp->tmpTotalByteCount = hApp->TotalByteCount+4;
        /* Erase internal memory here */
        hApp->WriteAddr = 0x74000;
        hApp->ReadAddr = APP_STARTADDR;
        if(hApp->tmpTotalByteCount > 2)
        {
          FlashTaskRead(&hApp->FlashTask, hApp->ReadAddr, 2, (tm_u8_t *)&hApp->ReadBuf);
          hApp->ReadAddr += 2;
          hApp->tmpTotalByteCount -= 2;
        }
        else
        {
          FlashTaskRead(&hApp->FlashTask, hApp->ReadAddr, hApp->tmpTotalByteCount, (tm_u8_t *)&hApp->ReadBuf);
          hApp->tmpTotalByteCount = 0;
          retval = 1;
        }
        hApp->AppState = WRITEDATA;
      }
      break;
    case WRITEDATA:
      if(FlashTaskStateGet(&hApp->FlashTask) == FLASHTASK_IDLE)
      {
#if 0
        for(i = 0; i < 4;i++)
        {
          vts_itoa(App.ReadBuf[i], buf);

        UART0QueueData(buf,vts_strlen(buf));
        UART0QueueData((tm_u8_t *)"\r\n",vts_strlen("\r\n"));
        }
#endif
        /* Pack bytes into 32bit word */
        hApp->ProgramHWord = 0;
        for(i = 0; i < 2; i++)
        {
          byte = hApp->ReadBuf[i];
          hApp->ProgramHWord = (hApp->ProgramHWord << (8*i)) | byte;
        }
        if(hApp->tmpTotalByteCount == 0)
        {
          retval = 1;
          break;
        }
        //UART0QueueData((tm_u8_t *)"writing data\r\n",vts_strlen("writing data\r\n"));
        /* write to internal memory here */
        __DI();
        FlashWriteWord((tm_u16_t *)hApp->WriteAddr,hApp->ProgramHWord);
        __EI();
        hApp->WriteAddr += 2;
        if(hApp->tmpTotalByteCount > 2)
        {
          FlashTaskRead(&hApp->FlashTask, hApp->ReadAddr, 4, (tm_u8_t *)&hApp->ReadBuf);
          hApp->ReadAddr += 2;
          hApp->tmpTotalByteCount -= 2;
        }
        else
        {
          FlashTaskRead(&hApp->FlashTask, hApp->ReadAddr, hApp->tmpTotalByteCount, (tm_u8_t *)&hApp->ReadBuf);
          hApp->tmpTotalByteCount = 0;
        }
      }
      break;
  }
  return retval;
}

int CompareVersions(tm_u8_t *CurVer, tm_u8_t *NewVer, tm_u8_t *RunVer)
{
  if((NewVer[0] == 0xFF) || (NewVer[1] == 0xFF) || (NewVer[2] == 0xFF) || (NewVer[3] == 0xFF))
  {
    return 0;
  }
  else if((NewVer[0] != CurVer[0]) || (NewVer[1] != CurVer[1]) ||  (NewVer[2] != CurVer[2]) || (NewVer[3] != CurVer[3]))
  {
    if((RunVer[0] == 0xFF) && (RunVer[1] == 0xFF) && (RunVer[2] == 0xFF) && (RunVer[3] == 0xFF))
      return 1;
    else
      return 0;
  }
  else
    return 0;
}
