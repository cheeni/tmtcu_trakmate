
#ifndef __TM_STARTUPAPP_H__
#define __TM_STARTUPAPP_H__

#include "_fr.h"
#include "extern.h"

#include "string.h"

#include "tmtypes.h"
#include "tm_gpios.h"
#include "tmtimer.h"
#include "tmuart.h"
#include "tmflash\inc\tmflashtask.h"
#include "sectinfo.h"

//#define PRINTCANDATA 1
enum tm_events
{
   EVENT_RX_CONFIG
  ,EVENT_TX_CONFIG
  ,EVENT_10MSEC
  ,EVENT_ONESEC
  ,EVENT_DMA
};

enum profile0status
{
   P0_DATASEND
  ,P0_WAITFORACK
};
typedef enum profile0status tm_p0status_t;


enum appstate
{
   GETCURVERSION
  ,GETNEWVERSION
  ,GETRUNVERSION
  ,GETBYTECOUNT
  ,WAITFORUARTEMPTY
  ,ERASEMEMORY
  ,WRITEDATA
};
typedef enum appstate tm_appstate_t;

struct App
{
  tm_flashtask_t  FlashTask;
  tm_appstate_t AppState;
  tm_u32_t      tmpTotalByteCount;
  tm_u32_t      TotalByteCount;
  tm_u32_t      ProgramBuf[8];
  tm_u32_t      WriteAddr;
  tm_u32_t      ReadAddr;
  tm_u16_t      ReadLen;
  tm_u16_t      Len;
  tm_u16_t      ProgramHWord;
  tm_u8_t       UART1RxBuffer[32];
  tm_u8_t       CurVer[5];
  tm_u8_t       NewVer[5];
  tm_u8_t       RunVer[5];
  tm_u8_t       ReadBuf[32];
};
typedef struct App tm_startupapp_t;

void StartupApp(void);
int MainProcess(tm_startupapp_t *hTMApp);
int CompareVersions(tm_u8_t *CurVer, tm_u8_t *NewVer, tm_u8_t *RunVer);

#endif /* __TM_STARTUPAPP_H__ */
