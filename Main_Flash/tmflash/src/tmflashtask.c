#include "tmflash\src\tmflashtask_private.h"

#pragma section CODE=CODE_MYAPP, attr=CODE
void FlashTaskInit(tm_flashtask_t *hFlashTask)
{
  memset(hFlashTask,0,sizeof(tm_flashtask_t));
  FlashInit(&hFlashTask->Flash);
  hFlashTask->FlashTaskState = FLASHTASK_IDLE;

}

void FlashTaskCtrl(tm_flashtask_t *hFlashTask,tm_bool_t bEvent)
{
  if(bEvent == TM_TRUE)
    FlashCtrl(&hFlashTask->Flash);
  switch(hFlashTask->FlashTaskState)
  {
    case FLASHTASK_IDLE:
      break;
    case FLASHTASK_READ:
      if(FlashStateGet(&hFlashTask->Flash) == FLASH_IDLE)
      {
        if(hFlashTask->DataLen != 0)
        {
          if(hFlashTask->DataLen <= FLASHBUFSIZE)
          {
            FlashRead(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, hFlashTask->DataLen);
            hFlashTask->DataLen = 0;
          }
          else
          {
            FlashRead(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, FLASHBUFSIZE);
            hFlashTask->Addr += FLASHBUFSIZE;
            hFlashTask->DataLen -= FLASHBUFSIZE;
            hFlashTask->pData += FLASHBUFSIZE;
          }
        }
        else
        {
          hFlashTask->FlashTaskState = FLASHTASK_IDLE;
        }
      }
      break;
    case FLASHTASK_WRITE:
      if(FlashStateGet(&hFlashTask->Flash) == FLASH_IDLE)
      {
        if(hFlashTask->DataLen != 0)
        {
          if(hFlashTask->DataLen <= FLASHBUFSIZE)
          {
            FlashWrite(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, hFlashTask->DataLen);
            hFlashTask->DataLen = 0;
          }
          else
          {
            FlashWrite(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, FLASHBUFSIZE);
            hFlashTask->Addr += FLASHBUFSIZE;
            hFlashTask->DataLen -= FLASHBUFSIZE;
            hFlashTask->pData += FLASHBUFSIZE;
          }
        }
        else
        {
          hFlashTask->FlashTaskState = FLASHTASK_IDLE;
        }
      }
      break;
    case FLASHTASK_ERASEBEFOREWRITE:
      if(FlashStateGet(&hFlashTask->Flash) == FLASH_IDLE)
      {
            //UART0QueueData((tm_u8_t *)"erased before write\r\n",vts_strlen("erased before write\r\n"));
        if(hFlashTask->NumberOfSectors != 0)
        {
          FlashSectorErase(&hFlashTask->Flash, hFlashTask->SectorAddr);
          hFlashTask->NumberOfSectors--;
          hFlashTask->SectorAddr += SECTORSIZE_64K;
        }
        else
        {
          /* All required sectors erased. Write data in chunks
           * of 256 */
          if(hFlashTask->DataLen <= FLASHBUFSIZE)
          {
            //UART0QueueData((tm_u8_t *)"writing to flash\r\n",vts_strlen("writing to flash\r\n"));
            FlashWrite(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, hFlashTask->DataLen);
            hFlashTask->DataLen = 0;
          }
          else
          {
            FlashWrite(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, FLASHBUFSIZE);
            hFlashTask->Addr += FLASHBUFSIZE;
            hFlashTask->DataLen -= FLASHBUFSIZE;
            hFlashTask->pData += FLASHBUFSIZE;
          }
          hFlashTask->FlashTaskState = FLASHTASK_WRITE;
        }
      }
      break;
    case FLASHTASK_ERASE:
      if(FlashStateGet(&hFlashTask->Flash) == FLASH_IDLE)
      {
        if(hFlashTask->NumberOfSectors != 0)
        {
          FlashSectorErase(&hFlashTask->Flash, hFlashTask->SectorAddr);
          hFlashTask->NumberOfSectors--;
          hFlashTask->SectorAddr += SECTORSIZE_64K;
        }
        else
        {
          //UART0QueueData((tm_u8_t *)"log erase complete\r\n", vts_strlen("log erase complete\r\n"));
          hFlashTask->FlashTaskState = FLASHTASK_IDLE;
        }
      }
      break;
  }
}

void FlashTaskRead(tm_flashtask_t *hFlashTask, tm_u32_t Addr, tm_u16_t Len, tm_u8_t *pData)
{
  hFlashTask->Addr = Addr;
  hFlashTask->pData = pData;
  hFlashTask->DataLen = Len;

  if(hFlashTask->DataLen <= FLASHBUFSIZE)
  {
    FlashRead(&hFlashTask->Flash, Addr, pData, Len);
    hFlashTask->DataLen = 0;
  }
  else
  {
    FlashRead(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, FLASHBUFSIZE);
    hFlashTask->Addr += FLASHBUFSIZE;
    hFlashTask->DataLen -= FLASHBUFSIZE;
    hFlashTask->pData += FLASHBUFSIZE;
  }
  hFlashTask->FlashTaskState = FLASHTASK_READ;
}

void FlashTaskWrite(tm_flashtask_t *hFlashTask, tm_u32_t Addr, tm_u16_t Len, tm_u8_t *pData)
{
  hFlashTask->Addr = Addr;
  hFlashTask->pData = pData;
  hFlashTask->DataLen = Len;
  hFlashTask->SectorAddr = Addr;// & ADDR_4KMASK;
  hFlashTask->NumberOfSectors = Len / SECTORSIZE_64K;
  if(hFlashTask->DataLen <= FLASHBUFSIZE)
  {
    FlashWrite(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, hFlashTask->DataLen);
    hFlashTask->DataLen = 0;
  }
  else
  {
    FlashWrite(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, FLASHBUFSIZE);
    hFlashTask->Addr += FLASHBUFSIZE;
    hFlashTask->DataLen -= FLASHBUFSIZE;
    hFlashTask->pData += FLASHBUFSIZE;
  }
  hFlashTask->FlashTaskState = FLASHTASK_WRITE;
}

void FlashTaskEraseAndWrite(tm_flashtask_t *hFlashTask, tm_u32_t Addr, tm_u16_t Len, tm_u8_t *pData)
{
  hFlashTask->Addr = Addr;
  hFlashTask->pData = pData;
  hFlashTask->DataLen = Len;
  hFlashTask->SectorAddr = Addr;
  hFlashTask->NumberOfSectors = Len / SECTORSIZE_64K;
  hFlashTask->FlashTaskState = FLASHTASK_ERASEBEFOREWRITE;
  FlashSectorErase(&hFlashTask->Flash, hFlashTask->SectorAddr);
}

tm_flashtaskstate_t FlashTaskStateGet(tm_flashtask_t *hFlashTask)
{
  return hFlashTask->FlashTaskState;
}

void FlashTaskEraseSoftImage(tm_flashtask_t *hFlashTask)
{
  hFlashTask->SectorAddr = APP_STARTADDR;
  hFlashTask->NumberOfSectors = 2;
  FlashSectorErase(&hFlashTask->Flash, hFlashTask->SectorAddr);
  hFlashTask->FlashTaskState = FLASHTASK_ERASE;
}

