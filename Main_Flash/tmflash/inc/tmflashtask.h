#ifndef __TMFLASHTASK_H__
#define __TMFLASHTASK_H__

#include "tmtypes.h"
#include "string.h"
#include "tmflash\inc\tmflash.h"

enum flashtaskstate
{
   FLASHTASK_IDLE
  ,FLASHTASK_UNPROTECT
  ,FLASHTASK_ERASEBEFOREWRITE
  ,FLASHTASK_ERASE
  ,FLASHTASK_WRITE
  ,FLASHTASK_READ
  ,FLASHTASK_LOGREAD
  ,FLASHTASK_WAITFORRD
  ,FLASHTASK_GETWRPTR
  ,FLASHTASK_WRPTREND
  ,FLASHTASK_GETRDPTR
  ,FLASHTASK_LOGWRITE
  ,FLASHTASK_CHECKSECTORBLANK
};
typedef enum flashtaskstate tm_flashtaskstate_t;

struct flashtask
{
  tm_flash_t          Flash;
  tm_flashtaskstate_t FlashTaskState;
  tm_u32_t            Addr;
  tm_u32_t            SectorAddr;
  tm_u32_t            WritePtr;
  tm_u32_t            ReaddPtr;
  tm_u16_t            DataLen;
  tm_u16_t            NumberOfSectors;
  tm_u8_t             *pData;
};
typedef struct flashtask tm_flashtask_t;

void FlashTaskInit(tm_flashtask_t *hFlashTask);
void FlashTaskCtrl(tm_flashtask_t *hFlashTask,tm_bool_t bEvent);
void FlashTaskRead(tm_flashtask_t *hFlashTask, tm_u32_t Addr, tm_u16_t Len, tm_u8_t *pData);
void FlashTaskWrite(tm_flashtask_t *hFlashTask, tm_u32_t Addr, tm_u16_t Len, tm_u8_t *pData);
void FlashTaskEraseAndWrite(tm_flashtask_t *hFlashTask, tm_u32_t Addr, tm_u16_t Len, tm_u8_t *pData);
tm_flashtaskstate_t FlashTaskStateGet(tm_flashtask_t *hFlashTask);
void FlashTaskEraseSoftImage(tm_flashtask_t *hFlashTask);

#endif /* __TMFLASHTASK_H__ */
