#ifndef __TMFLASH_H__

#define __TMFLASH_H__

#include "tmtypes.h"
#include "tmflash\inc\tmspi.h"
#include "string.h"

#define FLASHBUFSIZE 16

#define SECTORSIZE_64K 0x10000

/* First two sector are used to store application binary */
#define APP_BYTECOUNTADDR 0
#define APP_CRCADDR       4

#define APP_CURVER_ADDR   6
#define APP_NEWVER_ADDR   10
#define APP_RUNVER_ADDR   14

#define APP_STARTADDR     0x100

#define APP_MAXLEN    0x20000

/* Last Sector is used to store configuration */
#define CONFIG_STARTADDR  0x1FF0000
enum flashstate
{
   FLASH_IDLE
  ,FLASH_READ_BUSY
  ,FLASH_CHKREADSTATUS
  ,FLASH_WAITFOR_WE_SENT
  ,FLASH_WRITE_BUSY
  ,FLASH_CHECKWRITESTATUS
  ,FLASH_SECTOR_ERASE
  ,FLASH_SECTOR_ERASE_BUSY
  ,FLASH_CHKSECTORERASESTATUS
  ,FLASH_CHIP_ERASE
  ,FLASH_CHIP_ERASE_BUSY
  ,FLASH_CHKCHIPERASESTATUS
};
typedef enum flashstate tm_flashstate_t;

struct flash
{
  tm_dma_t DMA;
  tm_flashstate_t FlashState;
  tm_flashstate_t NextFlashState;
  tm_u32_t WriteAddr;
  tm_u16_t ReadLen;
  tm_u16_t WriteLen;
  tm_u16_t TxBuf[128];
  tm_u16_t RxBuf[128];
  tm_u8_t  *pData;
};
typedef struct flash tm_flash_t;

void FlashInit(tm_flash_t *hFlash);
void FlashCtrl(tm_flash_t *hFlash);
tm_bool_t FlashRead(tm_flash_t *hFlash, tm_u32_t Addr, tm_u8_t *pData, tm_u16_t Len);
tm_bool_t FlashWrite(tm_flash_t *hFlash, tm_u32_t Addr, tm_u8_t *pData, tm_u16_t Len);
tm_bool_t FlashSectorErase(tm_flash_t *hFlash, tm_u32_t SectAddr);
tm_flashstate_t FlashStateGet(tm_flash_t *hFlash);

#endif /* __TMFLASH_H__*/

