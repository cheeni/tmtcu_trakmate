
#ifndef _COMMON_H_
#define _COMMON_H_

#include "tmtypes.h"
#include "string.h"

#define INCCIRCULARINDEX(Index, Len)  { \
                                        if(((Index) + 1) >= (Len))  \
                                            (Index) = 0; \
                                        else \
                                          (Index)++; \
                                      }

#define INCCIRCULARPTR(ptr, Base, Len)  { \
                                        if(((ptr) + 1) >= ((Base) + (Len)))  \
                                            (ptr) = (Base); \
                                        else \
                                          (ptr)++; \
                                      }

#define DECCIRCULARINDEX(Index, Len)  { \
                                        if((Index) == 0)  \
                                            (Index) = (Len) - 1; \
                                        else \
                                          (Index)--; \
                                      }

#define DECCIRCULARPTR(ptr, Base, Len)  { \
                                        if((ptr) == (Base))  \
                                            (ptr) = ((Base) + (Len) - 1); \
                                        else \
                                          (ptr)--; \
                                      }

#define ADDCIRCULARINDEX(Index, AddLen, Len)  { \
                                        if(((Index) + (AddLen)) >= (Len))  \
                                            (Index) = (Index) + (AddLen) - (Len); \
                                        else \
                                          (Index) += (AddLen); \
                                      }

#define ADDCIRCULARPTR(ptr, AddLen, Base, Len)  { \
                                        if(((ptr) + (AddLen)) >= ((Base) + (Len)))  \
                                            (ptr) = (ptr) + (AddLen) - (Len); \
                                        else \
                                          (ptr) += (AddLen); \
                                      }

#define CARRIAGERETURN  0x0D
#define LINEFEED        0x0A
#define PROT_DLE        0x10
#define PROT_EOM        0x03

#endif /* _COMMON_H_ */

