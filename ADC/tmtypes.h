
#ifndef _TMTYPES_H_
#define _TMTYPES_H_

#define NULL 0
typedef unsigned char           tm_u8_t;
typedef char                    tm_s8_t;
typedef unsigned short int      tm_u16_t;
typedef short int               tm_s16_t;
typedef unsigned int            tm_u32_t;
typedef int                     tm_s32_t;
#ifdef __LONGLONG__
/* This is defined if -K LONGLONG option is enabled while building */
typedef unsigned long long      tm_u64_t;
typedef long long               tm_s64_t;
#endif

#define TM_FALSE ((tm_bool_t)0)
#define TM_TRUE  ((tm_bool_t)1)
#define TM_HIGH  1
#define TM_LOW   0

/** Boolean type: should only be assigned to TL_FALSE or TL_TRUE */
typedef tm_u8_t tm_bool_t;

#define DEBUGLEVEL_0 0
#define DEBUGLEVEL_1 1
#define DEBUGLEVEL_2 2
#define DEBUGLEVEL_3 3

#define DPRINTF(str) UART1QueueData((tm_u8_t *)(str),strlen((const char *)(str)))
#endif /* _TMTYPES_H_ */
