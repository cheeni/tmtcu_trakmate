#include "tmcommon.h"

void InitCfgUART(void);
void UART1Init(tm_u32_t BaudRate);
void UART1IntHandler(void);
void UART1_BufferRead(tm_u8_t *pData, tm_u8_t *Len);
void UART1_BufferWrite(tm_u8_t *pData, tm_u16_t Len);
void UART1QueueData(tm_u8_t *pData, tm_u16_t Len);
void UART1DeQueueData(void);
tm_bool_t UartQEmpty(void);

void InitGsmUART(void);
void UART8Init(tm_u32_t BaudRate);
void UART8IntHandler(void);
void UART8_BufferRead(tm_u8_t *pData, tm_u8_t *Len);
void UART8_BufferWrite(tm_u8_t *pData, tm_u16_t Len);
void UART8QueueData(tm_u8_t *pData, tm_u16_t Len);
void UART8DeQueueData(void);

void UART6_BufferRead(tm_u8_t *pData, tm_u8_t *Len);
void UART6_BufferWrite(tm_u8_t *pData, tm_u16_t Len);
void UART6QueueData(tm_u8_t *pData, tm_u16_t Len);
void UART6DeQueueData(void);
