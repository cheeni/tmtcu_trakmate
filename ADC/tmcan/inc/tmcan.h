
#ifndef __TMCAN__H__
#define __TMCAN__H__

#include "_fr.h"
#include "extern.h"
#include "tmcommon.h"
#include "tmtypes.h"
#include "tmuart.h"
#include "stdio.h"

#define CAN_RXBUFLEN 32
#define CAN_TXQUEUEBUFLEN 512
#define CAN_QUEUELEN 32


struct candata
{
  tm_u32_t id;
  tm_u8_t  buf[8];
  tm_u8_t  buf_len;
};
typedef struct candata can_t;

struct tm_can
{
  can_t     TxCANData[CAN_QUEUELEN];
  tm_u8_t   TxQWrIndex;
  tm_u8_t   TxQRdIndex;
  can_t     RxCANData[CAN_QUEUELEN];
  tm_u8_t   RxQWrIndex;
  tm_u8_t   RxQRdIndex;
  tm_bool_t bDataSent;
};

typedef struct tm_can tm_can_t;

void InitCAN(tm_can_t *hCAN);
void init_CAN_0(int bitrate);
void CAN_BufferWrite(can_t *pCANData);
void CAN_BufferRead(can_t pCANData[], tm_u8_t *NumofCANObject);
void CANQueueData(can_t *pCANData);
void CANDequeueData();
void CANRxFilterSet(tm_u32_t id, tm_u32_t FilterMask, tm_u8_t FilterNum);
void CANRxFilterReset(tm_u8_t FilterNum);

tm_u8_t can_tx(can_t *pCANData);
tm_u8_t can_rx(can_t *pCANData);

#endif /*  __TMCAN_H__ */
