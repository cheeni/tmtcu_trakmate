

#ifndef _TMCAN_PRIVATE_H_
#define _TMCAN_PRIVATE_H_

#include "tmcan\inc\tmcan.h"

#define MCTR_DLC_MASK 0xF
#define ARB_MSGVAL_MASK 0X80000000
#define ARB_XTD_MASK  0x40000000
#define ARB_DIR_MASK 0X20000000


#define ARB_STDID_MASK 0X1FFC0000
#define ARB_XID_MASK 0X1FFFFFFF

#define ARB_STDID_SHIFT 18



void canCreateMsgObj_0(void);
void State_judge_0(void);
void TxRx_Judge_0(void);
#endif /* _TMCAN_PRIVATE_H_ */
