

#include "tmcan\src\tmcan_private.h"

extern char debugstr[];
#define CAN_NORMAL_MODE 1

/* set parameter for CAN error state judge */
unsigned char Error_State_0=0x00;

/* set parameter for CAN received data */
unsigned short Rx0_data1=0x00;
unsigned short Rx0_data2=0x00;
unsigned short Rx0_data3=0x00;
unsigned short Rx0_data4=0x00;

extern int cantxevent;
extern int canrxevent;

tm_can_t g_CAN;

/*****************************************************************************
* CAN initialize
*****************************************************************************/
void init_CAN_0(int bitrate)
{
  int MsgNbr = 0;

  IO_KEYCDR = 0x01CE;
  IO_KEYCDR = 0x41CE;
  IO_KEYCDR = 0x81CE;
  IO_KEYCDR = 0xC1CE;
  IO_EPFR86.byte = 0xE5;

  IO_KEYCDR = 0x0E2B;
  IO_KEYCDR = 0x4E2B;
  IO_KEYCDR = 0x8E2B;
  IO_KEYCDR = 0xCE2B;
  IO_PFR11.byte = 0x83;
  /* TX0_1, RX0 */

  /* I/O initial setting */
  IO_KEYCDR = 0x0E0B;
  IO_KEYCDR = 0x4E0B;
  IO_KEYCDR = 0x8E0B;
  IO_KEYCDR = 0xCE0B;
  IO_DDR11.byte = 0x81;

  /* Peripheral terminal setting */
  IO_KEYCDR = 0x0E23;
  IO_KEYCDR = 0x4E23;
  IO_KEYCDR = 0x8E23;
  IO_KEYCDR = 0xCE23;
  IO_PFR03.byte = 0x21; /* TCAN Enable */

  /* Peripheral terminal setting */
  IO_KEYCDR = 0x0E03;
  IO_KEYCDR = 0x4E03;
  IO_KEYCDR = 0x8E03;
  IO_KEYCDR = 0xCE03;
  IO_DDR03.byte = 0x01; /* TCAN Enable */


  /* Drive low to bring out of stand by */
  IO_PDR03.byte = 0x00; /* TCAN Enable */

  IO_ICR[19].byte =0x14;  /* CAN1 TX/RX/Error status Int level set */
  /* enable digital input (RX0) */


  if(bitrate == 0)
  {
  /* PLL Freq is 80MHz. Divide by 5 to get 16MHz clock */
    IO_CANPRE.byte = 0x0C;
  }
  else if(bitrate == 1)
  {
  /* PLL Freq is 80MHz. Divide by 10 to get 8MHz clock */
    IO_CANPRE.byte = 0x0E;
  }
  else
  {
    IO_CANPRE.byte = 0x0C;
  }

  /* CAN Controller Stop */
  IO_CAN1.CTRLR.bit.Init = 1;

  /* CAN bus setting */
  IO_CAN1.CTRLR.hword = 0x0061; /* BTR/BRPE Write Enable */


  /* Based on 16MHz. tq = 2/16MHz */
  IO_CAN1.BTR.hword   = 0x2B41; /* 500kbps */

  IO_CAN1.BRPER.hword = 0x0000;
  IO_CAN1.CTRLR.hword = 0x0001; /* BTR/BRPE Write Disable */

  memset((void *)&g_CAN,0,sizeof(tm_can_t));
  g_CAN.TxQWrIndex = 0;
  g_CAN.TxQRdIndex = 0;
  g_CAN.RxQWrIndex = 0;
  g_CAN.RxQRdIndex = 0;
  g_CAN.bDataSent = TM_TRUE;

  IO_CAN1.CTRLR.bit.Init = 0;

  /* Init message RAM
   * Make all messages invalid  */
  for(MsgNbr = 0; MsgNbr < 128; MsgNbr++)
  {
    IO_CAN1.IF1ARB.word = 0;
    IO_CAN1.IF1CREQ.hword = MsgNbr + 1;
          __wait_nop();  //mag lost
          __wait_nop();  //mag lost
          __wait_nop();  //mag lost
          __wait_nop();  //mag lost
          __wait_nop();  //mag lost
          __wait_nop();  //mag lost
          __wait_nop();  //mag lost
    while(IO_CAN1.IF1CREQ.hword & 0x8000);
  }
  IO_CAN1.CTRLR.hword = 0x002A;   /* Test=0 CCE=0 DAR=1 EIE=1 SIE=0 IE=1 Init=1*/

}

//**************************************************************************
//    Message Object make
//**************************************************************************
void canCreateMsgObj_0(void)
{
  /*** Tx ***/
  /*MSK select*/
  IO_CAN1.IF1CMSK.hword = 0x00B3;   /*WR/RD=1 Mask=1 Arb=1 Control=1
                                      CIP=0 TxRqst/NewDat=0 DataA=1 DataB=1*/

  /* Sending ID 0x7DF */
  IO_CAN1.IF1ARB.word = 0xBF7C0000;   /*MsgVal=1 Xtd=0 Dir=1 ID(28-18)=0x7DF*/

  /*Data*/
  /* Len 0x2 Mode 0x01 PID = 0x0C */
  IO_CAN1.IF1DTA1.hword = 0x0201;
  IO_CAN1.IF1DTA2.hword = 0x0C00;
  IO_CAN1.IF1DTB1.hword = 0x0000;
  IO_CAN1.IF1DTB2.hword = 0x0000;


  /*MCTR*/
  IO_CAN1.IF1MCTR.hword = 0x0983;   /*NewDat=Nouse MsgLst=0 IntPnd=0
                                      UMask=0 TxIE=1 RxIE=0 RmtEn=0
                                      TxRqst=1(Nouse) EoB=1 DLC=3*/

  /*CREQ*/
#ifdef  CAN_NORMAL_MODE
  IO_CAN1.IF1CREQ.hword = 0x0001;   /*transmit IFx to message RAM use buffer1*/
#else
  IO_CAN1.IF1CREQ.hword = 0x8000;   /*transmit IFx to message RAM use buffer1*/
#endif


#if 0
  /*** Rx ***/
  /*MSK select*/
  IO_CAN1.IF2CMSK.hword = 0x00F3;   /*WR/RD=1 Mask=1 Arb=1 Control=1
                                      CIP=0 TxRqst/NewDat=0 DataA=1 DataB=1*/
  /*MSK Data*/
  /*
   * ID Mask all bits set = 0x7FF
   * Receive ids 0x7E8 and 7E9
   * ID Mask = 0x7F9
   * 000 111 1111 1111
   * 000 111 1110 1000 7E8
   * 0001 1111 1010 00 1FA0
   *
   * 000 111 1110 1001 7E8
   * 0001 1111 1010 01 1FA4
   *
   * */
  /*
   * If Msk(n) bit is 1 in IFxMSK register, then the messages are accepted
   * if ID(n) bit in IFxARB register is 1 then receive bit should be 1
   * if ID(n) bit in IFxARB register is 0 then receive bit should be 0
   * If Msk(n) bit 0, the bit ID(n) is not compared and messages with
   * 1 or 0 in the corresponding position is accepted
   *
   * */
  IO_CAN1.IF2MSK.word = 0x1FA00000;   /*MXtd=1 MDir=1 res=1 MID28-MID18 all=1
                                      MID17-MID0 all=0*/
  /*Arb Data*/
#if CAN_NORMAL_MODE   /* Normal mode */
  //1001 1111 1010 000
  IO_CAN1.IF2ARB.word = 0x9FA40000;   /*MsgVal=1 Xtd=0 Dir=0 ID(28-18)=2*/
#else   /* Test mode */
  IO_CAN1.IF2ARB.word = 0x80040000;   /*MsgVal=1 Xtd=0 Dir=0 ID(28-18)=1 TestMode Only*/
#endif

  /*MCTR*/
  IO_CAN1.IF2MCTR.hword = 0x1488;   /*NewDat=Nouse MsgLst=0 IntPnd=0
                                      UMask=1 TxIE=0 RxIE=1 RmtEn=0
                                      TxRqst=Nouse EoB=1 DLC=8*/

  /*CREQ*/
  IO_CAN1.IF2CREQ.hword = 0x0002;   /*transmit IFx to message RAM use buffer2*/
#endif
}

/*****************************************************************************
* CAN function
*****************************************************************************/
void State_judge_0(void)
{
  if(IO_CAN1.STATR.bit.BOff==0x01)    //bus off
  {
    Error_State_0=0x01;

    /*Restart bus*/
    IO_CAN1.CTRLR.bit.Init = 0;   /*enable CAN controller*/
    while((IO_CAN1.ERRCNT.bit.TEC!=0)||(IO_CAN1.ERRCNT.bit.REC!=0));
    //see if recovered
  }

  if(IO_CAN1.STATR.bit.EWarn==0x01)   //error warning
  {
    Error_State_0 = 0x02;
  }

  if(!((IO_CAN1.STATR.bit.BOff)|(IO_CAN1.STATR.bit.EWarn)|(IO_CAN1.STATR.bit.EPass))) //error active
  {
    Error_State_0 = 0x03;   //error active
  }
}

void TxRx_Judge_0(void)
{
  unsigned short MsgNbr = 0x00;
  unsigned long MsgBuffer;
  unsigned int msgid;

  ///check///
  MsgNbr = IO_CAN1.INTR.hword;    //stor MsgNbr

  if( (MsgNbr>=1) && (MsgNbr<=0x20) ) // valid buffer number
  {
    MsgBuffer = 0x01 << (MsgNbr-1);
    // Check whether the interrupt source is a valid buffer

    if(((IO_CAN1.MSGVAL12.hword.MSGVAL1 & MsgBuffer) != 0) && ((IO_CAN1.INTPND12.word & MsgBuffer) != 0))
    {
      // Check whether the interrupt cause is receive or transmit
      if( (IO_CAN1.NEWDT12.word & MsgBuffer) != 0 ) // is a receive interrupt
      {

        /*fetch data from msg RAM*/
        IO_CAN1.IF2CMSK.hword = 0x007F;   /*WR/RD=0 Mask=1 Arb=1 Control=1
                                              CIP=1 TxRqst/NewDat=1 DataA=1 DataB=1*/

        IO_CAN1.IF2CREQ.bit.MN = MsgNbr;    //transmit msgRAM to IF

        if(IO_CAN1.IF2MCTR.bit.MsgLst==0x01)
        {
          __wait_nop();  //mag lost
          IO_CAN1.IF2MCTR.hword = 0x1488;   //NewDat=0 MSGLST=0 INTPND=0 UMSK=1 TXIE=0
          //RXIE=1 RMTEN=0 TXRQST=0 EOB=1
          IO_CAN1.IF2CMSK.hword = 0x0090;   //WRRD=1 CONTROL=1 other=0
          //for clear MSGLST
          IO_CAN1.IF2CREQ.bit.MN = MsgNbr;
        }
        else
        {
          msgid = (IO_CAN1.IF2ARB.word & (0x1FFC0000)); // 0001 1111 1010 0100
          if(msgid & 0x01FA4)
             __wait_nop;

          if(IO_CAN1.IF2ARB.word & ARB_XTD_MASK)
          {
            /* Extended ID*/
            g_CAN.RxCANData[g_CAN.RxQWrIndex].id = (IO_CAN1.IF2ARB.word & ARB_XID_MASK );

            /* Set 31st bit to indicate extended id */
            g_CAN.RxCANData[g_CAN.RxQWrIndex].id |= 0x80000000;

          }
          else
          {
            /* Standard ID*/
            g_CAN.RxCANData[g_CAN.RxQWrIndex].id = (IO_CAN1.IF2ARB.word & ARB_STDID_MASK ) >> ARB_STDID_SHIFT;
          }
          g_CAN.RxCANData[g_CAN.RxQWrIndex].buf[0] = IO_CAN1.IF2DTA1.byte.IF2DTA1H;
          g_CAN.RxCANData[g_CAN.RxQWrIndex].buf[1] = IO_CAN1.IF2DTA1.byte.IF2DTA1L;

          g_CAN.RxCANData[g_CAN.RxQWrIndex].buf[2] = IO_CAN1.IF2DTA2.byte.IF2DTA2H;
          g_CAN.RxCANData[g_CAN.RxQWrIndex].buf[3] = IO_CAN1.IF2DTA2.byte.IF2DTA2L;

          g_CAN.RxCANData[g_CAN.RxQWrIndex].buf[4] = IO_CAN1.IF2DTB1.byte.IF2DTB1H;
          g_CAN.RxCANData[g_CAN.RxQWrIndex].buf[5] = IO_CAN1.IF2DTB1.byte.IF2DTB1L;

          g_CAN.RxCANData[g_CAN.RxQWrIndex].buf[6] = IO_CAN1.IF2DTB2.byte.IF2DTB2H;
          g_CAN.RxCANData[g_CAN.RxQWrIndex].buf[7] = IO_CAN1.IF2DTB2.byte.IF2DTB2L;
          g_CAN.RxCANData[g_CAN.RxQWrIndex].buf_len = IO_CAN1.IF2MCTR.hword & MCTR_DLC_MASK;   //NewDat=0 MSGLST=0 INTPND=0 UMSK=1 TXIE=0
          INCCIRCULARINDEX(g_CAN.RxQWrIndex, CAN_QUEUELEN);
          canrxevent = TM_TRUE;

        }
      }
      else if ( (IO_CAN1.TREQR12.word & MsgBuffer) == 0 ) // is transmission done
      {
        /* for clear Tx intPnd */
        IO_CAN1.IF1MCTR.hword = 0x0888;   //NEWDAT=0 MSGLST=0 INTPND=0 UMASK=0
        //TXIE=1 RXIE=0 RMTEN=0 TXRQST=0 EOB=1 DLC=8

        IO_CAN1.IF1CMSK.hword = 0x0090;   //WRRD=1 MASK=0 ARB=0 CONTROL=1
        //CIP=0 TXREQ=0 DTAA/B=0
        IO_CAN1.IF1CREQ.bit.MN = MsgNbr;    //IF->RAM
        g_CAN.bDataSent = TM_TRUE;
        cantxevent = TM_TRUE;
      }
    }
  }
}

/*****************************************************************************
* CAN Interrupt
*****************************************************************************/

void __interrupt CAN_1_int(void)
{

  if(IO_CAN1.INTR.hword == 0x8000)    /* status int */
  {
    State_judge_0();    //judge state only when INTR==0x8000
  }
  else if((IO_CAN1.INTR.hword >= 0x0001)&&(IO_CAN1.INTR.hword <= 0x0020))  /* buffer int */
  {
    TxRx_Judge_0();   //judge Tx or Rx interrupt
  }
  else
  {
    __wait_nop();

  }
}


/*****************************************************************************/
/* initialized reload timer */
/*****************************************************************************/
void initial_reload_timer_1(void)
{
  /* reload timer 1 setting */
  IO_ICR[2].byte = 0x12;    /* reload timer 1  "level 2" */
  RT1_TMRLRA = 0x6000;    /* timer count data (8.192ms) */
  RT1_TMCSR = 0x081b;   /* set TMCSR1  */

}


/*****************************************************************************/

void CAN_BufferWrite(can_t *pCANData)
{
#if 1
  /* Write to message interface  buffer and init transmission */
  /* Message Object Create */
  tm_u32_t arb;

  /*MSK select*/
  IO_CAN1.IF1CMSK.hword = 0x00B3;   /*WR/RD=1 Mask=1 Arb=1 Control=1
                                      CIP=0 TxRqst/NewDat=0 DataA=1 DataB=1*/

  /* Sending ID 0x7DF */
  if(pCANData->id & 0x80000000)
  {
    arb = pCANData->id;
    arb |= ARB_MSGVAL_MASK;
    arb |= ARB_XTD_MASK;
    arb |= ARB_DIR_MASK;

    IO_CAN1.IF1ARB.word = arb;
  }
  else
  {
    arb = pCANData->id << ARB_STDID_SHIFT;
    arb |= ARB_MSGVAL_MASK;
    arb |= ARB_DIR_MASK;

    IO_CAN1.IF1ARB.word = arb;
    sprintf(debugstr,"%x",arb);
    DPRINTF("ARB:");
    DPRINTF(debugstr);
    DPRINTF("\r\n");
  }

  /*Data*/
    IO_CAN1.IF1DTA1.byte.IF1DTA1H = pCANData->buf[0];
    IO_CAN1.IF1DTA1.byte.IF1DTA1L = pCANData->buf[1];

    IO_CAN1.IF1DTA2.byte.IF1DTA2H = pCANData->buf[2];
    IO_CAN1.IF1DTA2.byte.IF1DTA2L = pCANData->buf[3];

    IO_CAN1.IF1DTB1.byte.IF1DTB1H = pCANData->buf[4];
    IO_CAN1.IF1DTB1.byte.IF1DTB1L = pCANData->buf[5];

    IO_CAN1.IF1DTB2.byte.IF1DTB2H = pCANData->buf[6];
    IO_CAN1.IF1DTB2.byte.IF1DTB2L = pCANData->buf[7];

  /*MCTR*/
  IO_CAN1.IF1MCTR.hword = 0x0983;   /*NewDat=Nouse MsgLst=0 IntPnd=0
                                      UMask=0 TxIE=1 RxIE=0 RmtEn=0
                                      TxRqst=1(Nouse) EoB=1 DLC=3*/
  //if(pCANData->buf_len > 8)
   // pCANData->buf_len = 8;
  //IO_CAN1.IF1MCTR.hword |= pCANData->buf_len;   /*NewDat=Nouse MsgLst=0 IntPnd=0 */

  /*CREQ*/
#ifdef  CAN_NORMAL_MODE
  IO_CAN1.IF1CREQ.hword = 0x0001;   /*transmit IFx to message RAM use buffer1*/
#else
  IO_CAN1.IF1CREQ.hword = 0x8000;   /*transmit IFx to message RAM use buffer1*/
#endif


#endif

}

void CAN_BufferRead(can_t pCANData[], tm_u8_t *NumofCANObject)
{
  int i;

  i = 0;
  while(g_CAN.RxQRdIndex != g_CAN.RxQWrIndex)
  {
    memcpy(&pCANData[i], &g_CAN.RxCANData[g_CAN.RxQRdIndex],sizeof(can_t));
    INCCIRCULARINDEX(g_CAN.RxQRdIndex, CAN_QUEUELEN);
    i++;
  }
  *NumofCANObject = (tm_u8_t)i;
}

void CANQueueData(can_t *pCANData)
{

  memcpy(&g_CAN.TxCANData[g_CAN.TxQWrIndex],pCANData,sizeof(can_t));
  INCCIRCULARINDEX(g_CAN.TxQWrIndex, CAN_QUEUELEN);

  if(g_CAN.bDataSent == TM_TRUE)
  {
    g_CAN.bDataSent = TM_FALSE;
    CAN_BufferWrite(&g_CAN.TxCANData[g_CAN.TxQRdIndex]);
    INCCIRCULARINDEX(g_CAN.TxQRdIndex, CAN_QUEUELEN);
  }
}

void CANDequeueData()
{
  if(g_CAN.TxQWrIndex != g_CAN.TxQRdIndex)
  {
    g_CAN.bDataSent = TM_FALSE;
    CAN_BufferWrite(&g_CAN.TxCANData[g_CAN.TxQRdIndex]);
    INCCIRCULARINDEX(g_CAN.TxQRdIndex, CAN_QUEUELEN);
  }
}

void CANRxFilterSet(tm_u32_t id, tm_u32_t FilterMask, tm_u8_t FilterNum)
{
  switch(FilterNum)
  {
    case 0:
      /*** Rx ***/
      /*MSK select*/
      IO_CAN1.IF2CMSK.hword = 0x00F3;   /*WR/RD=1 Mask=1 Arb=1 Control=1
                                          CIP=0 TxRqst/NewDat=0 DataA=1 DataB=1*/
      /*MSK Data*/
      /*
       * ID Mask all bits set = 0x7FF
       * Receive ids 0x7E8 and 7E9
       * ID Mask = 0x7F9
       * 000 111 1111 1111
       * 000 111 1110 1000 7E8
       * 0001 1111 1010 00 1FA0
       *
       * 000 111 1110 1001 7E8
       * 0001 1111 1010 01 1FA4
       *
       * */
      /*
       * If Msk(n) bit is 1 in IFxMSK register, then the messages are accepted
       * if ID(n) bit in IFxARB register is 1 then receive bit should be 1
       * if ID(n) bit in IFxARB register is 0 then receive bit should be 0
       * If Msk(n) bit 0, the bit ID(n) is not compared and messages with
       * 1 or 0 in the corresponding position is accepted
       *
       * */
      //IO_CAN1.IF2MSK.word = 0x1FA00000;   /*MXtd=1 MDir=1 res=1 MID28-MID18 all=1
                                            //MID17-MID0 all=0*/
      IO_CAN1.IF2MSK.word = 0xCFFFFF00;
      /*Arb Data*/
#if CAN_NORMAL_MODE   /* Normal mode */
      //1001 1111 1010 000
      if(id & ARB_MSGVAL_MASK )
      {
        /* Extended id */
        //IO_CAN1.IF2MSK.word |= 0x80000000;   /*MXtd=1 MDir=1 res=1 MID28-MID18 all=1  */
        IO_CAN1.IF2ARB.word = 0;
        IO_CAN1.IF2ARB.word = id | 0xC0000000;
      }
      else
      {
        IO_CAN1.IF2ARB.word = 0;
        IO_CAN1.IF2ARB.word = (id << ARB_STDID_SHIFT) | 0x80000000;
      }
#else   /* Test mode */
      IO_CAN1.IF2ARB.word = 0x80040000;   /*MsgVal=1 Xtd=0 Dir=0 ID(28-18)=1 TestMode Only*/
#endif

      /*MCTR*/
      IO_CAN1.IF2MCTR.hword = 0x1488;   /*NewDat=Nouse MsgLst=0 IntPnd=0
                                          UMask=1 TxIE=0 RxIE=1 RmtEn=0
                                          TxRqst=Nouse EoB=1 DLC=8*/

      /*CREQ*/
      IO_CAN1.IF2CREQ.hword = 0x0002;   /*transmit IFx to message RAM use buffer2*/
      break;
    case 1:
      /*** Rx ***/
      /*MSK select*/
      IO_CAN1.IF2CMSK.hword = 0x00F3;   /*WR/RD=1 Mask=1 Arb=1 Control=1
                                          CIP=0 TxRqst/NewDat=0 DataA=1 DataB=1*/

      IO_CAN1.IF2MSK.word = 0x1FA00000;   /*MXtd=1 MDir=1 res=1 MID28-MID18 all=1
                                            MID17-MID0 all=0*/
      /*Arb Data*/
#if CAN_NORMAL_MODE   /* Normal mode */
      //1001 1111 1010 000
      if(id & ARB_MSGVAL_MASK )
      {
        /* Extended id */
        //IO_CAN1.IF2MSK.word |= 0x80000000;   /*MXtd=1 MDir=1 res=1 MID28-MID18 all=1  */
        IO_CAN1.IF2ARB.word = 0;
        IO_CAN1.IF2ARB.word = id | 0xC0000000;
      }
      else
      {
        IO_CAN1.IF2ARB.word = 0;
        IO_CAN1.IF2ARB.word = (id << ARB_STDID_SHIFT) | 0x80000000;
      }
      //1001 1111 1010 000
#else   /* Test mode */
      IO_CAN1.IF2ARB.word = 0x80040000;   /*MsgVal=1 Xtd=0 Dir=0 ID(28-18)=1 TestMode Only*/
#endif

      /*MCTR*/
      IO_CAN1.IF2MCTR.hword = 0x1488;   /*NewDat=Nouse MsgLst=0 IntPnd=0
                                          UMask=1 TxIE=0 RxIE=1 RmtEn=0
                                          TxRqst=Nouse EoB=1 DLC=8*/

      /*CREQ*/
      IO_CAN1.IF2CREQ.hword = 0x0003;   /*transmit IFx to message RAM use buffer2*/
      break;
    case 2:
      /*** Rx ***/
      /*MSK select*/
      IO_CAN1.IF2CMSK.hword = 0x00F3;   /*WR/RD=1 Mask=1 Arb=1 Control=1
                                          CIP=0 TxRqst/NewDat=0 DataA=1 DataB=1*/

      IO_CAN1.IF2MSK.word = FilterMask;
      /*Arb Data*/
#if CAN_NORMAL_MODE   /* Normal mode */
      //1001 1111 1010 000
      if(id & ARB_MSGVAL_MASK )
      {
        /* Extended id */
        //IO_CAN1.IF2MSK.word |= 0x80000000;   /*MXtd=1 MDir=1 res=1 MID28-MID18 all=1  */
        IO_CAN1.IF2ARB.word = id | 0xC0000000;
      }
      else
      {
        IO_CAN1.IF2ARB.word = 0;
        IO_CAN1.IF2ARB.word = (id << ARB_STDID_SHIFT) | 0x80000000;
      }
      //1001 1111 1010 000
#else   /* Test mode */
      IO_CAN1.IF2ARB.word = 0x80040000;   /*MsgVal=1 Xtd=0 Dir=0 ID(28-18)=1 TestMode Only*/
#endif

      /*MCTR*/
      IO_CAN1.IF2MCTR.hword = 0x1488;   /*NewDat=Nouse MsgLst=0 IntPnd=0
                                          UMask=1 TxIE=0 RxIE=1 RmtEn=0
                                          TxRqst=Nouse EoB=1 DLC=8*/

      /*CREQ*/
      IO_CAN1.IF2CREQ.hword = 0x0003;   /*transmit IFx to message RAM use buffer2*/
      break;
    case 3:
      /*** Rx ***/
      /*MSK select*/
      IO_CAN1.IF2CMSK.hword = 0x00F3;   /*WR/RD=1 Mask=1 Arb=1 Control=1
                                          CIP=0 TxRqst/NewDat=0 DataA=1 DataB=1*/

      IO_CAN1.IF2MSK.word = FilterMask;
      /*Arb Data*/
#if CAN_NORMAL_MODE   /* Normal mode */
      //1001 1111 1010 000
      if(id & ARB_MSGVAL_MASK )
      {
        /* Extended id */
        //IO_CAN1.IF2MSK.word |= 0x80000000;   /*MXtd=1 MDir=1 res=1 MID28-MID18 all=1  */
        IO_CAN1.IF2ARB.word = id | 0xC0000000;
      }
      else
      {
        IO_CAN1.IF2ARB.word = 0;
        IO_CAN1.IF2ARB.word = (id << ARB_STDID_SHIFT) | 0x80000000;
      }
      //1001 1111 1010 000
#else   /* Test mode */
      IO_CAN1.IF2ARB.word = 0x80040000;   /*MsgVal=1 Xtd=0 Dir=0 ID(28-18)=1 TestMode Only*/
#endif

      /*MCTR*/
      IO_CAN1.IF2MCTR.hword = 0x1488;   /*NewDat=Nouse MsgLst=0 IntPnd=0
                                          UMask=1 TxIE=0 RxIE=1 RmtEn=0
                                          TxRqst=Nouse EoB=1 DLC=8*/

      /*CREQ*/
      IO_CAN1.IF2CREQ.hword = 0x0004;   /*transmit IFx to message RAM use buffer2*/
      break;
    case 4:
      /*** Rx ***/
      /*MSK select*/
      IO_CAN1.IF2CMSK.hword = 0x00F3;   /*WR/RD=1 Mask=1 Arb=1 Control=1
                                          CIP=0 TxRqst/NewDat=0 DataA=1 DataB=1*/

      IO_CAN1.IF2MSK.word = FilterMask;
      /*Arb Data*/
#if CAN_NORMAL_MODE   /* Normal mode */
      //1001 1111 1010 000
      if(id & ARB_MSGVAL_MASK )
      {
        /* Extended id */
        //IO_CAN1.IF2MSK.word |= 0x80000000;   /*MXtd=1 MDir=1 res=1 MID28-MID18 all=1  */
        IO_CAN1.IF2ARB.word = id | 0xC0000000;
      }
      else
      {
        IO_CAN1.IF2ARB.word = 0;
        IO_CAN1.IF2ARB.word = (id << ARB_STDID_SHIFT) | 0x80000000;
      }
      //1001 1111 1010 000
#else   /* Test mode */
      IO_CAN1.IF2ARB.word = 0x80040000;   /*MsgVal=1 Xtd=0 Dir=0 ID(28-18)=1 TestMode Only*/
#endif

      /*MCTR*/
      IO_CAN1.IF2MCTR.hword = 0x1488;   /*NewDat=Nouse MsgLst=0 IntPnd=0
                                          UMask=1 TxIE=0 RxIE=1 RmtEn=0
                                          TxRqst=Nouse EoB=1 DLC=8*/

      /*CREQ*/
      IO_CAN1.IF2CREQ.hword = 0x0005;   /*transmit IFx to message RAM use buffer2*/
      break;
  }
}

void CANRxFilterReset(tm_u8_t FilterNum)
{
  switch(FilterNum)
  {
    case 0:
      break;
    case 1:
      break;
    case 2:
      break;
    case 3:
      break;
  }
}

tm_u8_t can_tx(can_t *pCANData)
{
  int diff;

  diff = g_CAN.TxQWrIndex - g_CAN.TxQRdIndex;
  if(diff < 0)
    diff = diff + CAN_QUEUELEN;

  if(diff == (CAN_QUEUELEN - 1))
    return TM_TRUE;

  CANQueueData(pCANData);
  return TM_FALSE;
}

tm_u8_t can_rx(can_t *pCANData)
{
  if(g_CAN.RxQRdIndex != g_CAN.RxQWrIndex)
  {
    memcpy(pCANData, &g_CAN.RxCANData[g_CAN.RxQRdIndex],sizeof(can_t));
    INCCIRCULARINDEX(g_CAN.RxQRdIndex, CAN_QUEUELEN);
    return TM_FALSE;
  }
  return TM_TRUE;
}
