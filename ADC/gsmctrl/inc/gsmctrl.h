#ifndef __GSMCTRL_H__
#define __GSMCTRL_H__

#include <tmtypes.h>
#include <tmcommon.h>
#include "string.h"
#include "stdlib.h"
#include "stdio.h"
#include "tmtimer.h"
#include "tmuart.h"
#include "tmapp\inc\tmdbase.h"

#define MAXRESPLEN              64
#define CMDBUFLEN               64
#define MAXRXDATALEN            512

#define PROFILE0UNACKTIMOUT     15

#define MAXPROFILE0DATALEN      512
#define MAXPROFILE1DATALEN      512
enum gsmstate
{
   GSMINIT
  ,GSMWAITFORSYSSTART
  ,GSMSWITCHEDON
  ,GSMSWITCHOFF
  ,GSMSWITCHEDOFF
  ,GSMWAIT_SMSORESP
  ,GSM_EMERGENCYRESET
  ,GSM_DEASSERTRESET
  ,GSM_IDLE
  ,GSM_INITPROFILE
  ,GSM_CONNECTPROFILE
  ,GSM_SENDDATA
  ,GSM_NETWORKREG
  ,GSMWAIT_CREGRESP
  ,GSM_DISCONNECT
  ,GSM_RECVDATA

  ,GSMCHECKSTART
  ,GSM_RESTART
  ,CMDPROCESS_MODE0
  ,CMDPROCESS_MODE1
  ,WAITRESPONSE_MODE0
  ,WAITRESPONSE_MODE1
  ,CONNECTESTABLISH_MODE0
  ,CONNECTESTABLISH_MODE0_1
  ,CONNECTESTABLISH_MODE0_2
  ,CONNECTESTABLISH_MODE1
  ,WAITCONNECTRESPONSE_MODE0
  ,WAITCONNECTRESPONSE_MODE1
  ,DATA_COMMAND_MODE_WRITE
  ,DATA_COMMAND_MODE_WRITE1
  ,DATA_COMMAND_MODE_CHECKUNACKDATA
  ,DATA_COMMAND_UNACKDATARESP
  ,DATA_COMMAND_MODE_WRITE2
  ,DATA_COMMAND_MODE_ACK_WRITE
  ,GPRS_DATA_WRITE_RESPONSE_SISW0
  ,GPRS_DATA_WRITE_RESPONSE_SISW1
  ,GPRS_DATA_WRITE_RESPONSE_SISW_ACKPKT0
  ,GPRS_DATA_WRITE_RESPONSE_SISW_ACKPKT1
  ,GPRS_DATA_WRITE_RESPONSE_OK
  ,GPRS_DATA_WRITE_RESPONSE_CLOSE
  ,DATA_COMMAND_MODE
  ,GPRS_SHUT_MODE
  ,GSMREADYWAIT_MODE
  ,SYSPWRDOWN_MODE
  ,SMSREAD_MODE
  ,SMSSEND_MODE
  ,SYSPWRDOWN_MODE_INIT
  ,PDP_DEACT_ENDMODE
  ,GPRS_DATA_READ_INIT
  ,GPRS_DATA_READRESP_WAIT
  ,STATUSCMDPROCESS_MODE
  ,WAITSTATUSRESPONSE_MODE
  ,GSMWAITINIT_MODE
  ,SIMREADY_MODE
  ,WAITSIMREADY_MODE
  ,GSMCHECKFORCPINSTATUS
  ,GSMWAITFORPWOK
  ,GSMWAITFORPINOK
  ,ENABLEVERBOSERESPONSE
  ,GSMTRYDEFAULTPW
  ,GSMWAITFORPWOK1234
  ,GSMWAITFORPWCHG
  ,GSMSHORTRESP
  ,GSMTRYPW1234
  ,GSMTRYPW4321
  ,GSMWAITFORPWOK4321
  ,GSMTRYPW9999
  ,GSMWAITFORPWOK9999
  ,GSMCHGPW
  ,WAITFORSIMREADY
  ,DISABLECPIN
  ,WAITFORDISABLECPIN
  ,DISABLECPINAGAIN
};
typedef enum gsmstate tm_gsmstate_t;

enum gsminitprofiestate
{
   GSMPROFILE_SENDCMD
  ,GSMPROFILE_WAITCMDRESP
};
typedef enum gsminitprofiestate tm_gsminitprofilestate_t;

enum gsmconnprofilestate
{
   GSMCONN_CONNECT
  ,GSMCONN_WAITCONN
};
typedef enum gsmconnprofilestate tm_gsmconnprofilestate_t;

enum gsmdisconnprofilestate
{
   GSMCONN_DISCONNECT
  ,GSMCONN_WAITDISCONN
};
typedef enum gsmdisconnprofilestate tm_gsmdisconnprofilestate_t;

enum gsmsenddatastate
{
   GSMSEND_WAITFORREADY
  ,GSMSEND_CMDWRITE
  ,GSMSEND_WAITDATAREADY
  ,GSMSEND_WRITEDATA
  ,GSMSEND_WRITEOK
  ,GSMSEND_GETUNACKLEN
  ,GSMSEND_WAITFORUNACKLEN
};
typedef enum gsmsenddatastate tm_gsmsenddatastate_t;

enum gsmrecvdatastate
{
   GSMRECV_CMDREAD
  ,GSMRECV_WAITDATAREADY
  ,GSMRECV_READDATA
  ,GSMRECV_READOK
};
typedef enum gsmrecvdatastate tm_gsmrecvdatastate_t;

enum mdm_response
{
   MDM_OK=0
  ,MDM_ERROR
  ,MDM_NOCARRIER
  ,MDM_CONNECT0
  ,MDM_CONNECT0_URC
  ,MDM_CONNECT1
  ,MDM_CONNECT1_URC
  ,MDM_CONNECTFAIL
  ,MDM_SENDOK
  ,MDM_CLOSED
  ,MDM_CALLREADY
  ,MDM_SISR
  ,MDM_SISR_URC
  ,MDM_CLOSEOK
  ,MDM_CREG
  ,MDM_CGREG
  ,MDM_CONNECTSTATUS
  ,MDM_IPSTATUS
  ,MDM_RING
  ,MDM_ERROR3
  ,MDM_SENDFAIL
  ,MDM_SMS
  ,MDM_SMS_URC
  ,MDM_SMSLISTRESP
  ,MDM_SMSREADRESP
  ,MDM_SMSWRITE
  ,MDM_SMSDELETE
  ,MDM_SMSCMSERROR
  ,MDM_CSQ
  ,MDM_LOWVOLTAGEPOWERDOWN
  ,MDM_LOWVOLTAGEWARNING
  ,MDM_SIMREADY
  ,MDM_CPINWAIT
  ,MDM_CPINREADY
  ,MDM_SIMBUSY
  ,MDM_CMEERROR
  ,MDM_WRONGPW
  ,MDM_SIMREADYURC
  ,MDM_SHUTDOWN
  ,MAXRESPONSES
};
typedef enum mdm_response tm_mdm_response_t;

struct tm_responselist
{
  tm_mdm_response_t ResponseID;
  const char *pResponse;
  tm_u8_t ResponseLength;
};
typedef struct tm_responselist tm_responselist_t;

enum resp_state
{
    WAITFOR_BEGINCR
   ,DET_BEGINCRLF
   ,WAITFOR_LINEFEED
   ,WAITFOR_SECONDCR
   ,WAITFOR_SECONDLF
   ,DET_RESPONSEEND
   ,DET_RESPONSE
   ,WAITFOR_ENDLINEFEED
   ,SMSINDEXGET
   ,SMSNUMBERGET
   ,SMSMESSAGEGET
   ,PROFILE0_READDATA
   ,PROFILE1_READDATA
   ,PROCESS_DATA_OTA
};
typedef enum resp_state tm_respstate_t;

enum response_type
{
   NONETYPE
  ,ATRESPONSE
  ,URC
};

enum profile_status
{
   PROFILE_IDLE
  ,PROFILE_INITIALISED
  ,PROFILE_CONNECTED
  ,PROFILE_DISCONNECTED
};
typedef enum profile_status tm_pstatus_t;

struct rxdata
{
  tm_u8_t RxDataBuf[MAXRXDATALEN];
  tm_u16_t RxDataBufIndex;
};
typedef struct rxdata tm_rxdata_t;


enum gsm_action
{
   PROFILE0_DATASEND
  ,PROFILE1_DATASEND
  ,PROFILE2_DATASEND
  ,PROFILE0_DATARECV
  ,PROFILE1_DATARECV
  ,PROFILE2_DATARECV
  ,SMSSEND
  ,SMSRECEIVE
  ,POWERDOWN
  ,WAKEUP
  ,PROFILE0_CONNECT
  ,PROFILE1_CONNECT
  ,PROFILE2_CONNECT
  ,PROFILE0_DISCONNECT
  ,PROFILE1_DISCONNECT
  ,PROFILE2_DISCONNECT
  ,ENDOFTASK   /* Set task to ENDOFTASK if it has to be ignored */
};

typedef enum gsm_action tm_gsmaction_t;

struct gsmtask
{
  tm_gsmaction_t  Action;
  tm_s32_t        TimeStamp;
};
typedef struct gsmtask tm_gsmtask_t;

enum profilenum
{
   PROFILE0
  ,PROFILE1
  ,PROFILE2
  ,MAXPROFILENUM
};
typedef enum profilenum tm_profilenum_t;

struct profile
{
  tm_profilenum_t Profile;
  tm_pstatus_t    ProfileStatus;
  tm_gsminitprofilestate_t GsmInitProfileState;
  tm_gsmconnprofilestate_t GsmConnProfileState;
  tm_gsmdisconnprofilestate_t GsmDisconnProfileState;
  tm_gsmsenddatastate_t    GsmSendDataState;
  tm_gsmrecvdatastate_t    GsmRecvDataState;
  tm_u8_t         *pTxData;
  tm_s16_t        TxDataLen;
  tm_s16_t        RxDataLen;
  tm_s16_t        UnackDataLen;
  tm_u8_t         CmdCount;
  tm_bool_t       bSendData;
  tm_bool_t       bConnectionReady;
  tm_u8_t         ConnectionTryCount;
};
typedef struct profile tm_profile_t;

enum profilestatus
{
   PROFILE0_CONNECTED
  ,PROFILE1_CONNECTED
  ,PROFILE2_CONNECTED
  ,PROFILE0_DISCONNECTED
  ,PROFILE1_DISCONNECTED
  ,PROFILE2_DISCONNECTED
  ,PROFILE_CMDERROR
  ,PROFILE_CMDTIMEOUT
  ,PROFILE_CONNECTIONFAIL
  ,PROFILE0_DATASENT
  ,PROFILE_MODEMRESTART
};
typedef enum profilestatus tm_profilestatus_t;

typedef void (*pProfileData)(tm_u8_t *pData, tm_u16_t Len, void *pUserData);
typedef void (*pProfileStatus)(tm_profilestatus_t ProfileStatus, void *pUserData);

struct gsmcallback
{
  pProfileData pProfile0Data;
  pProfileData pProfile1Data;
  pProfileStatus pProfile0Status;
  pProfileStatus pProfile1Status;
  pProfileStatus pProfile2Status;
};
typedef struct gsmcallback tm_gsmcallback_t;

struct gsm
{
  tm_gsmstate_t GsmState;
  tm_respstate_t ResponseState;
  tm_mdm_response_t resptype;
  tm_mdm_response_t RespDetectID;
  tm_timer_t    *hGsmStartTimer;
  tm_timer_t    *hGsmCREGTimer;
  tm_timer_t    *hGsmAtRespTimer;
  tm_timer_t    *hDataReadyTimer;
  tm_timer_t    *hUnackTimer;
  tm_dbase_t    *hSysDbase;
  tm_profile_t  Profile[MAXPROFILENUM];
  tm_profile_t  *hActiveProfile;
  tm_gsmaction_t CurrentAction;
  tm_rxdata_t   Profie0_RxData;
  tm_rxdata_t   Profie1_RxData;
  pProfileData  pProfile0Data;
  pProfileData  pProfile1Data;
  pProfileStatus  pProfile0Status;
  pProfileStatus  pProfile1Status;
  pProfileStatus  pProfile2Status;
  void          *pUserData;
  tm_u16_t      GSMCntrlCallCount;
  tm_u8_t       RxResponseBuf[MAXRESPLEN];
  tm_u8_t       CmdBuffer[CMDBUFLEN];
  tm_gsmtask_t  GsmTasks[ENDOFTASK];
  tm_s32_t      TimeStamp;
  tm_u8_t       RxResponseBufIndex;           /**< Index to the buffer that holds the response characters  */
  tm_bool_t     RespType;
  tm_bool_t     bATEcho;
  tm_bool_t     bSendDataReady;
  tm_bool_t     bSendCmd;
  tm_bool_t     bRespDetected;
  tm_bool_t     bCRdetected;
  tm_bool_t     ResponseFound;
  tm_u8_t       CREGTryCount;
  tm_bool_t     bCREGOk;
};
typedef struct gsm tm_gsm_t;
typedef struct gsm *tm_hgsm_t;

void GsmInit(tm_gsm_t *hGsm, tm_dbase_t *hSysDbase, tm_gsmcallback_t *pCallBack, void *pUserData);
void GsmCtrl(tm_gsm_t *hGsm);
void HandleGsmData(tm_gsm_t *hGsm, tm_u8_t *pData, tm_u8_t Len);
void IncGsmTimeStamp(tm_gsm_t *hGsm);
void GsmSendData(tm_gsm_t *hGsm, tm_profilenum_t ProfileNum, tm_u8_t *pData, tm_s16_t Len);
void GsmConnect(tm_gsm_t *hGsm, tm_profilenum_t ProfileNum);
void GsmRestart(tm_gsm_t *hGsm);

#endif /* __GSMCTRL_H__ */
