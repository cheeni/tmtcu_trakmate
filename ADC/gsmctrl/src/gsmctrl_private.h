#ifndef __GSMCTRL_PRIVATE_H__
#define __GSMCTRL_PRIVATE_H__

#include <gsmctrl\inc\gsmctrl.h>

/* define various timer outs */
#define CREGTIMEROUT      120
#define ATRESPONSETIMEOUT 6
#define CONNECTIONTIMEOUT 150
#define DATAREADYTIMEOUT  60
#define PROFILE0_CONNECTIONATTEMPTS 4 



#define GSM_ON_BIT 0X80
#define GSM_ON  IO_PDR09.byte &= (~GSM_ON_BIT);
#define GSM_OFF IO_PDR09.byte |= GSM_ON_BIT;

#define GSM_RESET_BIT 0X01
#define GSM_RESET_ASSERT    IO_PDR07.byte &= (~GSM_RESET_BIT);
#define GSM_RESET_DEASSERT  IO_PDR07.byte |= GSM_RESET_BIT;

/** Expected notfications from GSM Modem  **/
const char  GSMOKresponse[]  = "OK";
const char  GSMERRORresponse[] = "ERROR";
const char  GSMNOCARRIERresponse[] = "NO CARRIER";
const char  GSMCONNECT0response[] = "^SISW: 0";
const char  GSMCONNECT1response[] = "^SISW: 1";
const char  GSMSENDOKresponse[] = "SEND OK";
const char  GSMSENDFAILresponse[] = "SEND FAIL";
const char  GSMCLOSEDresponse[] = "^SIS:";
const char  GSMSISIresponse[] = "^SISI:";
const char  GSMCALLREADYresponse[] = "^SYSSTART";
const char  GSMSISRresponse[] = "^SISR:";
const char  GSMCLOSEOKresponse[] = "CLOSE OK";
const char  GSMCONNECFAILTresponse[] = "CONNECT FAIL";
const char  CREGresponse[] = "+CREG:";
const char  CGREGresponse[] = "+CGREG:";
const char  SIMREADYresponse[] = "^SSET:";
const char  GSMIPSTATUSresponse[] = "STATE: IP STATUS";
const char  RINGresponse[] = "RING";
const char  GSMERROR3response[] = "ERROR: 3"; 
const char  SMSResponse[] = "+CMTI: \"MT\",";
const char  SMSListResponse[] = "^SMGL:";
const char  SMSReadResponse[] = "+CMGR:";
const char  SMSWriteResponse[] = "+CMGS:";
const char  SMSDelResponse[] = "+CMGD:";
const char  SMSCmsErrResponse[] = "+CMS ERROR ";
const char  CSQResponse[] = "+CSQ: ";
const char  UnderVoltageResponse[] = "UNDER-VOLTAGE POWER DOWN";
const char  UnderVoltageWarning[] = "UNDER-VOLTAGE WARNNING";
const char  SMICommand[] = "ACK";
const char  CFGCommand[] = "CFG";
const char  GSMWaitForPIn[] = "+CPIN: SIM PIN";
const char  GSMPinReady[] = "+CPIN: READY";
const char  SimBusy[] = "+CME ERROR: SIM BUSY";
const char  IncorrectPW[] = "+CME ERROR: INCORRECT PASSWORD";
const char  CMEError[] = "+CME ERROR";
const char  SIMREADYURCresponse[] = "^SSIM READY";
const char  GSMSMSOresponse[] = "^SHUTDOWN";

tm_responselist_t  GSMResponses[] = { /* MAXRESPONSES = MAXRESPONSES + 1 for adding one more response detection */
                               {MDM_OK, GSMOKresponse, sizeof(GSMOKresponse)-1},
                               {MDM_CSQ, CSQResponse, sizeof(CSQResponse)-1},
                               {MDM_ERROR, GSMERRORresponse, sizeof(GSMERRORresponse)-1},
                               {MDM_NOCARRIER, GSMNOCARRIERresponse, sizeof(GSMNOCARRIERresponse)-1},
                               {MDM_CONNECT0, GSMCONNECT0response, sizeof(GSMCONNECT0response)-1},
                               {MDM_CONNECT1, GSMCONNECT1response, sizeof(GSMCONNECT1response)-1},
                               {MDM_SENDOK, GSMSENDOKresponse, sizeof(GSMSENDOKresponse)-1},
                               {MDM_CLOSED, GSMCLOSEDresponse, sizeof(GSMCLOSEDresponse)-1},
                               {MDM_CALLREADY, GSMCALLREADYresponse, sizeof(GSMCALLREADYresponse)-1},
                               {MDM_SISR, GSMSISRresponse, sizeof(GSMSISRresponse)-1},
                               {MDM_CLOSEOK, GSMCLOSEOKresponse, sizeof(GSMCLOSEOKresponse)-1},
                               {MDM_CONNECTFAIL, GSMCONNECFAILTresponse, sizeof(GSMCONNECFAILTresponse)-1},
                               {MDM_CREG, CREGresponse, sizeof(CREGresponse)-1},
                               {MDM_CONNECTSTATUS, GSMSISIresponse, sizeof(GSMSISIresponse)-1},
                               {MDM_CREG, CGREGresponse, sizeof(CGREGresponse)-1},
                               {MDM_SIMREADY, SIMREADYresponse, sizeof(SIMREADYresponse)-1},
                               {MDM_IPSTATUS, GSMIPSTATUSresponse, sizeof(GSMIPSTATUSresponse)-1},
                               {MDM_RING, RINGresponse, sizeof(RINGresponse)-1},
                               {MDM_ERROR, GSMERROR3response, sizeof(GSMERROR3response)-1},
                               {MDM_ERROR, GSMSENDFAILresponse, sizeof(GSMSENDFAILresponse)-1},
                               {MDM_SMS, SMSResponse, sizeof(SMSResponse)-1},
                               {MDM_SMSLISTRESP, SMSListResponse, sizeof(SMSListResponse)-1},
                               {MDM_SMSREADRESP, SMSReadResponse, sizeof(SMSReadResponse)-1},
                               {MDM_SMSWRITE, SMSWriteResponse, sizeof(SMSWriteResponse)-1},
                               {MDM_SMSDELETE, SMSDelResponse, sizeof(SMSDelResponse)-1},
                               {MDM_SMSCMSERROR, SMSCmsErrResponse, sizeof(SMSCmsErrResponse)-1},
                               {MDM_LOWVOLTAGEWARNING, UnderVoltageResponse, sizeof(UnderVoltageResponse)-1},
                               {MDM_LOWVOLTAGEWARNING, UnderVoltageWarning, sizeof(UnderVoltageWarning)-1},
                               {MDM_CPINWAIT, GSMWaitForPIn, sizeof(GSMWaitForPIn)-1},
                               {MDM_CPINREADY, GSMPinReady, sizeof(GSMPinReady)-1},
                               {MDM_SIMBUSY, SimBusy, sizeof(SimBusy)-1},
                               {MDM_WRONGPW, IncorrectPW, sizeof(IncorrectPW)-1},
                               {MDM_CMEERROR, CMEError, sizeof(CMEError)-1},
                               {MDM_SIMREADYURC, SIMREADYURCresponse, sizeof(SIMREADYURCresponse)-1},
                               {MDM_SHUTDOWN, GSMSMSOresponse, sizeof(GSMSMSOresponse)-1}
                              };


/** AT Commands required for GSM Modem to make GPRS Connection **/
char *pProfile0InitCmds[] =
{
   "ATE1+CMGF=1+CNMI=2,1\r"
  ,"AT+CFUN=1\r"
  ,"AT^SICS=0,contype,GPRS0\r"
  ,"AT^SICS=0,apn,"
  ,"AT^SICS=0,dns1,"                   /* To set dns1 */
  ,"AT^SICS=0,dns2,"                   /* To set dns2 */
  ,"AT^SISS=0,srvtype,socket\r"
  ,"AT^SISS=0,conid,0\r"
  ,"AT^SISS=0,address,sock"
  ,NULL
};

char *pProfile1InitCmds[] =
{
   "AT^SICS=1,contype,GPRS0\r"
  ,"AT^SICS=1,apn,"
  ,"AT^SISS=1,srvtype,socket\r"
  ,"AT^SISS=1,conid,0\r"
  ,"AT^SISS=1,address,sock"
  ,NULL
};

static void InitResponseDet(tm_gsm_t *hGsm);
static tm_bool_t DetResponse(tm_gsm_t *hGsm, tm_u8_t Byte);
void InitProfiles(tm_gsm_t *hGsm);
tm_gsmaction_t GetTask(tm_gsm_t *hGsm);
void InitAction(tm_gsm_t *hGsm);
void InitProfile(tm_gsm_t *hGsm);
void ConnectProfile(tm_gsm_t *hGsm);
void SendProfileData(tm_gsm_t *hGsm);
void GSMUARTDataSend(tm_u8_t *pData, tm_s16_t Len);
void InitProfile0(tm_gsm_t *hGsm);
void InitProfile1(tm_gsm_t *hGsm);
void InitProfile2(tm_gsm_t *hGsm);
static void CREGStatusGet(tm_gsm_t *hGsm,  char *str);
void ConnectProfile0(tm_gsm_t *hGsm);
void ConnectProfile1(tm_gsm_t *hGsm);
void ConnectProfile2(tm_gsm_t *hGsm);
void SendDataProfile0(tm_gsm_t *hGsm);
void SendDataProfile1(tm_gsm_t *hGsm);
void SendDataProfile2(tm_gsm_t *hGsm);
static void GetSISInfo(tm_gsm_t *hGsm,  char *str);
tm_bool_t HandleProfileDisconnection(tm_gsm_t *hGsm);
void DisconnectProfile(tm_gsm_t *hGsm);
void DisconnectProfile0(tm_gsm_t *hGsm);
void DisconnectProfile1(tm_gsm_t *hGsm);
void DisconnectProfile2(tm_gsm_t *hGsm);
void HandleURC(tm_gsm_t *hGsm);
void RecvDataProfile(tm_gsm_t *hGsm);
void RecvDataProfile0(tm_gsm_t *hGsm);
void RecvDataProfile1(tm_gsm_t *hGsm);
void RecvDataProfile2(tm_gsm_t *hGsm);
static tm_profilenum_t GetSISRInfo(tm_gsm_t *hGsm,  char *str, tm_bool_t bRespType);
static void GetSISIInfo(tm_gsm_t *hGsm,  char *str);


#endif /* __GSMCTRL_PRIVATE_H__ */
