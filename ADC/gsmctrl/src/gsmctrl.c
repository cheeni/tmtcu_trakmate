#include "_fr.h"
#include <gsmctrl\src\gsmctrl_private.h>

extern int gsmcntrlevent;
extern char debugstr[];
extern int bP0DataSent;

void P0DataSent(void);


void GsmInit(tm_gsm_t *hGsm, tm_dbase_t *hSysDbase, tm_gsmcallback_t *pCallBack, void *pUserData)
{
  tm_bool_t retval;
  int i;

  memset(hGsm,0,sizeof(tm_gsm_t));

  hGsm->hSysDbase = hSysDbase;
  hGsm->pProfile0Data = pCallBack->pProfile0Data;
  hGsm->pProfile1Data = pCallBack->pProfile1Data;
  hGsm->pProfile0Status = pCallBack->pProfile0Status;
  hGsm->pProfile1Status = pCallBack->pProfile1Status;
  hGsm->pProfile2Status = pCallBack->pProfile2Status;
  hGsm->pUserData = pUserData;

  hGsm->GsmState = GSMINIT;
  InitResponseDet(hGsm);
  hGsm->GSMCntrlCallCount = 0;
  InitProfiles(hGsm);
  retval = CreateTimer(&hGsm->hGsmStartTimer, 5, NULL, hGsm);
  if(retval == TM_FALSE)
  {
    /* Timer creatation error*/
    DPRINTF("Error creating hGsmStartTimer\r\n");
  }
  retval = CreateTimer(&hGsm->hGsmCREGTimer, 5, NULL, hGsm);
  if(retval == TM_FALSE)
  {
    /* Timer creatation error*/
    DPRINTF("Error creating hGsmCREGTimer\r\n");
  }
  retval = CreateTimer(&hGsm->hGsmAtRespTimer, 5, NULL, hGsm);
  if(retval == TM_FALSE)
  {
    /* Timer creatation error*/
    DPRINTF("Error creating hGsmAtRespTimer\r\n");
  }
  retval = CreateTimer(&hGsm->hDataReadyTimer, 5, NULL, hGsm);
  if(retval == TM_FALSE)
  {
    /* Timer creatation error*/
    DPRINTF("Error creating hDataReadyTimer\r\n");
  }
  retval = CreateTimer(&hGsm->hUnackTimer, 5, NULL, hGsm);
  if(retval == TM_FALSE)
  {
    /* Timer creatation error*/
    DPRINTF("Error creating hUnackTimer\r\n");
  }
  for(i = 0; i < ENDOFTASK; i++)
  {
    hGsm->GsmTasks[i].Action = (tm_gsmaction_t) i;
    hGsm->GsmTasks[i].TimeStamp = -1;               /* Set timestamp to -1 to indicate it is invlaid */
  }
  hGsm->TimeStamp = 0;
}

void InitProfiles(tm_gsm_t *hGsm)
{
  int i;
  for(i = 0; i < MAXPROFILENUM; i++)
  {
    hGsm->Profile[i].Profile = (tm_profilenum_t)i;
    hGsm->Profile[i].ProfileStatus = PROFILE_IDLE;
  }
}

void GsmCtrl(tm_gsm_t *hGsm)
{
  hGsm->GSMCntrlCallCount++;
  switch(hGsm->GsmState)
  {
    case GSMINIT:
      if(hGsm->GSMCntrlCallCount < 200)
        break;
      /* Switch ON GSM Module after 1 second */
      hGsm->GSMCntrlCallCount = 0;
      GSM_ON
      hGsm->GsmState = GSM_EMERGENCYRESET;
      ResetTimer(hGsm->hGsmStartTimer,20);
      break;
    case GSMWAITFORSYSSTART:
      if(IsTimerExpired(hGsm->hGsmStartTimer) == TM_TRUE)
      {
        SuspendTimer(hGsm->hGsmStartTimer);
        /* Not expecting SYSSTART when auto baud is set */
        hGsm->GsmState = GSM_NETWORKREG;
        InitResponseDet(hGsm);
        hGsm->CREGTryCount = 0;
        hGsm->bCREGOk = TM_FALSE;
        ResetTimer(hGsm->hGsmCREGTimer,50);
        break;
      }
      if (hGsm->ResponseFound && hGsm->bRespDetected)
      {
        if (hGsm->RespDetectID==MDM_CALLREADY)
        {
          hGsm->GsmState = GSM_NETWORKREG;
          InitResponseDet(hGsm);
          hGsm->CREGTryCount = 0;
          hGsm->bCREGOk = TM_FALSE;
          ResetTimer(hGsm->hGsmCREGTimer,50);
        }
      }
      break;
    case GSMSWITCHOFF:
      strcpy((char *)hGsm->CmdBuffer, "AT^SMSO\r");
      GSM_OFF
      hGsm->bSendCmd = TM_TRUE;
      GSMUARTDataSend(hGsm->CmdBuffer, strlen((const char *)hGsm->CmdBuffer));
      hGsm->GsmState = GSMWAIT_SMSORESP;
      ResetTimer(hGsm->hGsmAtRespTimer, ATRESPONSETIMEOUT);
      break;
    case GSMWAIT_SMSORESP:
      if (hGsm->ResponseFound && hGsm->bRespDetected)
      {
        if (hGsm->RespDetectID == MDM_SHUTDOWN)
        {
          SuspendTimer(hGsm->hGsmAtRespTimer);
          hGsm->GsmState = GSMINIT;
        }
      }
      if(IsTimerExpired(hGsm->hGsmAtRespTimer) == TM_TRUE)
      {
        SuspendTimer(hGsm->hGsmAtRespTimer);
        hGsm->GsmState = GSM_EMERGENCYRESET;
      }
      break;
    case GSM_EMERGENCYRESET:
      if(hGsm->GSMCntrlCallCount < 100)
        break;
      GSM_RESET_ASSERT
      hGsm->GSMCntrlCallCount = 0;
      hGsm->GsmState = GSM_DEASSERTRESET;
      break;
    case GSM_DEASSERTRESET:
      if(hGsm->GSMCntrlCallCount < 200)
        return;
      GSM_RESET_DEASSERT
      hGsm->GsmState = GSMWAITFORSYSSTART;
      ResetTimer(hGsm->hGsmStartTimer,20);
      break;
    case GSMSWITCHEDON:
      break;
    case GSM_NETWORKREG:
      if(hGsm->GSMCntrlCallCount < 400)
        return;
      if(IsTimerExpired(hGsm->hGsmCREGTimer) == TM_TRUE)
      {
        /* Switch off and switch ON  module */
        hGsm->GsmState = GSMSWITCHOFF;
        break;
      }
      strcpy((char *)hGsm->CmdBuffer, "AT+CREG?\r");
      hGsm->bSendCmd = TM_TRUE;
      GSMUARTDataSend(hGsm->CmdBuffer, strlen((const char *)hGsm->CmdBuffer));
      hGsm->GsmState = GSMWAIT_CREGRESP;
      ResetTimer(hGsm->hGsmAtRespTimer, ATRESPONSETIMEOUT);
      break;
    case GSMWAIT_CREGRESP:
      if (hGsm->ResponseFound && hGsm->bRespDetected)
      {
        if(hGsm->RespDetectID == MDM_CREG)
          break;
        if(hGsm->RespDetectID == MDM_OK )
        {
          SuspendTimer(hGsm->hGsmCREGTimer);
          if (hGsm->bCREGOk == TM_TRUE)
          {
            hGsm->GsmState = GSM_IDLE;
          }
          else
          {
            hGsm->GsmState = GSM_NETWORKREG;
          }
        }
        else
        {
          hGsm->GsmState = GSM_NETWORKREG;
        }
      }
      if(IsTimerExpired(hGsm->hGsmAtRespTimer) == TM_TRUE)
      {
        /* No response for ATRESPONSETIMEOUT seconds. Resetting modem */
        hGsm->GsmState = GSM_EMERGENCYRESET;
      }
      break;
    case GSM_IDLE:
        /* TODO: Handle any URCs that are received */
        /* TODO: If a profile connection is open check whether it needs to be kept open
         *       or close it */
        if(HandleProfileDisconnection(hGsm) == TM_TRUE)
          break;
        hGsm->CurrentAction = GetTask(hGsm);
        if(hGsm->CurrentAction != ENDOFTASK)
        {
          /* InitAction modifies GsmState */
          InitAction(hGsm);
          hGsm->GSMCntrlCallCount = 0;
        }
      break;
    case GSM_INITPROFILE:
      HandleURC(hGsm);
      InitProfile(hGsm);
      break;
    case GSM_CONNECTPROFILE:
      HandleURC(hGsm);
      ConnectProfile(hGsm);
      break;
    case GSM_SENDDATA:
      HandleURC(hGsm);
      SendProfileData(hGsm);
      break;
    case GSM_DISCONNECT:
      HandleURC(hGsm);
      DisconnectProfile(hGsm);
      break;
    case GSM_RECVDATA:
      HandleURC(hGsm);
      RecvDataProfile(hGsm);
      break;
    default:
      break;
  }
  if (hGsm->ResponseFound == TM_TRUE)
  {
    hGsm->ResponseFound = TM_FALSE;
    hGsm->bRespDetected = TM_FALSE;
  }
}

void HandleURC(tm_gsm_t *hGsm)
{
  if (hGsm->ResponseFound && hGsm->bRespDetected)
  {
    if(hGsm->RespDetectID == MDM_CONNECT0_URC || hGsm->RespDetectID == MDM_CONNECT1_URC || hGsm->RespDetectID == MDM_SMS_URC|| hGsm->RespDetectID == MDM_SISR_URC || hGsm->RespDetectID == MDM_CONNECTSTATUS)
    {
      /* The URCs would have updated status elsewhere. Do not pass them to any functions */
      hGsm->ResponseFound = TM_FALSE;
      hGsm->bRespDetected = TM_FALSE;
    }
  }
}

void InitProfile(tm_gsm_t *hGsm)
{
  tm_profile_t *hActiveProfile;

  hActiveProfile = hGsm->hActiveProfile;
  switch(hActiveProfile->Profile)
  {
    case PROFILE0:
      InitProfile0(hGsm);
      break;
    case PROFILE1:
      InitProfile1(hGsm);
      break;
    case PROFILE2:
      InitProfile2(hGsm);
      break;
  }
}

void InitProfile0(tm_gsm_t *hGsm)
{
  tm_profile_t *hActiveProfile;

  hActiveProfile = hGsm->hActiveProfile;
  switch(hActiveProfile->GsmInitProfileState)
  {
    case GSMPROFILE_SENDCMD:
      if(hGsm->GSMCntrlCallCount < 50)
        return;
      if(hActiveProfile->CmdCount == 4 || hActiveProfile->CmdCount == 5)
      {
        hActiveProfile->CmdCount++;
        break;
      }
      hGsm->CmdBuffer[0] = '\0';
      strcat((char *)hGsm->CmdBuffer, (char *)pProfile0InitCmds[hActiveProfile->CmdCount]);
      switch(hActiveProfile->CmdCount)
      {
        case 3:
          strcat((char *)hGsm->CmdBuffer,hGsm->hSysDbase->APN);
          strcat((char *)hGsm->CmdBuffer,"\r");
          break;
        case 8:
          strcat((char *)hGsm->CmdBuffer,"tcp://");
          strcat((char *)hGsm->CmdBuffer,hGsm->hSysDbase->PrimaryIP);
          //strcat((char *)hGsm->CmdBuffer,"122.166.212.137");
          //strcat((char *)hGsm->CmdBuffer,"prod.assettrackr.com");
          //strcat((char *)hGsm->CmdBuffer,"52.24.220.220");
          strcat((char *)hGsm->CmdBuffer,":");
          strcat((char *)hGsm->CmdBuffer,hGsm->hSysDbase->PrimaryPort);
          //strcat((char *)hGsm->CmdBuffer,"7788");
          //strcat((char *)hGsm->CmdBuffer,"8588");
          //strcat((char *)hGsm->CmdBuffer,"9011");
          strcat((char *)hGsm->CmdBuffer,"\r");
          break;
        default:
          strcat((char *)hGsm->CmdBuffer,"\r");
      }
      hActiveProfile->CmdCount++;
      hGsm->bSendCmd = TM_TRUE;
      GSMUARTDataSend(hGsm->CmdBuffer, strlen((const char *)hGsm->CmdBuffer));
      hActiveProfile->GsmInitProfileState = GSMPROFILE_WAITCMDRESP;
      ResetTimer(hGsm->hGsmAtRespTimer, ATRESPONSETIMEOUT);
      break;
    case GSMPROFILE_WAITCMDRESP:
      if (hGsm->ResponseFound && hGsm->bRespDetected)
      {
        if(hGsm->RespDetectID == MDM_OK)
        {
          SuspendTimer(hGsm->hGsmAtRespTimer);
          if (pProfile0InitCmds[hActiveProfile->CmdCount] == NULL)
          {
            hGsm->GsmState = GSM_CONNECTPROFILE;
            hActiveProfile->ConnectionTryCount = 0;
            hActiveProfile->GsmConnProfileState = GSMCONN_CONNECT;
            hGsm->GSMCntrlCallCount = 0;
            hActiveProfile->ProfileStatus = PROFILE_INITIALISED;
            //SuspendTimer(hGsm->hGSMATRespTimer);
            //if(hGsm->hSysDbase->bGSMDebugEnable==TM_TRUE)
            //{
              //DPRINTF("Modem Initialised\r\n");
            //}
          }
          else
            hActiveProfile->GsmInitProfileState = GSMPROFILE_SENDCMD;
        }
        else if(hGsm->RespDetectID == MDM_ERROR)
        {
          //if(hGsm->hSysDbase->bGSMDebugEnable==TM_TRUE)
          //{
            //DPRINTF("Modem Initialisation failed\r\n");
          //}
          hGsm->GsmState = GSMSWITCHOFF;
          hGsm->CurrentAction = ENDOFTASK;
          hActiveProfile->ProfileStatus = PROFILE_IDLE;
          hGsm->pProfile0Status(PROFILE_MODEMRESTART, hGsm->pUserData);
        }
        else
        {
          /* TODO: Handle other resoponses */
        }
      }
      if(IsTimerExpired(hGsm->hGsmAtRespTimer) == TM_TRUE)
      {
        /* No response for ATRESPONSETIMEOUT seconds. Resetting modem */
        hGsm->GsmState = GSM_EMERGENCYRESET;
        hGsm->Profile[PROFILE0].ProfileStatus = PROFILE_IDLE;
        hGsm->CurrentAction = ENDOFTASK;
        hGsm->pProfile0Status(PROFILE_MODEMRESTART, hGsm->pUserData);
      }
      break;
  }
}

void InitProfile1(tm_gsm_t *hGsm)
{
  tm_profile_t *hActiveProfile;

  hActiveProfile = hGsm->hActiveProfile;
  switch(hActiveProfile->GsmInitProfileState)
  {
    case GSMPROFILE_SENDCMD:
      if(hGsm->GSMCntrlCallCount < 50)
        return;
      hGsm->CmdBuffer[0] = '\0';
      strcat((char *)hGsm->CmdBuffer, (char *)pProfile1InitCmds[hActiveProfile->CmdCount]);
      switch(hActiveProfile->CmdCount)
      {
        case 1:
          strcat((char *)hGsm->CmdBuffer,hGsm->hSysDbase->APN);
          strcat((char *)hGsm->CmdBuffer,"\r");
          break;
        case 4:
          strcat((char *)hGsm->CmdBuffer,"tcp://");

          /* Fleetjack */
          strcat((char *)hGsm->CmdBuffer,hGsm->hSysDbase->OTAIP);
          strcat((char *)hGsm->CmdBuffer,":");
          strcat((char *)hGsm->CmdBuffer,hGsm->hSysDbase->OTAPort);

          /* TrakMate */
          //strcat((char *)hGsm->CmdBuffer,"122.166.212.137");
          //strcat((char *)hGsm->CmdBuffer,":");
          //strcat((char *)hGsm->CmdBuffer,"9010");

          /* Assettrackr */
          //strcat((char *)hGsm->CmdBuffer,"prod.assettrackr.com");
          //strcat((char *)hGsm->CmdBuffer,":");
          //strcat((char *)hGsm->CmdBuffer,"8588");
          strcat((char *)hGsm->CmdBuffer,"\r");
          break;
        default:
          strcat((char *)hGsm->CmdBuffer,"\r");
      }
      hActiveProfile->CmdCount++;
      hGsm->bSendCmd = TM_TRUE;
      GSMUARTDataSend(hGsm->CmdBuffer, strlen((const char *)hGsm->CmdBuffer));
      hActiveProfile->GsmInitProfileState = GSMPROFILE_WAITCMDRESP;
      ResetTimer(hGsm->hGsmAtRespTimer, ATRESPONSETIMEOUT);
      break;
    case GSMPROFILE_WAITCMDRESP:
      if (hGsm->ResponseFound && hGsm->bRespDetected)
      {
        if(hGsm->RespDetectID == MDM_OK)
        {
          SuspendTimer(hGsm->hGsmAtRespTimer);
          if (pProfile1InitCmds[hActiveProfile->CmdCount] == NULL)
          {
            hGsm->GsmState = GSM_CONNECTPROFILE;
            hActiveProfile->GsmConnProfileState = GSMCONN_CONNECT;
            hGsm->GSMCntrlCallCount = 0;
            hActiveProfile->ProfileStatus = PROFILE_INITIALISED;
            //SuspendTimer(hGsm->hGSMATRespTimer);
            //if(hGsm->hSysDbase->bGSMDebugEnable==TM_TRUE)
            {
              DPRINTF("Modem Initialised\r\n");
            }
          }
          else
            hActiveProfile->GsmInitProfileState = GSMPROFILE_SENDCMD;
        }
        else if(hGsm->RespDetectID == MDM_ERROR)
        {
          //if(hGsm->hSysDbase->bGSMDebugEnable==TM_TRUE)
          {
            DPRINTF("Modem Initialisation failed\r\n");
          }
          hGsm->GsmState = GSMSWITCHOFF;
          hGsm->CurrentAction = ENDOFTASK;
          hActiveProfile->ProfileStatus = PROFILE_IDLE;
        }
        else
        {
          /* TODO: Handle other resoponses */
        }
      }
      if(IsTimerExpired(hGsm->hGsmAtRespTimer) == TM_TRUE)
      {
        /* No response for ATRESPONSETIMEOUT seconds. Resetting modem */
        hGsm->GsmState = GSM_EMERGENCYRESET;
        hGsm->Profile[PROFILE1].ProfileStatus = PROFILE_IDLE;
        hGsm->CurrentAction = ENDOFTASK;
      }
      break;
  }
}

void InitProfile2(tm_gsm_t *hGsm)
{
}


void ConnectProfile(tm_gsm_t *hGsm)
{
  tm_profile_t *hActiveProfile;

  hActiveProfile = hGsm->hActiveProfile;
  switch(hActiveProfile->Profile)
  {
    case PROFILE0:
      ConnectProfile0(hGsm);
      break;
    case PROFILE1:
      ConnectProfile1(hGsm);
      break;
    case PROFILE2:
      ConnectProfile2(hGsm);
      break;
  }
}

void ConnectProfile0(tm_gsm_t *hGsm)
{
  tm_profile_t *hActiveProfile;

  hActiveProfile = hGsm->hActiveProfile;
  switch(hActiveProfile->GsmConnProfileState)
  {
    case GSMCONN_CONNECT:
      if(hGsm->GSMCntrlCallCount < 50)
        return;
      if(hGsm->hSysDbase->DebugLevel >= DEBUGLEVEL_2)
      {
        sprintf(debugstr," ConnectionTryCount = %d\r\n",hActiveProfile->ConnectionTryCount);
        DPRINTF(debugstr);
      }
      if(hActiveProfile->ConnectionTryCount > PROFILE0_CONNECTIONATTEMPTS)
      {
        if(hGsm->hSysDbase->DebugLevel >= DEBUGLEVEL_2)
        {
          sprintf(debugstr," Connection attempts expired. Restarting modem\r\n");
          DPRINTF(debugstr);
        }
        hGsm->GsmState = GSM_EMERGENCYRESET;
        hGsm->Profile[PROFILE0].ProfileStatus = PROFILE_IDLE;
        hGsm->CurrentAction = ENDOFTASK;
        hGsm->pProfile0Status(PROFILE_MODEMRESTART, hGsm->pUserData);
        break;
      }
      strcpy((char *)hGsm->CmdBuffer,"AT^SISO=0\r");
      hActiveProfile->GsmConnProfileState = GSMCONN_WAITCONN;
      hGsm->bSendCmd = TM_TRUE;
      GSMUARTDataSend(hGsm->CmdBuffer, strlen((const char *)hGsm->CmdBuffer));
      ResetTimer(hGsm->hGsmAtRespTimer, CONNECTIONTIMEOUT);
      hActiveProfile->ConnectionTryCount++;
      break;
    case GSMCONN_WAITCONN:
      if (hGsm->ResponseFound && hGsm->bRespDetected)
      {
        if(hGsm->RespDetectID == MDM_OK)
        {
          SuspendTimer(hGsm->hGsmAtRespTimer);
          hActiveProfile->bConnectionReady = TM_FALSE;
          hGsm->GsmTasks[PROFILE0_CONNECT].TimeStamp = -1;
          hActiveProfile->ProfileStatus = PROFILE_CONNECTED;
          hGsm->CurrentAction = ENDOFTASK;
          hGsm->GsmState = GSM_IDLE;
          hGsm->pProfile0Status(PROFILE0_CONNECTED, hGsm->pUserData);
        }
        else
        {
          /* TODO Handle connection error  */
          hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
          hGsm->GsmState = GSM_DISCONNECT;
        }
      }
      /* TODO: Issue AT^SISC and try again ?*/
      if(IsTimerExpired(hGsm->hGsmAtRespTimer) == TM_TRUE)
      {
        hGsm->GsmState = GSM_EMERGENCYRESET;
        hActiveProfile->ProfileStatus = PROFILE_IDLE;
        hGsm->CurrentAction = ENDOFTASK;
        hGsm->pProfile0Status(PROFILE_MODEMRESTART, hGsm->pUserData);
      }
      break;
  }
}

void ConnectProfile1(tm_gsm_t *hGsm)
{
  tm_profile_t *hActiveProfile;

  hActiveProfile = hGsm->hActiveProfile;
  switch(hActiveProfile->GsmConnProfileState)
  {
    case GSMCONN_CONNECT:
      if(hGsm->GSMCntrlCallCount < 50)
        return;
      strcpy((char *)hGsm->CmdBuffer,"AT^SISO=1\r");
      hActiveProfile->GsmConnProfileState = GSMCONN_WAITCONN;
      hGsm->bSendCmd = TM_TRUE;
      GSMUARTDataSend(hGsm->CmdBuffer, strlen((const char *)hGsm->CmdBuffer));
      ResetTimer(hGsm->hGsmAtRespTimer, CONNECTIONTIMEOUT);
      break;
    case GSMCONN_WAITCONN:
      if (hGsm->ResponseFound && hGsm->bRespDetected)
      {
        if(hGsm->RespDetectID == MDM_OK)
        {
          SuspendTimer(hGsm->hGsmAtRespTimer);
          hActiveProfile->bConnectionReady = TM_FALSE;
          hActiveProfile->GsmSendDataState = GSMSEND_WAITFORREADY;
          hGsm->GsmState = GSM_SENDDATA;
          hGsm->Profile[PROFILE1].ProfileStatus = PROFILE_CONNECTED;
        }
        else
        {
          /* TODO Handle connection error  */
          hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
          hGsm->GsmState = GSM_DISCONNECT;
        }
      }
      /* TODO: Issue AT^SISC and try again ?*/
      if(IsTimerExpired(hGsm->hGsmAtRespTimer) == TM_TRUE)
      {
        hGsm->GsmState = GSM_EMERGENCYRESET;
        hGsm->Profile[PROFILE1].ProfileStatus = PROFILE_IDLE;
        hGsm->GsmTasks[PROFILE1_DATASEND].TimeStamp = hGsm->TimeStamp + 10;
      }
      break;
  }
}

void ConnectProfile2(tm_gsm_t *hGsm)
{
}

void DisconnectProfile(tm_gsm_t *hGsm)
{
  tm_profile_t *hActiveProfile;

  hActiveProfile = hGsm->hActiveProfile;
  switch(hActiveProfile->Profile)
  {
    case PROFILE0:
      DisconnectProfile0(hGsm);
      break;
    case PROFILE1:
      DisconnectProfile1(hGsm);
      break;
    case PROFILE2:
      DisconnectProfile2(hGsm);
      break;
  }
}

void DisconnectProfile0(tm_gsm_t *hGsm)
{
  tm_profile_t *hActiveProfile;

  hActiveProfile = hGsm->hActiveProfile;
  switch(hActiveProfile->GsmDisconnProfileState)
  {
    case GSMCONN_DISCONNECT:
      if(hGsm->GSMCntrlCallCount < 50)
        return;
      strcpy((char *)hGsm->CmdBuffer,"AT^SISC=0\r");
      hActiveProfile->GsmDisconnProfileState = GSMCONN_WAITDISCONN;
      hGsm->bSendCmd = TM_TRUE;
      GSMUARTDataSend(hGsm->CmdBuffer, strlen((const char *)hGsm->CmdBuffer));
      ResetTimer(hGsm->hGsmAtRespTimer, CONNECTIONTIMEOUT);
      break;
    case GSMCONN_WAITDISCONN:
      if (hGsm->ResponseFound && hGsm->bRespDetected)
      {
        if(hGsm->RespDetectID == MDM_OK)
        {
          SuspendTimer(hGsm->hGsmAtRespTimer);
          hActiveProfile->bConnectionReady = TM_FALSE;
          hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
          hGsm->GsmState = GSM_IDLE;
          hActiveProfile->ProfileStatus = PROFILE_INITIALISED;
          hGsm->CurrentAction = ENDOFTASK;
          hGsm->pProfile0Status(PROFILE0_DISCONNECTED, hGsm->pUserData);
        }
        else
        {
          /* TODO Handle connection error  */
          hGsm->GsmState = GSM_EMERGENCYRESET;
          hActiveProfile->ProfileStatus = PROFILE_IDLE;
          hGsm->CurrentAction = ENDOFTASK;
          hGsm->pProfile0Status(PROFILE_MODEMRESTART, hGsm->pUserData);
        }
      }
      /* TODO: Issue AT^SISC and try again ?*/
      if(IsTimerExpired(hGsm->hGsmAtRespTimer) == TM_TRUE)
      {
        hGsm->GsmState = GSM_EMERGENCYRESET;
        hActiveProfile->ProfileStatus = PROFILE_IDLE;
        hGsm->CurrentAction = ENDOFTASK;
        hGsm->pProfile0Status(PROFILE_MODEMRESTART, hGsm->pUserData);
      }
      break;
  }
}

void DisconnectProfile1(tm_gsm_t *hGsm)
{
  tm_profile_t *hActiveProfile;

  hActiveProfile = hGsm->hActiveProfile;
  switch(hActiveProfile->GsmDisconnProfileState)
  {
    case GSMCONN_DISCONNECT:
      if(hGsm->GSMCntrlCallCount < 50)
        return;
      strcpy((char *)hGsm->CmdBuffer,"AT^SISC=1\r");
      hActiveProfile->GsmDisconnProfileState = GSMCONN_WAITDISCONN;
      hGsm->bSendCmd = TM_TRUE;
      GSMUARTDataSend(hGsm->CmdBuffer, strlen((const char *)hGsm->CmdBuffer));
      ResetTimer(hGsm->hGsmAtRespTimer, CONNECTIONTIMEOUT);
      break;
    case GSMCONN_WAITDISCONN:
      if (hGsm->ResponseFound && hGsm->bRespDetected)
      {
        if(hGsm->RespDetectID == MDM_OK)
        {
          SuspendTimer(hGsm->hGsmAtRespTimer);
          hActiveProfile->bConnectionReady = TM_FALSE;
          hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
          hGsm->GsmState = GSM_IDLE;
          hGsm->Profile[PROFILE1].ProfileStatus = PROFILE_INITIALISED;
          hActiveProfile->bConnectionReady = TM_FALSE;
          /* Reschedule the task only if the data is not sent out */
          if(hGsm->GsmTasks[PROFILE1_DATASEND].TimeStamp != -1)
            hGsm->GsmTasks[PROFILE1_DATASEND].TimeStamp = hGsm->TimeStamp + 10;
        }
        else
        {
          /* TODO Handle connection error  */
          hGsm->GsmState = GSM_EMERGENCYRESET;
          /* Try again after 10 seconds */
          /* Reschedule the task only if the data is not sent out */
          if(hGsm->GsmTasks[PROFILE1_DATASEND].TimeStamp != -1)
            hGsm->GsmTasks[PROFILE1_DATASEND].TimeStamp = hGsm->TimeStamp + 10;
          hActiveProfile->ProfileStatus = PROFILE_IDLE;
        }
      }
      /* TODO: Issue AT^SISC and try again ?*/
      if(IsTimerExpired(hGsm->hGsmAtRespTimer) == TM_TRUE)
      {
        hGsm->GsmState = GSM_EMERGENCYRESET;
        hActiveProfile->ProfileStatus = PROFILE_IDLE;
        /* Reschedule the task only if the data is not sent out */
        if(hGsm->GsmTasks[PROFILE1_DATASEND].TimeStamp != -1)
          hGsm->GsmTasks[PROFILE1_DATASEND].TimeStamp = hGsm->TimeStamp + 10;
      }
      break;
  }
}

void DisconnectProfile2(tm_gsm_t *hGsm)
{
}


void SendProfileData(tm_gsm_t *hGsm)
{
  tm_profile_t *hActiveProfile;

  hActiveProfile = hGsm->hActiveProfile;
  switch(hActiveProfile->Profile)
  {
    case PROFILE0:
      SendDataProfile0(hGsm);
      break;
    case PROFILE1:
      SendDataProfile1(hGsm);
      break;
    case PROFILE2:
      SendDataProfile2(hGsm);
      break;
  }
}

void SendDataProfile0(tm_gsm_t *hGsm)
{
  tm_profile_t *hActiveProfile;

  hActiveProfile = hGsm->hActiveProfile;
  switch(hActiveProfile->GsmSendDataState)
  {
    case GSMSEND_WAITFORREADY:
      if(IsTimerExpired(hGsm->hDataReadyTimer) == TM_TRUE)
      {
        SuspendTimer(hGsm->hDataReadyTimer);
        hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
        hGsm->GsmState = GSM_DISCONNECT;
        break;
      }
      if(IsTimerRunning(hGsm->hDataReadyTimer) == TM_FALSE)
      {
        ResetTimer(hGsm->hDataReadyTimer, DATAREADYTIMEOUT);
      }
      if(hActiveProfile->ProfileStatus == PROFILE_DISCONNECTED)
      {
        SuspendTimer(hGsm->hDataReadyTimer);
        hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
        hGsm->GsmState = GSM_DISCONNECT;
        break;
      }
      if(hActiveProfile->bConnectionReady == TM_TRUE)
      {
        SuspendTimer(hGsm->hDataReadyTimer);
        hActiveProfile->bConnectionReady = TM_FALSE;
        hActiveProfile->GsmSendDataState = GSMSEND_CMDWRITE;
        hGsm->GSMCntrlCallCount = 0;
        hActiveProfile->ConnectionTryCount = 0;
      }
      break;
    case GSMSEND_CMDWRITE:
      if(hGsm->GSMCntrlCallCount < 50)
        return;
      strcpy((char *)hGsm->CmdBuffer,"AT^SISW=0,");
      sprintf(debugstr,"%d",strlen((char *)hActiveProfile->pTxData));
      strcat((char *)hGsm->CmdBuffer,debugstr);
      strcat((char *)hGsm->CmdBuffer,"\r");
      hActiveProfile->GsmSendDataState = GSMSEND_WAITDATAREADY;
      hGsm->bSendCmd = TM_TRUE;
      GSMUARTDataSend(hGsm->CmdBuffer, strlen((const char *)hGsm->CmdBuffer));
      ResetTimer(hGsm->hGsmAtRespTimer, ATRESPONSETIMEOUT);
      break;
    case GSMSEND_WAITDATAREADY:
      if (hGsm->ResponseFound && hGsm->bRespDetected)
      {
        SuspendTimer(hGsm->hGsmAtRespTimer);
        if (hGsm->RespDetectID == MDM_CONNECT0)
        {
          GSMUARTDataSend(hActiveProfile->pTxData,strlen((char *)hActiveProfile->pTxData));
          hActiveProfile->GsmSendDataState = GSMSEND_WRITEOK;
        }
        else if(hGsm->RespDetectID == MDM_ERROR)
        {
          /* TODO Handle connection error  */
          hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
          hGsm->GsmState = GSM_DISCONNECT;
        }
      }
      if(IsTimerExpired(hGsm->hGsmAtRespTimer) == TM_TRUE)
      {
        SuspendTimer(hGsm->hGsmAtRespTimer);
        hGsm->GsmState = GSM_EMERGENCYRESET;
        hActiveProfile->ProfileStatus = PROFILE_IDLE;
        hGsm->CurrentAction = ENDOFTASK;
        hGsm->pProfile0Status(PROFILE_MODEMRESTART, hGsm->pUserData);
      }
      break;
    case GSMSEND_WRITEOK:
      if (hGsm->ResponseFound && hGsm->bRespDetected)
      {
        if (hGsm->RespDetectID == MDM_OK)
        {
          /* Check for data ack */
          ResetTimer(hGsm->hUnackTimer, PROFILE0UNACKTIMOUT);
          hActiveProfile->GsmSendDataState = GSMSEND_GETUNACKLEN;
          hGsm->GSMCntrlCallCount = 0;
        }
      }
      break;
    case GSMSEND_GETUNACKLEN:
      if(hGsm->GSMCntrlCallCount < 50)
        return;
      hActiveProfile->UnackDataLen = 0;
      strcpy((char *)hGsm->CmdBuffer,"AT^SISI=0\r");
      hActiveProfile->GsmSendDataState = GSMSEND_WAITFORUNACKLEN;
      hGsm->bSendCmd = TM_TRUE;
      GSMUARTDataSend(hGsm->CmdBuffer, strlen((const char *)hGsm->CmdBuffer));
      ResetTimer(hGsm->hGsmAtRespTimer, ATRESPONSETIMEOUT);
      break;
    case GSMSEND_WAITFORUNACKLEN:
      if(IsTimerExpired(hGsm->hGsmAtRespTimer) == TM_TRUE)
      {
        SuspendTimer(hGsm->hGsmAtRespTimer);
        SuspendTimer(hGsm->hUnackTimer);
        hGsm->GsmState = GSM_EMERGENCYRESET;
        hActiveProfile->ProfileStatus = PROFILE_IDLE;
        hGsm->CurrentAction = ENDOFTASK;
        hGsm->pProfile0Status(PROFILE_MODEMRESTART, hGsm->pUserData);
      }
      if(IsTimerExpired(hGsm->hUnackTimer) == TM_TRUE)
      {
        SuspendTimer(hGsm->hUnackTimer);
        SuspendTimer(hGsm->hGsmAtRespTimer);
        hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
        hGsm->GsmState = GSM_DISCONNECT;
        hGsm->GSMCntrlCallCount = 0;
      }
      if (hGsm->ResponseFound && hGsm->bRespDetected)
      {
        if (hGsm->RespDetectID == MDM_OK)
        {
          if(hActiveProfile->UnackDataLen == 0)
          {
            SuspendTimer(hGsm->hUnackTimer);
            hActiveProfile->GsmSendDataState = GSMSEND_WAITFORREADY;
            hGsm->GsmState = GSM_IDLE;
            /* Successfully data written */
            hGsm->pProfile0Status(PROFILE0_DATASENT, hGsm->pUserData);
          }
          else
          {
            hActiveProfile->GsmSendDataState = GSMSEND_GETUNACKLEN;
            hGsm->GSMCntrlCallCount = 0;
          }
        }
        else
        {
          SuspendTimer(hGsm->hUnackTimer);
          hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
          hGsm->GsmState = GSM_DISCONNECT;
        }
      }
      break;
  }
}

void SendDataProfile1(tm_gsm_t *hGsm)
{
  tm_profile_t *hActiveProfile;

  hActiveProfile = hGsm->hActiveProfile;
  switch(hActiveProfile->GsmSendDataState)
  {
    case GSMSEND_WAITFORREADY:
      if(IsTimerExpired(hGsm->hDataReadyTimer) == TM_TRUE)
      {
        SuspendTimer(hGsm->hDataReadyTimer);
        hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
        hGsm->GsmState = GSM_DISCONNECT;
        break;
      }
      if(IsTimerRunning(hGsm->hDataReadyTimer) == TM_FALSE)
      {
        ResetTimer(hGsm->hDataReadyTimer, DATAREADYTIMEOUT);
      }
      if(hActiveProfile->ProfileStatus == PROFILE_DISCONNECTED)
      {
        SuspendTimer(hGsm->hDataReadyTimer);
        hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
        hGsm->GsmState = GSM_DISCONNECT;
        break;
      }
      if(hActiveProfile->bConnectionReady == TM_TRUE)
      {
        SuspendTimer(hGsm->hDataReadyTimer);
        hActiveProfile->bConnectionReady = TM_FALSE;
        hActiveProfile->GsmSendDataState = GSMSEND_CMDWRITE;
        hGsm->GSMCntrlCallCount = 0;
      }
      break;
    case GSMSEND_CMDWRITE:
      if(hGsm->GSMCntrlCallCount < 50)
        return;
      strcpy((char *)hGsm->CmdBuffer,"AT^SISW=1,");
      sprintf(debugstr,"%d",strlen((char *)hActiveProfile->pTxData));
      strcat((char *)hGsm->CmdBuffer,debugstr);
      strcat((char *)hGsm->CmdBuffer,"\r");
      hActiveProfile->GsmSendDataState = GSMSEND_WAITDATAREADY;
      hGsm->bSendCmd = TM_TRUE;
      GSMUARTDataSend(hGsm->CmdBuffer, strlen((const char *)hGsm->CmdBuffer));
      ResetTimer(hGsm->hGsmAtRespTimer, ATRESPONSETIMEOUT);
      break;
    case GSMSEND_WAITDATAREADY:
      if (hGsm->ResponseFound && hGsm->bRespDetected)
      {
        SuspendTimer(hGsm->hGsmAtRespTimer);
        if (hGsm->RespDetectID == MDM_CONNECT1)
        {
          GSMUARTDataSend(hActiveProfile->pTxData,strlen((char *)hActiveProfile->pTxData));
          hActiveProfile->GsmSendDataState = GSMSEND_WRITEOK;
        }
        else if(hGsm->RespDetectID == MDM_ERROR)
        {
          /* TODO Handle connection error  */
          hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
          hGsm->GsmState = GSM_DISCONNECT;
        }
      }
      if(IsTimerExpired(hGsm->hGsmAtRespTimer) == TM_TRUE)
      {
        SuspendTimer(hGsm->hGsmAtRespTimer);
        hGsm->GsmState = GSM_EMERGENCYRESET;
        hActiveProfile->ProfileStatus = PROFILE_IDLE;
        hGsm->GsmTasks[PROFILE1_DATASEND].TimeStamp = hGsm->TimeStamp + 10;
      }
      break;
    case GSMSEND_WRITEOK:
      if (hGsm->ResponseFound && hGsm->bRespDetected)
      {
        if (hGsm->RespDetectID == MDM_OK)
        {
          /* Successfully data written */
          //P1DataSent();
          hActiveProfile->GsmSendDataState = GSMSEND_WAITFORREADY;
          hGsm->GsmState = GSM_IDLE;
          hGsm->GsmTasks[PROFILE1_DATASEND].TimeStamp = -1;
        }
      }
      break;
  }
}

void SendDataProfile2(tm_gsm_t *hGsm)
{
}

void RecvDataProfile(tm_gsm_t *hGsm)
{
  tm_profile_t *hActiveProfile;

  hActiveProfile = hGsm->hActiveProfile;
  switch(hActiveProfile->Profile)
  {
    case PROFILE0:
      RecvDataProfile0(hGsm);
      break;
    case PROFILE1:
      RecvDataProfile1(hGsm);
      break;
    case PROFILE2:
      RecvDataProfile2(hGsm);
      break;
  }
}

void RecvDataProfile0(tm_gsm_t *hGsm)
{
  tm_profile_t *hActiveProfile;

  hActiveProfile = hGsm->hActiveProfile;
  switch(hActiveProfile->GsmRecvDataState)
  {
    case GSMRECV_CMDREAD:
      if(hActiveProfile->ProfileStatus == PROFILE_DISCONNECTED)
      {
        hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
        hGsm->GsmState = GSM_DISCONNECT;
        break;
      }
      if(hGsm->GSMCntrlCallCount < 50)
        return;
      strcpy((char *)hGsm->CmdBuffer,"AT^SISR=0,512\r");
      hActiveProfile->GsmRecvDataState = GSMRECV_WAITDATAREADY;
      hGsm->bSendCmd = TM_TRUE;
      GSMUARTDataSend(hGsm->CmdBuffer, strlen((const char *)hGsm->CmdBuffer));
      ResetTimer(hGsm->hGsmAtRespTimer, ATRESPONSETIMEOUT);
      break;
    case GSMRECV_WAITDATAREADY:
      if (hGsm->ResponseFound && hGsm->bRespDetected)
      {
        SuspendTimer(hGsm->hGsmAtRespTimer);
        if (hGsm->RespDetectID == MDM_SISR)
        {
          hActiveProfile->GsmRecvDataState = GSMRECV_READOK;
          ResetTimer(hGsm->hGsmAtRespTimer, ATRESPONSETIMEOUT);
        }
        else if(hGsm->RespDetectID == MDM_ERROR)
        {
          /* TODO Handle connection error  */
          hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
          hGsm->GsmState = GSM_DISCONNECT;
        }
      }
      if(IsTimerExpired(hGsm->hGsmAtRespTimer) == TM_TRUE)
      {
        SuspendTimer(hGsm->hGsmAtRespTimer);
        hGsm->GsmState = GSM_EMERGENCYRESET;
        hActiveProfile->ProfileStatus = PROFILE_IDLE;
        hGsm->pProfile0Status(PROFILE_MODEMRESTART, hGsm->pUserData);
      }
      break;
    case GSMRECV_READOK:
      if(IsTimerExpired(hGsm->hGsmAtRespTimer) == TM_TRUE)
      {
        SuspendTimer(hGsm->hGsmAtRespTimer);
        hGsm->GsmState = GSM_EMERGENCYRESET;
        hActiveProfile->ProfileStatus = PROFILE_IDLE;
        hGsm->pProfile0Status(PROFILE_MODEMRESTART, hGsm->pUserData);
        break;
      }
      /* TODO: Timeout this state */
      if (hGsm->ResponseFound && hGsm->bRespDetected)
      {
        if (hGsm->RespDetectID == MDM_OK)
        {
          /* Successfully data read */
          hActiveProfile->GsmRecvDataState = GSMRECV_CMDREAD;
          hGsm->GsmState = GSM_IDLE;
          hGsm->GsmTasks[PROFILE0_DATARECV].TimeStamp = -1;
        }
      }
      break;
  }
}

void RecvDataProfile1(tm_gsm_t *hGsm)
{
  tm_profile_t *hActiveProfile;

  hActiveProfile = hGsm->hActiveProfile;
  switch(hActiveProfile->GsmRecvDataState)
  {
    case GSMRECV_CMDREAD:
      if(hActiveProfile->ProfileStatus == PROFILE_DISCONNECTED)
      {
        hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
        hGsm->GsmState = GSM_DISCONNECT;
        break;
      }
      if(hGsm->GSMCntrlCallCount < 50)
        return;
      strcpy((char *)hGsm->CmdBuffer,"AT^SISR=1,512\r");
      hActiveProfile->GsmRecvDataState = GSMRECV_WAITDATAREADY;
      hGsm->bSendCmd = TM_TRUE;
      GSMUARTDataSend(hGsm->CmdBuffer, strlen((const char *)hGsm->CmdBuffer));
      ResetTimer(hGsm->hGsmAtRespTimer, ATRESPONSETIMEOUT);
      break;
    case GSMRECV_WAITDATAREADY:
      if (hGsm->ResponseFound && hGsm->bRespDetected)
      {
        SuspendTimer(hGsm->hGsmAtRespTimer);
        if (hGsm->RespDetectID == MDM_SISR)
        {
          hActiveProfile->GsmRecvDataState = GSMRECV_READOK;
          ResetTimer(hGsm->hGsmAtRespTimer, ATRESPONSETIMEOUT);
        }
        else if(hGsm->RespDetectID == MDM_ERROR)
        {
          /* TODO Handle connection error  */
          hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
          hGsm->GsmState = GSM_DISCONNECT;
        }
      }
      if(IsTimerExpired(hGsm->hGsmAtRespTimer) == TM_TRUE)
      {
        SuspendTimer(hGsm->hGsmAtRespTimer);
        hGsm->GsmState = GSM_EMERGENCYRESET;
        hActiveProfile->ProfileStatus = PROFILE_IDLE;
      }
      break;
    case GSMRECV_READOK:
      if(IsTimerExpired(hGsm->hGsmAtRespTimer) == TM_TRUE)
      {
        SuspendTimer(hGsm->hGsmAtRespTimer);
        hGsm->GsmState = GSM_EMERGENCYRESET;
        hActiveProfile->ProfileStatus = PROFILE_IDLE;
        break;
      }
      /* TODO: Timeout this state */
      if (hGsm->ResponseFound && hGsm->bRespDetected)
      {
        if (hGsm->RespDetectID == MDM_OK)
        {
          /* Successfully data read */
          hActiveProfile->GsmRecvDataState = GSMRECV_CMDREAD;
          hGsm->GsmState = GSM_IDLE;
          hGsm->GsmTasks[PROFILE1_DATARECV].TimeStamp = -1;
#if 0
          debugstr[0] = '\0';
          for(i = 0; i < hGsm->Profile[PROFILE1].RxDataLen; i++)
          {
            sprintf(&debugstr[strlen(debugstr)], "%d ",hGsm->Profie1_RxData.RxDataBuf[i]);
          }
          sprintf(&debugstr[strlen(debugstr)], "\r\n");
          DPRINTF(debugstr);
#endif
          hGsm->pProfile1Data(hGsm->Profie1_RxData.RxDataBuf, hGsm->Profile[PROFILE1].RxDataLen, hGsm->pUserData);

        }
      }
      break;
  }
}

void RecvDataProfile2(tm_gsm_t *hGsm)
{
}

tm_bool_t HandleProfileDisconnection(tm_gsm_t *hGsm)
{
  tm_bool_t retval = TM_FALSE;
  if(hGsm->Profile[PROFILE0].ProfileStatus == PROFILE_DISCONNECTED)
  {
    hGsm->GsmState = GSM_DISCONNECT;
    hGsm->hActiveProfile = &hGsm->Profile[PROFILE0];
    hGsm->hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
    retval = TM_TRUE;
  }
  else if(hGsm->Profile[PROFILE1].ProfileStatus == PROFILE_DISCONNECTED)
  {
    hGsm->GsmState = GSM_DISCONNECT;
    hGsm->hActiveProfile = &hGsm->Profile[PROFILE1];
    hGsm->hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
    retval = TM_TRUE;
  }
  else if(hGsm->Profile[PROFILE2].ProfileStatus == PROFILE_DISCONNECTED)
  {
    hGsm->GsmState = GSM_DISCONNECT;
    hGsm->hActiveProfile = &hGsm->Profile[PROFILE2];
    hGsm->hActiveProfile->GsmDisconnProfileState = GSMCONN_DISCONNECT;
    retval = TM_TRUE;
  }
  return retval;
}


tm_gsmaction_t GetTask(tm_gsm_t *hGsm)
{
  /* Pick the task with the lease timestap */
  int i;
  int timestamp;
  tm_gsmaction_t actionnum;

  /* Set time stamp to maximum */
  timestamp = 0x7FFFFFF;
  actionnum = ENDOFTASK;
  for(i = 0; i < ENDOFTASK; i++)
  {
    if(hGsm->GsmTasks[i].TimeStamp == -1)
      continue;
    if(timestamp > hGsm->GsmTasks[i].TimeStamp)
    {
      actionnum = (tm_gsmaction_t) i;
      timestamp = hGsm->GsmTasks[i].TimeStamp;
    }
  }
  if(actionnum != ENDOFTASK)
  {
    hGsm->GsmTasks[actionnum].TimeStamp = -1;; 
  }
  return actionnum;
}

void InitAction(tm_gsm_t *hGsm)
{

  /* Update active profile */
  switch(hGsm->CurrentAction)
  {
    case PROFILE0_DATASEND:
      hGsm->hActiveProfile = &hGsm->Profile[0];
      hGsm->hActiveProfile->CmdCount = 0;
      switch(hGsm->hActiveProfile->ProfileStatus)
      {
        case PROFILE_IDLE:
          hGsm->GsmState = GSM_INITPROFILE;
          hGsm->hActiveProfile->GsmInitProfileState = GSMPROFILE_SENDCMD;
          hGsm->GSMCntrlCallCount = 0;
          break;
        case PROFILE_INITIALISED:
          hGsm->GsmState = GSM_CONNECTPROFILE;
          hGsm->hActiveProfile->GsmConnProfileState = GSMCONN_CONNECT;
          hGsm->GSMCntrlCallCount = 0;
          break;
        case PROFILE_CONNECTED:
          hGsm->GsmState = GSM_SENDDATA;
          hGsm->hActiveProfile->GsmSendDataState = GSMSEND_WAITFORREADY;
          hGsm->GSMCntrlCallCount = 0;
          break;
        case PROFILE_DISCONNECTED:
          hGsm->GsmState = GSM_CONNECTPROFILE;
          hGsm->hActiveProfile->GsmConnProfileState = GSMCONN_CONNECT;
          hGsm->GSMCntrlCallCount = 0;
          break;
      }
      break;
    case PROFILE1_DATASEND:
      hGsm->hActiveProfile = &hGsm->Profile[1];
      hGsm->hActiveProfile->CmdCount = 0;
      switch(hGsm->hActiveProfile->ProfileStatus)
      {
        case PROFILE_IDLE:
          hGsm->GsmState = GSM_INITPROFILE;
          hGsm->hActiveProfile->GsmInitProfileState = GSMPROFILE_SENDCMD;
          hGsm->GSMCntrlCallCount = 0;
          break;
        case PROFILE_INITIALISED:
          hGsm->GsmState = GSM_CONNECTPROFILE;
          hGsm->hActiveProfile->GsmConnProfileState = GSMCONN_CONNECT;
          hGsm->GSMCntrlCallCount = 0;
          break;
        case PROFILE_CONNECTED:
          hGsm->GsmState = GSM_SENDDATA;
          hGsm->hActiveProfile->GsmSendDataState = GSMSEND_WAITFORREADY;
          hGsm->GSMCntrlCallCount = 0;
          break;
        case PROFILE_DISCONNECTED:
          hGsm->GsmState = GSM_CONNECTPROFILE;
          hGsm->hActiveProfile->GsmConnProfileState = GSMCONN_CONNECT;
          hGsm->GSMCntrlCallCount = 0;
          break;
      }
      break;
    case PROFILE2_DATASEND:
      break;
    case PROFILE0_DATARECV:
      hGsm->hActiveProfile = &hGsm->Profile[0];
      hGsm->hActiveProfile->CmdCount = 0;
      switch(hGsm->hActiveProfile->ProfileStatus)
      {
        case PROFILE_IDLE:
          hGsm->GsmState = GSM_INITPROFILE;
          hGsm->hActiveProfile->GsmInitProfileState = GSMPROFILE_SENDCMD;
          hGsm->GSMCntrlCallCount = 0;
          break;
        case PROFILE_INITIALISED:
          hGsm->GsmState = GSM_CONNECTPROFILE;
          hGsm->hActiveProfile->GsmConnProfileState = GSMCONN_CONNECT;
          hGsm->GSMCntrlCallCount = 0;
          break;
        case PROFILE_CONNECTED:
          hGsm->GsmState = GSM_RECVDATA;
          hGsm->hActiveProfile->GsmRecvDataState = GSMRECV_CMDREAD;
          hGsm->GSMCntrlCallCount = 0;
          break;
        case PROFILE_DISCONNECTED:
          hGsm->GsmState = GSM_CONNECTPROFILE;
          hGsm->hActiveProfile->GsmConnProfileState = GSMCONN_CONNECT;
          hGsm->GSMCntrlCallCount = 0;
          break;
      }
      break;
    case PROFILE1_DATARECV:
      hGsm->hActiveProfile = &hGsm->Profile[1];
      hGsm->hActiveProfile->CmdCount = 0;
      switch(hGsm->hActiveProfile->ProfileStatus)
      {
        case PROFILE_IDLE:
          hGsm->GsmState = GSM_INITPROFILE;
          hGsm->hActiveProfile->GsmInitProfileState = GSMPROFILE_SENDCMD;
          hGsm->GSMCntrlCallCount = 0;
          break;
        case PROFILE_INITIALISED:
          hGsm->GsmState = GSM_CONNECTPROFILE;
          hGsm->hActiveProfile->GsmConnProfileState = GSMCONN_CONNECT;
          hGsm->GSMCntrlCallCount = 0;
          break;
        case PROFILE_CONNECTED:
          hGsm->GsmState = GSM_RECVDATA;
          hGsm->hActiveProfile->GsmRecvDataState = GSMRECV_CMDREAD;
          hGsm->GSMCntrlCallCount = 0;
          break;
        case PROFILE_DISCONNECTED:
          hGsm->GsmState = GSM_CONNECTPROFILE;
          hGsm->hActiveProfile->GsmConnProfileState = GSMCONN_CONNECT;
          hGsm->GSMCntrlCallCount = 0;
          break;
      }
    case SMSSEND:
      break;
    case SMSRECEIVE:
      break;
    case POWERDOWN:
      break;
    case WAKEUP:
      break;
    case PROFILE0_CONNECT:
      hGsm->hActiveProfile = &hGsm->Profile[0];
      switch(hGsm->hActiveProfile->ProfileStatus)
      {
        case PROFILE_IDLE:
          hGsm->GsmState = GSM_INITPROFILE;
          hGsm->hActiveProfile->GsmInitProfileState = GSMPROFILE_SENDCMD;
          hGsm->GSMCntrlCallCount = 0;
          break;
        case PROFILE_INITIALISED:
          hGsm->GsmState = GSM_CONNECTPROFILE;
          hGsm->hActiveProfile->GsmConnProfileState = GSMCONN_CONNECT;
          hGsm->GSMCntrlCallCount = 0;
          break;
        case PROFILE_CONNECTED:
          hGsm->pProfile0Status(PROFILE0_CONNECTED, hGsm->pUserData);
          break;
        case PROFILE_DISCONNECTED:
          hGsm->GsmState = GSM_CONNECTPROFILE;
          hGsm->hActiveProfile->GsmConnProfileState = GSMCONN_CONNECT;
          hGsm->GSMCntrlCallCount = 0;
          break;
      }
      break;
    case PROFILE1_CONNECT:
      break;
    case PROFILE2_CONNECT:
      break;
    default:
      DPRINTF("Invalid action\r\n");
  }
}

void InitResponseDet(tm_gsm_t *hGsm)
{
  hGsm->ResponseState = WAITFOR_BEGINCR;
  hGsm->bCRdetected = TM_FALSE;
}

void HandleGsmData(tm_gsm_t *hGsm, tm_u8_t *pData, tm_u8_t Len)
{
  int i;
  for(i = 0; i < Len; i++)
  {
    if(DetResponse(hGsm, pData[i]) == TM_TRUE)
    {
      hGsm->ResponseFound = TM_TRUE;
      /* Trigger gsmcntrlevent to handle the detected response */
      gsmcntrlevent = TM_TRUE;
    }
  }
}

static tm_bool_t DetResponse(tm_gsm_t *hGsm, tm_u8_t Byte)
{
  tm_bool_t responsedetected=TM_FALSE;
  tm_responselist_t  *pResponsesProfile;
  tm_u8_t i=0;
  int strsrchresp=1;
  tm_profilenum_t Profile;

  pResponsesProfile = GSMResponses;

#if 0
  Include this when SMS parsing is implemented
  if (Byte == '>' && (hGsm->GsmState == SMSREAD_MODE || hGsm->GsmState == SMSSEND_MODE) && (hGsm->SmsParser.state == SMSGET_RESPONSE || hGsm->SmsParser.state == SMSWRITE))
  {
    if (!hGsm->bSendDataReady)
    {
      hGsm->bSendDataReady = TM_TRUE;
      return TM_FALSE;
    }
  }
  else if (Byte == 0x1A && (hGsm->GsmStat == SMSREAD_MODE  || hGsm->GsmState == SMSSEND_MODE) && (hGsm->SmsParser.state == SMSGETWRITERESPONSE || hGsm->SmsParser.state == SMSWRITERESP))
  {
    if (!hGsm->bLookForSMSWriteResp)
    {
      hGsm->bLookForSMSWriteResp=TM_TRUE;
      hGsm->ResponseState = WAITFOR_SECONDCR;
      hGsm->ResponseFound=TM_FALSE;
      hGsm->bRespDetected=TM_FALSE;
      return TM_FALSE;
    }
  }
#endif

  switch (hGsm->ResponseState)
  {
    case WAITFOR_BEGINCR:
      if (Byte == CARRIAGERETURN)
      {
        if(hGsm->bATEcho == TM_TRUE)
        {
          hGsm->RespType = ATRESPONSE;
          hGsm->bATEcho = TM_FALSE;
        }
        else
        {
          hGsm->RespType = URC;
        }
        hGsm->ResponseState = WAITFOR_LINEFEED;
      }
      if(Byte == '^')
      {
        /* To parse ^SYSSTART response */
        hGsm->ResponseState = DET_RESPONSE;
        hGsm->RxResponseBufIndex = 0;
        hGsm->RxResponseBuf[hGsm->RxResponseBufIndex++] = Byte;
        hGsm->RespType = NONETYPE;
      }
      if(Byte == 'A' && hGsm->bSendCmd == TM_TRUE)
      {
        /* Got back A send rest of the command */
        hGsm->bSendCmd = TM_FALSE;
        hGsm->bATEcho = TM_TRUE;
        hGsm->RespType = ATRESPONSE;
        hGsm->ResponseState = DET_RESPONSE;
        hGsm->RxResponseBufIndex = 0;
        hGsm->RxResponseBuf[hGsm->RxResponseBufIndex++] = Byte;
      }
      break;
    case WAITFOR_LINEFEED:
      if (Byte == LINEFEED)
      {
        hGsm->ResponseState = WAITFOR_SECONDCR;
        hGsm->RxResponseBufIndex = 0;
      }
      else if(Byte != CARRIAGERETURN)
      {
        hGsm->ResponseState = WAITFOR_BEGINCR;
      }
      break;
    case WAITFOR_SECONDCR:
      if (Byte != CARRIAGERETURN)
      {
        //hGsm->RxResponseBuf[hGsm->RxResponseBufIndex++] = (tm_u8_t)vts_toupper(Byte);
        hGsm->RxResponseBuf[hGsm->RxResponseBufIndex++] = Byte;
        hGsm->ResponseState = DET_RESPONSE;
      }
      else
      {
        hGsm->ResponseState = WAITFOR_SECONDLF;
      }
      break;
    case WAITFOR_SECONDLF:
      if (Byte != LINEFEED)
      {
        hGsm->ResponseState = WAITFOR_BEGINCR;
      }
      else
      {
        hGsm->ResponseState = DET_RESPONSE;
      }
      break;
    case DET_RESPONSE:
      if (Byte == CARRIAGERETURN)
      {
        if(hGsm->bATEcho == TM_TRUE)
        {
          /* Received AT COMMAND echo. Now look for response */
          hGsm->ResponseState = WAITFOR_BEGINCR;
          //hGsm->bATEcho = TM_FALSE;
        }
        else
        {
          hGsm->ResponseState = WAITFOR_ENDLINEFEED;
          hGsm->RxResponseBuf[hGsm->RxResponseBufIndex++] = '\0';
        }
      }
      else
      {
        //hGsm->RxResponseBuf[hGsm->RxResponseBufIndex++] = (tm_u8_t)vts_toupper(Byte);
        hGsm->RxResponseBuf[hGsm->RxResponseBufIndex++] = Byte;
        if (hGsm->RxResponseBufIndex>=MAXRESPLEN)
        {
          hGsm->RxResponseBufIndex=0;
        }
      }
      break;
    case WAITFOR_ENDLINEFEED:
      if (Byte == LINEFEED)
      {
        for (i=0;i<MAXRESPONSES;i++)
        {
          strsrchresp = strncmp((const char *)hGsm->RxResponseBuf, pResponsesProfile[i].pResponse, pResponsesProfile[i].ResponseLength);
          if (!strsrchresp)
          {
            //vts_strcpy(debugstr,"DEBUG: ");
            //vts_strcat(debugstr,"Response Detected\r\n");
            //UART0QueueData((tm_u8_t *)debugstr, vts_strlen((const char *)debugstr));
            hGsm->bRespDetected=TM_TRUE;
            hGsm->RespDetectID=pResponsesProfile[i].ResponseID;
            //hGsm->SmsParser.resptype = pResponsesProfile[i].ResponseID;
            switch (hGsm->RespDetectID)
            {
#if 0
              case MDM_SMS:
                if ((tm_u16_t)vts_strlen((char *)hGsm->RxResponseBuf) > pResponsesProfile[i].ResponseLength &&
                    (tm_u16_t)vts_strlen((char *)hGsm->RxResponseBuf) < pResponsesProfile[i].ResponseLength+MAXSMSINDEX )
                {
                  vts_strcpy((char *)hGsm->SMSReadIndexstr, (const char*)&hGsm->RxResponseBuf[pResponsesProfile[i].ResponseLength]);
                  hGsm->bNewSMSarrived=TM_TRUE;
                  //TODO: To be added CheckForSMS(hGsm);
                }
                break;
              case MDM_CSQ:
                if ((tm_u16_t)vts_strlen((char *)hGsm->RxResponseBuf) > pResponsesProfile[i].ResponseLength )
                {
                  CalculateSignalStrength(hGsm,&hGsm->RxResponseBuf[pResponsesProfile[i].ResponseLength]);
                }
                break;
              case MDM_LOWVOLTAGEWARNING:  
                hGsm->bVoltageLow=TM_TRUE;
                break;
              case MDM_SMSLISTRESP:
                /* get senders mobile number */
                getindex(hGsm);
                hGsm->SmsParser.msgindex = 0;
                hGsm->ResponseState = SMSMESSAGEGET;
                break;
              case MDM_SMSREADRESP:
                /* get senders mobile number */
                getsendernum(hGsm);
                hGsm->SmsParser.msgindex = 0;
                hGsm->ResponseState = SMSMESSAGEGET;
                break;
              case MDM_OK:
              case MDM_ERROR:
                hGsm->SmsParser.smsresponsereceived = TM_TRUE;
                //hGsm->CmdTimer = hGsm->SecondstimeCnt + 1;
                hGsm->GSMCntrlCallCount = 0;
                break;
              case MDM_CPINREADY:
                hGsm->bCPINReady = TM_TRUE;
                break;
              case MDM_CPINWAIT:
                hGsm->bCPINWait = TM_TRUE;
                break;
              case MDM_SIMREADY:
                if (hGsm->RxResponseBuf[pResponsesProfile[i].ResponseLength+1]=='1')
                  hGsm->bSimReady = TM_TRUE;
                else
                  hGsm->bSimReady = TM_FALSE;
                break;
              case MDM_SIMREADYURC:
                hGsm->bSimReady = TM_TRUE;
                break;
              case MDM_SISR:
                /*UART0QueueData("Recieved sisr ",vts_strlen("Recieved sisr "));
                  UART0QueueData((tm_u8_t *)&hGsm->RxResponseBuf[pResponsesProfile[i].ResponseLength],vts_strlen(&hGsm->RxResponseBuf[pResponsesProfile[i].ResponseLength]));
                  UART0QueueData("*\r\n",3);*/
                ReadDataPacketdetails(hGsm,(char *)&hGsm->RxResponseBuf[pResponsesProfile[i].ResponseLength]);

                /* If URC data read will be initiated after completing the current write */
                if(hGsm->RespType == URC)
                {
                  if(hGsm->ReadpktData.ProfileId== 0)
                    if(hGsm->ReadpktData.ReadDatalength == 1)
                    {
                      hGsm->bSISR0 = TM_TRUE;
                      /*if(hGsm->hSysDbase->bGSMDebugEnable==TM_TRUE)
                        UART0QueueData((tm_u8_t *)"sisr received\r\n",vts_strlen("sisr received\r\n"));*/
                    }
                  if(hGsm->ReadpktData.ProfileId== 1)
                    if(hGsm->ReadpktData.ReadDatalength == 1)
                    {
                      hGsm->bSISR1 = TM_TRUE;
                    }
                  break;
                }
                /* Depending on profile ID either either it is data from polling server
                 * or data from the OTA server. 0 -> Polling server 1-> OTA server  */
                {
                  if (hGsm->ReadpktData.ReadDatalength && hGsm->ReadpktData.bCmdDetect)
                  {
                    if (hGsm->ReadpktData.ProfileId==1)
                    {
                      if(hGsm->hSysDbase->bGSMDebugEnable==TM_TRUE)
                        UART0QueueData((tm_u8_t *)"ota data\r\n",vts_strlen("ota data\r\n"));
                      hGsm->ResponseState = PROCESS_DATA_OTA;
                    }
                    else
                    {
                      hGsm->CmdLen=0;
                      //UART0QueueData((tm_u8_t *)"smi data\r\n",vts_strlen("smi data\r\n"));
                      hGsm->ResponseState = PROCESS_DATA_SMI;
                      memset(&hGsm->SMI1CmdString,0,SMICMD_STRING_SIZE);
                    }
                    hGsm->ReadpktData.ActualReadDatalength=0;
                  }
                }
                break;
              case MDM_CONNECT0:
                /*if (hGsm->GsmState==DATA_COMMAND_UNACKDATARESP)
                  GetUnAckLen(hGsm,(char *)&hGsm->RxResponseBuf[pResponsesProfile[i].ResponseLength]);*/
                break;
              case MDM_CONNECTSTATUS:
                GetSISiInfo(hGsm,(char *)&hGsm->RxResponseBuf[pResponsesProfile[i].ResponseLength]);
                break;
              default:
                break;
#endif
              case MDM_CREG:
                //if (hGsm->GsmState!=WAITSTATUSRESPONSE_MODE)
                  CREGStatusGet(hGsm,(char *)&hGsm->RxResponseBuf[pResponsesProfile[i].ResponseLength]);
                break;
              case MDM_OK:
              case MDM_ERROR:
                //hGsm->SmsParser.smsresponsereceived = TM_TRUE;
                hGsm->GSMCntrlCallCount = 0;
                break;
              case MDM_CONNECT0:
                if(hGsm->RespType == URC)
                {
                  hGsm->Profile[PROFILE0].bConnectionReady = TM_TRUE;
                  hGsm->RespDetectID = MDM_CONNECT0_URC;
                }
                break;
              case MDM_CONNECT1:
                if(hGsm->RespType == URC)
                {
                  hGsm->Profile[PROFILE1].bConnectionReady = TM_TRUE;
                  hGsm->RespDetectID = MDM_CONNECT1_URC;
                }
                break;
              case MDM_SISR:
                if(hGsm->RespType == URC)
                {
                  hGsm->RespDetectID = MDM_SISR_URC;
                  /* GetSISRInfo updates task */
                  GetSISRInfo(hGsm,(char *)&hGsm->RxResponseBuf[pResponsesProfile[i].ResponseLength], hGsm->RespType);
                }
                else
                {
                  Profile = GetSISRInfo(hGsm,(char *)&hGsm->RxResponseBuf[pResponsesProfile[i].ResponseLength], hGsm->RespType);
                  switch(Profile)
                  {
                    case PROFILE0:
                      hGsm->ResponseState = PROFILE0_READDATA;
                      hGsm->Profie0_RxData.RxDataBufIndex = 0;
                      break;
                    case PROFILE1:
                      hGsm->ResponseState = PROFILE1_READDATA;
                      hGsm->Profie1_RxData.RxDataBufIndex = 0;
                      break;
                    case PROFILE2:
                      break;
                  }

                }
                break;
              case MDM_CLOSED:
                GetSISInfo(hGsm,(char *)&hGsm->RxResponseBuf[pResponsesProfile[i].ResponseLength]);
                break;
              case MDM_CONNECTSTATUS:
                GetSISIInfo(hGsm,(char *)&hGsm->RxResponseBuf[pResponsesProfile[i].ResponseLength]);
                break;
              default:
                break;
            }
            break;
          }
        }
#if 0
        if (hGsm->GsmState==WAITSTATUSRESPONSE_MODE)
        {
          if (hGsm->bRespDetected && hGsm->RespDetectID==MDM_OK)
          {
          }
          else
            vts_strcat((char *)&hGsm->SmsParser.messageresponse[0],(char *)&hGsm->RxResponseBuf[0]);
        }
#endif
        if(hGsm->RespType == ATRESPONSE)
        {
          hGsm->bSendCmd = TM_FALSE;
        }
#if 0
        if(hGsm->RespType == URC)
        {
          DataToSerial(hGsm,(tm_u8_t *)"URC RESP\r\n",(tm_u16_t)10);
        }
        if(hGsm->RespType == NONETYPE)
        {
          DataToSerial(hGsm,(tm_u8_t *)"NONE RESP\r\n",(tm_u16_t)11);
        }
#endif
        responsedetected=TM_TRUE;
        hGsm->RxResponseBufIndex=0;
        hGsm->bCRdetected=TM_FALSE;
        if(hGsm->ResponseState == WAITFOR_ENDLINEFEED)
        {
          /* If response state is not WAITFOR_ENDLINEFEED then it means the response han not ended yet */
          hGsm->ResponseState = WAITFOR_BEGINCR;
        }
      }
      else
      {
        hGsm->ResponseState = WAITFOR_BEGINCR;
      }
      break;
    case PROFILE0_READDATA:
      hGsm->Profie0_RxData.RxDataBuf[hGsm->Profie0_RxData.RxDataBufIndex++] = Byte;
      if(hGsm->Profie0_RxData.RxDataBufIndex == hGsm->Profile[PROFILE0].RxDataLen)
      {
        hGsm->ResponseState = WAITFOR_BEGINCR;
      }
      break;
    case PROFILE1_READDATA:
      hGsm->Profie1_RxData.RxDataBuf[hGsm->Profie1_RxData.RxDataBufIndex++] = Byte;
      if(hGsm->Profie1_RxData.RxDataBufIndex == hGsm->Profile[PROFILE1].RxDataLen)
      {
        hGsm->ResponseState = WAITFOR_BEGINCR;
      }
      break;
#if 0
    case PROCESS_DATA_SMI:
      ProcessDataSMI(hGsm,1,Byte);
      hGsm->ReadpktData.ActualReadDatalength++;
      if (hGsm->ReadpktData.ActualReadDatalength==hGsm->ReadpktData.ReadDatalength)
      {
        //InitResponseDet(hGsm);
        //UART0QueueData((tm_u8_t *)"exiting smi data\r\n",vts_strlen("exiting smi data\r\n"));
        hGsm->ResponseState = WAITFOR_BEGINCR;
        hGsm->ReadpktData.ReadDatalength=0;
        hGsm->ReadpktData.ProfileId=TM_TRUE;
        hGsm->ReadpktData.bCmdDetect=TM_FALSE;
      }
      break;
    case PROCESS_DATA_OTA:
      ProcessDataOTA(hGsm,1,Byte);
      hGsm->ReadpktData.ActualReadDatalength++;
      if (hGsm->ReadpktData.ActualReadDatalength==hGsm->ReadpktData.ReadDatalength)
      {
        //InitResponseDet(hGsm);
        hGsm->ResponseState = WAITFOR_BEGINCR;
        hGsm->ReadpktData.ReadDatalength=0;
        hGsm->ReadpktData.ProfileId=TM_TRUE;
        hGsm->ReadpktData.bCmdDetect=TM_FALSE;
      }
      break;
    case SMSMESSAGEGET:
      /* <CR><LF>+CMGR:<..><CR><LF><Message><CR><LF><CR><LF>OK<CR><LF> */
      if(hGsm->SmsParser.msgindex < 50)
        hGsm->SmsParser.message[hGsm->SmsParser.msgindex++] = Byte;
      else
      {
        /* Message too long. Reset index. This is Ok since none of the commands are that long and the
         * message is most like not a command */
        hGsm->SmsParser.msgindex = 0;
      }
      if(Byte == LINEFEED)
      {
        if(hGsm->SmsParser.msgindex == 2)
        {
          /* may be OK response */
          //hGsm->ResponseState = FINDOK;
          //hGsm->ResponseState = DET_RESPONSE;
          hGsm->ResponseState = WAITFOR_SECONDCR;

          hGsm->RxResponseBufIndex = 0;
        }
        else
        {
          /* end of text message */
          //hGsm->SmsParser.message[hGsm->SmsParser.msgindex++] = '\n';
          //parse(&hGsm->CmdParser, hGsm->SmsParser.message,&hGsm->SmsParser.msgindex,CMD_SMS);
          hGsm->SmsParser.message[hGsm->SmsParser.msgindex] = '\0';
          /* call parser */
          //hGsm->ResponseState = SMSCHECKNEXTMSG;
          //hGsm->ResponseState = DET_RESPONSE;
          hGsm->ResponseState = WAITFOR_SECONDCR;
          //hGsm->SmsParser.msgindex = 0;
          hGsm->SmsParser.lfcount = 0;
        }
      }
      break;
#endif
#if 0
    case SMSCHECKNEXTMSG:
      hGsm->RxResponseBufIndex = 0;
      hGsm->RxResponseBuf[hGsm->RxResponseBufIndex++] = Byte;
      if(Byte == '+')
      {
        /* more message */
        hGsm->ResponseState = DET_RESPONSE;
      }
      else
      {
        hGsm->ResponseState = FINDOK;
      }
      break;
    case FINDOK:
      hGsm->RxResponseBuf[hGsm->RxResponseBufIndex++] = Byte;
      if(Byte == LINEFEED)
      {
        hGsm->SmsParser.lfcount++;
        if(hGsm->SmsParser.lfcount == 2)
        {
          if(!vts_strncmp((const char *)hGsm->RxResponseBuf,"\r\nOK\r\n",vts_strlen("\r\nOK\r\n")))
          {
            responsedetected = TM_TRUE;
          }
          else
          {
            /* we must get the response right otherwise restart response detection*/
            hGsm->RxResponseBufIndex=0;
            hGsm->SmsParser.lfcount = 0;
          }
        }
      }
      break;
    case SMSSENDDATA:
      if(Byte == 0x1A)
      {
        hGsm->ResponseState = WAITFOR_SECONDCR;
      }
#endif
    default:
      break;
  }

  return responsedetected;

}

#if 0
tm_bool_t GsmWakeUp(tm_gsm_t *hGsm)
{
}

tm_bool_t GsmPowerDown(tm_gsm_t *hGsm)
{
}
#endif

void GsmSendData(tm_gsm_t *hGsm, tm_profilenum_t ProfileNum, tm_u8_t *pData, tm_s16_t Len)
{
  tm_profile_t *hProfile;

  hProfile = &hGsm->Profile[ProfileNum];

  /* Update task list */
  switch(ProfileNum)
  {
    case PROFILE0:
      if(hGsm->GsmTasks[PROFILE0_DATASEND].TimeStamp == -1)
      {
        hGsm->GsmTasks[PROFILE0_DATASEND].TimeStamp = hGsm->TimeStamp;
        hProfile->TxDataLen = Len;
        hProfile->pTxData = pData;
      }
      break;
    case PROFILE1:
      if(hGsm->GsmTasks[PROFILE1_DATASEND].TimeStamp == -1)
      {
        hGsm->GsmTasks[PROFILE1_DATASEND].TimeStamp = hGsm->TimeStamp;
        hProfile->TxDataLen = Len;
        hProfile->pTxData = pData;
      }
      break;
    case PROFILE2:
      if(hGsm->GsmTasks[PROFILE2_DATASEND].TimeStamp == -1)
        hGsm->GsmTasks[PROFILE2_DATASEND].TimeStamp = hGsm->TimeStamp;
      break;
  }
}

void GsmConnect(tm_gsm_t *hGsm, tm_profilenum_t ProfileNum)
{
  switch(ProfileNum)
  {
    case PROFILE0:
      if(hGsm->GsmTasks[PROFILE0_CONNECT].TimeStamp == -1)
      {
        hGsm->GsmTasks[PROFILE0_CONNECT].TimeStamp = hGsm->TimeStamp;
      }
      break;
    case PROFILE1:
      if(hGsm->GsmTasks[PROFILE1_CONNECT].TimeStamp == -1)
      {
        hGsm->GsmTasks[PROFILE1_CONNECT].TimeStamp = hGsm->TimeStamp;
      }
      break;
    case PROFILE2:
      if(hGsm->GsmTasks[PROFILE2_CONNECT].TimeStamp == -1)
        hGsm->GsmTasks[PROFILE2_CONNECT].TimeStamp = hGsm->TimeStamp;
      break;
  }
}

void GsmRestart(tm_gsm_t *hGsm)
{
  hGsm->GsmState = GSM_EMERGENCYRESET;
}


void IncGsmTimeStamp(tm_gsm_t *hGsm)
{
  hGsm->TimeStamp++;
}


void GSMUARTDataSend(tm_u8_t *pData, tm_s16_t Len)
{
  UART8QueueData(pData, Len);
}

static void CREGStatusGet(tm_gsm_t *hGsm,  char *str)
{
  tm_u8_t i=0;
  int Data;
  char *pChar;

  pChar= str;
  while(*str != '\0')
  {
    if (*str == ',')
    {
      *str = '\0';
      i++;
      switch(i)
      {
        case 2:
          Data = atoi(pChar);
          if ( Data == 1 || Data == 5 )
            hGsm->bCREGOk=TM_TRUE;
          break;
        default:
          break;
      }
      pChar = str+1;
    }
    if(*str == ' ')
      pChar = str + 1;
    str++;
  }
  if (*pChar != '\0')
  {
    Data = atoi(pChar);
    if (i == 1)
    {
      if ( Data==1 || Data==5 )
        hGsm->bCREGOk = TM_TRUE;
    }
  }
}

static void GetSISInfo(tm_gsm_t *hGsm,  char *str)
{
  int CommaCount;
  int len;
  char *pChar;
  tm_profilenum_t Profile = MAXPROFILENUM;
  tm_s16_t urcCause = 0;
  tm_s16_t urcInfoId = 0;

  len = strlen(str);
  pChar= str;
  //UART0QueueData((tm_u8_t *)str,(tm_u16_t)vts_strlen(str));
  CommaCount = 0;
  while(len)
  {
    len--;
    if((*str) == ' ')
      pChar = str + 1;
    if ((*str) == ',' || len == 0)
    {
      if(*str == ',')
        *str='\0';
      CommaCount++;
      switch (CommaCount)
      {
        case 1:
          Profile = (tm_profilenum_t)atoi(pChar);
          break;
        case 2:
          urcCause = atoi(pChar);
          break;
        case 3:
          urcInfoId = atoi(pChar);
          break;
      }
      if(*str == '\0')
        pChar = str + 1;
    }
    str++;
  }
  if(urcInfoId >= 1 && urcInfoId <= 2000)
  {
    /* Service error */
    switch(Profile)
    {
      case PROFILE0:
        hGsm->Profile[PROFILE0].ProfileStatus = PROFILE_DISCONNECTED;
        //hGsm->GsmLedStatus = GSM_LED_BLINK;
        break;
      case PROFILE1:
        hGsm->Profile[PROFILE1].ProfileStatus = PROFILE_DISCONNECTED;
        break;
      case PROFILE2:
        hGsm->Profile[PROFILE2].ProfileStatus = PROFILE_DISCONNECTED;
        break;
    }
  }
}

static tm_profilenum_t GetSISRInfo(tm_gsm_t *hGsm,  char *str, tm_bool_t bRespType)
{
  int CommaCount = 0;
  tm_profilenum_t Profile;
  int DataLen;
  char *pChar;
  int Len;

  pChar= str;
  Len = strlen(str);

  while(Len)
  {
    Len--;
    if (*str==',' || Len == 0)
    {
      if(*str == ',')
        *str='\0';
      CommaCount++;
      switch (CommaCount)
      {
        case 1:
          Profile = (tm_profilenum_t) atoi(pChar);
          break;
        case 2:
          DataLen = atoi(pChar);
          break;
      }
      pChar = str+ 1;
    }
    if(*str == ' ')
    {
      pChar = str + 1;
    }
    str++;
  }
  if(bRespType == URC && DataLen == 1)
  {
    /* If it is SISR URC add read task */
    switch(Profile)
    {
      case PROFILE0:
        if(hGsm->GsmTasks[PROFILE0_DATARECV].TimeStamp == -1)
        {
          hGsm->GsmTasks[PROFILE0_DATARECV].TimeStamp = hGsm->TimeStamp;
        }
        break;
      case PROFILE1:
        if(hGsm->GsmTasks[PROFILE1_DATARECV].TimeStamp == -1)
        {
          hGsm->GsmTasks[PROFILE1_DATARECV].TimeStamp = hGsm->TimeStamp;
        }
        break;
      case PROFILE2:
        break;
    }
  }
  else
  {
    /* Update RxData Len*/
    switch(Profile)
    {
      case PROFILE0:
        hGsm->Profile[PROFILE0].RxDataLen = DataLen;
        break;
      case PROFILE1:
        hGsm->Profile[PROFILE1].RxDataLen = DataLen;
        break;
      case PROFILE2:
        hGsm->Profile[PROFILE2].RxDataLen = DataLen;
        break;
    }
  }
  return Profile;
}

static void GetSISIInfo(tm_gsm_t *hGsm,  char *str)
{
  int CommaCount;
  int len;
  char *pChar;
  tm_profilenum_t Profile = MAXPROFILENUM;
  tm_s16_t srvState = 0;
  tm_s16_t unackDataLen = 0;

  len = strlen(str);
  pChar= str;
  //UART0QueueData((tm_u8_t *)str,(tm_u16_t)vts_strlen(str));
  CommaCount = 0;
  while(len)
  {
    len--;
    if((*str) == ' ')
      pChar = str + 1;
    if ((*str) == ',' || len == 0)
    {
      if(*str == ',')
        *str='\0';
      CommaCount++;
      switch (CommaCount)
      {
        case 1:
          Profile = (tm_profilenum_t)atoi(pChar);
          break;
        case 2:
          srvState = atoi(pChar);
          break;
        case 6:
          unackDataLen = atoi(pChar);
          break;
      }
      if(*str == '\0')
        pChar = str + 1;
    }
    str++;
  }
  if(Profile == PROFILE0)
  {
    hGsm->Profile[PROFILE0].UnackDataLen = unackDataLen;
  }
}

