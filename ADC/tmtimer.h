#ifndef __TM_TIMER_H__

#define __TM_TIMER_H__

#include "tmcommon.h"
#include "tmuart.h"

#define TEMMSECCOUNT 4
#define ONESECCOUNT  400

enum timer_state
{
   TMTIME_NOTCREATED = 0
  ,TMTIME_STARTED
  ,TMTIME_SUSPENDED
  ,TMTIME_EXPIRED
};

typedef enum timer_state tm_timerstate_t;

typedef void(*tm_callback_f)(void *);
struct timer
{
  void              *pUserData;
  tm_s32_t          ExpiryTime;
  tm_callback_f     fptr;
  tm_timerstate_t   TimerState;
  tm_bool_t         bCreated;
};

typedef struct timer tm_timer_t;

void Init10msecTimer();
void InitTimers(void);
tm_bool_t CreateTimer(tm_timer_t **hTimer, tm_s32_t ExpiryTime, tm_callback_f fptr, void *pUserData);
void ResetTimer(tm_timer_t *hTimer, int TimeCount);
void CheckTimers();
void SuspendTimer(tm_timer_t *hTimer);
tm_bool_t IsTimerExpired(tm_timer_t *hTimer);
tm_bool_t IsTimerRunning(tm_timer_t *hTimer);

#endif /* __TM_TIMER_H__ */
