#ifndef __PARSER_H__
#define __PARSER_H__

#include "tmcommon.h"
#include "tmtypes.h"
#include "_fr.h"
#include "ctype.h"
#include "tmuart.h"
#include "stdio.h"
#include "stdlib.h"
#include "tmcan\inc\tmcan.h"
#include "tmapp\inc\j1939.h"
#include "tmapp\inc\tmdbase.h"
#include "tmadc\inc\tmadc.h"
#include "tmflash\inc\tmflashtask.h"


#define NEWLINE '\n'
#define SPACE   ' '
#define TAB     '\t'
#define BACKSPACE 8

#define APN_SIZE          30
#define SERIPADDR_SIZE    20
#define SERVERNAME_SIZE   40
#define PORTNO_SIZE       8
#define DNS_SIZE          18

#define MAXWORDS 4
#define MAXWORDLEN 50
#define MAXCMDLEN 100
#define NULL 0

#define RESPONSEBUFLEN 512


enum cmdstatus
{
   CMD_OK=0
  ,CMD_ERROR
  ,CMDEMPTYLINE
  ,CMD_INPROGRESS
  ,CMDDONE
  ,GSMNOTREADY
  ,CMDTOOLONG
  ,CMDOUTOFRANGE
  ,CMDPWRDWNERR
  ,CMDFLASHDONE
  ,CMDOK_SMO
  ,SMO_END
  ,CMDOK_SERIALUPGRADE  /*Response when serial upgrade command is issued */
  ,CMD_REBOOT
};
typedef enum cmdstatus tm_cmdstatus_t;

struct tm_cmdparser
{
  tm_dbase_t *hSysDbase;
  tm_flashtask_t *hFlashTask;
  void  *papp;
  tm_u8_t        *pResponseBuf;
  tm_bool_t *hDinStatus;
  int   wordindex;
  int   wordlen;
  int   cmdstrindex;
  char  words[MAXWORDS][MAXWORDLEN];
  char  cmdstr[MAXCMDLEN+1];  /*+1 for the end of string character \0 */
};
typedef struct tm_cmdparser tm_cmdparser_t;

typedef tm_cmdstatus_t (*funcptr)(tm_cmdparser_t *,char *);

struct tm_wordstr
{
  char *cmdstr;
  struct tm_wordstr *next;
  funcptr fptr;
  int  param;
  tm_u16_t maxlen;
};
typedef struct tm_wordstr tm_wordstr_t;

enum parsercmd
{
   CMD_CNFGPORT=0
  ,CMD_GPRS
  ,CMD_SMS
  ,CMD_SERIALTOGPRSREAD
};
typedef enum parsercmd tm_parsercmd_t;



void cmdparserinit(tm_cmdparser_t *cmd, tm_dbase_t *hSysDbase, tm_dbase_t *hDbase2, tm_flashtask_t *hFlashTask, tm_bool_t *hDinStatus);
tm_cmdstatus_t parse(tm_cmdparser_t *cmd, tm_u8_t *str, tm_u16_t len,tm_u8_t *pResponseBuf);

#endif /* _PARSER_H_ */

