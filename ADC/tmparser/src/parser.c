
#include "tmparser\src\parser_private.h"

extern char debugstr[];

extern float SpdLmtMax;
extern float SpdLmtMin;
extern char SoftwareVer[];
extern tm_bool_t bStartOTA;


static tm_cmdstatus_t matchword(tm_cmdparser_t *cmd, tm_wordstr_t *ptr, int wordindex)
{
  int i;

  i = 0;
  while(ptr[i].cmdstr != NULL && strcmp(cmd->words[wordindex],ptr[i].cmdstr))
    i++;
  if(ptr[i].cmdstr == NULL)
  {
    //printf("command not found\n");
    return CMD_ERROR;
  }
  //printf("matched word %d string %s#\n",wordindex,ptr[i].cmdstr);
  //UART0QueueData(ptr[i].cmdstr,vts_strlen(ptr[i].cmdstr));
  //UART0QueueData("\r\n",2);

  if(ptr[i].next != NULL)
  {
    return matchword(cmd,ptr[i].next,++wordindex);
  }
  else
  {
    if(ptr[i].fptr != NULL)
    {
      if(ptr[i].param != 0)
      {
        /* if the parameter is missing return error */
        if(cmd->wordindex != ptr[i].param)
        {
          return CMD_ERROR;
        }
        wordindex++;
        if (ptr[i].maxlen < strlen(cmd->words[wordindex]))
          return CMDTOOLONG;
        //UART0QueueData(cmd->words[wordindex],vts_strlen(cmd->words[wordindex]));
        //UART0QueueData("calling function\r\n",vts_strlen("calling function\r\n"));
        return (*ptr[i].fptr)(cmd,cmd->words[wordindex]);
      }
      else
      {
        return (*ptr[i].fptr)(cmd,NULL);
      }
    }
    else
    {
      return CMD_OK;
    }
  }
}

static tm_cmdstatus_t findwords(tm_cmdparser_t *cmd)
{
  int i;
  char *str;
  tm_cmdstatus_t retval = CMDEMPTYLINE;

  i = 0;
  str = cmd->cmdstr;
  cmd->wordindex = 0;
  cmd->wordlen = 0;
  //DPRINTF("parsing string %s*\n",cmd->cmdstr);
  //Remove leading spaces and tabs
  while(str[i] == SPACE || str[i] == TAB)
    i++;
  if(str[i] == NEWLINE || str[i] == '\r' || str[i] == ':')
  {
    //DPRINTF("returning cmdprocess\n");
    return CMDEMPTYLINE;
  }
  do
  {
    while(str[i] != SPACE && str[i] != TAB && str[i] != NEWLINE && str[i] != '\r' && str[i] != ':')
    {
      if (cmd->wordindex<2)
        cmd->words[cmd->wordindex][cmd->wordlen++] = (char)toupper((int)str[i]);
      else
        cmd->words[cmd->wordindex][cmd->wordlen++] = str[i];
      //cmd->cmdstr[cmd->cmdstrindex++] = (char)toupper(str[i]);

      i++;
    }
    cmd->words[cmd->wordindex][cmd->wordlen] = '\0';
    //printf("found word %s\n",cmd->words[cmd->wordindex]);
    //UART0QueueData(cmd->words[cmd->wordindex],vts_strlen(cmd->words[cmd->wordindex]));
    cmd->wordindex++;
    if (cmd->wordindex>=MAXWORDS)
    {
      return retval;
    }
    cmd->wordlen = 0;
    /*Remove trailing space and tabs */
    while(str[i] == SPACE || str[i] == TAB || str[i] == NEWLINE || str[i] == '\r' || str[i] == ':')
      i++;
  } while(str[i] != '\0');

  retval = matchword(cmd,ws_1,0);
  return retval;
}

//void cmdparserinit(tm_cmdparser_t *cmd, tm_sysdbase_t *hSysDbase, tm_dbase2_t *hDbase2, tm_flashtask_t *hFlashTask,tm_iostatus_t* pIOStatus)
void cmdparserinit(tm_cmdparser_t *cmd, tm_dbase_t *hSysDbase, tm_dbase_t *hDbase2, tm_flashtask_t *hFlashTask, tm_u8_t *hDinStatus)
{
  cmd->cmdstrindex = 0;
  cmd->hSysDbase = hSysDbase;
  //cmd->hDbase2 = NULL;
  cmd->hFlashTask = hFlashTask;
  cmd->hDinStatus = hDinStatus;
}

void cmdparserreinit(tm_cmdparser_t *cmd)
{
  cmd->cmdstrindex = 0;
}

tm_cmdstatus_t parse(tm_cmdparser_t *cmd, tm_u8_t *str, tm_u16_t len,tm_u8_t *pResponseBuf)
{
  int i=0;
  tm_u8_t ch=SPACE;
  char EndMarker=NEWLINE;
  tm_cmdstatus_t retval = CMD_INPROGRESS;

  cmd->pResponseBuf = pResponseBuf;

  i = 0;
  for(i=0; i<len;i++)
  {
    if (str[i]==BACKSPACE)
    {
      if (cmd->cmdstrindex)
        cmd->cmdstrindex--;
    }
    else
    {
      cmd->cmdstr[cmd->cmdstrindex++] = str[i];
    }
    //Srinivas: Fix this if (Source==CMD_CNFGPORT)
    {
      if (str[i]==BACKSPACE)
      {
        UART1QueueData(&ch,1);
        UART1QueueData(&str[i],1);
      }
    }

    if(str[i] == EndMarker || str[i] == ':')
    {
      cmd->cmdstr[cmd->cmdstrindex] = '\0';
      /* remove tabs and multiple spaces */
      if (cmd->cmdstrindex > 1)
      {
        retval = findwords(cmd);
      }
      //resetcmdstruct(cmd);
      cmd->cmdstrindex = 0;
      /* find and compare words */
      /* call set */
    }
    if(cmd->cmdstrindex >= MAXCMDLEN)
      cmd->cmdstrindex = 0;
  }
  //len = 0;
  return retval;
}

static tm_cmdstatus_t validateipaddr(char *str)
{
  int count;
  int bytecount = 0;
  int num = 0;
  char ch;

  count = 0;
  while((ch = *str++) != '\0')
  {
    if(isdigit((int)ch))
    {
      count++;
      num=num*10;
      num=num+(ch - '0');
      /* each byte should be less than or equal to 255.*/
      if(num > 255)
      {
        return CMD_ERROR;
      }
    }
    else if (ch == '.')
    {
      if(count == 0)
      {
        /* Found '.' without any digits before that */
        return CMD_ERROR;
      }
      count = 0;
      num = 0;
      bytecount++;
      if(bytecount > 3)
      {
        //DPRINTF("more than 4 bytes\n");
        return CMD_ERROR;
      }
    }
    else
    {
      //DPRINTF("recieved char %c\n",ch);
      return CMD_ERROR;
    }
  }
  if(count == 0)
  {
    /* No digits after the last '.'  */
    return CMD_ERROR;
  }
  if(bytecount != 3)
    return CMD_ERROR;
  else
  return CMD_OK;
}


/* Set Profile0 IP address */
static  tm_cmdstatus_t SetServerIP0(tm_cmdparser_t *cmd, char *str)
{
  tm_cmdstatus_t retval;

  retval = validateipaddr(str);
  if(retval == 0)
  {
    strcpy(cmd->hSysDbase->PrimaryIP, str);
    retval = CMD_OK;
  }
  else
  {
    retval = CMD_ERROR;
  }
  return retval;
}


/* Set profile 0 port */
static  tm_cmdstatus_t SetServerPort0(tm_cmdparser_t *cmd, char *str)
{
  int Len;
  int i;
  char *ptr;

  Len = strlen(str);
  ptr = str;
  i = 0;

  while(Len--)
  {
    if(!isdigit((int) ptr[i]))
    {
      /* If not a digit error*/
      return CMD_ERROR;
    }
    i++;
  }
  i = atoi(str);
  /* Negative values will be caught in the isdigit check */
  if(i > 65535)
  {
    return CMD_ERROR;
  }
  strcpy(cmd->hSysDbase->PrimaryPort, str);
  return CMD_OK;
}

static  tm_cmdstatus_t SetAPN(tm_cmdparser_t *cmd, char *str)
{
  if(strlen(str) < MAXSIZE_APN);
  strcpy(cmd->hSysDbase->APN, str);
  return CMD_OK;
}

static  tm_cmdstatus_t SetCANRxFilter(tm_cmdparser_t *cmd, char *str)
{
  char *pchar;
  tm_u32_t id;
  tm_u32_t mask;
  int Len;
  int CommaCount;

  pchar = str;
  Len = strlen(str);

  CommaCount = 0;
  while(Len)
  {
    Len--;
    if(*str == ',' || Len == 0)
    {
      if(*str == ',')
        *str = '\0';
      CommaCount++;
      if(CommaCount == 1)
      {
        sscanf(pchar,"%x",&id);
      }
      if(CommaCount == 2)
      {
        sscanf(pchar,"%x",&mask);
      }
      pchar = str + 1;
    }
    str++;
  }
  CANRxFilterSet(id,mask,2);

  return CMD_OK;
}

static  tm_cmdstatus_t CANTxData(tm_cmdparser_t *cmd, char *str)
{
  can_t txcandata;
  char *pchar;
  tm_u32_t id;
  tm_u32_t data;
  int Len;
  int CommaCount;

  pchar = str;
  Len = strlen(str);

  CommaCount = 0;
  while(Len)
  {
    Len--;
    if(*str == ',' || Len == 0)
    {
      if(*str == ',')
        *str = '\0';
      CommaCount++;
      if(CommaCount == 1)
      {
        sscanf(pchar,"%x",&id);
        txcandata.id = id;
      }
      if(CommaCount > 1 )
      {
        sscanf(pchar,"%d",&data);
        txcandata.buf[CommaCount-2] = data;
      }
      pchar = str + 1;
    }
    str++;
  }
  txcandata.buf_len = CommaCount - 1;
  if(can_tx(&txcandata) == TM_TRUE)
    return CMD_ERROR;
  else
    return CMD_OK;
}

extern int USE_J1939;
static tm_cmdstatus_t SetJ1939(tm_cmdparser_t *cmd, char *str)
{
  if(strcmp(str,"ENABLE") == 0)
  {
    USE_J1939 = TM_TRUE;
    j1939_init();
    return CMD_OK;
  }
  else if(strcmp(str,"DISABLE") == 0)
  {
    USE_J1939 = TM_FALSE;
    return CMD_OK;
  }
  else
  {
    USE_J1939 = TM_FALSE;
    return CMD_ERROR;

  }
}

static tm_cmdstatus_t SetCANBitRate(tm_cmdparser_t *cmd, char *str)
{
  if(strlen(str) != 1)
    return CMD_ERROR;
  if(*str == '0')
  {
    init_CAN_0(0);
    return CMD_OK;
  }
  else if(*str == '1')
  {
    init_CAN_0(1);
    return CMD_OK;
  }
  else
  {
    init_CAN_0(0);
    return CMD_ERROR;
  }
}

static tm_cmdstatus_t SetDOp1(tm_cmdparser_t *cmd, char *str)
{
  if(strcmp(str,"HIGH") == 0)
  {
    IO_PDR04.byte |= 0x08;
    return CMD_OK;
  }
  else if(strcmp(str,"LOW") == 0)
  {
    IO_PDR04.byte &= (~0x08);
    return CMD_OK;
  }
  else
  {
    return CMD_ERROR;
  }
}

static tm_cmdstatus_t SetDOp2(tm_cmdparser_t *cmd, char *str)
{
  return CMD_ERROR;
}

static tm_cmdstatus_t ResetDevice(tm_cmdparser_t *cmd, char *str)
{
  Vectors_InitIrqLevels();
  __asm("   LDI #1,R0");
  __asm("   LDI:32 #_IO_RSTCR,R12");
  __asm("   STB R0, @R12");
  __asm("   LDUB @R12, R0");
  __asm("   MOV R0,R0");
  __asm("   NOP");
  __asm("   NOP");
  return CMD_OK;
}

static tm_cmdstatus_t SetSpeedLmt(tm_cmdparser_t *cmd, char *str)
{
  char *pChar;
  int Len;
  int CommaCount;
  float SpeedMax;
  float SpeedMin;


  pChar = str;
  Len = strlen(str);

  CommaCount = 0;
  while(Len)
  {
    Len--;
    if(*str == ',' || Len == 0)
    {
      if(*str == ',')
        *str = '\0';
      CommaCount++;
      if(CommaCount == 1)
      {
        SpeedMin = atof(pChar);
      }
      if(CommaCount == 2 )
      {
        SpeedMax = atof(pChar);
      }
      pChar = str + 1;
    }
    str++;
  }
  if(CommaCount != 2)
    return CMD_ERROR;
  else
  {
    SpdLmtMax = SpeedMax;
    SpdLmtMin = SpeedMin;
    return CMD_OK;
  }
}


static tm_cmdstatus_t ShowConfig(tm_cmdparser_t *cmd, char *str)
{
  cmd->pResponseBuf[0] = '\0';
  strcat((char *)cmd->pResponseBuf, "\r");

  strcat((char *)cmd->pResponseBuf, "DevID: ");
  strcat((char *)cmd->pResponseBuf, cmd->hSysDbase->DevID);
  strcat((char *)cmd->pResponseBuf, "\r");

  strcat((char *)cmd->pResponseBuf, "LIN: ");
  strcat((char *)cmd->pResponseBuf, cmd->hSysDbase->LIN);
  strcat((char *)cmd->pResponseBuf, "\r");

  strcat((char *)cmd->pResponseBuf, "IP: ");
  strcat((char *)cmd->pResponseBuf, cmd->hSysDbase->PrimaryIP);
  strcat((char *)cmd->pResponseBuf, "\r");

  strcat((char *)cmd->pResponseBuf, "Port: ");
  strcat((char *)cmd->pResponseBuf, cmd->hSysDbase->PrimaryPort);
  strcat((char *)cmd->pResponseBuf, "\r");

  strcat((char *)cmd->pResponseBuf, "OTA IP: ");
  strcat((char *)cmd->pResponseBuf, cmd->hSysDbase->OTAIP);
  strcat((char *)cmd->pResponseBuf, "\r");

  strcat((char *)cmd->pResponseBuf, "OTA Port: ");
  strcat((char *)cmd->pResponseBuf, cmd->hSysDbase->OTAPort);
  strcat((char *)cmd->pResponseBuf, "\r");

  strcat((char *)cmd->pResponseBuf, "APN: ");
  strcat((char *)cmd->pResponseBuf, cmd->hSysDbase->APN);
  strcat((char *)cmd->pResponseBuf, "\r");

  strcat((char *)cmd->pResponseBuf, "UpdateRate: ");
  sprintf(debugstr,"%d",cmd->hSysDbase->UpdateRate);
  strcat((char *)cmd->pResponseBuf, debugstr);
  strcat((char *)cmd->pResponseBuf, "\r\n");

  strcat((char *)cmd->pResponseBuf, "SoftwareVer: ");
  strcat((char *)cmd->pResponseBuf, SoftwareVer);
  strcat((char *)cmd->pResponseBuf, "\r\n");

  return CMD_OK;
}


static tm_cmdstatus_t FactorySet(tm_cmdparser_t *cmd, char *str)
{
  if(strcmp(str,"DEFAULT") != 0)
  {
    return CMD_ERROR;
  }

  strcpy(cmd->hSysDbase->PrimaryIP, "122.166.212.137");
  strcpy(cmd->hSysDbase->PrimaryPort, "9020");
  strcpy(cmd->hSysDbase->OTAIP, "122.166.212.137");
  strcpy(cmd->hSysDbase->OTAPort, "8021");
  strcpy(cmd->hSysDbase->APN, "AIRTELGPRS.COM");
  cmd->hSysDbase->UpdateRate = 10;
  cmd->hSysDbase->PwdUpdateRate = 600;
  cmd->hSysDbase->DebugLevel = 0;
  return SaveConfig(cmd, NULL);
}

static tm_cmdstatus_t SaveConfig(tm_cmdparser_t *cmd, char *str)
{
  DPRINTF("save config called\r\n");
  if(FlashTaskStateGet(cmd->hFlashTask) != FLASHTASK_IDLE)
  {
    return CMD_ERROR;
  }
  else
  {
    FlashTaskEraseAndWrite(cmd->hFlashTask, CONFIG_STARTADDR, sizeof(tm_dbase_t), (tm_u8_t *)cmd->hSysDbase );
    return CMD_OK;
  }
}

static tm_cmdstatus_t SerialUpgradeStart(tm_cmdparser_t *cmd, char *str)
{
  DPRINTF("firmware upgrade start\r\n");
  return CMDOK_SERIALUPGRADE;
}

static tm_cmdstatus_t StartOTA(tm_cmdparser_t *cmd, char *str)
{
  bStartOTA = TM_TRUE;
  return CMD_OK;
}

static tm_cmdstatus_t SetDevID(tm_cmdparser_t *cmd, char *str)
{
  if(strlen(str) > MAXSIZE_DEVID)
  {
    return CMD_ERROR;
  }
  else
  {
    strcpy(cmd->hSysDbase->DevID,str);
    return CMD_OK;
  }
}

static tm_cmdstatus_t SetLIN(tm_cmdparser_t *cmd, char *str)
{
  if(strlen(str) > MAXSIZE_DEVID)
  {
    return CMD_ERROR;
  }
  else
  {
    strcpy(cmd->hSysDbase->LIN,str);
    return CMD_OK;
  }
}

/* Set Profile0 IP address */
static  tm_cmdstatus_t SetOTAIP(tm_cmdparser_t *cmd, char *str)
{
  tm_cmdstatus_t retval;

  retval = validateipaddr(str);
  if(retval == 0)
  {
    strcpy(cmd->hSysDbase->OTAIP, str);
    retval = CMD_OK;
  }
  else
  {
    retval = CMD_ERROR;
  }
  return retval;
}


/* Set profile 0 port */
static  tm_cmdstatus_t SetOTAPort(tm_cmdparser_t *cmd, char *str)
{
  int Len;
  int i;
  char *ptr;

  Len = strlen(str);
  ptr = str;
  i = 0;

  while(Len--)
  {
    if(!isdigit((int) ptr[i]))
    {
      /* If not a digit error*/
      return CMD_ERROR;
    }
    i++;
  }
  i = atoi(str);
  /* Negative values will be caught in the isdigit check */
  if(i > 65535)
  {
    return CMD_ERROR;
  }
  strcpy(cmd->hSysDbase->OTAPort, str);
  return CMD_OK;
}

static  tm_cmdstatus_t SetDebugLevel(tm_cmdparser_t *cmd, char *str)
{
  int debuglevel;

  debuglevel = atoi(str);
  if(debuglevel < DEBUGLEVEL_0 && debuglevel > DEBUGLEVEL_3)
  {
    return CMD_ERROR;
  }
  cmd->hSysDbase->DebugLevel = debuglevel;
  return CMD_OK;
}

static tm_cmdstatus_t ShowIOStatus(tm_cmdparser_t *cmd, char *str)
{
  if (cmd->hDinStatus[MAINPWR] == TM_TRUE)
    strcpy((char *)cmd->pResponseBuf, "PWR:1");
  else
    strcpy((char *)cmd->pResponseBuf, "PWR:0");

  if (cmd->hDinStatus[IGN] == TM_TRUE)
    strcat((char *)cmd->pResponseBuf, ",IGN:1");
  else
    strcat((char *)cmd->pResponseBuf, ",IGN:0");

  if (cmd->hDinStatus[DIN1] == TM_TRUE)
    strcat((char *)cmd->pResponseBuf, ",IN1:1");
  else
    strcat((char *)cmd->pResponseBuf, ",IN1:0");

  if (cmd->hDinStatus[DIN2] == TM_TRUE)
    strcat((char *)cmd->pResponseBuf, ",IN2:1");
  else
    strcat((char *)cmd->pResponseBuf, ",IN2:0");

  if (cmd->hDinStatus[DIN3] == TM_TRUE)
    strcat((char *)cmd->pResponseBuf, ",IN3:1");
  else
    strcat((char *)cmd->pResponseBuf, ",IN3:0");

  if (cmd->hDinStatus[DIN4] == TM_TRUE)
    strcat((char *)cmd->pResponseBuf, ",IN4:1");
  else
    strcat((char *)cmd->pResponseBuf, ",IN4:0");

  if (cmd->hDinStatus[DIN5] == TM_TRUE)
    strcat((char *)cmd->pResponseBuf, ",IN5:1");
  else
    strcat((char *)cmd->pResponseBuf, ",IN5:0");

  return CMD_OK;
}

static tm_cmdstatus_t ClearLog(tm_cmdparser_t *cmd, char *str)
{
  if(FlashTaskStateGet(cmd->hFlashTask) != FLASHTASK_IDLE)
  {
    return CMD_ERROR;
  }
  else
  {
    FlashTaskLogDataErase(cmd->hFlashTask);
    return CMD_OK;
  }
}

static tm_cmdstatus_t SetUpdateRate(tm_cmdparser_t *cmd, char *str)
{
  int updaterate;

  updaterate = atoi(str);
  if(updaterate < UPDATERATE_MIN && updaterate > UPDATERATE_MAX)
  {
    return CMD_ERROR;
  }
  else
  {
    cmd->hSysDbase->UpdateRate = updaterate;
    return CMD_OK;
  }
}
