#ifndef __PARSER_PRIVATE_H__
#define __PARSER_PRIVATE_H__

#include "tmparser\inc\parser.h"

#define UPDATERATE_MAX         50000
#define UPDATERATE_MIN         5
#define UPDATERATE_DEFAULT     30

#define MINTIMOFFSET           -24
#define MAXTIMOFFSET           24

#define NOOFPOSTOBESENT_BEFORE_PWDN_DEFAULT     1
#define NOOFPOSTOBESENT_BEFORE_PWDN_MAX         60
#define NOOFPOSTOBESENT_BEFORE_PWDN_MIN         1
#define WAKEUPTIME_AFTER_PWDN_DEFAULT           10
#define WAKEUPTIME_AFTER_PWDN_MAX               300
#define WAKEUPTIME_AFTER_PWDN_MIN               3

#define DISTANCE_TRACK_MIN      50
#define DISTANCE_TRACK_DEFAULT  70
#define DISTANCE_TRACK_MAX      200

#define ANGLE_TRACK_MIN      5
#define ANGLE_TRACK_DEFAULT  10
#define ANGLE_TRACK_MAX      20

#define VEHICLEONTIME_MAX         300
#define VEHICLEONTIME_MIN         5
#define VEHICLEONTIME_DEFAULT     120

#define MINACC_THRESHOLD2G   100
#define MAXACC_THRESHOLD2G   2000

#define SERVERIP_SIZE 16

static tm_cmdstatus_t matchword(tm_cmdparser_t *cmd, tm_wordstr_t *ptr, int wordindex);
static tm_cmdstatus_t findwords(tm_cmdparser_t *cmd);

static tm_cmdstatus_t validateipaddr(char *str);
static  tm_cmdstatus_t SetServerIP0(tm_cmdparser_t *cmd, char *str);
static  tm_cmdstatus_t SetServerPort0(tm_cmdparser_t *cmd, char *str);
static  tm_cmdstatus_t SetAPN(tm_cmdparser_t *cmd, char *str);
static  tm_cmdstatus_t SetCANRxFilter(tm_cmdparser_t *cmd, char *str);
static  tm_cmdstatus_t CANTxData(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t SetJ1939(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t SetCANBitRate(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t SetDOp1(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t SetDOp2(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t ResetDevice(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t SetSpeedLmt(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t ShowConfig(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t SaveConfig(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t SerialUpgradeStart(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t StartOTA(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t SetDevID(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t SetLIN(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t SetOTAIP(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t SetOTAPort(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t SetDebugLevel(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t FactorySet(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t ShowIOStatus(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t ClearLog(tm_cmdparser_t *cmd, char *str);
static tm_cmdstatus_t SetUpdateRate(tm_cmdparser_t *cmd, char *str);

static tm_wordstr_t ws_2_set[] =
{
   {"PRIMARYIP",NULL,SetServerIP0,3,SERVERIP_SIZE}
  ,{"PRIMARYPORT",NULL,SetServerPort0,3,SERVERIP_SIZE}
  ,{"APN",NULL,SetAPN,3,10}
  ,{"CANRXFILT",NULL,SetCANRxFilter,3,20}
  ,{"CANTXREQ",NULL,CANTxData,3,20}
  ,{"CANBITRATE",NULL,SetCANBitRate,3,2}
  ,{"J1939",NULL,SetJ1939,3,7}
  ,{"DOP1",NULL,SetDOp1,3,7}
  ,{"DOP2",NULL,SetDOp2,3,7}
  ,{"SPEEDLMT",NULL,SetSpeedLmt,3,12}
  ,{"DEVID",NULL,SetDevID,3,20}
  ,{"LIN",NULL,SetLIN,3,20}
  ,{"OTAIP",NULL,SetOTAIP,3,20}
  ,{"OTAPORT",NULL,SetOTAPort,3,20}
  ,{"DEBUGLEVEL",NULL,SetDebugLevel,3,2}
  ,{"FACTORY",NULL,FactorySet,3,7}
  ,{"UPDATERATE",NULL,SetUpdateRate,3,4}
  ,{NULL, NULL, NULL, 0,0}
};

static tm_wordstr_t ws_2_show[] =
{
   {"CONFIG",NULL,ShowConfig,0,0}
  ,{"IOSTATUS",NULL,ShowIOStatus,0,0}
  ,{NULL, NULL, NULL, 0,0}
};

static tm_wordstr_t ws_2_save[] =
{
   {"CONFIG",NULL,SaveConfig,0,0}
  ,{NULL, NULL, NULL, 0,0}
};

static tm_wordstr_t ws_ugd[] =
{
  {"UPGRADE",NULL,SerialUpgradeStart,0,0},
  {NULL, NULL, NULL, 0,0}
};

static tm_wordstr_t ws_ota[] =
{
  {"OTA",NULL,StartOTA,0,0},
  {NULL, NULL, NULL, 0,0}
};

static tm_wordstr_t ws_log[] =
{
  {"LOG",NULL,ClearLog,0,0},
  {NULL, NULL, NULL, 0,0}
};

static tm_wordstr_t ws_1[] =
{
  {"SET",ws_2_set,NULL,0,0},
  {"RESET",NULL,ResetDevice,0,0},
  {"SHOW",ws_2_show,NULL,0,0},
  {"SAVE",ws_2_save,NULL,0,0},
  {"FIRMWARE",ws_ugd,NULL,0,0},
  {"START",ws_ota,NULL,0,0},
  {"CLEAR",ws_log,NULL,0,0},
  {NULL, NULL, NULL, 0,0}
};

static tm_cmdstatus_t validateipaddr(char *str);
static tm_cmdstatus_t SetUpdateRate(tm_cmdparser_t *cmd, char *str);

#endif /* _PARSER_PRIVATE_H_ */
