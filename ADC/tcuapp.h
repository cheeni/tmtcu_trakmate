
#ifndef __TM_TCUAPP_H__
#define __TM_TCUAPP_H__

#include <gsmctrl\inc\gsmctrl.h>
#include "tmparser\inc\parser.h"
#include "tmcan\inc\tmcan.h"
#include "tmapp\inc\j1939.h"
#include "tmapp\inc\j1939tp.h"
#include "tmapp\inc\tmpacket.h"
#include "tmgps\inc\tmgps.h"
#include "tmadc\inc\tmadc.h"
#include "tmapp\inc\tmdbase.h"
#include "tmapp\inc\tmpgn.h"
#include "tmflash\inc\tmflashtask.h"
#include "tmapp\inc\tmserialugd.h"

//#define PRINTCANDATA 1

#define PACKETQUEUETHRESHOLD 4
enum tm_events
{
   EVENT_RX_CONFIG
  ,EVENT_TX_CONFIG
  ,EVENT_RX_GSM
  ,EVENT_TX_GSM
  ,EVENT_RX_GPS
  ,EVENT_TX_GPS
  ,EVENT_10MSEC
  ,EVENT_GSMCTRL
  ,EVENT_ONESEC
  ,EVENT_FLASH_RX
  ,EVENT_ANALOGIN
  ,EVENT_RX_UART3
  ,EVENT_TX_UART3
  ,EVENT_RX_ACCELERO
  ,EVENT_TX_ACCELERO
  ,EVENT_CANRX
  ,EVENT_CANTX
  ,EVENT_ADC
  ,EVENT_DMA
};

enum profile0state
{
   P0_IDLE
  ,P0_WAITFORCONNECTION
  ,P0_CONNECTED
  ,P0_WAITFORACK
};
typedef enum profile0state tm_p0state_t;

enum profile1status
{
   WAITFOROTACOMMAND
  ,WAITFOROTACOMPLETE
  ,WAITINITOTA
  ,WAITFORCRC
  ,WAITFORVERSEND
  ,WAITFORPGMSTART
  ,GETBIN
};
typedef enum profile1status tm_p1status_t;

struct profile0
{
  tm_timer_t *hPeriodicPacketTimer;
  tm_p0state_t P0State;
  tm_packet_t   PacketQueue[PACKETQUEUELEN];
  tm_packet_t   FlashQueue[FLASHPACKETQUEUELEN];
  tm_packet_t   ConstructPacket;
  tm_packet_t   NewPacket;
  tm_packet_t   LoggedPacket;
  tm_packet_t   SendPacket;
  tm_gpsdata_t  GpsData;
  tm_profilestatus_t GsmProfile0Status;
  tm_s16_t      SeqNum;
  tm_s8_t     Profile0Data[MAXPROFILE0DATALEN];
  tm_u8_t   PacketWrIndex;
  tm_u8_t   PacketRdIndex;
  tm_u8_t   FlashPacketWrIndex;
  tm_u8_t   FlashPacketRdIndex;
  tm_bool_t bDataSent;
  tm_bool_t bDataAck;
  tm_bool_t bTCPAck;
  tm_bool_t bNewPacket;
  tm_bool_t bGsmConnected;
};
typedef struct profile0 tm_profile0_t;

struct profile1
{
  tm_timer_t *hOTATimer;
  tm_p1status_t P1Status;
  tm_s8_t     Profile1Data[MAXPROFILE1DATALEN];
  tm_s8_t     Profile1RxData[MAXPROFILE1DATALEN];
  tm_u8_t   PacketWrIndex;
  tm_u8_t   PacketRdIndex;
  tm_bool_t bDataSent;
  tm_bool_t bDataAck;
  tm_bool_t bTCPAck;
  tm_bool_t bDataReceived;
};
typedef struct profile1 tm_profile1_t;

enum configportstate
{
   COMMAND_STATE
  ,SMO_STATE
  ,SERIALUGD_STATE
};
typedef enum configportstate tm_cfgportstate_t;


enum resetstate
{
   WAITFORRESETCMD
  ,RESETSYSTEM
};
typedef enum resetstate tm_resettate_t;

struct App
{
  tm_gsm_t Gsm;
  tm_cmdparser_t Parser;
  tm_profile0_t Profile0;
  tm_profile1_t Profile1;
  tm_gps_t    Gps;
  tm_dbase_t  DBase;
  can_t       RxCANData[CAN_RXBUFLEN];
  j1939_t     TxMsg;
  tm_flashtask_t  FlashTask;
  tm_serialugd_t SerialUgd;
  tm_serialugd_t OTAUgd;
  tm_cfgportstate_t CfgPortState;
  tm_timer_t  *hSerialUgdTimeout;
  tm_timer_t  *hOTAUgdTimeout;
  tm_gsmcallback_t pGsmCallBack;
  tm_resettate_t ResetState;
  float       AnalogVal[MAXANALOGVAL];
  tm_u16_t    PowerUpUpdateRate;
  tm_u16_t    CANTimerCount;
  tm_bool_t   DinStatus[MAXDIN];
  tm_u8_t     ResponseBuf[64];
  tm_u8_t     CanRxLen;
  tm_u8_t     CANPGNIndex;
  tm_bool_t   bPeriodicPacketSent;
  tm_bool_t   bHandleUGD;
  tm_bool_t   bReset;
};
typedef struct App tm_app_t;
typedef struct App *tm_happ_t;

void MainProcessInit(tm_app_t *hTMApp);
void MainProcess(tm_app_t *hTMApp);
void MakePacket(tm_app_t *hTMApp, tm_packet_t *hPacket, int *Len);
void GetCANData(tm_app_t *hTMApp);
void GetDigitalInputStatus(tm_app_t *hTMApp);
void GetAnalogVal(tm_app_t *hTMApp);
void HandleConfigPortData(tm_app_t *hApp, tm_u8_t *pData, tm_u16_t Len);
static void SerialTimerExpired(void *pUserData);
void WaitForFlashTask(tm_app_t *hApp);
void SanityCheck(tm_app_t *hTMApp);

#endif /* __TM_TCUAPP_H__ */
