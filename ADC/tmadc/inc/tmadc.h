#ifndef __TMADC_H__
#define __TMADC_H__

#include "_fr.h"
#include "extern.h"
#include "stdio.h"
#include "tmuart.h"
#include "tmcommon.h"
#include "tmtypes.h"

enum digitalinputs
{
   DIN1
  ,DIN2
  ,DIN3
  ,DIN4
  ,DIN5
  ,MAINPWR
  ,IGN
  ,MAXDIN
};

enum adcinputs
{
   ADC_DIN_4 = 0
  ,ADC_DIN_5
  ,ADC_DIN_3
  ,ADC_DIN_2
  ,ADC_DIN_1
  ,ADC_AIN3
  ,ADC_AIN2
  ,ADC_AIN1
  ,ADC_CARBATT
  ,ADC_INTBATT
  ,NBROFANALOGCHANNELS
};

enum analogval
{
   AIN1
  ,AIN2
  ,AIN3
  ,CARBAT
  ,INTBAT
  ,MAXANALOGVAL
};

#define MASK_STATE 0xF800
#define MASK_ONECOUNT 0xFC00

struct iostatus
{
  tm_u8_t    Status;
  tm_u16_t    Count;
};
typedef struct iostatus tm_iostatus_t;

struct diglitch
{
  tm_u16_t state_high;
  tm_u16_t state_low;
  tm_bool_t high_detected;
};
typedef struct diglitch tm_diglitch_t;

struct adc
{
  tm_s16_t AnalogInput[NBROFANALOGCHANNELS];
  float    f_AnalogInput[NBROFANALOGCHANNELS];
  tm_bool_t DinStatus[MAXDIN];
  tm_diglitch_t Diglitch[MAXDIN];
};
typedef struct adc tm_adc_t;

struct adcdin
{
  float adc_din1;
  float adc_din2;
  float adc_din3;
  float adc_din4;
  float adc_din5;
};
typedef struct adcdin tm_adcdin_t;


void initADC(void);
void AdcSamplingTrigger(void);
void UpdateDigitalInputs(tm_bool_t *hDin);
void PrintAdcVal(void);
void UpdateDigitalInputs(tm_bool_t *hDinStatus);
void GetAnalogIn(float *AnalogVal);

#endif /* __TMADC_H__ */
