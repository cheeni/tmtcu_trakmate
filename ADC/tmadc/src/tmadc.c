#include "tmadc\src\tmadc_private.h"

extern int adcevent;
extern char debugstr[];

tm_adc_t Adc;

void initADC(void)
{
  tm_u8_t byte;

  /* Enable AN15 - AN12 */
  KEYCDR = 0x24AE;
  KEYCDR = 0x64AE;
  KEYCDR = 0xA4AE;
  KEYCDR = 0xE4AE;
  IO_ADERL0.hword = 0xF000;

  /* Enable AN19 - AN16 */
  KEYCDR = 0x24AC;
  KEYCDR = 0x64AC;
  KEYCDR = 0xA4AC;
  KEYCDR = 0xE4AC;
  IO_ADERH0.hword = 0x100F;

  /* Enable AN19 - AN16 */
  KEYCDR = 0x24B2;
  KEYCDR = 0x64B2;
  KEYCDR = 0xA4B2;
  KEYCDR = 0xE4B2;
  IO_ADERL1.hword = 0x4000;

  IO_ICR[32].byte = 0x10;   /* A/D interrupt level set */

  /* compare time 1750ns */
  AD0_ADMD_CT = 0x00;
  AD1_ADMD_CT = 0x00;
  /* sampling time 750ns */
  AD0_ADMD_ST = 0x00;
  AD1_ADMD_ST = 0x00;

  //  AD0_ADTSE = 0x00000001;   /* AN0 soft trigger valid */
  AD0_ADTSE = 0x100FF000;   /* AN31 soft trigger valid */
  AD1_ADTSE = 0x00004000;   /* AN31 soft trigger valid */

  /* Enable interrupt for only one channel and use the same interrupt to readd
   * all other channels */
  //AD0_ADTCS12 = 0x2000;   /* INT Valid,software start ADC31 */
  AD0_ADTCS13 = 0x2000;   /* INT Valid,software start ADC31 */

  //  AD0_ADTECS0 = 0x0000;/* AD0 */
  AD0_ADTECS12 = 0x000C;/* AD12 */
  AD0_ADTECS13 = 0x000D;/* AD13 */
  AD0_ADTECS14 = 0x000E;/* AD14 */
  AD0_ADTECS15 = 0x000F;/* AD15 */
  AD0_ADTECS16 = 0x0010;/* AD16 */
  AD0_ADTECS17 = 0x0011;/* AD17 */
  AD0_ADTECS18 = 0x0012;/* AD18 */
  AD0_ADTECS19 = 0x0013;/* AD19 */
  AD0_ADTECS28 = 0x001C;/* AD19 */
  AD1_ADTECS14 = 0x000E;/* AD19 */

  /* Start conversion in 10 msec interrupt */
  //AD0_ADTSS_START = 0x1;  /* AN0 set A/D control start */

  for(byte = 0; byte < MAXDIN; byte++)
  {
    Adc.DinStatus[byte] = TM_FALSE;
    Adc.Diglitch[byte].state_high = 0xFFFF;
    Adc.Diglitch[byte].state_low = 0;
    Adc.Diglitch[byte].high_detected = TM_FALSE;
  }
}

void AdcSamplingTrigger(void)
{
  AD0_ADTSS_START = 0x1;  /* AN0 set A/D control start */
  AD1_ADTSS_START = 0x1;  /* AN0 set A/D control start */
}

/* ADC 0-31ch Int routine */
__interrupt void ADC_int_0_12(void)
{
  //  ad_data0 = AD0_ADTCD0_D;/* AD0 */
  Adc.AnalogInput[ADC_DIN_4]  = AD0_ADTCD12_D;/* AD12 DIGI_IN_4 */
  Adc.AnalogInput[ADC_DIN_5]  = AD0_ADTCD13_D;/* AD12 DIGI_IN_5 */
  Adc.AnalogInput[ADC_DIN_3]  = AD0_ADTCD14_D;/* AD12 PIN 10 SOS Input  */
  Adc.AnalogInput[ADC_DIN_2]  = AD0_ADTCD15_D;/* AD12 Hand brake */
  Adc.AnalogInput[ADC_DIN_1]  = AD0_ADTCD16_D;/* AD12 Brake */
  Adc.AnalogInput[ADC_AIN3]  = AD0_ADTCD17_D;/* AD12 AnalogIn3*/
  Adc.AnalogInput[ADC_AIN2]  = AD0_ADTCD18_D;/* AD12 AnalogIn2*/
  Adc.AnalogInput[ADC_AIN1]  = AD0_ADTCD19_D;/* AD12 AnalogIn1*/
  Adc.AnalogInput[ADC_INTBATT]  = AD0_ADTCD28_D;/* AD28 Internal battery */
  Adc.AnalogInput[ADC_CARBATT]  = AD1_ADTCD14_D;/* AD46 Car battery */

  //  AD0_ADTCS0_INT = 0;  /* Int frag clear AD0 */
  AD0_ADTCS13_INT = 0;  /* Int frag clear AD31 */
  adcevent = TM_TRUE;

}

void UpdateDigitalInputs(tm_bool_t *hDinStatus)
{
  tm_u8_t byte;
  int i;

  for(i = 0; i < NBROFANALOGCHANNELS; i++)
  {
    if(i != ADC_INTBATT)
      Adc.f_AnalogInput[i] = Adc.AnalogInput[i] * ADC_CONVERSION_FACTOR;
    else
      Adc.f_AnalogInput[i] = Adc.AnalogInput[i] * ADC_CONVERSION_FACTOR_INTBATT ;
  }

  if(Adc.f_AnalogInput[ADC_DIN_1] > THRESHOLD_HIGH)
    Adc.DinStatus[DIN1] = TM_TRUE;
  else if(Adc.f_AnalogInput[ADC_DIN_1] < THRESHOLD_LOW)
    Adc.DinStatus[DIN1] = TM_FALSE;

  if(Adc.f_AnalogInput[ADC_DIN_2] > THRESHOLD_HIGH)
    Adc.DinStatus[DIN2] = TM_TRUE;
  else if(Adc.f_AnalogInput[ADC_DIN_2] < THRESHOLD_LOW)
    Adc.DinStatus[DIN2] = TM_FALSE;

  if(Adc.f_AnalogInput[ADC_DIN_3] > THRESHOLD_HIGH)
    Adc.DinStatus[DIN3] = TM_TRUE;
  else if(Adc.f_AnalogInput[ADC_DIN_3] < THRESHOLD_LOW)
    Adc.DinStatus[DIN3] = TM_FALSE;

  if(Adc.f_AnalogInput[ADC_DIN_4] > THRESHOLD_HIGH)
    Adc.DinStatus[DIN4] = TM_TRUE;
  else if(Adc.f_AnalogInput[ADC_DIN_4] < THRESHOLD_LOW)
    Adc.DinStatus[DIN4] = TM_FALSE;

  if(Adc.f_AnalogInput[ADC_DIN_5] > THRESHOLD_HIGH)
    Adc.DinStatus[DIN5] = TM_TRUE;
  else if(Adc.f_AnalogInput[ADC_DIN_5] < THRESHOLD_LOW)
    Adc.DinStatus[DIN5] = TM_FALSE;

  if(Adc.f_AnalogInput[ADC_CARBATT] > THRESHOLD_HIGH)
    Adc.DinStatus[MAINPWR] = TM_TRUE;
  else if(Adc.f_AnalogInput[ADC_CARBATT] < THRESHOLD_LOW)
    Adc.DinStatus[MAINPWR] = TM_FALSE;

  byte = IO_PDDR05.byte;

  /* Reverse login for Ignition */
  if(byte & IGN_MON_BIT)
    Adc.DinStatus[IGN] = TM_FALSE;
  else
    Adc.DinStatus[IGN] = TM_TRUE;

  DiglitchInputs(hDinStatus);
}

void PrintAdcVal(void)
{
  int i;

  for(i = 0; i < NBROFANALOGCHANNELS; i++)
  {
    sprintf(debugstr,"%d = %.2f\r\n",i,((float)Adc.AnalogInput[i] * ADC_CONVERSION_FACTOR));
    DPRINTF(debugstr);
  }
}

void DiglitchInputs(tm_bool_t *hDinStatus)
{
  int i;
  int tmp;

  for(i = 0; i < MAXDIN; i++)
  {
    tmp = Adc.DinStatus[i];

    Adc.Diglitch[i].state_high = (Adc.Diglitch[i].state_high << 1) | !tmp | MASK_STATE;
    Adc.Diglitch[i].state_high &= 0xFFFF;
    Adc.Diglitch[i].state_low = (Adc.Diglitch[i].state_low << 1) | tmp | MASK_STATE;
    Adc.Diglitch[i].state_low &= 0xFFFF;

    if(Adc.Diglitch[i].state_high == MASK_ONECOUNT)
    {
      //DPRINTF("High detected\r\n");
      Adc.Diglitch[i].high_detected = TM_TRUE;
      hDinStatus[i] = TM_TRUE;
    }
    if(Adc.Diglitch[i].state_low == MASK_ONECOUNT)
    {
      if(Adc.Diglitch[i].high_detected == TM_TRUE)
      {
        //DPRINTF("Low detected\r\n");
        Adc.Diglitch[i].high_detected = TM_FALSE;
        hDinStatus[i] = TM_FALSE;
      }
    }
  }
}

void GetAnalogIn(float *AnalogVal)
{
  AnalogVal[AIN1] = Adc.f_AnalogInput[ADC_AIN1];
  AnalogVal[AIN2] = Adc.f_AnalogInput[ADC_AIN2];
  AnalogVal[AIN3] = Adc.f_AnalogInput[ADC_AIN3];
  AnalogVal[CARBAT] = Adc.f_AnalogInput[ADC_CARBATT];
  AnalogVal[INTBAT] = Adc.f_AnalogInput[ADC_INTBATT];
}
