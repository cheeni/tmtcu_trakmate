#ifndef __TMADC_PRIVATE__
#define __TMADC_PRIVATE__

#include "tmadc\inc\tmadc.h"

#define ADC_CONVERSION_FACTOR (3.3 * 19.0/4096.0)
#define ADC_CONVERSION_FACTOR_INTBATT (3.3 * 2.0/4096.0)

/* P107 - p100
 * AN12 - AN19
 * */
#define AIN_8_BIT 0X80
#define AIN_7_BIT 0X40
#define AIN_6_BIT 0X20
#define AIN_5_BIT 0X10
#define AIN_4_BIT 0X08
#define AIN_3_BIT 0X04
#define AIN_2_BIT 0X02
#define AIN_1_BIT 0X01


#define CAR_ADC_BIT 0X10
#define IGN_MON_BIT 0X08


#define THRESHOLD_HIGH 3.3
#define THRESHOLD_LOW  1.0

void DiglitchInputs(tm_bool_t *hDinStatus);


#endif /* __TMADC_PRIVATE__ */
