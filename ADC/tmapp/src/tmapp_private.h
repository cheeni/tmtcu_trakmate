#ifndef __TMAPP_PRIVATE_H__
#define __TMAPP_PRIVATE_H__

#include "tcuapp.h"

#define PGMCOMMAND "PGM"

void Profile0Process(tm_app_t *hTMApp);
void Profile1Process(tm_app_t *hTMApp);
void PeriodicTimerExpired(void *hApp);
void AddPacketToQueue(tm_app_t *hTMApp);
void ConstructPacket(tm_app_t *hTMApp, tm_packettype_t PacketType);
tm_bool_t CheckForPacket(tm_app_t *hTMApp);
void  AddPgn(char *pData, tm_packet_t *hPacket);
void SetPGNFilter(tm_app_t *hTMApp, tm_u32_t pgn);
void MakePacket(tm_app_t *hTMApp, tm_packet_t *hPacket, int *Len);
void MakePgnPacket(tm_app_t *hTMApp, tm_packet_t *hPacket, int *Len);
void PGNHandle(tm_dtc_t *hDTC, tm_dtc_t *hDTCP, tm_pgndata_t *hPGNData, j1939_t *msg);
void Profile0DataGet(tm_u8_t *pData, tm_u16_t Len, void *pUserData);
void Profile1DataGet(tm_u8_t *pData, tm_u16_t Len, void *pUserData);
void HandleOTA(tm_app_t *hTMApp);
static void OTATimerExpired(void *pUserData);
void OTAUgdInit(tm_app_t *hTMApp, tm_timer_t *hSerialUgdTimeout);
void OTAUpgradeProcess(tm_app_t *hTMApp, tm_u16_t Size, tm_u8_t Byte);
static tm_bool_t ProgramToFlash(tm_app_t *hTMApp);
void SendOTAAck(tm_app_t *hTMApp, tm_u8_t *pData);
void ResetCtrl(tm_app_t *hTMApp);
static int CheckQueue(tm_app_t *hTMApp);
static void ManageQueue(tm_app_t *hTMApp);
static void Profile0StatusUpdate(tm_profilestatus_t ProfileStatus, void *pUserData);
static void Profile1StatusUpdate(tm_profilestatus_t ProfileStatus, void *pUserData);
static void Profile2StatusUpdate(tm_profilestatus_t ProfileStatus, void *pUserData);
void ParseDM1(tm_dtc_t *hDTC, j1939_t *msg);
void ParseDM2(tm_dtc_t *hDTC, j1939_t *msg);
void MakeDTCPacket(tm_app_t *hTMApp, tm_packet_t *hPacket, int *Len);
void MakeDTCPPacket(tm_app_t *hTMApp, tm_packet_t *hPacket, int *Len);

#endif  /* __TMAPP_PRIVATE_H__ */
