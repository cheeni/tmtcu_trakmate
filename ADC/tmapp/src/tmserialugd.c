#include "tmapp\src\tmserialugd_private.h"

extern char SoftwareVer[];
extern char HardwareVer[];

void SerialUgdInit(tm_serialugd_t *hSerialUgd, tm_flashtask_t *hFlashTask, tm_timer_t *hSerialUgdTimeout)
{
  hSerialUgd->ProgramState = UGD_QUERY;
  hSerialUgd->FlashState = UGDFLASH_IDLE;
  hSerialUgd->UgdMode = LOOKFOR_UGD;
  hSerialUgd->hFlashTask = hFlashTask;
  hSerialUgd->CmdStringIndex = 0;
  hSerialUgd->hSerialUgdTimeout = hSerialUgdTimeout;
  ResetTimer(hSerialUgd->hSerialUgdTimeout, 10);
}

void SerialUpgradeProcess(tm_serialugd_t *hSerialUgd, tm_u16_t Size, tm_u8_t Byte)
{
  tm_u16_t i=0;
  tm_u8_t j;
  tm_u8_t CmdSize;

  CmdSize = sizeof(UGDCommand)-1;

  for (i = 0; i < Size ; i++)
  {
    switch (hSerialUgd->UgdMode)
    {
      case LOOKFOR_UGD:
        hSerialUgd->CmdString[hSerialUgd->CmdStringIndex]=(tm_u8_t)toupper(Byte);
        if (hSerialUgd->CmdStringIndex >= (CmdSize-1))
        {
          if (!strncmp((const char *)hSerialUgd->CmdString, UGDCommand, CmdSize))
          {
            DPRINTF("DEBUG GPRSUGD_MODE\r\n");
            hSerialUgd->UgdMode = UGDDATA;
            hSerialUgd->UGDDataIndex = 0;
            hSerialUgd->bDLEFound = TM_FALSE;
          }
          else
          {
            for (j = 0; j < CmdSize; j++)
            {
              hSerialUgd->CmdString[j] = hSerialUgd->CmdString[j+1];
            }
          }
        }
        else
          hSerialUgd->CmdStringIndex++;
        break;
      case UGDDATA:
        if(Byte == PROT_DLE)
        {
          if(hSerialUgd->bDLEFound == TM_TRUE)
          {
            hSerialUgd->bDLEFound = TM_FALSE;
            hSerialUgd->UGDData[hSerialUgd->UGDDataIndex++] = Byte;
            if (hSerialUgd->UGDDataIndex>=MAXUGDDATALEN)
            {
              hSerialUgd->UGDDataIndex=0;
              hSerialUgd->UgdMode=LOOKFOR_UGD;
              DPRINTF("SYNC ERROR\r\n");
            }

          }
          else
          {
            hSerialUgd->bDLEFound = TM_TRUE;
          }
        }
        else if (Byte == PROT_EOM)
        {
          if(hSerialUgd->bDLEFound == TM_TRUE)
          {
            hSerialUgd->UgdMode = LOOKFOR_UGD;
            WriteToFlash(hSerialUgd);
          }
          else
          {
            hSerialUgd->UGDData[hSerialUgd->UGDDataIndex++] = Byte;
            if (hSerialUgd->UGDDataIndex>=MAXUGDDATALEN)
            {
              hSerialUgd->UGDDataIndex=0;
              hSerialUgd->UgdMode=LOOKFOR_UGD;
              DPRINTF("SYNC ERROR\r\n");
            }
          }
        }
        else
        {
          hSerialUgd->UGDData[hSerialUgd->UGDDataIndex++] = Byte;
          if (hSerialUgd->UGDDataIndex>=MAXUGDDATALEN)
          {
            hSerialUgd->UGDDataIndex=0;
            hSerialUgd->UgdMode=LOOKFOR_UGD;
            DPRINTF("SYNC ERROR\r\n");
          }
        }
        break;
      default:
        break;
    }
  }
}

void HandleUpgrade(tm_serialugd_t *hSerialUgd)
{
  int i;
  tm_u16_t FrameCalcCRC;

  switch(hSerialUgd->FlashState)
  {
    case UGDFLASH_IDLE:
      if(FlashTaskStateGet(hSerialUgd->hFlashTask) == FLASHTASK_IDLE)
      {
        if(hSerialUgd->bSendAck == TM_TRUE)
        {
          hSerialUgd->bSendAck = TM_FALSE;
          DPRINTF(hSerialUgd->ResponseBuf);
        }
      }
      break;
    case UGDFLASH_ERASE:
      if(FlashTaskStateGet(hSerialUgd->hFlashTask) == FLASHTASK_IDLE)
      {
        //UART0QueueData((tm_u8_t *)"Flash erased\r\n",vts_strlen("Flash erased\r\n"));
        DPRINTF("serialtest erase done\r\n");
        FlashTaskWrite(hSerialUgd->hFlashTask, APP_BYTECOUNTADDR, 4, (tm_u8_t *)&hSerialUgd->TotalByteCount);
        hSerialUgd->FlashState = UGDFLASH_BYTECOUNTWAIT;
      }
      break;
    case UGDFLASH_BYTECOUNTWAIT:
      if(FlashTaskStateGet(hSerialUgd->hFlashTask) == FLASHTASK_IDLE)
      {
        DPRINTF("serialtest byte coute ok\r\n");
        FlashTaskWrite(hSerialUgd->hFlashTask, APP_CRCADDR, 2, (tm_u8_t *)&hSerialUgd->RxCRC);
        hSerialUgd->FlashState = UGDFLASH_CRCWRITEWAIT;
      }
      break;
    case UGDFLASH_CRCWRITEWAIT:
      if(FlashTaskStateGet(hSerialUgd->hFlashTask) == FLASHTASK_IDLE)
      {
        DPRINTF("serialtest crc ok\r\n");
        strcpy((char *)hSerialUgd->ResponseBuf,"OK\r\n");
        hSerialUgd->bSendAck = TM_TRUE;
        hSerialUgd->FlashState = UGDFLASH_IDLE;
      }
      break;
    case UGDFLASH_WRITECURVER:
      if(FlashTaskStateGet(hSerialUgd->hFlashTask) == FLASHTASK_IDLE)
      {
        FlashTaskWrite(hSerialUgd->hFlashTask, APP_CURVER_ADDR, 4, (tm_u8_t *)SoftwareVer);
        /* Send Ack */
        hSerialUgd->FlashState = UGDFLASH_WRITENEWVER;
      }
      break;
    case UGDFLASH_WRITENEWVER:
      if(FlashTaskStateGet(hSerialUgd->hFlashTask) == FLASHTASK_IDLE)
      {
        FlashTaskWrite(hSerialUgd->hFlashTask, APP_NEWVER_ADDR, 4, (tm_u8_t *)hSerialUgd->SoftVer);
        hSerialUgd->FlashState = UGDFLASH_NEWVERWAIT;
      }
      break;
    case UGDFLASH_NEWVERWAIT:
      if(FlashTaskStateGet(hSerialUgd->hFlashTask) == FLASHTASK_IDLE)
      {
        hSerialUgd->FlashState = UGDFLASH_IDLE;

        /* Send Ack */
        hSerialUgd->bSendAck = TM_TRUE;
        strcpy((char *)hSerialUgd->ResponseBuf,"CRC OK\r\n");
      }
      break;
    case UGDFLASH_WRITEDATA:
      if(FlashTaskStateGet(hSerialUgd->hFlashTask) == FLASHTASK_IDLE)
      {
        /* Send Ack */
        //UART0QueueData((tm_u8_t *)"UGD_DATA data written\r\n",vts_strlen("UGD_DATA data written\r\n"));
        DPRINTF("serialtest writedata success\r\n");
        hSerialUgd->FlashState = UGDFLASH_IDLE;
        strcpy((char *)hSerialUgd->ResponseBuf,"OK\r\n");
        hSerialUgd->bSendAck = TM_TRUE;
      }
      break;
    case UGDFLASH_CHECKCRC:
      if(FlashTaskStateGet(hSerialUgd->hFlashTask) == FLASHTASK_IDLE)
      {
        FrameCalcCRC = 0;
        for(i=0; i < hSerialUgd->RdLen; i++)
        {
          hSerialUgd->CalcCRC += hSerialUgd->DataBuf[i];
          FrameCalcCRC += hSerialUgd->DataBuf[i];
          hSerialUgd->CalcCRC &= 0xFFFF;
        }
#if 0
        if(printdata == 0)
        {
          printdata = 1;
          //UART0QueueData((tm_u8_t *)"First bytes  ",12);
          for(i=0; i < 1; i++)
          {
            itoa((int)hSerialUgd->tmpTotalByteCount,tmpbuf);
            //UART0QueueData((tm_u8_t *)tmpbuf,vts_strlen(tmpbuf));
            //UART0QueueData((tm_u8_t *)" ",1);
          }
        }
        //if(hSerialUgd->hSysDbase->bGSMDebugEnable==TM_TRUE)
          //UART0QueueData((tm_u8_t *)"FCRC ",vts_strlen("FCRC "));
        itoa((int)(FrameCalcCRC & 0xFFFF),tmpbuf);
        //if(hSerialUgd->hSysDbase->bGSMDebugEnable==TM_TRUE)
        //{
          //UART0QueueData((tm_u8_t *)tmpbuf,vts_strlen(tmpbuf));
          //UART0QueueData((tm_u8_t *)" FCRC\r\n",vts_strlen(" FCRC\r\n"));
        //}

#endif
        if(hSerialUgd->tmpTotalByteCount > 0)
        {
          if(hSerialUgd->tmpTotalByteCount > 256)
          {
            hSerialUgd->RdLen = 256;
            FlashTaskRead(hSerialUgd->hFlashTask, hSerialUgd->FlashAddr, 256,hSerialUgd->DataBuf);
            hSerialUgd->FlashAddr += 256;
            hSerialUgd->tmpTotalByteCount -= 256;
          }
          else
          {
            hSerialUgd->RdLen = hSerialUgd->tmpTotalByteCount;
            FlashTaskRead(hSerialUgd->hFlashTask, hSerialUgd->FlashAddr, hSerialUgd->tmpTotalByteCount,hSerialUgd->DataBuf);
            hSerialUgd->tmpTotalByteCount = 0;
          }
        }
        else
        {
          if(hSerialUgd->CalcCRC == hSerialUgd->RxCRC)
          {
            /* CRC OK*/
            //if(hSerialUgd->hSysDbase->bGSMDebugEnable==TM_TRUE)
              //UART0QueueData((tm_u8_t *)"CRC OK\r\n",vts_strlen("CRC OK\r\n"));
            hSerialUgd->FlashState = UGDFLASH_WRITECURVER;
            //hSerialUgd->bSendAck = TM_TRUE;
            //vts_strcpy((char *)hSerialUgd->ResponseBuf,"CRC OK");
          }
          else
          {
            /* CRC NOK*/
            //if(hSerialUgd->hSysDbase->bGSMDebugEnable==TM_TRUE)
              //UART0QueueData((tm_u8_t *)"CRC NOK\r\n",vts_strlen("CRC NOK\r\n"));
            hSerialUgd->FlashState = UGDFLASH_IDLE;
            hSerialUgd->bSendAck = TM_TRUE;
            strcpy((char *)hSerialUgd->ResponseBuf,"CRC NOK\r\n");
          }
        }
      }
      break;
  }
}

static tm_bool_t WriteToFlash(tm_serialugd_t *hSerialUgd)
{
  tm_u16_t MsgLength;
  tm_u16_t i;
  tm_u16_t FrameRxCRC;
  tm_u8_t MsgID;
  tm_u16_t FrameCalcCRC=0;


  ResetTimer(hSerialUgd->hSerialUgdTimeout, 10);

  MsgID = hSerialUgd->UGDData[0];
#if 0
  if(MsgID == MSG_UGDSTART_ID )
  {
    //UART0QueueData((tm_u8_t *)"Received bytes* ",vts_strlen("Received bytes* "));
    for(i = 0; i < hSerialUgd->UGDDataIndex; i++)
    {
      itoa(hSerialUgd->UGDData[i],tmpbuf);
      //UART0QueueData((tm_u8_t *)tmpbuf,vts_strlen(tmpbuf));
      //UART0QueueData((tm_u8_t *)" ",vts_strlen(" "));
    }
    //UART0QueueData((tm_u8_t *)"*Received bytes\r\n",vts_strlen("*Received bytes\r\n"));
  }
#endif
  MsgLength = (tm_u16_t)(hSerialUgd->UGDData[1] << 8 | hSerialUgd->UGDData[2]);

  MsgLength -= 3;

  //hSerialUgd->bHandleUGD = TM_TRUE;
  /* Reset soft download timer timer */
  if (!hSerialUgd->UGDDataIndex)
    return TM_FALSE;

  /* Get CRC from received block */
  FrameRxCRC= (tm_u16_t)((hSerialUgd->UGDData[(MsgLength+3)] << 8) |(hSerialUgd->UGDData[(MsgLength + 4)] ));

  /* Calculate CRC from the received block */
  for(i=0; i< (hSerialUgd->UGDDataIndex-2); i++)
  {
    FrameCalcCRC += (tm_u16_t)hSerialUgd->UGDData[i];
    //FrameCalcCRC &= 0xFFFF;
  }
  //if(hSerialUgd->hSysDbase->bGSMDebugEnable==TM_TRUE)
  //UART0QueueData((tm_u8_t *)"DEBUG OCRC ",vts_strlen("OCRC "));
  //vts_itoa((int)(FrameCalcCRC & 0xFFFF),tmpbuf);
  //if(hSerialUgd->hSysDbase->bGSMDebugEnable==TM_TRUE)
  //{
    //UART0QueueData((tm_u8_t *)tmpbuf,vts_strlen(tmpbuf));
    //UART0QueueData((tm_u8_t *)"DEBUG OCRC\r\n",vts_strlen("DEBUG OCRC\r\n"));
  //}

  if (FrameCalcCRC != FrameRxCRC)
  {
    //itoa((int)(FrameCalcCRC & 0xFFFF),(char *)hSerialUgd->ResponseBuf);
    strcpy((char *)hSerialUgd->ResponseBuf," BLOCK ERROR\r\n");
    //itoa(FrameRxCRC,tmpbuf);
    //if(hSerialUgd->hSysDbase->bGSMDebugEnable==TM_TRUE)
    //{
      //UART0QueueData((tm_u8_t *)tmpbuf,vts_strlen(tmpbuf));
      //UART0QueueData((tm_u8_t *)"BLOCK ERROR\r\n",vts_strlen("BLOCK ERROR\r\n"));
    //}
    //hSerialUgd->bProfile1DataSend = TM_TRUE;
    DPRINTF(hSerialUgd->ResponseBuf);
    return TM_TRUE;
  }
  switch (hSerialUgd->ProgramState)
  {
    case UGD_QUERY:
      if (MsgID == MSG_UGDQUERY_ID)
      {
        DPRINTF("DEBUG UGD_QUERY\r\n");
        /* Send hardware version and software version  */
        //if(hSerialUgd->hSysDbase->bGSMDebugEnable==TM_TRUE)
          //UART0QueueData((tm_u8_t *)"Received UGD_QUERY\r\n",vts_strlen("Received UGD_QUERY\r\n"));
        strcpy((char *)hSerialUgd->ResponseBuf,"<");
        strcat((char *)hSerialUgd->ResponseBuf,SoftwareVer);
        strcat((char *)hSerialUgd->ResponseBuf,"><");
        strcat((char *)hSerialUgd->ResponseBuf,HardwareVer);
        strcat((char *)hSerialUgd->ResponseBuf,"><OK>\r\n");
        hSerialUgd->ProgramState = UGD_START;
        //hSerialUgd->bProfile1DataSend = TM_TRUE;
        DPRINTF(hSerialUgd->ResponseBuf);
      }
      else
      {
        /* Return error */
        strcpy((char *)hSerialUgd->ResponseBuf,"ERROR: Expecting QUERY\r\n");
        //hSerialUgd->bProfile1DataSend = TM_TRUE;
        DPRINTF(hSerialUgd->ResponseBuf);
      }
      break;
    case UGD_START:
      /* Receive and store CRC and total byte count  */
      if(MsgID != MSG_UGDSTART_ID || (MsgLength != UGDSTART_MSGLEN))
      {
        /* Return error */
        strcpy((char *)hSerialUgd->ResponseBuf,"NOK\r\n");
        hSerialUgd->ProgramState = UGD_QUERY;
        //hSerialUgd->bProfile1DataSend = TM_TRUE;
        DPRINTF(hSerialUgd->ResponseBuf);
      }
      else
      {
        DPRINTF("DEBUG UGD_START\r\n");
        hSerialUgd->RxCRC= (tm_u16_t)((hSerialUgd->UGDData[7] << 8) |(hSerialUgd->UGDData[8] ));
        hSerialUgd->TotalByteCount = (hSerialUgd->UGDData[3] << 24) |(hSerialUgd->UGDData[4] << 16) |
          (hSerialUgd->UGDData[5] << 8) |(hSerialUgd->UGDData[6]);
        /* Init erase flash */
        hSerialUgd->SoftVer[0] = hSerialUgd->UGDData[9];
        hSerialUgd->SoftVer[1] = hSerialUgd->UGDData[10];
        hSerialUgd->SoftVer[2] = hSerialUgd->UGDData[11];
        hSerialUgd->SoftVer[3] = hSerialUgd->UGDData[12];
        hSerialUgd->SoftVer[4] = '\0';
        hSerialUgd->ProgramState = UGD_DATA;
        hSerialUgd->FlashAddr = APP_STARTADDR;  /* Set FlashAddr to store received software image */
        hSerialUgd->FlashState = UGDFLASH_ERASE;
        FlashTaskEraseSoftImage(hSerialUgd->hFlashTask);
        //hSerialUgd->bSendAck = TM_TRUE;
        //vts_strcpy((char *)hSerialUgd->ResponseBuf,"OK");
      }
      break;
    case UGD_DATA:
      /* Init write to flash */
      if (MsgID == MSG_UGDDATA_ID)
      {
        //UART0QueueData((tm_u8_t *)"Received UGD_DATA\r\n",vts_strlen("Received UGD_DATA\r\n"));
        if(FlashTaskStateGet(hSerialUgd->hFlashTask) != FLASHTASK_IDLE)
           DPRINTF("serialtest flash busy\r\n");
        if(MsgLength > 256)
           DPRINTF("serialtest msg len big\r\n");
        if(MsgLength == 0)
           DPRINTF("serialtest msg len 0\r\n");
        if(FlashTaskStateGet(hSerialUgd->hFlashTask) != FLASHTASK_IDLE || MsgLength > 256 || MsgLength == 0)
        {
          //hSerialUgd->bProfile1DataSend = TM_TRUE;
          strcpy((char *)hSerialUgd->ResponseBuf,"NOK\r\n");
        DPRINTF(hSerialUgd->ResponseBuf);
          hSerialUgd->ProgramState = UGD_QUERY;
          //UART0QueueData((tm_u8_t *)"UGD_DATA error\r\n",vts_strlen("UGD_DATA error\r\n"));
          break;
        }
        //UART0QueueData((tm_u8_t *)"UGD_DATA writing to flash\r\n",vts_strlen("UGD_DATA writing to flash\r\n"));
        if(MsgLength != 256)
          DPRINTF("serialtest msglenght not 256\r\n");
        else
          DPRINTF("serialtest msglenght 256\r\n");
        DPRINTF("serialtest received data\r\n");
        if(FlashTaskStateGet(hSerialUgd->hFlashTask) != FLASHTASK_IDLE)
          DPRINTF("serialtest flash busy\r\n");
        FlashTaskWrite(hSerialUgd->hFlashTask, hSerialUgd->FlashAddr, MsgLength, (tm_u8_t *)&hSerialUgd->UGDData[3]);
        hSerialUgd->FlashState = UGDFLASH_WRITEDATA;
        hSerialUgd->FlashAddr += MsgLength;
      }
      else if(MsgID == MSG_UGDEND_ID)
      {
        /* Read back from flash and check crc */
        DPRINTF("DEBUG UGD_DATA\r\n");
        hSerialUgd->FlashState = UGDFLASH_CHECKCRC;
        hSerialUgd->CalcCRC = 0;
        hSerialUgd->FlashAddr = APP_STARTADDR;
        hSerialUgd->tmpTotalByteCount = hSerialUgd->TotalByteCount;
        hSerialUgd->ProgramState = UGD_OK;
        hSerialUgd->bSendAck = TM_TRUE;
        hSerialUgd->RdLen = 256;
        FlashTaskRead(hSerialUgd->hFlashTask, hSerialUgd->FlashAddr, 256,hSerialUgd->DataBuf);
        hSerialUgd->FlashAddr += 256;
        hSerialUgd->tmpTotalByteCount -= 256;
      }
      break;
    case UGD_OK:
      if(MsgID == MSG_UGDCRCOK_ID)
      {
        /* Reset Processor */
        DPRINTF("Upgrade successfull. Reset\r\n");
        Vectors_InitIrqLevels();
        __asm("   LDI #1,R0");
        __asm("   LDI:32 #_IO_RSTCR,R12");
        __asm("   STB R0, @R12");
        __asm("   LDUB @R12, R0");
        __asm("   MOV R0,R0");
        __asm("   NOP");
        __asm("   NOP");
      }
      break;
    case UGD_END:
    default:
      break;
  }
  return TM_TRUE;
}
