#ifndef __TM_SERIALUGD_PRIVATE_H__
#define __TM_SERIALUGD_PRIVATE_H__

#include "tmapp\inc\tmserialugd.h"
#include "ctype.h"

const char  UGDCommand[] = "UGD";

static tm_bool_t WriteToFlash(tm_serialugd_t *hSerialUgd);

#endif /* __TM_SERIALUGD_PRIVATE_H__ */
