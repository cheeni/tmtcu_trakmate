#include "tmapp\src\tmapp_private.h"

extern char debugstr[];
tm_bool_t bStartOTA;
tm_pgndata_t PGNData;
tm_dtc_t DTC;
tm_dtc_t DTCP;
extern char SoftwareVer[];
extern char HardwareVer[];

j1939_t msg_pgns[] = {

   {64777,NULL,3,0,0,6}
  ,{65292,NULL,3,0,0,3}
  ,{65483,NULL,3,0,0,6}
  ,{65254,NULL,3,0,0,6}
  ,{65217,NULL,3,0,0,6}
  ,{65276,NULL,3,0,0,6}
  ,{65226,NULL,3,0,0,6}
  ,{59904,NULL,3,0,0,6} /* To get DM2 message, to get previously active */
  ,{65265,NULL,3,0,0,6}
  ,{61444,NULL,3,0,0,3}
  ,{65253,NULL,3,0,0,6}
  ,{65282,NULL,3,0,0,3}
  ,{65263,NULL,3,0,0,6}
  ,{65262,NULL,3,0,0,6}
};

tm_j1939data_t J1939Data[MAXPGN];

void printpgn(void);


int USE_J1939 = TM_TRUE;

void MainProcessInit(tm_app_t *hTMApp)
{
  tm_bool_t retval;
  int i;

  bStartOTA = TM_FALSE;
  hTMApp->Profile0.P0State = P0_IDLE;
  hTMApp->Profile1.P1Status = WAITFOROTACOMMAND;

  hTMApp->ResetState = WAITFORRESETCMD;
  hTMApp->bReset = TM_FALSE;

  hTMApp->pGsmCallBack.pProfile0Data = Profile0DataGet;
  hTMApp->pGsmCallBack.pProfile1Data = Profile1DataGet;
  hTMApp->pGsmCallBack.pProfile0Status = Profile0StatusUpdate;
  hTMApp->pGsmCallBack.pProfile1Status = Profile1StatusUpdate;
  hTMApp->pGsmCallBack.pProfile2Status = Profile2StatusUpdate;

  hTMApp->Profile0.bDataSent = TM_TRUE;
  GsmInit(&hTMApp->Gsm, &hTMApp->DBase, &hTMApp->pGsmCallBack, hTMApp);

  hTMApp->bPeriodicPacketSent = TM_FALSE;
  retval = CreateTimer(&hTMApp->Profile0.hPeriodicPacketTimer,5,PeriodicTimerExpired,hTMApp);
  if(retval == TM_FALSE)
  {
    DPRINTF("Error creating hPeriodicPacketTimer timer\r\n");
  }

  hTMApp->PowerUpUpdateRate = hTMApp->DBase.UpdateRate;
  ResetTimer(hTMApp->Profile0.hPeriodicPacketTimer, hTMApp->PowerUpUpdateRate);

  CreateTimer(&hTMApp->hOTAUgdTimeout, 10, OTATimerExpired, hTMApp);

  for(i = PGN_64777; i < MAXPGN; i++)
  {
    J1939Data[i].buf_len = 0;
  }
  hTMApp->CANPGNIndex = 0;
  if(USE_J1939 == TM_TRUE)
    j1939_init();
  FlashTaskGetRdWrPtrs(&hTMApp->FlashTask);
}


/*
 * Packet format
 *
 * ^PERIODIC,DevId,SeqNum,Latitude,Longitude,Date,Time,Speed,MainPwr,Ignition,DIn1,DIn2,DIn3,DIn4,DIn5,AIn1,AIn2,AIn3,VehicleBattery,IntBattery,GPSOdo,Live, Heading#
 *
 * */

void MakePacket(tm_app_t *hTMApp, tm_packet_t *hPacket, int *Len)
{

  if(hPacket->PacketType == PACKET_CAN)
  {
    MakePgnPacket(hTMApp, &hTMApp->Profile0.SendPacket, Len);
    return;
  }
  if(hPacket->PacketType == PACKET_DTC)
  {
    MakeDTCPacket(hTMApp, &hTMApp->Profile0.SendPacket, Len);
    return;
  }
  if(hPacket->PacketType == PACKET_DTCP)
  {
    MakeDTCPPacket(hTMApp, &hTMApp->Profile0.SendPacket, Len);
    return;
  }

  hTMApp->Profile0.Profile0Data[0] = '\0';

  switch(hPacket->PacketType)
  {
    case PACKET_PERIODIC:
      strcat(hTMApp->Profile0.Profile0Data,"^PERIODIC");
      break;
    case PACKET_IGNON:
      strcat(hTMApp->Profile0.Profile0Data,"^IGN_ON");
      break;
    case PACKET_IGNOFF:
      strcat(hTMApp->Profile0.Profile0Data,"^IGN_OFF");
      break;
    case PACKET_SOS:
      strcat(hTMApp->Profile0.Profile0Data,"^SOS");
      break;
  }
  strcat(hTMApp->Profile0.Profile0Data,",");
  strcat(hTMApp->Profile0.Profile0Data,hTMApp->DBase.DevID);

  sprintf(debugstr,",%d",hPacket->SeqNum);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%2.5f",hPacket->Latitude);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%3.5f",hPacket->Longitude);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->UTC);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%0.1f",hPacket->Speed);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  if(hPacket->bMainPwr == TM_TRUE)
    strcat(hTMApp->Profile0.Profile0Data,",1");
  else
    strcat(hTMApp->Profile0.Profile0Data,",0");

  if(hPacket->bIgnition == TM_TRUE)
    strcat(hTMApp->Profile0.Profile0Data,",1");
  else
    strcat(hTMApp->Profile0.Profile0Data,",0");

  sprintf(debugstr,",%d",hPacket->DIn1);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->DIn2);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->DIn3);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->DIn4);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->DIn5);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%0.2f",hPacket->AnalogIn1);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%0.2f",hPacket->AnalogIn2);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%0.2f",hPacket->AnalogIn3);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%0.2f",hPacket->VehBatt);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%0.2f",hPacket->IntBatt);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%0.1f",hPacket->GPSOdo);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->bLive);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%0.1f",hPacket->Heading);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  strcat(hTMApp->Profile0.Profile0Data,"#");

  *Len = strlen((char *)hTMApp->Profile0.Profile0Data);

  //DPRINTF("Sending packet: ");
  //DPRINTF(hTMApp->Profile0.Profile0Data);
  //DPRINTF("\r\n");
}

/*
 * CAN Packet format
 *
 * ^CAN,DevId,SeqNum,Latitude,Longitude,UTC,HRLFC,SweetSpot,TopGear,SweetSpotPercent,Seconds,Minute,Hour,Month,Day,Year,MinuteOffset,HourOffset,
 * TotalDistance,FuelLevel,AmberWarningLamp,RedStopLamp, MalfunctionLamp,SPNLSB,SPN8_2ndByte,FailurMode,SPN3_MSB,OccurenceCount,CCA,CCES,CCSS,EngineSpeed,EngineStartMode,
 * EngineOperatingHours,PowerKeyPos, AccPedalIdelSwitch, VehicleSpeed,ControlerTrimMode,EngineOilPressure,EngineCoolantTemp,AccPedalPosition,TripFuel#
 *
 * */
void MakePgnPacket(tm_app_t *hTMApp, tm_packet_t *hPacket, int *Len)
{
  hTMApp->Profile0.Profile0Data[0] = '\0';

  //DPRINTF("MakePacket called\r\n");
  strcat(hTMApp->Profile0.Profile0Data,"^CAN,");

  strcat(hTMApp->Profile0.Profile0Data,hTMApp->DBase.DevID);

  sprintf(debugstr,",%d",hPacket->SeqNum);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%2.5f",hPacket->Latitude);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%3.5f",hPacket->Longitude);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->UTC);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%0.2f",hPacket->CANData.PGNData.HRLFC.hrlfc);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.VP12.SweetSpot);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.VP12.TopGear);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.VP203.SweetSpotPercent);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.TD_IC.Secs);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.TD_IC.Min);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.TD_IC.Hr);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.TD_IC.Month);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.TD_IC.Day);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.TD_IC.Year);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.TD_IC.LocalMinuteOffset);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.TD_IC.LocalHrOffset);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%0.2f",hPacket->CANData.PGNData.HRVD_I.TotalDistance);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%0.2f",hPacket->CANData.PGNData.DD_X_IC.FuelLevel);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.DM1_Active.AmberWarningLamp);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.DM1_Active.RedStopLamp);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.DM1_Active.MalfuncLamp);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.DM1_Active.FlashMalfuctionLamp);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.DM1_Active.SPN8_LSB_SPN_EMS);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.DM1_Active.SPN8_2ndB_SPN_EMS);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.DM1_Active.FailurMode);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.DM1_Active.SPN3_MSB_EMS);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.DM1_Active.OccurenceCount);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.CCVS.CCA);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.CCVS.CCES);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.CCVS.CCSS);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%0.2f",hPacket->CANData.PGNData.EEC1.EngineSpeed);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.EEC1.EngineStartMode);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%0.2f",hPacket->CANData.PGNData.EngineHours.EngineOpHrs);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.VP2.PowerKeyPos);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.VP2.AccPedalIdelSwitch);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%0.2f",hPacket->CANData.PGNData.VP2.VehicleSpeed);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.VP2.ControlerTrimMode);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);


  sprintf(debugstr,",%d",hPacket->CANData.PGNData.EELP1.EngineOilPressure);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.PGNData.ET1.EngineCoolantTemp);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%0.2f",hPacket->CANData.PGNData.VP2.AccPedalPosition);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%0.2f",hPacket->CANData.PGNData.HRLFC.TripFuel);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->bLive);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  strcat(hTMApp->Profile0.Profile0Data,"#");

  *Len = strlen((char *)hTMApp->Profile0.Profile0Data);
  //DPRINTF("Sending can packet: ");
  //DPRINTF(hTMApp->Profile0.Profile0Data);
  //DPRINTF("\r\n");


#if 0

  pData = hTMApp->Profile0.Profile0Data;
  for(k = 0; k < 2; k++)
  {
    j = pgn_index;

    //if(hPacket->J1939Data[j].buf_len != 0)
    {
      switch(j)
      {
        case PGN_64777:
          strcat(pData,",\"PGN_64777\":");
          strcat(pData,"{");
          strcat(pData,"\"HRLFC\":");
          sprintf(debugstr,"%0.2f}",hPacket->PGNData.HRLFC.hrlfc);
          strcat(pData,debugstr);
          break;
        case PGN_65292:
          strcat(pData,",\"PGN_65292\":");
          strcat(pData,"{");
          strcat(pData,"\"sweetSpot\":");
          sprintf(debugstr,"%d",hPacket->PGNData.VP12.SweetSpot);
          strcat(pData,debugstr);

          strcat(pData,",\"TopGear\":");
          sprintf(debugstr,"%d}",hPacket->PGNData.VP12.TopGear);
          strcat(pData,debugstr);
          break;
        case PGN_65483:
          strcat(pData,",\"PGN_65483\":");
          strcat(pData,"{");
          strcat(pData,"\"sweetSpotPercent\":");
          sprintf(debugstr,"%d}",hPacket->PGNData.VP203.SweetSpotPercent);
          strcat(pData,debugstr);


          break;
        case PGN_65254:
          strcat(pData,",\"PGN_65254\":");

          strcat(pData,"{");
          strcat(pData,"\"Secs\":");
          sprintf(debugstr,"%d",hPacket->PGNData.TD_IC.Secs);
          strcat(pData,debugstr);

          strcat(pData,",\"Min\":");
          sprintf(debugstr,"%d",hPacket->PGNData.TD_IC.Min);
          strcat(pData,debugstr);

          strcat(pData,",\"Hrs\":");
          sprintf(debugstr,"%d",hPacket->PGNData.TD_IC.Hr);
          strcat(pData,debugstr);

          strcat(pData,",\"Month\":");
          sprintf(debugstr,"%d",hPacket->PGNData.TD_IC.Month);
          strcat(pData,debugstr);

          strcat(pData,",\"Day\":");
          sprintf(debugstr,"%d",hPacket->PGNData.TD_IC.Day);
          strcat(pData,debugstr);

          strcat(pData,",\"Year\":");
          sprintf(debugstr,"%d",hPacket->PGNData.TD_IC.Year);
          strcat(pData,debugstr);

          strcat(pData,",\"LocalMinOff\":");
          sprintf(debugstr,"%d",hPacket->PGNData.TD_IC.LocalMinuteOffset);
          strcat(pData,debugstr);

          strcat(pData,",\"LocalHrOffset\":");
          sprintf(debugstr,"%d}",hPacket->PGNData.TD_IC.LocalHrOffset);
          strcat(pData,debugstr);


          break;
        case PGN_65217:
          strcat(pData,",\"PGN_65217\":");

          strcat(pData,"{");
          strcat(pData,"\"TotalDistance\":");
          sprintf(debugstr,"%f}",hPacket->PGNData.HRVD_I.TotalDistance);
          strcat(pData,debugstr);



          break;
        case PGN_65276:
          strcat(pData,",\"PGN_65276\":");

          strcat(pData,"{");
          strcat(pData,"\"FuelLevel\":");
          sprintf(debugstr,"%f}",hPacket->PGNData.DD_X_IC.FuelLevel);
          strcat(pData,debugstr);


          break;
        case PGN_65226:
          strcat(pData,",\"PGN_65226\":");

          strcat(pData,"{");
          strcat(pData,"\"AmberWarn\":");
          sprintf(debugstr,"%d",hPacket->PGNData.DM1_Active.AmberWarningLamp);
          strcat(pData,debugstr);

          strcat(pData,",\"RedStop\":");
          sprintf(debugstr,"%d",hPacket->PGNData.DM1_Active.RedStopLamp);
          strcat(pData,debugstr);

          strcat(pData,",\"MalfunctionLamp\":");
          sprintf(debugstr,"%d",hPacket->PGNData.DM1_Active.MalfuncLamp);
          strcat(pData,debugstr);

          strcat(pData,",\"SPNLSB\":");
          sprintf(debugstr,"%d",hPacket->PGNData.DM1_Active.SPN8_LSB_SPN_EMS);
          strcat(pData,debugstr);

          strcat(pData,",\"SPN8_2ndByte\":");
          sprintf(debugstr,"%d",hPacket->PGNData.DM1_Active.SPN8_2ndB_SPN_EMS);
          strcat(pData,debugstr);

          strcat(pData,",\"FailureMode\":");
          sprintf(debugstr,"%d",hPacket->PGNData.DM1_Active.FailurMode);
          strcat(pData,debugstr);

          strcat(pData,",\"SPN3_MSB\":");
          sprintf(debugstr,"%d",hPacket->PGNData.DM1_Active.SPN3_MSB_EMS);
          strcat(pData,debugstr);

          strcat(pData,",\"OccurenceCount\":");
          sprintf(debugstr,"%d}",hPacket->PGNData.DM1_Active.OccurenceCount);
          strcat(pData,debugstr);




          break;
        case PGN_65265:
          strcat(pData,",\"PGN_65265\":");
          strcat(pData,"{");
          strcat(pData,"\"CCA\":");
          sprintf(debugstr,"%d",hPacket->PGNData.CCVS.CCA);
          strcat(pData,debugstr);

          strcat(pData,",\"CCES\":");
          sprintf(debugstr,"%d",hPacket->PGNData.CCVS.CCES);
          strcat(pData,debugstr);

          strcat(pData,",\"CCSS\":");
          sprintf(debugstr,"%d}",hPacket->PGNData.CCVS.CCSS);
          strcat(pData,debugstr);
          break;
        case PGN_61444:
          strcat(pData,",\"PGN_61444\":");
          strcat(pData,"{");
          strcat(pData,"\"EngineSpeed\":");
          sprintf(debugstr,"%0.2f",hPacket->PGNData.EEC1.EngineSpeed);
          strcat(pData,debugstr);

          strcat(pData,",\"EngineStartMode\":");
          sprintf(debugstr,"%d}",hPacket->PGNData.EEC1.EngineStartMode);
          strcat(pData,debugstr);
          break;
        case PGN_65253:
          strcat(pData,",\"PGN_65253\":");
          strcat(pData,"{");
          strcat(pData,"\"EngineOpHrs\":");
          sprintf(debugstr,"%0.2f}",hPacket->PGNData.EngineHours.EngineOpHrs);
          strcat(pData,debugstr);
          break;
        case PGN_65282:
          strcat(pData,",\"PGN_65282\":");
          strcat(pData,"{");
          strcat(pData,"\"PowerSupKeyPos\":");
          sprintf(debugstr,"%d",hPacket->PGNData.VP2.PowerKeyPos);
          strcat(pData,debugstr);

          strcat(pData,",\"AccPedalLowIdleSwitch\":");
          sprintf(debugstr,"%d",hPacket->PGNData.VP2.AccPedalIdelSwitch);
          strcat(pData,debugstr);

          strcat(pData,",\"VehicleSpeed\":");
          sprintf(debugstr,"%0.2f",hPacket->PGNData.VP2.VehicleSpeed);
          strcat(pData,debugstr);

          strcat(pData,",\"ControlerTrimMode\":");
          sprintf(debugstr,"%d}",hPacket->PGNData.VP2.ControlerTrimMode);
          strcat(pData,debugstr);
          break;
        case PGN_65263:
          strcat(pData,",\"PGN_65263\":");
          strcat(pData,"{");
          strcat(pData,"\"EngineOilPressure\":");
          sprintf(debugstr,"%d}",hPacket->PGNData.EELP1.EngineOilPressure);
          strcat(pData,debugstr);
          break;
        case PGN_65262:
          strcat(pData,",\"PGN_65262\":");
          strcat(pData,"{");
          strcat(pData,"\"EngineCoolantTemp\":");
          sprintf(debugstr,"%d}",hPacket->PGNData.ET1.EngineCoolantTemp);
          strcat(pData,debugstr);
          break;
        default:
          strcat(pData,",\"PGN_INVALID\":");
          break;
      }
    }
    pgn_index++;
    if(pgn_index >= MAXPGN)
      pgn_index = PGN_64777;
  }
  strcat(pData,"}");
  *Len = strlen((char *)hTMApp->Profile0.Profile0Data);
  //DPRINTF("Sending can packet ");
  //DPRINTF(pData);
  //DPRINTF("\r\n");
#endif
}

void MakeDTCPacket(tm_app_t *hTMApp, tm_packet_t *hPacket, int *Len)
{
  int i;

  hTMApp->Profile0.Profile0Data[0] = '\0';

  strcat(hTMApp->Profile0.Profile0Data,"^DTCA,");

  strcat(hTMApp->Profile0.Profile0Data,hTMApp->DBase.DevID);

  sprintf(debugstr,",%d",hPacket->SeqNum);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%2.5f",hPacket->Latitude);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%3.5f",hPacket->Longitude);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->UTC);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.DTC.FaultCodeCount);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  for(i = 0; i < hPacket->CANData.DTC.FaultCodeCount; i++)
  {
    sprintf(debugstr,",%d",hPacket->CANData.DTC.DM[i].SPN);
    strcat(hTMApp->Profile0.Profile0Data,debugstr);
    sprintf(debugstr,",%d",hPacket->CANData.DTC.DM[i].FailureMode);
    strcat(hTMApp->Profile0.Profile0Data,debugstr);
    sprintf(debugstr,",%d",hPacket->CANData.DTC.DM[i].ConversionMethod);
    strcat(hTMApp->Profile0.Profile0Data,debugstr);
    sprintf(debugstr,",%d",hPacket->CANData.DTC.DM[i].OccurenceCount);
    strcat(hTMApp->Profile0.Profile0Data,debugstr);
  }
  strcat(hTMApp->Profile0.Profile0Data,"#");
  *Len = strlen((char *)hTMApp->Profile0.Profile0Data);
}

void MakeDTCPPacket(tm_app_t *hTMApp, tm_packet_t *hPacket, int *Len)
{
  int i;

  hTMApp->Profile0.Profile0Data[0] = '\0';

  strcat(hTMApp->Profile0.Profile0Data,"^DTCP,");

  strcat(hTMApp->Profile0.Profile0Data,hTMApp->DBase.DevID);

  sprintf(debugstr,",%d",hPacket->SeqNum);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%2.5f",hPacket->Latitude);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%3.5f",hPacket->Longitude);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->UTC);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  sprintf(debugstr,",%d",hPacket->CANData.DTCP.FaultCodeCount);
  strcat(hTMApp->Profile0.Profile0Data,debugstr);

  for(i = 0; i < hPacket->CANData.DTCP.FaultCodeCount; i++)
  {
    sprintf(debugstr,",%d",hPacket->CANData.DTCP.DM[i].SPN);
    strcat(hTMApp->Profile0.Profile0Data,debugstr);
    sprintf(debugstr,",%d",hPacket->CANData.DTCP.DM[i].FailureMode);
    strcat(hTMApp->Profile0.Profile0Data,debugstr);
    sprintf(debugstr,",%d",hPacket->CANData.DTCP.DM[i].ConversionMethod);
    strcat(hTMApp->Profile0.Profile0Data,debugstr);
    sprintf(debugstr,",%d",hPacket->CANData.DTCP.DM[i].OccurenceCount);
    strcat(hTMApp->Profile0.Profile0Data,debugstr);
  }
  strcat(hTMApp->Profile0.Profile0Data,"#");
  *Len = strlen((char *)hTMApp->Profile0.Profile0Data);
}
void MainProcess(tm_app_t *hTMApp)
{
  tm_u8_t candatabuf[8];
  tm_u32_t rpgn;
  tm_u8_t status;

  if(USE_J1939 == TM_TRUE)
    j1939_update();

  /* Check periodic data */
  Profile0Process(hTMApp);

  /* OTA */
  Profile1Process(hTMApp);

  /* If any fault code occured send it */
  if(DTC.FaultCodeCount != 0)
  {
    ConstructPacket(hTMApp, PACKET_DTC);
    AddPacketToQueue(hTMApp);
    DTC.FaultCodeCount = 0;
  }
  if(DTCP.FaultCodeCount != 0)
  {
    ConstructPacket(hTMApp, PACKET_DTCP);
    AddPacketToQueue(hTMApp);
    DTCP.FaultCodeCount = 0;
  }
  /* Reset control */
  ResetCtrl(hTMApp);


  /* CAN Data */
  /* Send pgn requests */
  if(USE_J1939 == TM_TRUE)
  {
    hTMApp->CANTimerCount++;
    if(hTMApp->CANTimerCount == 100)
    {
      hTMApp->CANTimerCount = 0;
      /* Get PGN every 100msec */
      rpgn = msg_pgns[hTMApp->CANPGNIndex].pgn;

      /* Set rx filter for the pgn that you send  */
      SetPGNFilter(hTMApp, rpgn);

      hTMApp->TxMsg.pgn = J1939_PGN_REQUEST;

      hTMApp->TxMsg.buf = candatabuf;
      hTMApp->TxMsg.buf[0] = (tm_u8_t)rpgn;
      hTMApp->TxMsg.buf[1] = (tm_u8_t)(rpgn >> 8);
      hTMApp->TxMsg.buf[2] = (tm_u8_t)(rpgn >> 16);

      hTMApp->TxMsg.buf_len = 3;
      hTMApp->TxMsg.dst = msg_pgns[hTMApp->CANPGNIndex].dst;
      hTMApp->TxMsg.src = msg_pgns[hTMApp->CANPGNIndex].src;
      hTMApp->TxMsg.pri = msg_pgns[hTMApp->CANPGNIndex].pri;

      //DPRINTF("Sending message:");
      //sprintf(debugstr,"id = %x\r\n",rpgn);
      //DPRINTF(debugstr);
      //sprintf(debugstr,"rpgn = %x\n",rpgn);
      //DPRINTF(debugstr);
      //sprintf(debugstr,"data0 = %x\n",hTMApp->TxMsg.buf[0]);
      //DPRINTF(debugstr);
      //sprintf(debugstr,"data1 = %x\n",hTMApp->TxMsg.buf[1]);
      //DPRINTF(debugstr);
      //sprintf(debugstr,"data2 = %x\n",hTMApp->TxMsg.buf[2]);
      //DPRINTF(debugstr);

      j1939_tx_app(&hTMApp->TxMsg, &status);
      hTMApp->CANPGNIndex++;
      if(hTMApp->CANPGNIndex >= 14)
        hTMApp->CANPGNIndex = 0;
#ifdef PRINTCANDATA
      printpgn();
#endif
    }
  }

  ManageQueue(hTMApp);
}

void SetPGNFilter(tm_app_t *hTMApp, tm_u32_t pgn)
{
  tm_u32_t id;
  tm_u32_t idn_mask;

  id = 0x80000000 | (pgn << 8);
  if(pgn == 65226 || pgn == 59904)
    idn_mask = 0x80000000;
  else
    idn_mask = 0x80000000 | (0xFFFF << 8);
  CANRxFilterSet(id, idn_mask, 3);
}

void Profile0Process(tm_app_t *hTMApp)
{
  tm_profile0_t *hProfile0;
  int Len;

  hProfile0 = &hTMApp->Profile0;
  switch(hProfile0->P0State)
  {
    case P0_IDLE:
      Len = CheckQueue(hTMApp);
      if(Len != 0)
      {
        if(hTMApp->DBase.DebugLevel >= 2)
        {
          sprintf(debugstr,"Calling GsmConnect\r\n");
          DPRINTF(debugstr);
        }
        GsmConnect(&hTMApp->Gsm, PROFILE0);
        hProfile0->P0State = P0_WAITFORCONNECTION;
        hProfile0->bGsmConnected = TM_FALSE;
      }
      break;
    case P0_WAITFORCONNECTION:
      if(hProfile0->bGsmConnected == TM_TRUE)
      {
        if(hTMApp->DBase.DebugLevel >= 2)
        {
          sprintf(debugstr,"gsm connected 2\r\n");
          DPRINTF(debugstr);
        }
        hProfile0->P0State = P0_CONNECTED;
      }
      /* If not connected after a time out restart Gsm */
      break;
    case P0_CONNECTED:
      if(hProfile0->bGsmConnected == TM_FALSE)
      {
        /* If connection broke, connect again */
        hProfile0->P0State = P0_IDLE;
        break;
      }
      if(hProfile0->bDataSent == TM_FALSE)
      {
        if(hTMApp->DBase.DebugLevel >= 2)
        {
          sprintf(debugstr,"Sending unsent packet %d\r\n",hTMApp->Profile0.SendPacket.SeqNum);
          DPRINTF(debugstr);
        }
        MakePacket(hTMApp, &hTMApp->Profile0.SendPacket, &Len);
        GsmSendData(&hTMApp->Gsm, PROFILE0, (tm_u8_t *)hProfile0->Profile0Data, Len);
        hProfile0->P0State = P0_WAITFORACK;
        hProfile0->bDataSent = TM_FALSE;
      }
      else if(CheckForPacket(hTMApp) == TM_TRUE)
      {
        if(hTMApp->DBase.DebugLevel >= 2)
        {
          sprintf(debugstr,"Sending packet %d\r\n",hTMApp->Profile0.SendPacket.SeqNum);
          DPRINTF(debugstr);
        }
        MakePacket(hTMApp, &hTMApp->Profile0.SendPacket, &Len);
        GsmSendData(&hTMApp->Gsm, PROFILE0, (tm_u8_t *)hProfile0->Profile0Data, Len);
        hProfile0->P0State = P0_WAITFORACK;
        hProfile0->bDataSent = TM_FALSE;
      }
      break;
    case P0_WAITFORACK:
      if(hProfile0->bDataSent == TM_TRUE)
      {
        hProfile0->P0State = P0_CONNECTED;
      }
      if(hProfile0->bGsmConnected == TM_FALSE)
      {
        hProfile0->P0State = P0_IDLE;
      }
      break;
  }
}

void Profile1Process(tm_app_t *hTMApp)
{
  tm_profile1_t *hProfile1;
  int Len;

  hProfile1 = &hTMApp->Profile1;
  switch(hProfile1->P1Status)
  {
    case WAITFOROTACOMMAND:
      if(bStartOTA == TM_TRUE)
      {
        bStartOTA = TM_FALSE;
        DPRINTF("Sending start data\r\n");
        //strcat(hProfile1->Profile1Data,"^ID=353351050012962&ACK=<CheckVersion>");
        strcpy(hProfile1->Profile1Data,"^ID=");
        strcat(hProfile1->Profile1Data,hTMApp->DBase.DevID);
        strcat(hProfile1->Profile1Data,"&ACK=<CheckVersion>");
        Len = strlen(hProfile1->Profile1Data);
        GsmSendData(&hTMApp->Gsm, PROFILE1, (tm_u8_t *)hProfile1->Profile1Data, Len);
        hProfile1->P1Status = WAITFOROTACOMPLETE;
        hTMApp->Profile1.bDataReceived = TM_FALSE;
        OTAUgdInit(hTMApp, hTMApp->hOTAUgdTimeout);
      }
      break;
    case WAITFOROTACOMPLETE:
      HandleOTA(hTMApp);
      break;
    case WAITINITOTA:
      break;
    case WAITFORCRC:
      /* If received, send version info */
      break;
    case WAITFORVERSEND:
      break;
    case WAITFORPGMSTART:
      break;
    case GETBIN:
      break;
  }
}

void PeriodicTimerExpired(void *hApp)
{
  tm_app_t *hTMApp;

  hTMApp = hApp;

  if(hTMApp->DBase.DebugLevel >= 2)
  {
    DPRINTF("Periodic timer expired\r\n");
  }
  ConstructPacket(hTMApp, PACKET_PERIODIC);
  AddPacketToQueue(hTMApp);
  ConstructPacket(hTMApp, PACKET_CAN);
  AddPacketToQueue(hTMApp);
  ResetTimer(hTMApp->Profile0.hPeriodicPacketTimer, hTMApp->PowerUpUpdateRate);
}

void ConstructPacket(tm_app_t *hTMApp, tm_packettype_t PacketType)
{
  tm_packet_t *hPacket;
  tm_gpsdata_t *hGpsData;
  int i;

  hPacket = &hTMApp->Profile0.ConstructPacket;
  hGpsData = &hTMApp->Profile0.GpsData;

  GetGpsData(&hTMApp->Gps, &hTMApp->Profile0.GpsData);

  hPacket->WriteIndicator = DATAWRITEIND;
  hPacket->PacketType = PacketType;
  hPacket->Latitude = hGpsData->Latitude;
  hPacket->Longitude = hGpsData->Longitude;
  hPacket->Speed = hGpsData->Speed;
  hPacket->UTC = hGpsData->UTC;
  hPacket->SeqNum = ++hTMApp->Profile0.SeqNum;
  hPacket->bMainPwr = hTMApp->DinStatus[MAINPWR];
  hPacket->bIgnition = hTMApp->DinStatus[IGN];
  hPacket->bLive = TM_TRUE;
  hPacket->GPSOdo = 0.0;
  hPacket->AnalogIn1 = hTMApp->AnalogVal[AIN1];
  hPacket->AnalogIn2 = hTMApp->AnalogVal[AIN2];
  hPacket->AnalogIn3 = hTMApp->AnalogVal[AIN3];
  hPacket->IntBatt = hTMApp->AnalogVal[INTBAT];
  hPacket->VehBatt = hTMApp->AnalogVal[CARBAT];
  hPacket->Heading = hGpsData->Heading;
  hPacket->DIn1 = hTMApp->DinStatus[DIN1];
  hPacket->DIn2 = hTMApp->DinStatus[DIN2];
  hPacket->DIn3 = hTMApp->DinStatus[DIN3];
  hPacket->DIn4 = hTMApp->DinStatus[DIN4];
  hPacket->DIn5 = hTMApp->DinStatus[DIN5];

#if 0
  hPacket->J1939Data[PGN_64777].buf_len = J1939Data[PGN_64777].buf_len;
  for(i = 0; i < J1939Data[PGN_64777].buf_len; i++)
    hPacket->J1939Data[PGN_64777].buf[i] = J1939Data[PGN_64777].buf[i];

  hPacket->J1939Data[PGN_65292].buf_len = J1939Data[PGN_65292].buf_len;
  for(i = 0; i < J1939Data[PGN_65292].buf_len; i++)
    hPacket->J1939Data[PGN_65292].buf[i] = J1939Data[PGN_65292].buf[i];

  hPacket->J1939Data[PGN_65483].buf_len = J1939Data[PGN_65483].buf_len;
  for(i = 0; i < J1939Data[PGN_65483].buf_len; i++)
    hPacket->J1939Data[PGN_65483].buf[i] = J1939Data[PGN_65483].buf[i];

  hPacket->J1939Data[PGN_65254].buf_len = J1939Data[PGN_65254].buf_len;
  for(i = 0; i < J1939Data[PGN_65254].buf_len; i++)
    hPacket->J1939Data[PGN_65254].buf[i] = J1939Data[PGN_65254].buf[i];

  hPacket->J1939Data[PGN_65217].buf_len = J1939Data[PGN_65217].buf_len;
  for(i = 0; i < J1939Data[PGN_65217].buf_len; i++)
    hPacket->J1939Data[PGN_65217].buf[i] = J1939Data[PGN_65217].buf[i];

  hPacket->J1939Data[PGN_65276].buf_len = J1939Data[PGN_65276].buf_len;
  for(i = 0; i < J1939Data[PGN_65276].buf_len; i++)
    hPacket->J1939Data[PGN_65276].buf[i] = J1939Data[PGN_65276].buf[i];

  hPacket->J1939Data[PGN_65226].buf_len = J1939Data[PGN_65226].buf_len;
  for(i = 0; i < J1939Data[PGN_65226].buf_len; i++)
    hPacket->J1939Data[PGN_65226].buf[i] = J1939Data[PGN_65226].buf[i];

  hPacket->J1939Data[PGN_65265].buf_len = J1939Data[PGN_65265].buf_len;
  for(i = 0; i < J1939Data[PGN_65265].buf_len; i++)
    hPacket->J1939Data[PGN_65265].buf[i] = J1939Data[PGN_65265].buf[i];

  hPacket->J1939Data[PGN_61444].buf_len = J1939Data[PGN_61444].buf_len;
  for(i = 0; i < J1939Data[PGN_61444].buf_len; i++)
    hPacket->J1939Data[PGN_61444].buf[i] = J1939Data[PGN_61444].buf[i];

  hPacket->J1939Data[PGN_65253].buf_len = J1939Data[PGN_65253].buf_len;
  for(i = 0; i < J1939Data[PGN_65253].buf_len; i++)
    hPacket->J1939Data[PGN_65253].buf[i] = J1939Data[PGN_65253].buf[i];

  hPacket->J1939Data[PGN_65282].buf_len = J1939Data[PGN_65282].buf_len;
  for(i = 0; i < J1939Data[PGN_65282].buf_len; i++)
    hPacket->J1939Data[PGN_65282].buf[i] = J1939Data[PGN_65282].buf[i];

  hPacket->J1939Data[PGN_65263].buf_len = J1939Data[PGN_65263].buf_len;
  for(i = 0; i < J1939Data[PGN_65263].buf_len; i++)
    hPacket->J1939Data[PGN_65263].buf[i] = J1939Data[PGN_65263].buf[i];

  hPacket->J1939Data[PGN_65262].buf_len = J1939Data[PGN_65262].buf_len;
  for(i = 0; i < J1939Data[PGN_65262].buf_len; i++)
    hPacket->J1939Data[PGN_65262].buf[i] = J1939Data[PGN_65262].buf[i];

#endif
  if(PacketType == PACKET_CAN)
  {
    hPacket->CANData.PGNData.HRLFC.hrlfc = PGNData.HRLFC.hrlfc;
    hPacket->CANData.PGNData.HRLFC.TripFuel = PGNData.HRLFC.TripFuel;

    hPacket->CANData.PGNData.VP12.SweetSpot  = PGNData.VP12.SweetSpot;
    hPacket->CANData.PGNData.VP12.TopGear   = PGNData.VP12.TopGear;

    hPacket->CANData.PGNData.VP203.SweetSpotPercent   = PGNData.VP203.SweetSpotPercent;

    hPacket->CANData.PGNData.TD_IC.Secs   = PGNData.TD_IC.Secs;
    hPacket->CANData.PGNData.TD_IC.Min   = PGNData.TD_IC.Min;
    hPacket->CANData.PGNData.TD_IC.Hr   = PGNData.TD_IC.Hr;
    hPacket->CANData.PGNData.TD_IC.Month   = PGNData.TD_IC.Month;
    hPacket->CANData.PGNData.TD_IC.Day   = PGNData.TD_IC.Day;
    hPacket->CANData.PGNData.TD_IC.Year   = PGNData.TD_IC.Year;
    hPacket->CANData.PGNData.TD_IC.LocalMinuteOffset   = PGNData.TD_IC.LocalMinuteOffset;
    hPacket->CANData.PGNData.TD_IC.LocalHrOffset   = PGNData.TD_IC.LocalHrOffset;

    hPacket->CANData.PGNData.HRVD_I.TotalDistance   = PGNData.HRVD_I.TotalDistance;

    hPacket->CANData.PGNData.DD_X_IC.FuelLevel   = PGNData.DD_X_IC.FuelLevel;

    hPacket->CANData.PGNData.DM1_Active.AmberWarningLamp   = PGNData.DM1_Active.AmberWarningLamp;
    hPacket->CANData.PGNData.DM1_Active.RedStopLamp   = PGNData.DM1_Active.RedStopLamp;
    hPacket->CANData.PGNData.DM1_Active.MalfuncLamp   = PGNData.DM1_Active.MalfuncLamp;
    hPacket->CANData.PGNData.DM1_Active.FlashMalfuctionLamp   = PGNData.DM1_Active.FlashMalfuctionLamp;
    hPacket->CANData.PGNData.DM1_Active.SPN8_LSB_SPN_EMS   = PGNData.DM1_Active.SPN8_LSB_SPN_EMS;
    hPacket->CANData.PGNData.DM1_Active.SPN8_2ndB_SPN_EMS   = PGNData.DM1_Active.SPN8_2ndB_SPN_EMS;
    hPacket->CANData.PGNData.DM1_Active.FailurMode   = PGNData.DM1_Active.FailurMode;
    hPacket->CANData.PGNData.DM1_Active.SPN3_MSB_EMS   = PGNData.DM1_Active.SPN3_MSB_EMS;
    hPacket->CANData.PGNData.DM1_Active.OccurenceCount   = PGNData.DM1_Active.OccurenceCount;

    hPacket->CANData.PGNData.CCVS.CCA   = PGNData.CCVS.CCA;
    hPacket->CANData.PGNData.CCVS.CCES   = PGNData.CCVS.CCES;
    hPacket->CANData.PGNData.CCVS.CCSS   = PGNData.CCVS.CCSS;

    hPacket->CANData.PGNData.EEC1.EngineSpeed   = PGNData.EEC1.EngineSpeed;
    hPacket->CANData.PGNData.EEC1.EngineStartMode   = PGNData.EEC1.EngineStartMode;

    hPacket->CANData.PGNData.EngineHours.EngineOpHrs   = PGNData.EngineHours.EngineOpHrs;

    hPacket->CANData.PGNData.VP2.PowerKeyPos   = PGNData.VP2.PowerKeyPos;
    hPacket->CANData.PGNData.VP2.AccPedalIdelSwitch   = PGNData.VP2.AccPedalIdelSwitch;
    hPacket->CANData.PGNData.VP2.AccPedalPosition   = PGNData.VP2.AccPedalPosition;
    hPacket->CANData.PGNData.VP2.VehicleSpeed   = PGNData.VP2.VehicleSpeed;
    hPacket->CANData.PGNData.VP2.ControlerTrimMode   = PGNData.VP2.ControlerTrimMode;

    hPacket->CANData.PGNData.EELP1.EngineOilPressure   = PGNData.EELP1.EngineOilPressure;

    hPacket->CANData.PGNData.ET1.EngineCoolantTemp   = PGNData.ET1.EngineCoolantTemp;
  }
  if(PacketType == PACKET_DTC)
  {
    hPacket->CANData.DTC.FaultCodeCount = DTC.FaultCodeCount;

    for(i = 0; i < hPacket->CANData.DTC.FaultCodeCount; i++)
    {
      hPacket->CANData.DTC.DM[i].SPN   = DTC.DM[i].SPN;
      hPacket->CANData.DTC.DM[i].FailureMode   = DTC.DM[i].FailureMode;
      hPacket->CANData.DTC.DM[i].ConversionMethod   = DTC.DM[i].ConversionMethod;
      hPacket->CANData.DTC.DM[i].OccurenceCount   = DTC.DM[i].OccurenceCount;
    }
  }
  if(PacketType == PACKET_DTCP)
  {
    hPacket->CANData.DTCP.FaultCodeCount = DTCP.FaultCodeCount;

    for(i = 0; i < hPacket->CANData.DTCP.FaultCodeCount; i++)
    {
      hPacket->CANData.DTCP.DM[i].SPN   = DTCP.DM[i].SPN;
      hPacket->CANData.DTCP.DM[i].FailureMode   = DTCP.DM[i].FailureMode;
      hPacket->CANData.DTCP.DM[i].ConversionMethod   = DTCP.DM[i].ConversionMethod;
      hPacket->CANData.DTCP.DM[i].OccurenceCount   = DTCP.DM[i].OccurenceCount;
    }
  }
}

void AddPacketToQueue(tm_app_t *hTMApp)
{
  tm_profile0_t *hProfile;
  int diff;

  hProfile = &hTMApp->Profile0;

#if 0
  if(hProfile->bNewPacket == TM_FALSE)
  {
    /* If NewPacket is empty copy  the latest packet to NewPacket */
    memcpy(&hProfile->NewPacket,&hProfile->ConstructPacket, sizeof(tm_packet_t));
    hProfile->bNewPacket = TM_TRUE;
    sprintf(debugstr,"packet           -> new packet 1 seq num = %d\r\n",hProfile->NewPacket.SeqNum);
    DPRINTF(debugstr);
  }
  else
  {
    /* If NewPacket is not empty, push NewPacket to queue and copy the latest packet to NewPacket */
    diff = CheckQueue(hTMApp);
    if(diff > PACKETQUEUETHRESHOLD || (IsLogEmpty(&hTMApp->FlashTask) == TM_FALSE) || (hProfile->FlashPacketWrIndex != hProfile->FlashPacketRdIndex))
    {
      /* Push it to flash queue */
      memcpy(&hProfile->FlashQueue[hProfile->FlashPacketWrIndex],&hProfile->NewPacket, sizeof(tm_packet_t));
      sprintf(debugstr,"packet New Packet ->  flashqueue seq num = %d\r\n",hProfile->NewPacket.SeqNum);
      DPRINTF(debugstr);
      INCCIRCULARINDEX(hProfile->FlashPacketWrIndex, FLASHPACKETQUEUELEN);
      memcpy(&hProfile->NewPacket,&hProfile->ConstructPacket, sizeof(tm_packet_t));
      sprintf(debugstr,"packet            -> New Packet 2 seq num = %d\r\n",hProfile->ConstructPacket.SeqNum);
      DPRINTF(debugstr);
    }
    else
    {
      /* Copy to queue */
      memcpy(&hProfile->PacketQueue[hProfile->PacketWrIndex],&hProfile->NewPacket, sizeof(tm_packet_t));
      sprintf(debugstr,"packet New Packet -> Main queue seq num = %d\r\n",hProfile->NewPacket.SeqNum);
      DPRINTF(debugstr);
      INCCIRCULARINDEX(hProfile->PacketWrIndex, PACKETQUEUELEN);
      memcpy(&hProfile->NewPacket,&hProfile->ConstructPacket, sizeof(tm_packet_t));
      sprintf(debugstr,"packet            -> New Packet 3 seq num = %d\r\n",hProfile->ConstructPacket.SeqNum);
      DPRINTF(debugstr);
    }
  }
#endif
    diff = CheckQueue(hTMApp);
    if(diff > PACKETQUEUETHRESHOLD || (IsLogEmpty(&hTMApp->FlashTask) == TM_FALSE) || (hProfile->FlashPacketWrIndex != hProfile->FlashPacketRdIndex))
    {
      /* Push it to flash queue */
      memcpy(&hProfile->FlashQueue[hProfile->FlashPacketWrIndex],&hProfile->ConstructPacket, sizeof(tm_packet_t));
      INCCIRCULARINDEX(hProfile->FlashPacketWrIndex, FLASHPACKETQUEUELEN);
    }
    else
    {
      /* Copy to queue */
      memcpy(&hProfile->PacketQueue[hProfile->PacketWrIndex],&hProfile->ConstructPacket, sizeof(tm_packet_t));
      INCCIRCULARINDEX(hProfile->PacketWrIndex, PACKETQUEUELEN);
    }

  ManageQueue(hTMApp);
}

tm_bool_t CheckForPacket(tm_app_t *hTMApp)
{
  tm_profile0_t *hProfile;

  hProfile = &hTMApp->Profile0;

#if 0
  if(hProfile->bNewPacket == TM_TRUE)
  {
    memcpy(&hProfile->SendPacket,&hProfile->NewPacket, sizeof(tm_packet_t));
    hProfile->bNewPacket = TM_FALSE;
    //sprintf(debugstr,"CheckLoggedPackets 1 wr ptr = %d rd ptr = %d\r\n",hProfile->PacketWrIndex,hProfile->PacketRdIndex);
    //DPRINTF(debugstr);

    return TM_TRUE;
  }
  else
  {
    if(hProfile->PacketWrIndex != hProfile->PacketRdIndex)
    {
      memcpy(&hProfile->SendPacket,&hProfile->PacketQueue[hProfile->PacketRdIndex], sizeof(tm_packet_t));
      INCCIRCULARINDEX(hProfile->PacketRdIndex, PACKETQUEUELEN);
      //sprintf(debugstr,"CheckLoggedPackets 2 wr ptr = %d rd ptr = %d\r\n",hProfile->PacketWrIndex,hProfile->PacketRdIndex);
      //DPRINTF(debugstr);

      return TM_TRUE;
    }
  }
#endif
  if(hProfile->PacketWrIndex != hProfile->PacketRdIndex)
  {
    memcpy(&hProfile->SendPacket,&hProfile->PacketQueue[hProfile->PacketRdIndex], sizeof(tm_packet_t));
    INCCIRCULARINDEX(hProfile->PacketRdIndex, PACKETQUEUELEN);

    return TM_TRUE;
  }
  else
    return TM_FALSE;
}

void GetCANData(tm_app_t *hTMApp)
{
  if(USE_J1939 == TM_FALSE)
  {
    CAN_BufferRead(hTMApp->RxCANData,&hTMApp->CanRxLen);
#if 1
    if(hTMApp->CanRxLen != 0)
    {
      int i,j;
      i = 0;
      sprintf(debugstr,"Received objects:%d\r\n",hTMApp->CanRxLen);
      DPRINTF(debugstr);
      while(hTMApp->CanRxLen--)
      {
        sprintf(debugstr,"Recived bytes = %d",hTMApp->RxCANData[i].buf_len);
        DPRINTF(debugstr);
        j = 0;
        if(hTMApp->RxCANData[i].id & 0x80000000)
        {
          /* Reset MSB bit which indicates Extended format */
          hTMApp->RxCANData[i].id &= 0x7FFFFFFF;
        }
        sprintf(debugstr,"\r\nid = %x",hTMApp->RxCANData[i].id);
        DPRINTF(debugstr);
        DPRINTF(" Data: ");
        while(hTMApp->RxCANData[i].buf_len--)
        {
          sprintf(debugstr,"%02x ",hTMApp->RxCANData[i].buf[j]);
          DPRINTF(debugstr);
          j++;
        }
        i++;
        DPRINTF("\r\n");
      }
    }
  }
#endif
}

void j1939app_init( void )
{
}

uint8_t j1939app_sa_get( void )
{
  return 0;
}

void printpgn(void)
{
  int i;
  
#if 1
  for(i = PGN_64777; i < MAXPGN; i++)
  {
  sprintf(debugstr,"Rx PGN = %x, buf_len = %d  ",msg_pgns[i].pgn, J1939Data[i].buf_len);
  DPRINTF(debugstr);
  sprintf(debugstr,"Data = %d, %d, %d, %d, %d, %d, %d, %d\r\n",J1939Data[i].buf[0],J1939Data[i].buf[1],J1939Data[i].buf[2],J1939Data[i].buf[3],J1939Data[i].buf[4],J1939Data[i].buf[5],J1939Data[i].buf[6],J1939Data[i].buf[7]);
  DPRINTF(debugstr);
  }
#endif
#if 0
  j1939_t msg_tmp;

  for(i = PGN_64777; i < MAXPGN;i++)
  {
    switch(i)
    {
      case PGN_64777:
        msg_tmp.pgn = 0xfd09;
        msg_tmp.buf[0] = 241;
        msg_tmp.buf[1] = 43;
        msg_tmp.buf[2] = 40;
        msg_tmp.buf[3] = 0;
        msg_tmp.buf[4] = 241;
        msg_tmp.buf[5] = 43;
        msg_tmp.buf[6] = 40;
        msg_tmp.buf[7] = 0;
        break;
      case PGN_65292:
        msg_tmp.pgn = 0xff0c;
        msg_tmp.buf[0] = 250;
        msg_tmp.buf[1] = 23;
        msg_tmp.buf[2] = 140;
        msg_tmp.buf[3] = 248;
        msg_tmp.buf[4] = 0;
        msg_tmp.buf[5] = 255;
        msg_tmp.buf[6] = 192;
        msg_tmp.buf[7] = 188;
        break;
      case PGN_65483:
        msg_tmp.pgn = 0xffcb;
        msg_tmp.buf[0] = 255;
        msg_tmp.buf[1] = 255;
        msg_tmp.buf[2] = 255;
        msg_tmp.buf[3] = 255;
        msg_tmp.buf[4] = 255;
        msg_tmp.buf[5] = 255;
        msg_tmp.buf[6] = 26;
        msg_tmp.buf[7] = 26;
        break;
      case PGN_65254:
        msg_tmp.pgn = 0xfee6;
        msg_tmp.buf[0] = 196;
        msg_tmp.buf[1] = 18;
        msg_tmp.buf[2] = 11;
        msg_tmp.buf[3] = 10;
        msg_tmp.buf[4] = 72;
        msg_tmp.buf[5] = 55;
        msg_tmp.buf[6] = 125;
        msg_tmp.buf[7] = 125;
        break;
      case PGN_65217:
        msg_tmp.pgn = 0xfec1;
        msg_tmp.buf[0] = 13;
        msg_tmp.buf[1] = 46;
        msg_tmp.buf[2] = 172;
        msg_tmp.buf[3] = 0;
        msg_tmp.buf[4] = 122;
        msg_tmp.buf[5] = 111;
        msg_tmp.buf[6] = 4;
        msg_tmp.buf[7] = 0;
        break;
      case PGN_65276:
        msg_tmp.pgn = 0xfefc;
        msg_tmp.buf[0] = 255;
        msg_tmp.buf[1] = 250;
        msg_tmp.buf[2] = 255;
        msg_tmp.buf[3] = 255;
        msg_tmp.buf[4] = 255;
        msg_tmp.buf[5] = 255;
        msg_tmp.buf[6] = 255;
        msg_tmp.buf[7] = 255;
        break;
      case PGN_65226:
        msg_tmp.pgn = 0xfeca;
        msg_tmp.buf[0] = 0;
        msg_tmp.buf[1] = 255;
        msg_tmp.buf[2] = 0;
        msg_tmp.buf[3] = 0;
        msg_tmp.buf[4] = 0;
        msg_tmp.buf[5] = 0;
        msg_tmp.buf[6] = 255;
        msg_tmp.buf[7] = 255;
        break;
      case PGN_65265:
        msg_tmp.pgn = 0xfef1;
        msg_tmp.buf[0] = 243;
        msg_tmp.buf[1] = 0;
        msg_tmp.buf[2] = 0;
        msg_tmp.buf[3] = 16;
        msg_tmp.buf[4] = 192;
        msg_tmp.buf[5] = 0;
        msg_tmp.buf[6] = 255;
        msg_tmp.buf[7] = 255;
        break;
      case PGN_61444:
        msg_tmp.pgn = 0xf004;
        msg_tmp.buf[0] = 240;
        msg_tmp.buf[1] = 125;
        msg_tmp.buf[2] = 137;
        msg_tmp.buf[3] = 190;
        msg_tmp.buf[4] = 18;
        msg_tmp.buf[5] = 0;
        msg_tmp.buf[6] = 243;
        msg_tmp.buf[7] = 255;
        break;
      case PGN_65253:
        msg_tmp.pgn = 0xfee5;
        msg_tmp.buf[0] = 198;
        msg_tmp.buf[1] = 16;
        msg_tmp.buf[2] = 0;
        msg_tmp.buf[3] = 0;
        msg_tmp.buf[4] = 215;
        msg_tmp.buf[5] = 61;
        msg_tmp.buf[6] = 0;
        msg_tmp.buf[7] = 0;
        break;
      case PGN_65282:
        msg_tmp.pgn = 0xff02;
        msg_tmp.buf[0] = 255;
        msg_tmp.buf[1] = 255;
        msg_tmp.buf[2] = 63;
        msg_tmp.buf[3] = 255;
        msg_tmp.buf[4] = 255;
        msg_tmp.buf[5] = 255;
        msg_tmp.buf[6] = 255;
        msg_tmp.buf[7] = 255;
        break;
      case PGN_65263:
        msg_tmp.pgn = 0xfeef;
        msg_tmp.buf[0] = 255;
        msg_tmp.buf[1] = 255;
        msg_tmp.buf[2] = 255;
        msg_tmp.buf[3] = 83;
        msg_tmp.buf[4] = 255;
        msg_tmp.buf[5] = 255;
        msg_tmp.buf[6] = 255;
        msg_tmp.buf[7] = 250;
        break;
      case PGN_65262:
        msg_tmp.pgn = 0xfeee;
        msg_tmp.buf[0] = 109;
        msg_tmp.buf[1] = 255;
        msg_tmp.buf[2] = 255;
        msg_tmp.buf[3] = 255;
        msg_tmp.buf[4] = 255;
        msg_tmp.buf[5] = 255;
        msg_tmp.buf[6] = 255;
        msg_tmp.buf[7] = 255;
        break;
    }
    PGNHandle(&PGNData, &msg_tmp);
  }
  //DPRINTF("Callind pgnhandle\r\n");

#endif
}
void j1939app_process( j1939_t *msg )
{

  //sprintf(debugstr,"Rx PGN = 0x%x, buf_len = %d  ",msg->pgn, msg->buf_len);
  //DPRINTF(debugstr);
  //sprintf(debugstr,"Data = %d, %d, %d, %d, %d, %d, %d, %d\r\n",msg->buf[0],msg->buf[1],msg->buf[2],msg->buf[3],msg->buf[4],msg->buf[5],msg->buf[6],msg->buf[7]);
  //DPRINTF(debugstr);

  PGNHandle(&DTC, &DTCP, &PGNData, msg);
#if 0
  switch(msg->pgn)
  {
    case 64777:
      J1939Data[PGN_64777].buf_len = msg->buf_len;
      for(i = 0; i < msg->buf_len; i++)
        J1939Data[PGN_64777].buf[i] = msg->buf[i];
      break;
    case 65292:
      J1939Data[PGN_65292].buf_len = msg->buf_len;
      for(i = 0; i < msg->buf_len; i++)
        J1939Data[PGN_65292].buf[i] = msg->buf[i];
      break;
    case 65483:
      J1939Data[PGN_65483].buf_len = msg->buf_len;
      for(i = 0; i < msg->buf_len; i++)
        J1939Data[PGN_65483].buf[i] = msg->buf[i];
      break;
    case 65254:
      J1939Data[PGN_65254].buf_len = msg->buf_len;
      for(i = 0; i < msg->buf_len; i++)
        J1939Data[PGN_65254].buf[i] = msg->buf[i];
      break;
    case 65217:
      J1939Data[PGN_65217].buf_len = msg->buf_len;
      for(i = 0; i < msg->buf_len; i++)
        J1939Data[PGN_65217].buf[i] = msg->buf[i];
      break;
    case 65276:
      J1939Data[PGN_65276].buf_len = msg->buf_len;
      for(i = 0; i < msg->buf_len; i++)
        J1939Data[PGN_65276].buf[i] = msg->buf[i];
      break;
    case 65226:
      if(msg->buf_len != 8)
      {
        sprintf(debugstr,"********************  Buflen is %d ********************  \r\n",msg->buf_len);
        DPRINTF(debugstr);
      }
      J1939Data[PGN_65226].buf_len = msg->buf_len;
      for(i = 0; i < msg->buf_len; i++)
        J1939Data[PGN_65226].buf[i] = msg->buf[i];
      break;
    case 65265:
      J1939Data[PGN_65265].buf_len = msg->buf_len;
      for(i = 0; i < msg->buf_len; i++)
        J1939Data[PGN_65265].buf[i] = msg->buf[i];
      break;
    case 61444:
      J1939Data[PGN_61444].buf_len = msg->buf_len;
      for(i = 0; i < msg->buf_len; i++)
        J1939Data[PGN_61444].buf[i] = msg->buf[i];
      break;
    case 65253:
      J1939Data[PGN_65253].buf_len = msg->buf_len;
      for(i = 0; i < msg->buf_len; i++)
        J1939Data[PGN_65253].buf[i] = msg->buf[i];
      break;
    case 65282:
      J1939Data[PGN_65282].buf_len = msg->buf_len;
      for(i = 0; i < msg->buf_len; i++)
        J1939Data[PGN_65282].buf[i] = msg->buf[i];
      break;
    case 65263:
      J1939Data[PGN_65263].buf_len = msg->buf_len;
      for(i = 0; i < msg->buf_len; i++)
        J1939Data[PGN_65263].buf[i] = msg->buf[i];
      break;
    case 65262:
      J1939Data[PGN_65262].buf_len = msg->buf_len;
      for(i = 0; i < msg->buf_len; i++)
        J1939Data[PGN_65262].buf[i] = msg->buf[i];
      break;
  }
#endif
}

/* Called by j1939_update @ 10msec */
void j1939app_update( void )
{
  /* Send any PGN requests here */
}

void GetDigitalInputStatus(tm_app_t *hTMApp)
{
  tm_u8_t DigitalInputStatus[MAXDIN];
  int i;

  for(i = 0; i < MAXDIN; i++)
  {
    DigitalInputStatus[i] = hTMApp->DinStatus[i];
  }

  UpdateDigitalInputs(hTMApp->DinStatus);

  for(i = 0; i < MAXDIN; i++)
  {
    if(DigitalInputStatus[i] != hTMApp->DinStatus[i])
    {
      switch(i)
      {
        case DIN1:
          break;
        case DIN2:
          break;
        case DIN3:
          if(hTMApp->DinStatus[i] == TM_TRUE)
          {
            DPRINTF("SOS on packet\r\n");
            ConstructPacket(hTMApp, PACKET_SOS);
            AddPacketToQueue(hTMApp);
            /* Buzzer ON */
            IO_PDR04.byte |= 0x08;
          }
          else
          {
            /* Buzzer OFF */
            IO_PDR04.byte &= (~0x08);
          }
          break;
        case DIN4:
          break;
        case DIN5:
          break;
        case MAINPWR:
          if(hTMApp->DinStatus[i] == TM_FALSE)
          {
            ConstructPacket(hTMApp, PACKET_MAINPWROFF);
          }
          break;
        case IGN:
          if(hTMApp->DinStatus[i] == TM_TRUE)
          {
            DPRINTF("Ignition on packet\r\n");
            ConstructPacket(hTMApp, PACKET_IGNON);
            AddPacketToQueue(hTMApp);
          }
          else
          {
            DPRINTF("Ignition off packet\r\n");
            ConstructPacket(hTMApp, PACKET_IGNOFF);
            AddPacketToQueue(hTMApp);
          }
          break;
      }
    }
  }
}

void GetAnalogVal(tm_app_t *hTMApp)
{
  GetAnalogIn(hTMApp->AnalogVal);
}

void Profile0DataGet(tm_u8_t *pData, tm_u16_t Len, void *pUserData)
{

}

void Profile1DataGet(tm_u8_t *pData, tm_u16_t Len, void *pUserData)
{
  tm_app_t *hTMApp;
  int i;
  tm_u8_t byte;

  hTMApp = (tm_app_t *)pUserData;
  memcpy(hTMApp->Profile1.Profile1RxData,pData, Len);
  hTMApp->Profile1.bDataReceived = TM_TRUE;
  for(i = 0; i < Len; i++)
  {
    byte = pData[i];
    //sprintf(debugstr,"%x\r\n",byte);
    //DPRINTF(debugstr);
    OTAUpgradeProcess(hTMApp, 1, byte);
  }

}

static void OTATimerExpired(void *pUserData)
{
  tm_app_t *hApp;

  hApp = (tm_app_t *)pUserData;
  DPRINTF("OTA UPGRADE FAILED TIMEOUT\r\n");
  hApp->Profile1.P1Status = WAITFOROTACOMMAND;
}

void OTAUgdInit(tm_app_t *hTMApp, tm_timer_t *hSerialUgdTimeout)
{
  tm_serialugd_t *hSerialUgd;

  hSerialUgd = &hTMApp->OTAUgd;

  hSerialUgd->ProgramState = UGD_QUERY;
  hSerialUgd->FlashState = UGDFLASH_IDLE;
  hSerialUgd->UgdMode = LOOKFOR_UGD;
  hSerialUgd->hFlashTask = &hTMApp->FlashTask;
  hSerialUgd->CmdStringIndex = 0;
  hSerialUgd->hSerialUgdTimeout = hSerialUgdTimeout;
  hSerialUgd->bDLEFound = TM_FALSE;
  ResetTimer(hSerialUgd->hSerialUgdTimeout, 120);
}

void OTAUpgradeProcess(tm_app_t *hTMApp, tm_u16_t Size, tm_u8_t Byte)
{
  tm_u16_t i=0;
  tm_u8_t j;
  tm_u8_t CmdSize;

  tm_serialugd_t *hSerialUgd;

  hSerialUgd = &hTMApp->OTAUgd;

  CmdSize = sizeof(PGMCOMMAND)-1;

  for (i = 0; i < Size ; i++)
  {
    switch (hSerialUgd->UgdMode)
    {
      case LOOKFOR_UGD:
        hSerialUgd->CmdString[hSerialUgd->CmdStringIndex]=(tm_u8_t)toupper(Byte);
        if (hSerialUgd->CmdStringIndex >= (CmdSize-1))
        {
          if (!strncmp((const char *)hSerialUgd->CmdString, PGMCOMMAND, CmdSize))
          {
            //DPRINTF("DEBUG GPRSUGD_MODE\r\n");
            hSerialUgd->UgdMode = UGDDATA;
            hSerialUgd->UGDDataIndex = 0;
            hSerialUgd->bDLEFound = TM_FALSE;
          }
          else
          {
            for (j = 0; j < CmdSize; j++)
            {
              hSerialUgd->CmdString[j] = hSerialUgd->CmdString[j+1];
            }
          }
        }
        else
          hSerialUgd->CmdStringIndex++;
        break;
      case UGDDATA:
        if(Byte == PROT_DLE)
        {
          if(hSerialUgd->bDLEFound == TM_TRUE)
          {
            hSerialUgd->bDLEFound = TM_FALSE;
            hSerialUgd->UGDData[hSerialUgd->UGDDataIndex++] = Byte;
            if (hSerialUgd->UGDDataIndex>=MAXUGDDATALEN)
            {
              hSerialUgd->UGDDataIndex=0;
              hSerialUgd->UgdMode=LOOKFOR_UGD;
              DPRINTF("SYNC ERROR\r\n");
            }

          }
          else
          {
            hSerialUgd->bDLEFound = TM_TRUE;
          }
        }
        else if (Byte == PROT_EOM)
        {
          if(hSerialUgd->bDLEFound == TM_TRUE)
          {
            hSerialUgd->UgdMode = LOOKFOR_UGD;
            ProgramToFlash(hTMApp);
          }
          else
          {
            hSerialUgd->UGDData[hSerialUgd->UGDDataIndex++] = Byte;
            if (hSerialUgd->UGDDataIndex>=MAXUGDDATALEN)
            {
              hSerialUgd->UGDDataIndex=0;
              hSerialUgd->UgdMode=LOOKFOR_UGD;
              DPRINTF("SYNC ERROR\r\n");
            }
          }
        }
        else
        {
          hSerialUgd->UGDData[hSerialUgd->UGDDataIndex++] = Byte;
          if (hSerialUgd->UGDDataIndex>=MAXUGDDATALEN)
          {
            hSerialUgd->UGDDataIndex=0;
            hSerialUgd->UgdMode=LOOKFOR_UGD;
            DPRINTF("SYNC ERROR\r\n");
          }
        }
        break;
      default:
        break;
    }
  }
}

static tm_bool_t ProgramToFlash(tm_app_t *hTMApp)
{
  tm_u16_t MsgLength;
  tm_u16_t i;
  tm_u16_t FrameRxCRC;
  tm_u8_t MsgID;
  tm_u16_t FrameCalcCRC=0;
  tm_serialugd_t *hSerialUgd;

  hSerialUgd = &hTMApp->OTAUgd;


  ResetTimer(hSerialUgd->hSerialUgdTimeout, 120);

  MsgID = hSerialUgd->UGDData[0];
#if 0
  if(MsgID == MSG_UGDSTART_ID )
  {
    //UART0QueueData((tm_u8_t *)"Received bytes* ",vts_strlen("Received bytes* "));
    for(i = 0; i < hSerialUgd->UGDDataIndex; i++)
    {
      itoa(hSerialUgd->UGDData[i],tmpbuf);
      //UART0QueueData((tm_u8_t *)tmpbuf,vts_strlen(tmpbuf));
      //UART0QueueData((tm_u8_t *)" ",vts_strlen(" "));
    }
    //UART0QueueData((tm_u8_t *)"*Received bytes\r\n",vts_strlen("*Received bytes\r\n"));
  }
#endif
  MsgLength = (tm_u16_t)(hSerialUgd->UGDData[1] << 8 | hSerialUgd->UGDData[2]);

  MsgLength -= 3;

  //hSerialUgd->bHandleUGD = TM_TRUE;
  /* Reset soft download timer timer */
  if (!hSerialUgd->UGDDataIndex)
    return TM_FALSE;

  /* Get CRC from received block */
  FrameRxCRC= (tm_u16_t)((hSerialUgd->UGDData[(MsgLength+3)] << 8) |(hSerialUgd->UGDData[(MsgLength + 4)] ));

  /* Calculate CRC from the received block */
  for(i=0; i< (hSerialUgd->UGDDataIndex-2); i++)
  {
    FrameCalcCRC += (tm_u16_t)hSerialUgd->UGDData[i];
    //FrameCalcCRC &= 0xFFFF;
  }
  //if(hSerialUgd->hSysDbase->bGSMDebugEnable==TM_TRUE)
  //UART0QueueData((tm_u8_t *)"DEBUG OCRC ",vts_strlen("OCRC "));
  //vts_itoa((int)(FrameCalcCRC & 0xFFFF),tmpbuf);
  //if(hSerialUgd->hSysDbase->bGSMDebugEnable==TM_TRUE)
  //{
    //UART0QueueData((tm_u8_t *)tmpbuf,vts_strlen(tmpbuf));
    //UART0QueueData((tm_u8_t *)"DEBUG OCRC\r\n",vts_strlen("DEBUG OCRC\r\n"));
  //}

  if (FrameCalcCRC != FrameRxCRC)
  {
    //itoa((int)(FrameCalcCRC & 0xFFFF),(char *)hSerialUgd->ResponseBuf);
    strcpy((char *)hSerialUgd->ResponseBuf," BLOCK ERROR");
    //itoa(FrameRxCRC,tmpbuf);
    //if(hSerialUgd->hSysDbase->bGSMDebugEnable==TM_TRUE)
    //{
      //UART0QueueData((tm_u8_t *)tmpbuf,vts_strlen(tmpbuf));
      //UART0QueueData((tm_u8_t *)"BLOCK ERROR\r\n",vts_strlen("BLOCK ERROR\r\n"));
    //}
    //hSerialUgd->bProfile1DataSend = TM_TRUE;
    //DPRINTF(hSerialUgd->ResponseBuf);
    SendOTAAck(hTMApp,hSerialUgd->ResponseBuf);
    return TM_TRUE;
  }
  switch (hSerialUgd->ProgramState)
  {
    case UGD_QUERY:
      if (MsgID == MSG_UGDQUERY_ID)
      {
        //DPRINTF("DEBUG UGD_QUERY\r\n");
        /* Send hardware version and software version  */
        //if(hSerialUgd->hSysDbase->bGSMDebugEnable==TM_TRUE)
          //UART0QueueData((tm_u8_t *)"Received UGD_QUERY\r\n",vts_strlen("Received UGD_QUERY\r\n"));
        strcat((char *)hSerialUgd->ResponseBuf,SoftwareVer);
        strcat((char *)hSerialUgd->ResponseBuf,"><");
        strcat((char *)hSerialUgd->ResponseBuf,HardwareVer);
        strcat((char *)hSerialUgd->ResponseBuf,"><OK");
        hSerialUgd->ProgramState = UGD_START;
        //hSerialUgd->bProfile1DataSend = TM_TRUE;
        //DPRINTF(hSerialUgd->ResponseBuf);
        SendOTAAck(hTMApp,hSerialUgd->ResponseBuf);
      }
      else
      {
        /* Return error */
        strcpy((char *)hSerialUgd->ResponseBuf,"ERROR: Expecting QUERY");
        //hSerialUgd->bProfile1DataSend = TM_TRUE;
        //DPRINTF(hSerialUgd->ResponseBuf);
        SendOTAAck(hTMApp,hSerialUgd->ResponseBuf);
      }
      break;
    case UGD_START:
      /* Receive and store CRC and total byte count  */
      if(MsgID != MSG_UGDSTART_ID || (MsgLength != UGDSTART_MSGLEN))
      {
        /* Return error */
        //sprintf(debugstr,"msg id %d\r\n",MsgID);
        //DPRINTF(debugstr);
        strcpy((char *)hSerialUgd->ResponseBuf,"NOK");
        hSerialUgd->ProgramState = UGD_QUERY;
        //hSerialUgd->bProfile1DataSend = TM_TRUE;
        //DPRINTF(hSerialUgd->ResponseBuf);
        SendOTAAck(hTMApp,hSerialUgd->ResponseBuf);
      }
      else
      {
        //DPRINTF("DEBUG UGD_START\r\n");
        hSerialUgd->RxCRC= (tm_u16_t)((hSerialUgd->UGDData[7] << 8) |(hSerialUgd->UGDData[8] ));
        hSerialUgd->TotalByteCount = (hSerialUgd->UGDData[3] << 24) |(hSerialUgd->UGDData[4] << 16) |
          (hSerialUgd->UGDData[5] << 8) |(hSerialUgd->UGDData[6]);
        /* Init erase flash */
        hSerialUgd->SoftVer[0] = hSerialUgd->UGDData[9];
        hSerialUgd->SoftVer[1] = hSerialUgd->UGDData[10];
        hSerialUgd->SoftVer[2] = hSerialUgd->UGDData[11];
        hSerialUgd->SoftVer[3] = hSerialUgd->UGDData[12];
        hSerialUgd->SoftVer[4] = '\0';
        hSerialUgd->ProgramState = UGD_DATA;
        hSerialUgd->FlashAddr = APP_STARTADDR;  /* Set FlashAddr to store received software image */
        hSerialUgd->FlashState = UGDFLASH_ERASE;
        FlashTaskEraseSoftImage(hSerialUgd->hFlashTask);
        //hSerialUgd->bSendAck = TM_TRUE;
        //vts_strcpy((char *)hSerialUgd->ResponseBuf,"OK");
      }
      break;
    case UGD_DATA:
      /* Init write to flash */
      if (MsgID == MSG_UGDDATA_ID)
      {
        //UART0QueueData((tm_u8_t *)"Received UGD_DATA\r\n",vts_strlen("Received UGD_DATA\r\n"));
        if(FlashTaskStateGet(hSerialUgd->hFlashTask) != FLASHTASK_IDLE || MsgLength > 256 || MsgLength == 0)
        {
          //hSerialUgd->bProfile1DataSend = TM_TRUE;
          strcpy((char *)hSerialUgd->ResponseBuf,"NOK");
        //DPRINTF(hSerialUgd->ResponseBuf);
          SendOTAAck(hTMApp,hSerialUgd->ResponseBuf);
          hSerialUgd->ProgramState = UGD_QUERY;
          //UART0QueueData((tm_u8_t *)"UGD_DATA error\r\n",vts_strlen("UGD_DATA error\r\n"));
          break;
        }
        //UART0QueueData((tm_u8_t *)"UGD_DATA writing to flash\r\n",vts_strlen("UGD_DATA writing to flash\r\n"));
        FlashTaskWrite(hSerialUgd->hFlashTask, hSerialUgd->FlashAddr, MsgLength, (tm_u8_t *)&hSerialUgd->UGDData[3]);
        hSerialUgd->FlashState = UGDFLASH_WRITEDATA;
        hSerialUgd->FlashAddr += MsgLength;
      }
      else if(MsgID == MSG_UGDEND_ID)
      {
        /* Read back from flash and check crc */
        //DPRINTF("DEBUG UGD_DATA\r\n");
        hSerialUgd->FlashState = UGDFLASH_CHECKCRC;
        hSerialUgd->CalcCRC = 0;
        hSerialUgd->FlashAddr = APP_STARTADDR;
        hSerialUgd->tmpTotalByteCount = hSerialUgd->TotalByteCount;
        hSerialUgd->ProgramState = UGD_OK;
        hSerialUgd->bSendAck = TM_TRUE;
        hSerialUgd->RdLen = 256;
        FlashTaskRead(hSerialUgd->hFlashTask, hSerialUgd->FlashAddr, 256,hSerialUgd->DataBuf);
        hSerialUgd->FlashAddr += 256;
        hSerialUgd->tmpTotalByteCount -= 256;
      }
      break;
    case UGD_OK:
      if(MsgID == MSG_UGDCRCOK_ID)
      {
        /* Reset Processor */
         DPRINTF("Upgrade successfull. Reset\r\n");
         hTMApp->bReset = TM_TRUE;
      }
      break;
    case UGD_END:
    default:
      break;
  }
  return TM_TRUE;
}

void HandleOTA(tm_app_t *hTMApp)
{
  int i;
  tm_u16_t FrameCalcCRC;
  tm_serialugd_t *hSerialUgd;

  hSerialUgd = &hTMApp->OTAUgd;

  switch(hSerialUgd->FlashState)
  {
    case UGDFLASH_IDLE:
      if(FlashTaskStateGet(hSerialUgd->hFlashTask) == FLASHTASK_IDLE)
      {
        if(hSerialUgd->bSendAck == TM_TRUE)
        {
          hSerialUgd->bSendAck = TM_FALSE;
          //DPRINTF(hSerialUgd->ResponseBuf);
          SendOTAAck(hTMApp,hSerialUgd->ResponseBuf);
        }
      }
      break;
    case UGDFLASH_ERASE:
      if(FlashTaskStateGet(hSerialUgd->hFlashTask) == FLASHTASK_IDLE)
      {
        //UART0QueueData((tm_u8_t *)"Flash erased\r\n",vts_strlen("Flash erased\r\n"));
        FlashTaskWrite(hSerialUgd->hFlashTask, APP_BYTECOUNTADDR, 4, (tm_u8_t *)&hSerialUgd->TotalByteCount);
        hSerialUgd->FlashState = UGDFLASH_BYTECOUNTWAIT;
      }
      break;
    case UGDFLASH_BYTECOUNTWAIT:
      if(FlashTaskStateGet(hSerialUgd->hFlashTask) == FLASHTASK_IDLE)
      {
        FlashTaskWrite(hSerialUgd->hFlashTask, APP_CRCADDR, 2, (tm_u8_t *)&hSerialUgd->RxCRC);
        hSerialUgd->FlashState = UGDFLASH_CRCWRITEWAIT;
      }
      break;
    case UGDFLASH_CRCWRITEWAIT:
      if(FlashTaskStateGet(hSerialUgd->hFlashTask) == FLASHTASK_IDLE)
      {
        strcpy((char *)hSerialUgd->ResponseBuf,"OK");
        hSerialUgd->bSendAck = TM_TRUE;
        hSerialUgd->FlashState = UGDFLASH_IDLE;
      }
      break;
    case UGDFLASH_WRITECURVER:
      if(FlashTaskStateGet(hSerialUgd->hFlashTask) == FLASHTASK_IDLE)
      {
        FlashTaskWrite(hSerialUgd->hFlashTask, APP_CURVER_ADDR, 4, (tm_u8_t *)SoftwareVer);
        /* Send Ack */
        hSerialUgd->FlashState = UGDFLASH_WRITENEWVER;
      }
      break;
    case UGDFLASH_WRITENEWVER:
      if(FlashTaskStateGet(hSerialUgd->hFlashTask) == FLASHTASK_IDLE)
      {
        FlashTaskWrite(hSerialUgd->hFlashTask, APP_NEWVER_ADDR, 4, (tm_u8_t *)hSerialUgd->SoftVer);
        hSerialUgd->FlashState = UGDFLASH_NEWVERWAIT;
      }
      break;
    case UGDFLASH_NEWVERWAIT:
      if(FlashTaskStateGet(hSerialUgd->hFlashTask) == FLASHTASK_IDLE)
      {
        hSerialUgd->FlashState = UGDFLASH_IDLE;

        /* Send Ack */
        hSerialUgd->bSendAck = TM_TRUE;
        strcpy((char *)hSerialUgd->ResponseBuf,"CRC OK");
      }
      break;
    case UGDFLASH_WRITEDATA:
      if(FlashTaskStateGet(hSerialUgd->hFlashTask) == FLASHTASK_IDLE)
      {
        /* Send Ack */
        //UART0QueueData((tm_u8_t *)"UGD_DATA data written\r\n",vts_strlen("UGD_DATA data written\r\n"));
        hSerialUgd->FlashState = UGDFLASH_IDLE;
        strcpy((char *)hSerialUgd->ResponseBuf,"OK");
        hSerialUgd->bSendAck = TM_TRUE;
      }
      break;
    case UGDFLASH_CHECKCRC:
      if(FlashTaskStateGet(hSerialUgd->hFlashTask) == FLASHTASK_IDLE)
      {
        FrameCalcCRC = 0;
        for(i=0; i < hSerialUgd->RdLen; i++)
        {
          hSerialUgd->CalcCRC += hSerialUgd->DataBuf[i];
          FrameCalcCRC += hSerialUgd->DataBuf[i];
          hSerialUgd->CalcCRC &= 0xFFFF;
        }
#if 0
        if(printdata == 0)
        {
          printdata = 1;
          //UART0QueueData((tm_u8_t *)"First bytes  ",12);
          for(i=0; i < 1; i++)
          {
            itoa((int)hSerialUgd->tmpTotalByteCount,tmpbuf);
            //UART0QueueData((tm_u8_t *)tmpbuf,vts_strlen(tmpbuf));
            //UART0QueueData((tm_u8_t *)" ",1);
          }
        }
        //if(hSerialUgd->hSysDbase->bGSMDebugEnable==TM_TRUE)
          //UART0QueueData((tm_u8_t *)"FCRC ",vts_strlen("FCRC "));
        itoa((int)(FrameCalcCRC & 0xFFFF),tmpbuf);
        //if(hSerialUgd->hSysDbase->bGSMDebugEnable==TM_TRUE)
        //{
          //UART0QueueData((tm_u8_t *)tmpbuf,vts_strlen(tmpbuf));
          //UART0QueueData((tm_u8_t *)" FCRC\r\n",vts_strlen(" FCRC\r\n"));
        //}

#endif
        if(hSerialUgd->tmpTotalByteCount > 0)
        {
          if(hSerialUgd->tmpTotalByteCount > 256)
          {
            hSerialUgd->RdLen = 256;
            FlashTaskRead(hSerialUgd->hFlashTask, hSerialUgd->FlashAddr, 256,hSerialUgd->DataBuf);
            hSerialUgd->FlashAddr += 256;
            hSerialUgd->tmpTotalByteCount -= 256;
          }
          else
          {
            hSerialUgd->RdLen = hSerialUgd->tmpTotalByteCount;
            FlashTaskRead(hSerialUgd->hFlashTask, hSerialUgd->FlashAddr, hSerialUgd->tmpTotalByteCount,hSerialUgd->DataBuf);
            hSerialUgd->tmpTotalByteCount = 0;
          }
        }
        else
        {
          if(hSerialUgd->CalcCRC == hSerialUgd->RxCRC)
          {
            /* CRC OK*/
            //if(hSerialUgd->hSysDbase->bGSMDebugEnable==TM_TRUE)
              //UART0QueueData((tm_u8_t *)"CRC OK\r\n",vts_strlen("CRC OK\r\n"));
            hSerialUgd->FlashState = UGDFLASH_WRITECURVER;
            //hSerialUgd->bSendAck = TM_TRUE;
            //vts_strcpy((char *)hSerialUgd->ResponseBuf,"CRC OK");
          }
          else
          {
            /* CRC NOK*/
            //if(hSerialUgd->hSysDbase->bGSMDebugEnable==TM_TRUE)
              //UART0QueueData((tm_u8_t *)"CRC NOK\r\n",vts_strlen("CRC NOK\r\n"));
            hSerialUgd->FlashState = UGDFLASH_IDLE;
            hSerialUgd->bSendAck = TM_TRUE;
            strcpy((char *)hSerialUgd->ResponseBuf,"CRC NOK");
          }
        }
      }
      break;
  }
}

void SendOTAAck(tm_app_t *hTMApp, tm_u8_t *pData)
{
  tm_profile1_t *hProfile1;
  tm_u16_t Len;
  hProfile1 = &hTMApp->Profile1;

  strcpy(hProfile1->Profile1Data,"^ID=");
  strcat(hProfile1->Profile1Data,hTMApp->DBase.DevID);
  strcat(hProfile1->Profile1Data,"&ACK=");
  strcat(hProfile1->Profile1Data,"<");
  strcat(hProfile1->Profile1Data, (const char *)pData);
  strcat(hProfile1->Profile1Data,">");
  Len = strlen(hProfile1->Profile1Data);
  GsmSendData(&hTMApp->Gsm, PROFILE1, (tm_u8_t *)hProfile1->Profile1Data, Len);
}

void ResetCtrl(tm_app_t *hTMApp)
{
  switch(hTMApp->ResetState)
  {
    case WAITFORRESETCMD:
      if(hTMApp->bReset == TM_TRUE)
      {
        /* Close all peripherals */
        hTMApp->ResetState = RESETSYSTEM;
      }
      break;
    case RESETSYSTEM:
      //IO_RSTCR.byte |= 0x1;
      /* Disable all interrupts */
      Vectors_InitIrqLevels();
      __asm("   LDI #1,R0");
      __asm("   LDI:32 #_IO_RSTCR,R12");
      __asm("   STB R0, @R12");
      __asm("   LDUB @R12, R0");
      __asm("   MOV R0,R0");
      __asm("   NOP");
      __asm("   NOP");
      break;
  }
}

static int CheckQueue(tm_app_t *hTMApp)
{
  tm_profile0_t *hProfile;
  int diff;

  hProfile = &hTMApp->Profile0;

  if(hProfile->PacketWrIndex >= hProfile->PacketRdIndex)
  {
    diff = hProfile->PacketWrIndex - hProfile->PacketRdIndex;
  }
  else
  {
    diff = PACKETQUEUELEN - (hProfile->PacketRdIndex - hProfile->PacketWrIndex);
  }
  return diff;
}

static void ManageQueue(tm_app_t *hTMApp)
{
  /* Check to log data to flash  */
  tm_profile0_t *hProfile;
  int diff;

  hProfile = &hTMApp->Profile0;

  /* Push data in flash queue to flash */
  if(hProfile->FlashPacketWrIndex != hProfile->FlashPacketRdIndex)
  {
    hProfile->FlashQueue[hProfile->FlashPacketRdIndex].bLive = TM_FALSE;
    if(FlashTaskLogPacket(&hTMApp->FlashTask, (tm_u8_t *)&hProfile->FlashQueue[hProfile->FlashPacketRdIndex], sizeof(tm_packet_t)) == TM_TRUE)
    {
      INCCIRCULARINDEX(hProfile->FlashPacketRdIndex, FLASHPACKETQUEUELEN);
      return;
    }
  }

  /* If there is room in queue and data is present either in flash
     or flash queue read it and put it queue */
  diff = CheckQueue(hTMApp);
  if(diff < PACKETQUEUETHRESHOLD)
  {
    if(CheckLoggedPackets(&hTMApp->FlashTask, (tm_u8_t *)&hProfile->LoggedPacket, sizeof(tm_packet_t)) == TM_TRUE)
    {
      if(hTMApp->DBase.DebugLevel >= 2)
      {
        DPRINTF("popped packet from flash\r\n");
      }
      memcpy(&hProfile->PacketQueue[hProfile->PacketWrIndex],&hProfile->LoggedPacket, sizeof(tm_packet_t));
      INCCIRCULARINDEX(hProfile->PacketWrIndex, PACKETQUEUELEN);
    }
  }
}

static void Profile0StatusUpdate(tm_profilestatus_t ProfileStatus, void *pUserData)
{
  tm_app_t *hTMApp;

  hTMApp = (tm_app_t *)pUserData;
  hTMApp->Profile0.GsmProfile0Status = ProfileStatus;

  if(ProfileStatus == PROFILE0_CONNECTED)
  {
        if(hTMApp->DBase.DebugLevel >= 2)
        {
          sprintf(debugstr,"hTmapp gsm connected\r\n");
          DPRINTF(debugstr);
        }
    hTMApp->Profile0.bGsmConnected = TM_TRUE;
  }
  if(ProfileStatus == PROFILE0_DISCONNECTED)
  {
    hTMApp->Profile0.bGsmConnected = TM_FALSE;
  }
  if(ProfileStatus == PROFILE0_DATASENT)
  {
    hTMApp->Profile0.bDataSent = TM_TRUE;
  }
  if(ProfileStatus == PROFILE_MODEMRESTART)
  {
    hTMApp->Profile0.bGsmConnected = TM_FALSE;
    if(hTMApp->Profile0.P0State == P0_WAITFORCONNECTION)
      hTMApp->Profile0.P0State = P0_IDLE;
  }
  if(hTMApp->DBase.DebugLevel >= 2)
  {
    sprintf(debugstr,"Profile statsu = %d\r\n",ProfileStatus);
    DPRINTF(debugstr);
  }

}

static void Profile1StatusUpdate(tm_profilestatus_t ProfileStatus, void *pUserData)
{
  tm_app_t *hTMApp;

  hTMApp = (tm_app_t *)pUserData;

}

static void Profile2StatusUpdate(tm_profilestatus_t ProfileStatus, void *pUserData)
{
  tm_app_t *hTMApp;

  hTMApp = (tm_app_t *)pUserData;


}
