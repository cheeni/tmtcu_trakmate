#include "tmapp\src\tmapp_private.h"

extern char debugstr[];

void PGNHandle(tm_dtc_t *hDTC, tm_dtc_t *hDTCP, tm_pgndata_t *hPGNData, j1939_t *msg)
{
  tm_u32_t tmp;

  switch(msg->pgn)
  {
    case 64777:
      tmp = 0;
      tmp = (msg->buf[7] << 24) | (msg->buf[6] << 16) | (msg->buf[5] << 8) | (msg->buf[4]);
      hPGNData->HRLFC.hrlfc = tmp * 0.001;
      tmp = (msg->buf[3] << 24) | (msg->buf[2] << 16) | (msg->buf[1] << 8) | (msg->buf[0]);
      hPGNData->HRLFC.TripFuel = tmp * 0.001;
#ifdef PRINTCANDATA
      sprintf(debugstr,"hrlfc = %f\r\nTripFuel = %0.2f",hPGNData->HRLFC.hrlfc,hPGNData->HRLFC.TripFuel);
      DPRINTF(debugstr);
#endif
      break;
    case 65292:
      hPGNData->VP12.SweetSpot = (msg->buf[5] & 0x07);
      hPGNData->VP12.TopGear = (msg->buf[6] & 0x03);
#ifdef PRINTCANDATA
      sprintf(debugstr,"sweetspot  = %d\r\nTopGear = %d\r\n",hPGNData->VP12.SweetSpot,hPGNData->VP12.TopGear);
      DPRINTF(debugstr);
#endif
      break;
    case 65483:
      hPGNData->VP203.SweetSpotPercent = msg->buf[6];
#ifdef PRINTCANDATA
      sprintf(debugstr,"SweetSpotPercent = %d\r\n",hPGNData->VP203.SweetSpotPercent);
      DPRINTF(debugstr);
#endif
      break;
    case 65254:
      hPGNData->TD_IC.Secs = msg->buf[0] >> 2;
      hPGNData->TD_IC.Min = msg->buf[1];
      hPGNData->TD_IC.Hr = msg->buf[2];
      hPGNData->TD_IC.Month = msg->buf[3];
      hPGNData->TD_IC.Day = msg->buf[4] >> 2;
      hPGNData->TD_IC.Year = msg->buf[5] + 1985;
      hPGNData->TD_IC.LocalMinuteOffset = msg->buf[6] - 125;
      hPGNData->TD_IC.LocalHrOffset = msg->buf[7] - 125;
#ifdef PRINTCANDATA
      sprintf(debugstr,"Secs = %d\r\nMin = %d\r\nHr = %d\r\nMonth = %d\r\nDay = %d\r\nYear = %d\r\nMinOffset = %d\r\nHrOffset = %d\r\n",hPGNData->TD_IC.Secs,hPGNData->TD_IC.Min,hPGNData->TD_IC.Hr,hPGNData->TD_IC.Month,hPGNData->TD_IC.Day,hPGNData->TD_IC.Year,hPGNData->TD_IC.LocalMinuteOffset,hPGNData->TD_IC.LocalHrOffset);
      DPRINTF(debugstr);
#endif
      break;
    case 65217:
      tmp = 0;
      tmp = (msg->buf[3] << 24) | (msg->buf[2] << 16) | (msg->buf[1] << 8) | (msg->buf[0]);
      hPGNData->HRVD_I.TotalDistance = tmp * 5.0/1000.0;
#ifdef PRINTCANDATA
      sprintf(debugstr,"TotalDistance = %f\r\n",hPGNData->HRVD_I.TotalDistance);
      DPRINTF(debugstr);
#endif
      break;
    case 65276:
      hPGNData->DD_X_IC.FuelLevel = msg->buf[1] * 0.4;
#ifdef PRINTCANDATA
      sprintf(debugstr,"FuelLevel = %f\r\n",hPGNData->DD_X_IC.FuelLevel);
      DPRINTF(debugstr);
#endif
      break;
    case 65226:
      hPGNData->DM1_Active.AmberWarningLamp = (msg->buf[0] & 0x0C) >> 2;
      hPGNData->DM1_Active.RedStopLamp = (msg->buf[0] & 0x30) >> 4;
      hPGNData->DM1_Active.MalfuncLamp = (msg->buf[0] & 0xC0) >> 6;
      hPGNData->DM1_Active.FlashMalfuctionLamp = (msg->buf[1] & 0xC0) >> 6;

      hPGNData->DM1_Active.SPN8_LSB_SPN_EMS = msg->buf[2];
      hPGNData->DM1_Active.SPN8_2ndB_SPN_EMS = msg->buf[3];
      hPGNData->DM1_Active.FailurMode = (msg->buf[4] & 0x1F);
      hPGNData->DM1_Active.SPN3_MSB_EMS = (msg->buf[4] & 0xE0) >> 5;
      hPGNData->DM1_Active.OccurenceCount = (msg->buf[5] & 0x7F);

      ParseDM1(hDTC, msg);

#ifdef PRINTCANDATA
      sprintf(debugstr,"AWL = %d\r\n,RSL = %d\r\nMFL = %d\r\nFMFL = %d\r\n,SPN8LSB = %d\r\nSPN8_2EMS = %d\r\nFail = %d\r\nSPN3 = %d\r\nOccr = %d\r\n",hPGNData->DM1_Active.AmberWarningLamp,hPGNData->DM1_Active.RedStopLamp,hPGNData->DM1_Active.MalfuncLamp,hPGNData->DM1_Active.FlashMalfuctionLamp,hPGNData->DM1_Active.SPN8_LSB_SPN_EMS,hPGNData->DM1_Active.SPN8_2ndB_SPN_EMS,hPGNData->DM1_Active.FailurMode,hPGNData->DM1_Active.SPN3_MSB_EMS,hPGNData->DM1_Active.OccurenceCount);
      DPRINTF(debugstr);
#endif
      break;
    case 65227:
      hPGNData->DM1_Active.AmberWarningLamp = (msg->buf[0] & 0x0C) >> 2;
      hPGNData->DM1_Active.RedStopLamp = (msg->buf[0] & 0x30) >> 4;
      hPGNData->DM1_Active.MalfuncLamp = (msg->buf[0] & 0xC0) >> 6;
      hPGNData->DM1_Active.FlashMalfuctionLamp = (msg->buf[1] & 0xC0) >> 6;

      hPGNData->DM1_Active.SPN8_LSB_SPN_EMS = msg->buf[2];
      hPGNData->DM1_Active.SPN8_2ndB_SPN_EMS = msg->buf[3];
      hPGNData->DM1_Active.FailurMode = (msg->buf[4] & 0x1F);
      hPGNData->DM1_Active.SPN3_MSB_EMS = (msg->buf[4] & 0xE0) >> 5;
      hPGNData->DM1_Active.OccurenceCount = (msg->buf[5] & 0x7F);

      ParseDM2(hDTCP, msg);

      break;
    case 65265:
    /* Wheel based speed. Start bit 8, Len 16 */
      hPGNData->CCVS.CCA = (msg->buf[3] & 0x03);
      hPGNData->CCVS.CCES = (msg->buf[3] & 0x0C) >> 2;
      hPGNData->CCVS.CCSS = msg->buf[5];
#if 0
      tmp = 0;
      tmp = (msg->buf[2] << 8) | msg->buf[1];
      if((tmp*0.00390625) < 150.0)
        hPGNData->VP2.VehicleSpeed = tmp * 0.00390625;
#endif
#ifdef PRINTCANDATA
      sprintf(debugstr,"CCA = %d\r\nCCES = %d\r\nCCSS = %d\r\n",hPGNData->CCVS.CCA,hPGNData->CCVS.CCES,hPGNData->CCVS.CCSS);
      DPRINTF(debugstr);
#endif
      break;
    case 61444:
      tmp = 0;
      tmp = (msg->buf[4] << 8) | msg->buf[3];
      hPGNData->EEC1.EngineSpeed = tmp * 0.125;
      hPGNData->EEC1.EngineStartMode = (msg->buf[6] & 0x0F);
#ifdef PRINTCANDATA
      sprintf(debugstr,"EngineSpeed = %f\r\nEngineStartMode = %d\r\n",hPGNData->EEC1.EngineSpeed,hPGNData->EEC1.EngineStartMode);
      DPRINTF(debugstr);
#endif
      break;
    case 65253:
      tmp = 0;
      tmp = (msg->buf[3] << 24) | (msg->buf[2] << 16) | (msg->buf[1] << 8) | (msg->buf[0]);
      hPGNData->EngineHours.EngineOpHrs = tmp * 0.05;
#ifdef PRINTCANDATA
      sprintf(debugstr,"EngineOpHrts = %f\r\n",hPGNData->EngineHours.EngineOpHrs);
      DPRINTF(debugstr);
#endif
      break;
    case 65282:
      hPGNData->VP2.PowerKeyPos = (msg->buf[1] & 0xC0) >> 6;
      hPGNData->VP2.AccPedalIdelSwitch = (msg->buf[2] & 0x03);
      hPGNData->VP2.AccPedalPosition = msg->buf[3] * 0.4;
#if 1
      tmp = 0;
      tmp = (msg->buf[5] << 8) | msg->buf[4];
      /* Update vehicle speed only if it less than 150.0. This is to discard invalid
       * data from the vehicle */
      if((tmp*0.0039) < 150.0)
        hPGNData->VP2.VehicleSpeed = tmp * 0.0039;
      hPGNData->VP2.ControlerTrimMode = (msg->buf[6] & 0xE0) >> 5;
#endif
#ifdef PRINTCANDATA
      sprintf(debugstr,"PowerKeyPos = %d\r\nAccPedal = %d\r\n,Speed = %f\r\nAccPos = %0.2f\r\n",hPGNData->VP2.PowerKeyPos,hPGNData->VP2.AccPedalIdelSwitch,hPGNData->VP2.VehicleSpeed,hPGNData->VP2.AccPedalPosition);
      DPRINTF(debugstr);
#endif
      break;
    case 65263:
      hPGNData->EELP1.EngineOilPressure = msg->buf[3] * 4;
#ifdef PRINTCANDATA
      sprintf(debugstr,"EngineOilPressure = %d\r\n",hPGNData->EELP1.EngineOilPressure);
      DPRINTF(debugstr);
#endif
      break;
    case 65262:
      hPGNData->ET1.EngineCoolantTemp = msg->buf[0] - 40;
#ifdef PRINTCANDATA
      sprintf(debugstr,"EngineCoolantTemp = %d\r\n",hPGNData->ET1.EngineCoolantTemp);
      DPRINTF(debugstr);
#endif
      break;
  }
}

void pgn_64777(tm_app_t *hTMApp)
{
}

void ParseDM1(tm_dtc_t *hDTC, j1939_t *msg)
{
  tm_u32_t spn;
  tm_u8_t  failuremore;
  tm_u8_t  conversion;
  tm_u8_t  occurancecount;
  tm_u16_t msgindex;
  tm_u16_t buflen;

  buflen = msg->buf_len - 2;

  msgindex = 2;
  hDTC->FaultCodeCount = 0;
  while(buflen > 0)
  {
    spn = 0;
    spn = ((msg->buf[msgindex + 2] & 0xE0) << 11) | (msg->buf[msgindex + 1] << 8 ) | msg->buf[msgindex];

    failuremore = msg->buf[msgindex + 2] & 0x1F;
    occurancecount = msg->buf[msgindex + 3] & 0x7F;
    if((msg->buf[msgindex + 3] & 0x80) == 0x80)
      conversion = 1;
    else
      conversion = 0;

    buflen -= 4;
    msgindex += 4;

    hDTC->DM[hDTC->FaultCodeCount].SPN = spn;
    hDTC->DM[hDTC->FaultCodeCount].FailureMode = failuremore;
    hDTC->DM[hDTC->FaultCodeCount].ConversionMethod = conversion;
    hDTC->DM[hDTC->FaultCodeCount].OccurenceCount = occurancecount;

    hDTC->FaultCodeCount++;
    if(hDTC->FaultCodeCount >= MAXNUMBEROFFAULTS)
    {
      break;
    }
  }
}

void ParseDM2(tm_dtc_t *hDTC, j1939_t *msg)
{
  tm_u32_t spn;
  tm_u8_t  failuremore;
  tm_u8_t  conversion;
  tm_u8_t  occurancecount;
  tm_u16_t msgindex;
  tm_u16_t buflen;

  buflen = msg->buf_len - 2;

  msgindex = 2;
  hDTC->FaultCodeCount= 0;
  while(buflen > 0)
  {
    spn = 0;
    spn = ((msg->buf[msgindex + 2] & 0xE0) << 11) | (msg->buf[msgindex + 1] << 8 ) | msg->buf[msgindex];

    failuremore = msg->buf[msgindex + 2] & 0x1F;
    occurancecount = msg->buf[msgindex + 3] & 0x7F;
    if((msg->buf[msgindex + 3] & 0x80) == 0x80)
      conversion = 1;
    else
      conversion = 0;

    buflen -= 4;
    msgindex += 4;

    hDTC->DM[hDTC->FaultCodeCount].SPN = spn;
    hDTC->DM[hDTC->FaultCodeCount].FailureMode = failuremore;
    hDTC->DM[hDTC->FaultCodeCount].ConversionMethod = conversion;
    hDTC->DM[hDTC->FaultCodeCount].OccurenceCount = occurancecount;

    hDTC->FaultCodeCount++;
    if(hDTC->FaultCodeCount >= MAXNUMBEROFFAULTS)
    {
      break;
    }
  }
}

