#ifndef J1939_H
#define J1939_H
#include "tmtypes.h"

typedef tm_u8_t uint8_t;
typedef tm_u16_t uint16_t;
typedef tm_u32_t uint32_t;



/* SAE J1939 Source Addresses found in J1939 p. 45. 252 is 
   for experimental use */
#define J1939_ADDR_DIAG_TOOL1                         249
#define J1939_ADDR_EXPERIMENTAL_USE                   252
#define J1939_ADDR_NULL                               254
#define J1939_ADDR_GLOBAL                             255

/* SAE J1939 parameter group numbers */
#define J1939_PGN_REQUEST                           59904
#define J1939_PGN_ADDRESS_CLAIMED                   60928
#define J1939_PGN_PROPRIETARY_A                     61184
#define J1939_PGN_TP_CM                             60416
#define J1939_PGN_TP_DT                             60160



typedef struct {

   uint32_t pgn;     /* parameter group number. */
   uint8_t *buf;     /* pointer to data. */
   uint16_t buf_len; /* size of data. */
   uint8_t dst;      /* destination of message. */
   uint8_t src;      /* source of message. */
   uint8_t pri;      /* priority of message. */

} j1939_t;



extern uint8_t
j1939_sa;

extern void
j1939_init( void );

extern uint8_t
j1939_bip_tx_rate_max_get ( void );

extern void
j1939_bip_tx_rate_allowed_set( uint8_t rate );

extern uint8_t
j1939_tx_app( j1939_t *msg, uint8_t *status );

extern void
j1939_update( void );

#endif
