#ifndef J1939CFG_H
#define J1939CFG_H



/* tick period in milliseconds */
#define J1939CFG_TICK_PERIOD                       10

/* source address */
#define J1939CFG_SA       J1939_ADDR_EXPERIMENTAL_USE  

/* multi-packet TX buffer size */
#define J1939CFG_TP_TX_BUF_NUM                      3
#define J1939CFG_TP_TX_BUF_SIZE                    64

/* multi-packet RX buffer size */
#define J1939CFG_TP_RX_BUF_NUM                      5
#define J1939CFG_TP_RX_BUF_SIZE                    64

/* NAME field found in J1939-81 */
#define J1939CFG_N_AAC              (0U) /* 1-bit Arbitrary Address Capable */
#define J1939CFG_N_IG               (1U) /* 3-bit Industry Group */
#define J1939CFG_N_VSI              (0U) /* 4-bit Vehicle System Instance */
#define J1939CFG_N_VS               (1U) /* 7-bit Vehicle System */ 
#define J1939CFG_N_F              (255U) /* 8-bit Function */
#define J1939CFG_N_FI               (0U) /* 5-bit Function Instance */
#define J1939CFG_N_EI               (0U) /* 3-bit ECU Instance */
#define J1939CFG_N_MC             (402U) /* 11-bit Manufacturer Code */
#define J1939CFG_N_IN              (0UL) /* 21-bit Indentity Number */

#endif
