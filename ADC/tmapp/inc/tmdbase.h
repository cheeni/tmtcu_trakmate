#ifndef __TMDASE_H__
#define __TMDASE_H__

#define MAXSIZE_DEVID 20
#define MAXSIZE_IP 16
#define MAXSIZE_PORT 6
#define MAXSIZE_APN 16

struct dbase
{
  char DevID[MAXSIZE_DEVID];
  char LIN[MAXSIZE_DEVID];
  char PrimaryIP[MAXSIZE_IP];
  char PrimaryPort[MAXSIZE_PORT];
  char OTAIP[MAXSIZE_IP];
  char OTAPort[MAXSIZE_PORT];
  char APN[MAXSIZE_APN];
  tm_s16_t UpdateRate;
  tm_s16_t PwdUpdateRate;
  int  DebugLevel;
};
typedef struct dbase tm_dbase_t;

#endif /* __TMDASE_H__ */

