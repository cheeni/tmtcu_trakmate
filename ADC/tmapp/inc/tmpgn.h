#ifndef __TMPGN_H__
#define __TMPGN_H__

#include "tmtypes.h"

#define MAXNUMBEROFFAULTS 20

struct pgndata
{
  struct HRLFC
  {
    float hrlfc;
    float TripFuel;
  }HRLFC;
  struct VP12
  {
    tm_u8_t SweetSpot;
    tm_u8_t TopGear;
  }VP12;
  struct VP203
  {
    tm_u8_t SweetSpotPercent;
  }VP203;
  struct TD_IC
  {
    tm_u16_t Year;
    tm_u16_t LocalMinuteOffset;
    tm_u16_t LocalHrOffset;
    tm_u8_t Secs;
    tm_u8_t Min;
    tm_u8_t Hr;
    tm_u8_t Month;
    tm_u8_t Day;
  }TD_IC;
  struct HRVD_I
  {
    float TotalDistance;
  }HRVD_I;
  struct DD_X_IC
  {
    float FuelLevel;
  }DD_X_IC;
  struct DM1_Active
  {
    tm_u8_t AmberWarningLamp;
    tm_u8_t RedStopLamp;
    tm_u8_t MalfuncLamp;
    tm_u8_t FlashMalfuctionLamp;
    tm_u8_t SPN8_LSB_SPN_EMS;
    tm_u8_t SPN8_2ndB_SPN_EMS;
    tm_u8_t FailurMode;
    tm_u8_t SPN3_MSB_EMS;
    tm_u8_t OccurenceCount;
  }DM1_Active;
  struct CCVS
  {
    tm_u8_t CCA;
    tm_u8_t CCES;
    tm_u8_t CCSS;
  }CCVS;
  struct EEC1
  {
    float EngineSpeed;
    tm_u8_t EngineStartMode;
  }EEC1;
  struct EngineHours
  {
    float EngineOpHrs;
  }EngineHours;
  struct VP2
  {
    float     VehicleSpeed;
    float   AccPedalPosition;
    tm_u16_t  AccPedalIdelSwitch;
    tm_u8_t   PowerKeyPos;
    tm_u8_t   ControlerTrimMode;
  }VP2;
  struct EELP1
  {
    tm_u32_t EngineOilPressure;
  }EELP1;
  struct ET1
  {
    tm_s8_t EngineCoolantTemp;
  }ET1;
};
typedef struct pgndata tm_pgndata_t;

struct dm1
{
  tm_u32_t SPN;
  tm_u8_t  FailureMode;
  tm_u8_t  ConversionMethod;
  tm_u8_t  OccurenceCount;
};
typedef struct dm1 tm_dm1_t;

struct DTC
{
  tm_u8_t FaultCodeCount;
  tm_dm1_t DM[MAXNUMBEROFFAULTS];
};
typedef struct DTC tm_dtc_t;

#endif /* __TMPGN_H__ */
