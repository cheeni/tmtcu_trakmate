#ifndef __SERIALUGD_H__
#define __SERIALUGD_H__

#include "tmtypes.h"
#include "tmtimer.h"
#include "string.h"
#include "tmuart.h"
#include "vectors.h"
#include "tmflash\inc\tmflashtask.h"


#define UGDRESPONSEBUFLEN 64
#define MAXUGDDATALEN  300

enum ugd
{
   LOOKFOR_UGD
  ,UGDDATA
};

enum ugdprogramstate
{
   UGD_QUERY
  ,UGD_START
  ,UGD_DATA
  ,UGD_CONTINUE
  ,UGD_END
  ,UGD_IDLE
  ,UGD_OK
};
typedef enum ugdprogramstate tm_ugdpgmstate_t;

#define UGDSTART_MSGLEN 10
#define UGDCMDSIZE      8

enum ugdmsgid
{
   MSG_UGDSTART_ID=0
  ,MSG_UGDDATA_ID
  ,MSG_UGDEND_ID
  ,MSG_UGDQUERY_ID
  ,MSG_UGDCRCOK_ID
  ,MSG_UGDABORT_ID
};
typedef enum ugdmsgid tm_ugdmsgid_t;

enum UGDPGMFlashState
{
   UGDFLASH_IDLE
  ,UGDFLASH_ERASE
  ,UGDFLASH_BYTECOUNTWAIT
  ,UGDFLASH_WRITECURVER
  ,UGDFLASH_WRITENEWVER
  ,UGDFLASH_WRITEDATA
  ,UGDFLASH_CHECKCRC
  ,UGDFLASH_NEWVERWAIT
  ,UGDFLASH_CRCWRITEWAIT
};
typedef enum UGDPGMFlashState tm_ugdpgmflashstate_t;

struct serialugd
{
  tm_ugdpgmstate_t ProgramState;
  //tm_timer_t       *hSerialUgdOTAPktTimeOut;
  tm_ugdpgmflashstate_t FlashState;
  tm_timer_t      *hSerialUgdTimeout;
  tm_u32_t        TotalByteCount;
  tm_u32_t        tmpTotalByteCount;
  tm_u32_t        FlashAddr;
  tm_flashtask_t  *hFlashTask;
  tm_u16_t        RxCRC;
  tm_u16_t        RdLen;
  tm_u16_t        CalcCRC;
  tm_u16_t        MsgLen;
  tm_u8_t         CmdString[UGDCMDSIZE];
  tm_u8_t         CmdStringIndex;
  tm_u8_t         CRCBuffer[32];
  tm_u8_t         UGDData[MAXUGDDATALEN];
  tm_u16_t        UGDDataIndex;
  tm_u8_t         SoftVer[5];
  tm_u8_t         DataBuf[MAXUGDDATALEN];
  tm_u8_t         ResponseBuf[UGDRESPONSEBUFLEN];
  tm_u8_t         UgdMode;
  tm_bool_t       bSendAck;
  tm_bool_t       bDLEFound;
  tm_bool_t       bHandleUGD; /* sshold be moved out of this structure */
};
typedef struct serialugd tm_serialugd_t;

void SerialUgdInit(tm_serialugd_t *hSerialUgd, tm_flashtask_t *hFlashTask, tm_timer_t *hSerialUgdTimeout);
void SerialUpgradeProcess(tm_serialugd_t *hSerialUgd,tm_u16_t Size,tm_u8_t Byte);
void HandleUpgrade(tm_serialugd_t *hSerialUgd);


#endif /* __SERIALUGD_H__ */
