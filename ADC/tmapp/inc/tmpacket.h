#ifndef __TM_PACKET_H__
#define __TM_PACKET_H__

#include "tmtypes.h"
#include "tmapp\inc\tmpgn.h"

#define SIZE_DEVID      16
#define PACKETQUEUELEN        6
#define FLASHPACKETQUEUELEN   4

enum packettype
{
   PACKET_PERIODIC
  ,PACKET_IGNOFF
  ,PACKET_IGNON
  ,PACKET_MAINPWROFF
  ,PACKET_MAINPWRON
  ,PACKET_BRKOFF
  ,PACKET_BRKON
  ,PACKET_HANDBRKOFF
  ,PACKET_HANDBRKON
  ,PACKET_SOS
  ,PACKET_CAN
  ,PACKET_DTC
  ,PACKET_DTCP
};
typedef enum packettype tm_packettype_t;

enum pgns
{
   PGN_64777 = 0
  ,PGN_65292
  ,PGN_65483
  ,PGN_65254
  ,PGN_65217
  ,PGN_65276
  ,PGN_65226
  ,PGN_65265
  ,PGN_61444
  ,PGN_65253
  ,PGN_65282
  ,PGN_65263
  ,PGN_65262
  ,MAXPGN
};
typedef enum pgns tm_pgns_t;

/*
 *
   PGN_64777
    Start bit : 32
    Length : 32
   PGN_65292
    Start bit : 40, 48
    Length :     3, 2
   PGN_65483
    Start bit : 48
    Length :     8
   PGN_65254
    Start bit :
    Length :
   PGN_65217
    Start bit :
    Length :
   PGN_65276
    Start bit :
    Length :
   PGN_65226
    Start bit :
    Length :
   PGN_65265
    Start bit :
    Length :
   PGN_61444
    Start bit :
    Length :
   PGN_65253
    Start bit :
    Length :
   PGN_65282
    Start bit :
    Length :
   PGN_65263
    Start bit :
    Length :
   PGN_65262
    Start bit :
    Length :
 * */
struct j1939data
{
  tm_u8_t   buf[48];
  tm_u8_t   buf_len;
};
typedef struct j1939data tm_j1939data_t;

union CANData
{
  tm_pgndata_t PGNData;
  tm_dtc_t     DTC;
  tm_dtc_t     DTCP;
};
typedef union CANData tm_candata_t;
struct packet
{
  tm_u8_t   WriteIndicator;
  tm_packettype_t PacketType;
  tm_candata_t    CANData;
  float     Latitude;
  float     Longitude;
  float     Speed;
  float     GPSOdo;
  float     AnalogIn1;
  float     AnalogIn2;
  float     AnalogIn3;
  float     IntBatt;
  float     VehBatt;
  float     Heading;
  tm_u32_t  UTC;
  tm_s16_t  SeqNum;
  tm_bool_t bMainPwr;
  tm_bool_t bIgnition;
  tm_bool_t bLive;
  tm_bool_t DIn1;
  tm_bool_t DIn2;
  tm_bool_t DIn3;
  tm_bool_t DIn4;
  tm_bool_t DIn5;
};

typedef struct packet tm_packet_t;

#endif  /* __TM_PACKET_H__ */
