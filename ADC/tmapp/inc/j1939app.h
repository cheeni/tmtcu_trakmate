#ifndef J1939APP_H
#define J1939APP_H
#include "j1939.h"

extern void
j1939app_init( void );

extern uint8_t
j1939app_sa_get( void );

extern void
j1939app_process( j1939_t *msg );

extern void
j1939app_update( void );

#endif
