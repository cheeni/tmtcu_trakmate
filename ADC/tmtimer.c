
#include "_fr.h"
#include "extern.h"
#include "tmtypes.h"
#include "tmtimer_private.h"

#define MAXNUMOFTIMEROBJECTS 20

static tm_timer_t Timers[MAXNUMOFTIMEROBJECTS];

extern int tenmsecevent;
extern int onesecevent;
extern int gsmcntrlevent;


int timercount = 0;
int oneseccount = 0;

static tm_s64_t g_TimerCount = 0;

void Init10msecTimer()
{
  /* reload timer 0 setting */
  IO_ICR[2].byte = 0x10;  /* interrupt */
  RT0_TMRLRA = 0x4E1F;    /* 1ms = 50ns x (TMRLR+1) */
  RT0_TMCSR = 0x001B;     /* 50ns (2/40MHz) */
}

static int cancount = 0;
__interrupt void reload0_1_int(void)
{


  if(RT0_TMCSR_UF == 1)
  {
    /* Interrupt generated at every 2.5msec */
    timercount++;
    oneseccount++;
    RT0_TMCSR_UF = 0;		/* request clear */
    IO_WDTCPR1 = 0xa5;	/* H/W WDT Clear */
    if(oneseccount >= ONESECCOUNT)
    {
      oneseccount = 0;
      onesecevent = TM_TRUE;
      g_TimerCount++;
    }
    if(timercount >= TEMMSECCOUNT)
    {
      tenmsecevent = TM_TRUE;
      gsmcntrlevent = TM_TRUE;
      timercount = 0;
    }
  }

  if(RT1_TMCSR_UF == 1)
  {
    RT1_TMCSR_UF = 0;		/* request clear */
    IO_WDTCPR1 = 0xa5;	/* H/W WDT Clear */
    RT1_TMCSR_UF = 0x0;			/* request clear */


    if(cancount++ == 100)
    {
      cancount = 0;
      //IO_CAN1.IF1CMSK.hword = 0x00B3;	/* only updata DATA */


      //IO_CAN1.IF1MCTR.hword = 0x2983;		//NEWDAT=0 MSGLST=0 INTPND=1 UMASK=0
      //TXIE=1 RXIE=0 RMTEN=0 TXRQST=1 EOB=1 DLC=8
      //IO_CAN1.IF1CREQ.hword=0x01;		//IF -> RAM
    }
    //else
    //{
      //cancount = 100;
    //}
  }

  if(RT4_TMCSR_UF == 1)
  {
    RT4_TMCSR_UF = 0;		/* request clear */
    IO_WDTCPR1 = 0xa5;	/* H/W WDT Clear */
  }

  if(RT5_TMCSR_UF == 1)
  {
    RT5_TMCSR_UF = 0;		/* request clear */
    IO_WDTCPR1 = 0xa5;	/* H/W WDT Clear */
  }

}

void InitTimers(void)
{
  int i;
  for(i = 0; i < MAXNUMOFTIMEROBJECTS; i++)
  {
    Timers[i].pUserData = NULL;
    Timers[i].ExpiryTime = 0;
    Timers[i].fptr = NULL;
    Timers[i].TimerState = TMTIME_NOTCREATED;
    Timers[i].pUserData = NULL;
  }
  g_TimerCount = 0;
}

tm_bool_t CreateTimer(tm_timer_t **hTimer, tm_s32_t ExpiryTime, tm_callback_f fptr, void *pUserData)
{
  /* Find empty handle */
  tm_timer_t *pTimer;
  tm_bool_t  bCreated = TM_FALSE;
  int i;

  pTimer = Timers;
  for(i = 0; i < MAXNUMOFTIMEROBJECTS; i++)
  {
    if(pTimer[i].TimerState == TMTIME_NOTCREATED)
    {
      *hTimer = &pTimer[i];
      (*hTimer)->ExpiryTime = g_TimerCount + ExpiryTime;
      (*hTimer)->fptr = fptr;
      (*hTimer)->pUserData = pUserData;
      (*hTimer)->TimerState = TMTIME_SUSPENDED;
      bCreated = TM_TRUE;
      break;
    }
  }
  return bCreated;
}

void ResetTimer(tm_timer_t *hTimer, int TimeCount)
{
  hTimer->ExpiryTime = g_TimerCount + TimeCount;
  hTimer->TimerState = TMTIME_STARTED;
}

void CheckTimers(void)
{
  int i;

  for(i = 0; i < MAXNUMOFTIMEROBJECTS; i++)
  {
    if(Timers[i].TimerState == TMTIME_STARTED)
    {
      if(Timers[i].ExpiryTime <= g_TimerCount)
      {
        Timers[i].TimerState = TMTIME_EXPIRED;
        if(Timers[i].fptr != NULL)
          (*Timers[i].fptr)(Timers[i].pUserData);
      }
    }
  }
}

void SuspendTimer(tm_timer_t *hTimer)
{
  hTimer->TimerState = TMTIME_SUSPENDED;
}

tm_bool_t IsTimerExpired(tm_timer_t *hTimer)
{
  if(hTimer->TimerState == TMTIME_EXPIRED)
    return TM_TRUE;
  else
    return TM_FALSE;
}

tm_bool_t IsTimerRunning(tm_timer_t *hTimer)
{
  if(hTimer->TimerState == TMTIME_STARTED)
    return TM_TRUE;
  else
    return TM_FALSE;
}
