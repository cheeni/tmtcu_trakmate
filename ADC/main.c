/*******************************************************************************
* Copyright (C) 2013 Spansion LLC. All Rights Reserved. 
*
* This software is owned and published by: 
* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software contains source code for use with Spansion 
* components. This software is licensed by Spansion to be adapted only 
* for use in systems utilizing Spansion components. Spansion shall not be 
* responsible for misuse or illegal use of this software for devices not 
* supported herein.  Spansion is providing this software "AS IS" and will 
* not be responsible for issues arising from incorrect user implementation 
* of the software.  
*
* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE,
* REGARDING THE SOFTWARE (INCLUDING ANY ACOOMPANYING WRITTEN MATERIALS), 
* ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED USE, INCLUDING, 
* WITHOUT LIMITATION, THE IMPLIED WARRANTY OF MERCHANTABILITY, THE IMPLIED 
* WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE OR USE, AND THE IMPLIED 
* WARRANTY OF NONINFRINGEMENT.  
* SPANSION SHALL HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, 
* NEGLIGENCE OR OTHERWISE) FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT 
* LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, 
* LOSS OF BUSINESS INFORMATION, OR OTHER PECUNIARY LOSS) ARISING FROM USE OR 
* INABILITY TO USE THE SOFTWARE, INCLUDING, WITHOUT LIMITATION, ANY DIRECT, 
* INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, 
* SAVINGS OR PROFITS, 
* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 
* YOU ASSUME ALL RESPONSIBILITIES FOR SELECTION OF THE SOFTWARE TO ACHIEVE YOUR
* INTENDED RESULTS, AND FOR THE INSTALLATION OF, USE OF, AND RESULTS OBTAINED 
* FROM, THE SOFTWARE.  
*
* This software may be replicated in part or whole for the licensed use, 
* with the restriction that this Disclaimer and Copyright notice must be 
* included with each copy of this software, whether used in part or whole, 
* at all times.  
*****************************************************************************/
/**************************************************************************
 *  Date   :        2013/11/07
 *  Program:        A/D CONVERTER
 *****************************************************************************/

#include "_fr.h"
#include "extern.h"

#include "string.h"

#include "tmtypes.h"
#include "tm_gpios.h"
#include "tmtimer.h"
#include "tcuapp.h"
#include "tmuart.h"
#include <gsmctrl\inc\gsmctrl.h>
#include "tmcan\inc\tmcan.h"
#include "tmgps\inc\tmgps.h"

char SoftwareVer[] = "1.10";
char HardwareVer[] = "1.00";

int tenmsecevent;
int uart1txevent;
int uart1rxevent;
int uart8txevent;
int uart8rxevent;
int gsmcntrlevent;
int onesecevent;
int cantxevent;
int canrxevent;
int uart6txevent;
int uart6rxevent;
int adcevent;
int dmaevent;

char debugstr[128];
static int eventNbr;

extern tm_u8_t tr_data[];
extern tm_u8_t txbuf[];
extern tm_u8_t tmp;
extern tm_u8_t num1;
extern tm_u16_t txlen;
tm_u8_t UART1RxBuffer[256];
tm_u8_t UART6RxBuffer[256];
tm_u8_t UART8RxBuffer[256];
tm_u8_t Len1;
tm_u8_t Len6;
tm_u8_t Len8;
can_t RxCANData[CAN_RXBUFLEN];
can_t TxCANData;
tm_u8_t CanRxLen;

void initial_reload_timer_1(void);

static tm_app_t App;

/*****************************************************************************/
/* Main Routine */
/*****************************************************************************/
void main(void)
{
  int LEDState = 0;
  int SendData = 0;
  // ---- Watchdog timer clear ---------------------------------------
  WDTCPR1 = 0xa5;
  // -----------------------------------------------------------------

  memset(&App,0,sizeof(tm_app_t));
  //IO_DIVR2.bit.DIVP = 0x4;				/* PCLK = 16MHz */

  // Initialisation of Interrupt vector levels
  Vectors_InitIrqLevels();

  /**************/
  //init_ADC();
  /**************/

  // Allow all interrupt levels
  __set_il(0x1F); /* ILM = 31 */
  // Enable interrupts
  __EI();


/* Initailise peripherals */

  /* Initialise GPIOs */
  InitGPIOs();
  cmdparserinit(&App.Parser, &App.DBase, NULL, &App.FlashTask, App.DinStatus);
  tenmsecevent = TM_FALSE;
  uart1txevent = TM_FALSE;
  uart1rxevent = TM_FALSE;
  uart8txevent = TM_FALSE;
  uart8rxevent = TM_FALSE;
  gsmcntrlevent = TM_FALSE;
  onesecevent   = TM_FALSE;
  cantxevent = TM_FALSE;
  canrxevent = TM_FALSE;
  uart6txevent = TM_FALSE;
  uart6rxevent = TM_FALSE;
  adcevent = TM_FALSE;
  dmaevent = TM_FALSE;

  InitTimers();
  /* Initailise Timer */
  Init10msecTimer();

  /* Initialise configuration port */
  InitCfgUART();
  FlashTaskInit(&App.FlashTask);

  FlashTaskRead(&App.FlashTask, CONFIG_STARTADDR,   sizeof(tm_dbase_t), (tm_u8_t *)&App.DBase);
  WaitForFlashTask(&App);

  SanityCheck(&App);

  FlashTaskRead(&App.FlashTask, APP_RUNVER_ADDR, 4, (tm_u8_t *)debugstr);
  WaitForFlashTask(&App);

  if(debugstr[0] == 0xff && debugstr[1] == 0xff && debugstr[2] == 0xff && debugstr[3] == 0xff)
  {
    FlashTaskWrite(&App.FlashTask, APP_RUNVER_ADDR,4,(tm_u8_t *)SoftwareVer);
    WaitForFlashTask(&App);
  }

  InitGsmUART();
  InitGPS(&App.Gps);
  MainProcessInit(&App);
  initial_reload_timer_1();
  init_CAN_0(1);
  initADC();
  //InitSPI();
  sprintf(debugstr,"Starting version %s\r\n",SoftwareVer);
  DPRINTF(debugstr);

  // Endless loop
  CreateTimer(&App.hSerialUgdTimeout, 10, SerialTimerExpired, &App);

  while(1)
  {
    /* Reset watchdog */
    WDTCPR1 = 0xa5;

    eventNbr = -1;
    __DI();
    /*TODO: Disable interrupts */
    if(dmaevent == TM_TRUE)
    {
      dmaevent = TM_FALSE;
      eventNbr = EVENT_DMA;
      goto eventfound;
    }
    if(uart8rxevent == TM_TRUE)
    {
      uart8rxevent = TM_FALSE;
      eventNbr = EVENT_RX_GSM;
      goto eventfound;
    }
    if(uart1rxevent == TM_TRUE)
    {
      uart1rxevent = TM_FALSE;
      eventNbr = EVENT_RX_CONFIG;
      goto eventfound;
    }
    if(uart6rxevent == TM_TRUE)
    {
      uart6rxevent = TM_FALSE;
      eventNbr = EVENT_RX_GPS;
      goto eventfound;
    }
    if(canrxevent == TM_TRUE)
    {
      canrxevent = TM_FALSE;
      eventNbr = EVENT_CANRX;
      goto eventfound;
    }
    if(tenmsecevent == TM_TRUE)
    {
      tenmsecevent = TM_FALSE;
      eventNbr = EVENT_10MSEC;
      goto eventfound;
    }
    if(adcevent == TM_TRUE)
    {
      adcevent = TM_FALSE;
      eventNbr = EVENT_ADC;
      goto eventfound;
    }
    if(gsmcntrlevent == TM_TRUE)
    {
      gsmcntrlevent = TM_FALSE;
      eventNbr = EVENT_GSMCTRL;
      goto eventfound;
    }
    if(onesecevent == TM_TRUE)
    {
      onesecevent = TM_FALSE;
      eventNbr = EVENT_ONESEC;
      goto eventfound;
    }
    if(uart8txevent == TM_TRUE)
    {
      uart8txevent = TM_FALSE;
      eventNbr = EVENT_TX_GSM;
      goto eventfound;
    }
    if(uart1txevent == TM_TRUE)
    {
      uart1txevent = TM_FALSE;
      eventNbr = EVENT_TX_CONFIG;
      goto eventfound;
    }
    if(uart6txevent == TM_TRUE)
    {
      uart6txevent = TM_FALSE;
      eventNbr = EVENT_TX_GPS;
      goto eventfound;
    }
    if(cantxevent == TM_TRUE)
    {
      cantxevent = TM_FALSE;
      eventNbr = EVENT_CANTX;
      goto eventfound;
    }
eventfound:
    /*TODO: Enable interrupts */
    __EI();
    WDTCPR1 = 0xa5;
    switch(eventNbr)
    {
      case EVENT_GSMCTRL:
        GsmCtrl(&App.Gsm);
        break;
      case EVENT_TX_CONFIG:
        UART1DeQueueData();
        break;
      case EVENT_RX_CONFIG:
        UART1_BufferRead(UART1RxBuffer, &Len1);
        if(Len1 > 0)
        {
          //UART1QueueData(UART1RxBuffer, Len1);
          //UART8QueueData(UART1RxBuffer, Len1);
          HandleConfigPortData(&App, UART1RxBuffer, Len1);
        }
        break;
      case EVENT_TX_GSM:
        UART8DeQueueData();
        break;
      case EVENT_RX_GSM:
        UART8_BufferRead(UART8RxBuffer, &Len8);
        if(App.DBase.DebugLevel >= DEBUGLEVEL_2)
        {
          if(Len8 > 0)
          {
            //DPRINTF("GSM Data: ");
            UART1QueueData(UART8RxBuffer, Len8);
            //DPRINTF(" #$\r\n");
          }
        }
        HandleGsmData(&App.Gsm, UART8RxBuffer, Len8);
        break;
      case EVENT_10MSEC:
        MainProcess(&App);
        AdcSamplingTrigger();
        FlashTaskCtrl(&App.FlashTask,TM_FALSE);
        if(App.bHandleUGD == TM_TRUE)
          HandleUpgrade(&App.SerialUgd);
        break;
      case EVENT_ONESEC:
        CheckTimers();
        //PrintAdcVal();
        IncGsmTimeStamp(&App.Gsm);
        SendData++;
        if(SendData >= 2)
        {
#if 0
          if(cantxevent == TM_FALSE)
          {
            TxCANData.id = 0x7DF;
            TxCANData.id = 0x18Db33f1;
            TxCANData.id |= 0x80000000;
            TxCANData.buf_len = 3;
            TxCANData.buf[0] = 2;
            TxCANData.buf[1] = 1;
            TxCANData.buf[2] = 0x0C;
            CANQueueData(&TxCANData);
          }
#endif
          //if(SendData % 30 == 0)
          //{
            //GsmSendData(&App.Gsm, PROFILE0, NULL, 10);
          //}
        }
        if(LEDState == 0)
        {
          LEDState = 1;
          IO_PDR07.byte = 0x09;
        }
        else
        {
          LEDState = 0;
          IO_PDR07.byte = 1;
        }
        break;
      case EVENT_CANRX:
        GetCANData(&App);
#if 0
        if(CanRxLen != 0)
        {
          int i,j;
          i = 0;
            sprintf(debugstr,"%d",CanRxLen);
            DPRINTF("Received objects: ");
            DPRINTF(debugstr);
            DPRINTF("\r\n");
          while(CanRxLen--)
          {
            sprintf(debugstr,"%d",RxCANData[i].buf_len);
            DPRINTF("Received bytes: ");
            DPRINTF(debugstr);
            j = 0;
            DPRINTF("\r\nID: ");
            if(RxCANData[i].id & 0x80000000)
            {
              /* Reset MSB bit which indicates Extended format */
              RxCANData[i].id &= 0x7FFFFFFF;
            }
            sprintf(debugstr,"%x",RxCANData[i].id);
            DPRINTF(debugstr);
            DPRINTF(" Data: ");
            while(RxCANData[i].buf_len--)
            {
              sprintf(debugstr,"%02x",RxCANData[i].buf[j]);
              DPRINTF(debugstr);
              DPRINTF(" ");
              j++;
            }
            i++;
            DPRINTF("\r\n");
          }
        }
#endif
        break;
      case EVENT_CANTX:
        CANDequeueData();
        break;
      case EVENT_RX_GPS:
        UART6_BufferRead(UART6RxBuffer, &Len6);
        HandleGPSData(&App.Gps, UART6RxBuffer, Len6);
        if(App.DBase.DebugLevel >= DEBUGLEVEL_3)
          UART1QueueData(UART6RxBuffer, Len6);
        //if(Len6 > 0)
        //{
          //DPRINTF("GSM Data: ");
          //UART1QueueData(UART6RxBuffer, Len6);
          //DPRINTF(" #$\r\n");
        //}
        break;
      case EVENT_TX_GPS:
        UART8DeQueueData();
        break;
      case EVENT_ADC:
        //PrintAdcVal();
        GetDigitalInputStatus(&App);
        GetAnalogVal(&App);
        break;
      case EVENT_DMA:
        FlashTaskCtrl(&App.FlashTask,TM_TRUE);
        break;
    }
    WDTCPR1 = 0xa5;
  }
}


void HandleConfigPortData(tm_app_t *hApp, tm_u8_t *pData, tm_u16_t Len)
{
  tm_cmdstatus_t CmdStatus;
  int i;
  hApp->ResponseBuf[0] = '\0';
  switch(hApp->CfgPortState)
  {
    case SERIALUGD_STATE:
      for(i = 0; i < Len; i++)
        SerialUpgradeProcess(&hApp->SerialUgd,1,pData[i]);
      break;
    case COMMAND_STATE:
    default:
        UART1QueueData(UART1RxBuffer, Len1);
        CmdStatus = parse(&hApp->Parser, pData, Len, &hApp->ResponseBuf[0]);
        if(CmdStatus == CMDOK_SERIALUPGRADE)
        {
          hApp->bHandleUGD = TM_TRUE;
          hApp->CfgPortState = SERIALUGD_STATE;
          SerialUgdInit(&hApp->SerialUgd, &hApp->FlashTask, hApp->hSerialUgdTimeout);
          DPRINTF("serial upgrade init\r\n");
          //ResetTimer(hApp->hSerialUgdTimeout, SERIALUGDTIMEOUT);
        }
        if(hApp->ResponseBuf[0] != '\0')
        {
          DPRINTF(hApp->ResponseBuf);
        }
        if(CmdStatus == CMD_OK || CmdStatus == CMDOK_SERIALUPGRADE)
        {
          DPRINTF("\r\nOK\r\n");
        }
        if(CmdStatus == CMD_ERROR || CmdStatus == CMDTOOLONG)
        {
          DPRINTF("\r\nERROR\r\n");
        }
      break;
  }
}

static void SerialTimerExpired(void *pUserData)
{
  tm_app_t *hApp;

  DPRINTF("UPGRADE FAILED TIMEOUT\r\n");
  hApp = (tm_app_t *)pUserData;
  hApp->bHandleUGD = TM_FALSE;
  hApp->CfgPortState = COMMAND_STATE;
}

void WaitForFlashTask(tm_app_t *hApp)
{
  WDTCPR1 = 0xa5;

  eventNbr = -1;
  __DI();
  while(1)
  {
    /* Wait for events to occur */
    //eventNbr = WaitForEvents();
    eventNbr = -1;
    if(dmaevent == TM_TRUE)
    {
      /* A response is found. Hanlde it */
      dmaevent = TM_FALSE;
      eventNbr = EVENT_DMA;
      goto eventfound1;
    }
eventfound1:
    __EI();
    if(eventNbr != -1)
    {
      FlashTaskCtrl(&App.FlashTask,TM_TRUE);
    }
    if(FlashTaskStateGet(&hApp->FlashTask) == FLASHTASK_IDLE)
      break;
  }
}

void SanityCheck(tm_app_t *hTMApp)
{
  char *ptr;

  if(hTMApp->DBase.DevID[0] == 0xFF)
    hTMApp->DBase.DevID[0] ='\0';

  if(hTMApp->DBase.LIN[0] == 0xFF)
    hTMApp->DBase.LIN[0] ='\0';

  if(hTMApp->DBase.PrimaryIP[0] == 0xFF)
    strcpy(hTMApp->DBase.PrimaryIP, "122.166.212.137");

  if(hTMApp->DBase.PrimaryPort[0] == 0xFF)
    strcpy(hTMApp->DBase.PrimaryPort, "9020");

  if(hTMApp->DBase.OTAIP[0] == 0xFF)
    strcpy(hTMApp->DBase.OTAIP, "122.166.212.137");

  if(hTMApp->DBase.OTAPort[0] == 0xFF)
    strcpy(hTMApp->DBase.OTAPort, "8021");

  if(hTMApp->DBase.APN[0] == 0xFF)
    strcpy(hTMApp->DBase.APN, "AIRTELGPRS.COM");

  if(hTMApp->DBase.APN[0] == 0xFF)
    strcpy(hTMApp->DBase.APN, "AIRTELGPRS.COM");

  ptr = (char *)&hTMApp->DBase.UpdateRate;
  if(ptr[0] == 0xFF)
    hTMApp->DBase.UpdateRate = 10;

  ptr = (char *)&hTMApp->DBase.PwdUpdateRate;
  if(ptr[0] == 0xFF)
    hTMApp->DBase.PwdUpdateRate = 600;

  ptr = (char *)&hTMApp->DBase.DebugLevel;
  if(ptr[0] == 0xFF)
    hTMApp->DBase.DebugLevel = 0;

}
