

#include "_fr.h"
#include "tmtypes.h"
#include "tmflash\inc\tmspi.h"

//#include "extern.h"

tm_u16_t cs_tr_data[] = {0x009F,0x0000,0x00,0x00,0x00,0x00};
tm_u16_t cs_re_data[6];

__interrupt void Multi_TX10_int(void);
__interrupt void Multi_RX10_int(void);
void Synch_CSIO10_TX(void);
void Synch_CSIO10_RX(void);

extern int dmaevent;

/*****************************************************************************/
/* DMAC initialize */
/*****************************************************************************/
void init_DMA(tm_dma_t *hDMA)
{
  /* DMA TX Initialisation */
  IO_DMACR.word  = 0x80000000;

  IO_DMA0.DCCR.bit.CE = 0;
  DMA0_DCSR = 0x0000;

  //DMA0_DSAR = (int)&cs_tr_data[0]; /* Source address */
  DMA0_DSAR = (int)hDMA->pTx; /* Source address */
  DMA0_DDAR = (int)&IO_MFS10.CSIO.RDR.hword.RDR0; /* Destination address. Read register of multifunction serial port */
  //DMA0_DTCR = 0x06;     /* Transfer count */
  DMA0_DTCR = hDMA->RxLen;     /* Transfer count */

  IO_ICSEL14.byte = 0x2;    /* Multi function serial Tx Interrupt */

  /* Interrupt souce is MFS10 Tx */
  IO_IORR00.byte = 0x65;
  DMA0_DCCR  = 0x07100B10;
  /* DMA start */
  //DMA0_DCCR  = 0x81100B00;
  IO_DMA0.DCCR.bit.CE = 1;
  //DMA0_DCCR= 0x01100B80;

  /* DMA RX Initialisation */

  IO_DMA1.DCCR.bit.CE = 0;
  DMA1_DCSR = 0x0000;

  DMA1_DSAR = (int)&IO_MFS10.CSIO.RDR.hword.RDR0; /* Destination address. Read register of multifunction serial port */
  //DMA1_DDAR = (int)&cs_re_data[0]; /* Source address */
  DMA1_DDAR = (int)hDMA->pRx; /* Source address */
  //DMA1_DTCR = 0x06;     /* Transfer count */
  DMA1_DTCR = hDMA->RxLen;     /* Transfer count */

  IO_ICSEL13.byte = 0x2;    /* Multi function serial Rx Interrupt */

  /* Interrupt souce is MFS10 Tx */
  IO_IORR01.byte = 0x64;
  DMA1_DCCR  = 0x0710B010;
  /* DMA start */
  //DM10_DCCR  = 0x81100B00;
  IO_DMA1.DCCR.bit.CE = 1;

  /* Set TBYTE0 to lenght of the transfer. If the length is greater than 64 split it into multiple writes */
  //IO_MFS10.CSIO.TBYTE0 = 0x6;
  IO_MFS10.CSIO.TBYTE0 = hDMA->RxLen;
  /* Enable transmit and enable fifo transmit interrupt */
  IO_MFS10.CSIO.SCR.bit.TXE = 1; /* Transmit enable */
  IO_MFS10.CSIO.FCR1.bit.FTIE = 1; /* Transmit enable */
}

void Synch_CSIO10(void)
{
  /* SIO3 mode setting */
  IO_MFS10.CSIO.SCR.bit.UPCL = 1;  /* CSIO Initial */

  IO_MFS10.CSIO.SMR.byte = 0x4C; /* Synchronous serial modeMark level is "H" */
  IO_MFS10.CSIO.SMR.bit.SOE = 1; /* Serial Data output enable */
  IO_MFS10.CSIO.SMR.bit.SCKE = 1; /* Serial Data output enable */

  IO_MFS10.CSIO.SCR.bit.SPI = 1;

  /* baud rate 1Mbps (16MHz) */
  IO_MFS10.CSIO.BGR = 15;  /* (16 x 1000000)/1000000-1 = 15 */

  /* CSIO ESCR resister setting */
  IO_MFS10.CSIO.ESCR.byte = 0x00;

  IO_MFS10.CSIO.SCSCR.hword = 0x0023;
  IO_MFS10.CSIO.SCSTR.word = 0x010A;

  IO_MFS10.CSIO.SACSR.bit.TSYNE = 1;

  //IO_MFS10.CSIO.SSR.bit.AWC = 1; /* DMA */


  /* FIFO Configuration */
  IO_MFS10.CSIO.FCR0.bit.FCL1 = 1;
  IO_MFS10.CSIO.FCR0.bit.FCL2 = 1;
  /* Generate FIFO interrupts after receiving 1 byte */
  IO_MFS10.CSIO.FBYTE.hword = 0x100;
  /* Generate FIFO interrupts after transmitting 1 byte */
  IO_MFS10.CSIO.FTICR.hword = 0x04;


  IO_MFS10.CSIO.SCR.bit.RIE = 1;  /* Receive int enable */
  IO_MFS10.CSIO.SCR.bit.RXE = 1; /* Recieve enable */

  IO_MFS10.CSIO.STMCR = 0x1;
  IO_MFS10.CSIO.SACSR.bit.TSYNE = 1;
  IO_MFS10.CSIO.SACSR.hword = 0x43;


  //IO_MFS10.CSIO.SCR.bit.TIE = 1;  /* Transmit int enable */
  //IO_MFS10.CSIO.SCR.bit.TXE = 1; /* Transmit enable */

  IO_MFS10.CSIO.FCR1.bit.FRIIE = 1;
  //IO_MFS10.CSIO.FCR1.bit.FTIE = 1;

  IO_MFS10.CSIO.FCR0.bit.FE1 = 1;
  IO_MFS10.CSIO.FCR0.bit.FE2 = 1;
}


void InitSPI(void)
{
  unsigned char portdata;
  /*
   * MISO = P055 SIN10_0 Input
   * MOSI = P061 SIN10_1 Output
   * SCK  = P057 SCK10_1 OUtput
   * CS   = P062 SCS10_1 Output
   * */

  /* Srilal I/F Output and clock Enable */
  IO_KEYCDR = 0x0E9F;
  IO_KEYCDR = 0x4E9F;
  IO_KEYCDR = 0x8E9F;
  IO_KEYCDR = 0xCE9F;
  IO_EPFR63.byte = 0xFF;   /* SOT10_1,SCK10_1 */

  /* SCS10_1 */
  IO_KEYCDR = 0x01BE;
  IO_KEYCDR = 0x41BE;
  IO_KEYCDR = 0x81BE;
  IO_KEYCDR = 0xC1BE;
  IO_EPFR70.byte = 0xE8;   /* SCS10_1 */

  /* Srilal I/F Output and clock Enable */
  IO_KEYCDR = 0x0E26;
  IO_KEYCDR = 0x4E26;
  IO_KEYCDR = 0x8E26;
  IO_KEYCDR = 0xCE26;
  IO_PFR06.byte = 0x06;  /* SOT10_1 and SCS10_1 as peripheral output */

  IO_KEYCDR = 0x0E25;
  IO_KEYCDR = 0x4E25;
  IO_KEYCDR = 0x8E25;
  IO_KEYCDR = 0xCE25;
  IO_PFR05.byte = 0x80;  /* SCS10_1 output and SIN10_1 as input */

  /*Configure reset pin P095 */
  IO_KEYCDR = 0x0E29;
  IO_KEYCDR = 0x4E29;
  IO_KEYCDR = 0x8E29;
  IO_KEYCDR = 0xCE29;
  IO_PFR09.byte = 0x00;

  IO_KEYCDR = 0x0E09;
  IO_KEYCDR = 0x4E09;
  IO_KEYCDR = 0x8E09;
  IO_KEYCDR = 0xCE09;

  /* Configure as output*/
  IO_DDR09.byte = 0xA0; /* CELL_ON_OFF and Flash Reset*/

  /*Drive it LOW */
  portdata = IO_PDR09.byte;
  portdata |= 0x20;
  IO_PDR09.byte = portdata;


  IO_KEYCDR = 0x24B2;
  IO_KEYCDR = 0x64B2;
  IO_KEYCDR = 0xA4B2;
  IO_KEYCDR = 0xE4B2;
  ADERL1 = 0x0000;

  KEYCDR = 0x0F40;
  KEYCDR = 0x4F40;
  KEYCDR = 0x8F40;
  KEYCDR = 0xcF40;
  IO_PORTEN.byte = 0xFD;

  /* Interrupt level setting */
  //IO_ICR[36].byte = 0x10;  /* Multi Func Serial RX0 */
  //IO_ICR[37].byte = 0x11;  /* Multi Func Serial TX0 */
  IO_ICR[46].byte = 0x10;  /* DMAC interrupt */

  //init_DMA();
  Synch_CSIO10();
}

/* Multi Fuction Serial TX10 Int routine */
int spitx_num1 = 0;
static int cstmp;
__interrupt void Multi_TX10_int(void)
{

  //Reset FDRQ bit to clear interrupt 
  //IO_MFS10.CSIO.FCR1.bit.FDRQ = 0;
#if 1
  IO_MFS10.CSIO.RDR.hword.RDR0 = cs_tr_data[spitx_num1]; /* transmission data set */
  //cstmp = IO_MFS10.UART.SSR.byte; /* dummy read */
  //
  //Reset FDRQ bit to clear interrupt 
  IO_MFS10.CSIO.FCR1.bit.FDRQ = 0;
  spitx_num1++;
  if(spitx_num1 == 6)
  {
    //IO_MFS10.CSIO.FCR1.bit.FTIE = 0; /* Transmit disable */
    IO_MFS10.CSIO.SCR.bit.TIE = 0; /* Transmit disable */
    IO_MFS10.CSIO.FCR1.bit.FTIE = 0;
    //IO_MFS10.CSIO.SCR.bit.TXE = 0; /* Transmit disable */
    spitx_num1 = 0;
    //IO_MFS10.CSIO.SCSCR.bit.SCAM = 0;
  }
#endif
}


/* Multi Function Serial RX10 Int routine */
static int rxcount = 0;
static int num2 = 0;
__interrupt void Multi_RX10_int(void)
{
#if 1
  if(IO_MFS10.CSIO.SSR.bit.ORE == 1)
  {
    IO_MFS10.CSIO.SSR.bit.REC = 1;  /* error flag clear */
  }
  else
  {
    cs_re_data[num2] = IO_MFS10.CSIO.RDR.hword.RDR0; /* reception data read */
    //cstmp = IO_MFS10.UART.SSR.byte; /* dummy read */
    num2++;
    if(num2 == 6)
    {
      num2 = 0;
      rxcount++;
      //IO_MFS10.CSIO.SCR.bit.TXE = 0; /* Transmit disable */
      //IO_MFS10.CSIO.FCR1.bit.FTIE = 0; /* Transmit disable */
      //IO_MFS10.CSIO.SCR.bit.RIE = 0; /* Recieve enable */
    }
  }
#endif
}

__interrupt void DMAC_int(void)
{
  int DMACStatus;

  DMACStatus = DMA0_DCSR;
  if((DMACStatus & 0x1) == 1)
  {
    DMA0_DCSR = 0x0;
    //IO_MFS10.CSIO.SCR.bit.TXE = 0; /* Transmit enable */
    //IO_MFS10.CSIO.SCR.bit.TIE = 0; /* Transmit enable */
    //IO_MFS10.CSIO.FCR1.bit.FTIE = 0; /* Transmit enable */
    DMACStatus = DMA0_DCCR;
    spitx_num1 = 0;
  }
  DMACStatus = DMA1_DCSR;
  if((DMACStatus & 0x1) == 1)
  {
    DMA1_DCSR = 0x0;
    IO_MFS10.CSIO.SCR.bit.TXE = 0; /* Transmit enable */
    //IO_MFS10.CSIO.SCR.bit.TIE = 0; /* Transmit enable */
    IO_MFS10.CSIO.FCR1.bit.FTIE = 0; /* Transmit enable */
    DMACStatus = DMA1_DCCR;
    dmaevent = TM_TRUE;
  }
}



