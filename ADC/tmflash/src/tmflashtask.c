#include "tmflash\src\tmflashtask_private.h"

extern char debugstr[];

static void printptrs(tm_flashtask_t *hFlashTask)
{
  //sprintf(debugstr,"wptr = %x\r\nrptr = %x\r\n",hFlashTask->WritePtr, hFlashTask->ReadPtr);
  //DPRINTF(debugstr);
}

void FlashTaskInit(tm_flashtask_t *hFlashTask)
{
  memset(hFlashTask,0,sizeof(tm_flashtask_t));
  FlashInit(&hFlashTask->Flash);
  hFlashTask->FlashTaskState = FLASHTASK_IDLE;
  hFlashTask->bPacketRead = TM_TRUE;
  hFlashTask->bPacketReady = TM_FALSE;

}

void FlashTaskCtrl(tm_flashtask_t *hFlashTask,tm_bool_t bEvent)
{
  int i;

  if(bEvent == TM_TRUE)
    FlashCtrl(&hFlashTask->Flash);
  switch(hFlashTask->FlashTaskState)
  {
    case FLASHTASK_IDLE:
      break;
    case FLASHTASK_READ:
      if(FlashStateGet(&hFlashTask->Flash) == FLASH_IDLE)
      {
        if(hFlashTask->DataLen != 0)
        {
          if(hFlashTask->DataLen <= FLASHBUFSIZE)
          {
            FlashRead(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, hFlashTask->DataLen);
            hFlashTask->DataLen = 0;
          }
          else
          {
            FlashRead(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, FLASHBUFSIZE);
            hFlashTask->Addr += FLASHBUFSIZE;
            hFlashTask->DataLen -= FLASHBUFSIZE;
            hFlashTask->pData += FLASHBUFSIZE;
          }
        }
        else
        {
          hFlashTask->FlashTaskState = FLASHTASK_IDLE;
        }
      }
      break;
    case FLASHTASK_LOGWRITE:
      if(FlashStateGet(&hFlashTask->Flash) == FLASH_IDLE)
      {
        if(hFlashTask->DataLen != 0)
        {
          if(hFlashTask->DataLen <= FLASHBUFSIZE)
          {
            FlashWrite(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, hFlashTask->DataLen);
            hFlashTask->DataLen = 0;
          }
          else
          {
            FlashWrite(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, FLASHBUFSIZE);
            hFlashTask->Addr += FLASHBUFSIZE;
            hFlashTask->DataLen -= FLASHBUFSIZE;
            hFlashTask->pData += FLASHBUFSIZE;
          }
        }
        else
        {
          if((hFlashTask->WritePtr & SECTORSIZEMASK) == 0)
          {
            /* Check whether next sector is empty, if empty do nothing,
               else erase it */
            hFlashTask->FlashTaskState = FLASHTASK_CHECKSECTORBLANK;
            FlashRead(&hFlashTask->Flash, hFlashTask->WritePtr, hFlashTask->RdPacket, 10);
          }
          else
            hFlashTask->FlashTaskState = FLASHTASK_IDLE;
        }
      }
      break;
    case FLASHTASK_CHECKSECTORBLANK:
      if(FlashStateGet(&hFlashTask->Flash) == FLASH_IDLE)
      {
        for(i = 0;i < 10; i++)
        {
          if(hFlashTask->RdPacket[i] != 0xFF)
          {
            /* Sector not empty, erase it*/
            //UART0QueueData("Erasing sector because it is not empty\r\n",vts_strlen("Erasing sector because it is not empty\r\n"));
            //printwrptr(hFlashTask);
            FlashSectorErase(&hFlashTask->Flash, hFlashTask->WritePtr);
            hFlashTask->FlashTaskState = FLASHTASK_ERASE;
            hFlashTask->NumberOfSectors = 0;
            hFlashTask->ReadPtr = hFlashTask->WritePtr + SECTORSIZE_256K;
            break;
          }
        }
        if(i == 10)
           hFlashTask->FlashTaskState = FLASHTASK_IDLE;
      }
      break;
    case FLASHTASK_WRITE:
      if(FlashStateGet(&hFlashTask->Flash) == FLASH_IDLE)
      {
        if(hFlashTask->DataLen != 0)
        {
          if(hFlashTask->DataLen <= FLASHBUFSIZE)
          {
            FlashWrite(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, hFlashTask->DataLen);
            hFlashTask->DataLen = 0;
          }
          else
          {
            FlashWrite(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, FLASHBUFSIZE);
            hFlashTask->Addr += FLASHBUFSIZE;
            hFlashTask->DataLen -= FLASHBUFSIZE;
            hFlashTask->pData += FLASHBUFSIZE;
          }
        }
        else
        {
          hFlashTask->FlashTaskState = FLASHTASK_IDLE;
        }
      }
      break;
    case FLASHTASK_ERASEBEFOREWRITE:
      if(FlashStateGet(&hFlashTask->Flash) == FLASH_IDLE)
      {
        //UART0QueueData((tm_u8_t *)"erased before write\r\n",vts_strlen("erased before write\r\n"));
        if(hFlashTask->NumberOfSectors != 0)
        {
          FlashSectorErase(&hFlashTask->Flash, hFlashTask->SectorAddr);
          hFlashTask->NumberOfSectors--;
          hFlashTask->SectorAddr += SECTORSIZE_256K;
        }
        else
        {
          /* All required sectors erased. Write data in chunks
           * of 256 */
          if(hFlashTask->DataLen <= FLASHBUFSIZE)
          {
            //UART0QueueData((tm_u8_t *)"writing to flash\r\n",vts_strlen("writing to flash\r\n"));
            FlashWrite(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, hFlashTask->DataLen);
            hFlashTask->DataLen = 0;
          }
          else
          {
            FlashWrite(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, FLASHBUFSIZE);
            hFlashTask->Addr += FLASHBUFSIZE;
            hFlashTask->DataLen -= FLASHBUFSIZE;
            hFlashTask->pData += FLASHBUFSIZE;
          }
          hFlashTask->FlashTaskState = FLASHTASK_WRITE;
        }
      }
      break;
    case FLASHTASK_ERASE:
      if(FlashStateGet(&hFlashTask->Flash) == FLASH_IDLE)
      {
        if(hFlashTask->NumberOfSectors != 0)
        {
          sprintf(debugstr,"NumberOfSectors remaining %d\r\n",hFlashTask->NumberOfSectors );
          DPRINTF(debugstr);
          FlashSectorErase(&hFlashTask->Flash, hFlashTask->SectorAddr);
          hFlashTask->NumberOfSectors--;
          hFlashTask->SectorAddr += SECTORSIZE_256K;
        }
        else
        {
          //UART0QueueData((tm_u8_t *)"log erase complete\r\n", vts_strlen("log erase complete\r\n"));
          hFlashTask->FlashTaskState = FLASHTASK_IDLE;
        }
      }
      break;
    case FLASHTASK_LOGREAD:
      if(FlashStateGet(&hFlashTask->Flash) == FLASH_IDLE)
      {
        //for(i = 0; i < 10;i++)
        //{
         // vts_itoa(hFlashTask->RdPacket[i], debugstr);
        //}
        if(hFlashTask->DataLen != 0)
        {
          if(hFlashTask->DataLen <= FLASHBUFSIZE)
          {
            FlashRead(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pReadData, hFlashTask->DataLen);
            hFlashTask->DataLen = 0;
          }
          else
          {
            FlashRead(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pReadData, FLASHBUFSIZE);
            hFlashTask->Addr += FLASHBUFSIZE;
            hFlashTask->DataLen -= FLASHBUFSIZE;
            hFlashTask->pReadData += FLASHBUFSIZE;
          }
        }
        else
        {
          hFlashTask->RdPacket[0] = DATAREADIND;
          FlashWrite(&hFlashTask->Flash, hFlashTask->ReadPtr, hFlashTask->RdPacket, 1);
          hFlashTask->FlashTaskState = FLASHTASK_WAITFORRD;
        }
      }
      break;
    case FLASHTASK_WAITFORRD:
      if(FlashStateGet(&hFlashTask->Flash) == FLASH_IDLE)
      {
        printptrs(hFlashTask);
        if(((hFlashTask->ReadPtr + FLASHPACKETSIZE) & SECTORSIZEMASK) == 0)
        {
          //UART0QueueData((tm_u8_t *)"erasing sector", vts_strlen("erasing sector"));
          DPRINTF("secotr read \r\n");
          printptrs(hFlashTask);
          FlashSectorErase(&hFlashTask->Flash, hFlashTask->ReadPtr);
          hFlashTask->FlashTaskState = FLASHTASK_ERASE;
          hFlashTask->NumberOfSectors = 0;
        }
        else
        {
          hFlashTask->FlashTaskState = FLASHTASK_IDLE;
        }
        hFlashTask->ReadPtr += FLASHPACKETSIZE;
        if(hFlashTask->ReadPtr > LASTPACKETADDR)
          hFlashTask->ReadPtr = LOGSTARTADDR;
        hFlashTask->bPacketReady = TM_TRUE;
        printptrs(hFlashTask);
      }
      break;
    case FLASHTASK_GETWRPTR:
      if(FlashStateGet(&hFlashTask->Flash) == FLASH_IDLE)
      {
        if(hFlashTask->ReadData[1] != DATAWRITEIND && hFlashTask->ReadData[1] != 0xFF)
        {
          /* Flash corrupted? */
          DPRINTF("Erasing log 1\r\n");
          FlashTaskLogDataErase(hFlashTask);
          break;
        }
        if(hFlashTask->ReadData[1] == DATAWRITEIND)
        {
          /* Continue till you find FF */
          DPRINTF("Found DATAWRITEIND 1\r\n");
          hFlashTask->FlashTaskState = FLASHTASK_WRPTREND;
          hFlashTask->Addr += FLASHPACKETSIZE;
          if(hFlashTask->Addr > LASTPACKETADDR)
          {
            /* If reached end of flash, then this must be the last
             * stored packet and the begenning of the flash must be
             * empty */
            DPRINTF("Found read write pointers 1\r\n");
            printptrs(hFlashTask);
            hFlashTask->WritePtr = hFlashTask->Addr - FLASHPACKETSIZE;
            hFlashTask->FlashTaskState = FLASHTASK_GETRDPTR;
            hFlashTask->FlashTaskState = FLASHTASK_IDLE;
          }
          else
          {
            FlashRead(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->ReadData, 10);
          }
        }
        else
        {
          hFlashTask->Addr += SECTORSIZE_256K;
          if(hFlashTask->Addr > LASTPACKETADDR)
          {
            /* Reached end of Flash. Log section is empyt */
            DPRINTF("Found read write pointers 2\r\n");
            printptrs(hFlashTask);
            hFlashTask->WritePtr = LOGSTARTADDR;
            hFlashTask->ReadPtr = LOGSTARTADDR;
            hFlashTask->FlashTaskState = FLASHTASK_IDLE;
            printptrs(hFlashTask);
          }
          else
          {
            FlashRead(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->ReadData, 10);
          }
        }
      }
      break;
    case FLASHTASK_WRPTREND:
      if(FlashStateGet(&hFlashTask->Flash) == FLASH_IDLE)
      {
        if(hFlashTask->ReadData[1] != 0xFF && hFlashTask->ReadData[1] != DATAWRITEIND)
        {
          /* Flash corrupted?*/
          DPRINTF("Erasing log 2\r\n");
          FlashTaskLogDataErase(hFlashTask);
          break;
        }
        if(hFlashTask->ReadData[1] == 0xFF)
        {
          /* Continue till you find FF */
          DPRINTF("Found write pointer 1\r\n");
          printptrs(hFlashTask);
          hFlashTask->WritePtr = hFlashTask->Addr;
          hFlashTask->Addr = hFlashTask->Addr - FLASHPACKETSIZE;
          FlashRead(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->ReadData, 10);
          hFlashTask->FlashTaskState = FLASHTASK_GETRDPTR;
        }
        else
        {
          hFlashTask->Addr += FLASHPACKETSIZE;
          if(hFlashTask->Addr > LASTPACKETADDR)
          {
            /* If wrapping around, then the first sector must be either empty
             * or is filled and should be erased */
            DPRINTF("Found write pointer 2\r\n");
            printptrs(hFlashTask);
            hFlashTask->FlashTaskState = FLASHTASK_GETRDPTR;
            hFlashTask->Addr = hFlashTask->Addr - FLASHPACKETSIZE;
            FlashRead(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->ReadData, 10);
            hFlashTask->WritePtr = LOGSTARTADDR;
          }
          else
          {
            FlashRead(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->ReadData, 10);
          }
        }
      }
      break;
    case FLASHTASK_GETRDPTR:
      if(FlashStateGet(&hFlashTask->Flash) == FLASH_IDLE)
      {
        if(hFlashTask->ReadData[0] != 0xFF && hFlashTask->ReadData[0] != DATAREADIND)
        {
          /* error condition Erase logging section */
            DPRINTF("Erasing log 3\r\n");
          FlashTaskLogDataErase(hFlashTask);
          printptrs(hFlashTask);
        }
        else if(hFlashTask->ReadData[1] != 0xFF && hFlashTask->ReadData[1] != DATAWRITEIND)
        {
          /* error condition */
            DPRINTF("Erasing log 4\r\n");
          FlashTaskLogDataErase(hFlashTask);
          printptrs(hFlashTask);
        }
        else if (hFlashTask->ReadData[0] == DATAREADIND && hFlashTask->ReadData[1] == DATAWRITEIND)
        {
          DPRINTF("Found rd wr pointers 1\r\n");
          hFlashTask->ReadPtr = hFlashTask->Addr + FLASHPACKETSIZE;
          if(hFlashTask->ReadPtr > LASTPACKETADDR)
            hFlashTask->ReadPtr = LOGSTARTADDR;
          hFlashTask->FlashTaskState = FLASHTASK_IDLE;
          printptrs(hFlashTask);
        }
        else if(hFlashTask->ReadData[0] == 0xFF && hFlashTask->ReadData[1] == 0xFF)
        {
          DPRINTF("Found rd wr pointers 2\r\n");
          hFlashTask->ReadPtr = hFlashTask->Addr + FLASHPACKETSIZE;
          if(hFlashTask->ReadPtr > LASTPACKETADDR)
            hFlashTask->ReadPtr = LOGSTARTADDR;
          hFlashTask->FlashTaskState = FLASHTASK_IDLE;
          printptrs(hFlashTask);
        }
        else
        {
          hFlashTask->Addr -= FLASHPACKETSIZE;
          if(hFlashTask->Addr < LOGSTARTADDR)
            hFlashTask->Addr = LASTPACKETADDR;
          // The following condition should never occur idealy
          if(hFlashTask->Addr == hFlashTask->WritePtr)
          {
            /* search complete */
          DPRINTF("Erasing log 5\r\n");
            FlashTaskLogDataErase(hFlashTask);
            printptrs(hFlashTask);
          }
          else
            FlashRead(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->ReadData, 10);
        }
      }
      break;
  }
}

void FlashTaskRead(tm_flashtask_t *hFlashTask, tm_u32_t Addr, tm_u16_t Len, tm_u8_t *pData)
{
  hFlashTask->Addr = Addr;
  hFlashTask->pData = pData;
  hFlashTask->DataLen = Len;

  if(hFlashTask->DataLen <= FLASHBUFSIZE)
  {
    FlashRead(&hFlashTask->Flash, Addr, pData, Len);
    hFlashTask->DataLen = 0;
  }
  else
  {
    FlashRead(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, FLASHBUFSIZE);
    hFlashTask->Addr += FLASHBUFSIZE;
    hFlashTask->DataLen -= FLASHBUFSIZE;
    hFlashTask->pData += FLASHBUFSIZE;
  }
  hFlashTask->FlashTaskState = FLASHTASK_READ;
}

void FlashTaskWrite(tm_flashtask_t *hFlashTask, tm_u32_t Addr, tm_u16_t Len, tm_u8_t *pData)
{
  hFlashTask->Addr = Addr;
  hFlashTask->pData = pData;
  hFlashTask->DataLen = Len;
  hFlashTask->SectorAddr = Addr;
  hFlashTask->NumberOfSectors = Len / SECTORSIZE_256K;
  if(hFlashTask->DataLen <= FLASHBUFSIZE)
  {
    FlashWrite(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, hFlashTask->DataLen);
    hFlashTask->DataLen = 0;
  }
  else
  {
    FlashWrite(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, FLASHBUFSIZE);
    hFlashTask->Addr += FLASHBUFSIZE;
    hFlashTask->DataLen -= FLASHBUFSIZE;
    hFlashTask->pData += FLASHBUFSIZE;
  }
  hFlashTask->FlashTaskState = FLASHTASK_WRITE;
}

void FlashTaskEraseAndWrite(tm_flashtask_t *hFlashTask, tm_u32_t Addr, tm_u16_t Len, tm_u8_t *pData)
{
  hFlashTask->Addr = Addr;
  hFlashTask->pData = pData;
  hFlashTask->DataLen = Len;
  hFlashTask->SectorAddr = Addr;
  hFlashTask->NumberOfSectors = Len / SECTORSIZE_256K;
  hFlashTask->FlashTaskState = FLASHTASK_ERASEBEFOREWRITE;
  FlashSectorErase(&hFlashTask->Flash, hFlashTask->SectorAddr);
}

tm_flashtaskstate_t FlashTaskStateGet(tm_flashtask_t *hFlashTask)
{
  return hFlashTask->FlashTaskState;
}

void FlashTaskEraseSoftImage(tm_flashtask_t *hFlashTask)
{
  hFlashTask->SectorAddr = APP_BYTECOUNTADDR;
  hFlashTask->NumberOfSectors = 2;
  FlashSectorErase(&hFlashTask->Flash, hFlashTask->SectorAddr);
  hFlashTask->FlashTaskState = FLASHTASK_ERASE;
}

void FlashTaskLogWrite(tm_flashtask_t *hFlashTask, tm_u32_t Addr, tm_u16_t Len, tm_u8_t *pData)
{
  hFlashTask->Addr = Addr;
  hFlashTask->pData = pData;
  hFlashTask->DataLen = Len;
  hFlashTask->SectorAddr = Addr;
  hFlashTask->NumberOfSectors = 0;

  //DPRINTF("FlashTaskLogWrite called\r\n");
  if(hFlashTask->DataLen < FLASHBUFSIZE)
  {
    FlashWrite(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, hFlashTask->DataLen);
    hFlashTask->DataLen = 0;
  }
  else
  {
    FlashWrite(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pData, FLASHBUFSIZE);
    hFlashTask->Addr += FLASHBUFSIZE;
    hFlashTask->DataLen -= FLASHBUFSIZE;
    hFlashTask->pData += FLASHBUFSIZE;
  }
  hFlashTask->FlashTaskState = FLASHTASK_LOGWRITE;
}

tm_bool_t FlashTaskLogPacket(tm_flashtask_t *hFlashTask, tm_u8_t *pData, tm_u16_t Len)
{
  if(hFlashTask->FlashTaskState == FLASHTASK_IDLE && FlashStateGet(&hFlashTask->Flash) == FLASH_IDLE)
  {
    printptrs(hFlashTask);
    FlashTaskLogWrite(hFlashTask, (hFlashTask->WritePtr + 1), Len , pData);
    hFlashTask->WritePtr += FLASHPACKETSIZE;
    if(hFlashTask->WritePtr > LASTPACKETADDR)
      hFlashTask->WritePtr = LOGSTARTADDR;
    return TM_TRUE;
  }
  else
  {
    /* Save data and write when flashtask is idle*/
    /* Have a queue of 4 packets ?*/
    //UART0QueueData((tm_u8_t *)"unable to log\r\n ", vts_strlen("unable to log\r\n "));
    return TM_FALSE;
  }
}

tm_bool_t CheckLoggedPackets(tm_flashtask_t *hFlashTask, tm_u8_t *pData,tm_u16_t Size)
{
  int i;
  if(hFlashTask->FlashTaskState != FLASHTASK_IDLE)
    return TM_FALSE;

  if(hFlashTask->WritePtr != hFlashTask->ReadPtr)
  {
    if(hFlashTask->bPacketRead == TM_TRUE && hFlashTask->FlashTaskState == FLASHTASK_IDLE)
    {
      hFlashTask->bPacketRead = TM_FALSE;
      hFlashTask->bPacketReady = TM_FALSE;
      hFlashTask->pData = pData;

      hFlashTask->Addr = hFlashTask->ReadPtr;
      hFlashTask->pReadData = hFlashTask->RdPacket;
      hFlashTask->DataLen = FLASHPACKETSIZE;

      if(hFlashTask->DataLen <= FLASHBUFSIZE)
      {
        FlashRead(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pReadData, hFlashTask->DataLen);
        hFlashTask->DataLen = 0;
      }
      else
      {
        FlashRead(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->pReadData, FLASHBUFSIZE);
        hFlashTask->Addr += FLASHBUFSIZE;
        hFlashTask->DataLen -= FLASHBUFSIZE;
        hFlashTask->pReadData += FLASHBUFSIZE;
      }
      hFlashTask->FlashTaskState = FLASHTASK_LOGREAD;
    }
  }
  if(hFlashTask->bPacketReady == TM_TRUE)
  {
    /* If packet read from flash return it to gsm module to send it*/
    hFlashTask->bPacketRead = TM_TRUE;
    hFlashTask->bPacketReady = TM_FALSE;
    for(i = 0; i < Size; i++)
      pData[i] = hFlashTask->RdPacket[i+1];
    return TM_TRUE;
  }
  else
    return TM_FALSE;
}

void FlashTaskGetRdWrPtrs(tm_flashtask_t *hFlashTask)
{
  hFlashTask->Addr = LOGSTARTADDR;
  FlashRead(&hFlashTask->Flash, hFlashTask->Addr, hFlashTask->ReadData, 10);
  hFlashTask->FlashTaskState = FLASHTASK_GETWRPTR;
}

void FlashTaskLogDataErase(tm_flashtask_t *hFlashTask)
{
  DPRINTF("FlashTaskLogDataErase called\r\n");
  hFlashTask->ReadPtr = LOGSTARTADDR;
  hFlashTask->WritePtr = LOGSTARTADDR;
  hFlashTask->SectorAddr = LOGSTARTADDR;
  hFlashTask->NumberOfSectors = NUMBEROFLOGSECTORS;
  FlashSectorErase(&hFlashTask->Flash, hFlashTask->SectorAddr);
  hFlashTask->FlashTaskState = FLASHTASK_ERASE;
}

tm_bool_t IsLogEmpty(tm_flashtask_t *hFlashTask)
{
  if(hFlashTask->WritePtr != hFlashTask->ReadPtr)
    return TM_FALSE;
  else
    return TM_TRUE;
}

