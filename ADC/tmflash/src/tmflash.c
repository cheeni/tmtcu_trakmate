#include "tmflash\src\tmflash_private.h"

void FlashInit(tm_flash_t *hFlash)
{
  memset((void *)hFlash,0,sizeof(tm_flash_t));

  hFlash->FlashState = FLASH_IDLE;
  InitSPI();
}

void FlashCtrl(tm_flash_t *hFlash)
{
  int i;
  tm_u8_t Status1;
  tm_u8_t Status2;

  switch(hFlash->FlashState)
  {
    case FLASH_IDLE:
      break;
    case FLASH_READ_BUSY:
        hFlash->TxBuf[0] = FLASH_READSTATUS1;
        hFlash->DMA.pTx = hFlash->TxBuf;
        hFlash->DMA.pRx = hFlash->RxBuf;
        hFlash->DMA.TxLen = 2;
        hFlash->DMA.RxLen = 2;
        init_DMA(&hFlash->DMA);
        hFlash->FlashState = FLASH_CHKREADSTATUS;
      break;
    case FLASH_CHKREADSTATUS:
        Status1 = hFlash->RxBuf[1];
        Status2 = hFlash->RxBuf[2];
        if(CheckStatus(Status1, Status2, READSTATUS) == TM_TRUE)
        {
          //UART0QueueData((tm_u8_t *)"Flash read complete \r\n",vts_strlen("Flash read complete \r\n"));
          for(i = 0; i < hFlash->ReadLen; i++)
          {
            hFlash->pData[i] = (tm_u8_t)(hFlash->RxBuf[5+i] & 0xFF);    /* It was 6+i earlier with 0x1B command */
          }
          hFlash->FlashState = FLASH_IDLE;
          break;
        }
        hFlash->TxBuf[0] = FLASH_READSTATUS1;
        hFlash->DMA.pTx = hFlash->TxBuf;
        hFlash->DMA.pRx = hFlash->RxBuf;
        hFlash->DMA.TxLen = 2;
        hFlash->DMA.RxLen = 2;
        init_DMA(&hFlash->DMA);
        hFlash->FlashState = FLASH_CHKREADSTATUS;
      break;
    case FLASH_WAITFOR_WE_SENT:

        hFlash->TxBuf[0] = FLASH_PAGEPRGM;
        hFlash->TxBuf[1] = (hFlash->WriteAddr & 0xFF000000) >> 24;
        hFlash->TxBuf[2] = (hFlash->WriteAddr & 0xFF0000) >> 16;
        hFlash->TxBuf[3] = (hFlash->WriteAddr & 0xFF00) >> 8;
        hFlash->TxBuf[4] = (hFlash->WriteAddr & 0xFF);

        for(i = 0; i < hFlash->WriteLen; i++)
          hFlash->TxBuf[5+i] = (tm_u16_t)hFlash->pData[i];

        hFlash->DMA.pTx = hFlash->TxBuf;
        hFlash->DMA.pRx = hFlash->RxBuf;
        hFlash->DMA.TxLen = 5;
        hFlash->DMA.RxLen = hFlash->WriteLen + 5;
        init_DMA(&hFlash->DMA);

        hFlash->FlashState = hFlash->NextFlashState;
      break;
    case FLASH_WRITE_BUSY:
        hFlash->TxBuf[0] = FLASH_READSTATUS1;
        hFlash->DMA.pTx = hFlash->TxBuf;
        hFlash->DMA.pRx = hFlash->RxBuf;
        hFlash->DMA.TxLen = 1;
        hFlash->DMA.RxLen = 2;
        init_DMA(&hFlash->DMA);
        hFlash->FlashState = FLASH_CHECKWRITESTATUS;
      break;
    case FLASH_CHECKWRITESTATUS:
        Status1 = hFlash->RxBuf[1];
        Status2 = hFlash->RxBuf[2];
        if(CheckStatus(Status1, Status2, READSTATUS) == TM_TRUE)
        {
          //UART0QueueData((tm_u8_t *)"Flash write complete\r\n",vts_strlen("Flash write complete\r\n"));
          hFlash->FlashState = FLASH_IDLE;
          break;
        }
        hFlash->TxBuf[0] = FLASH_READSTATUS1;
        hFlash->DMA.pTx = hFlash->TxBuf;
        hFlash->DMA.pRx = hFlash->RxBuf;
        hFlash->DMA.TxLen = 1;
        hFlash->DMA.RxLen = 2;
        hFlash->DMA.pRx = hFlash->RxBuf;
        init_DMA(&hFlash->DMA);
        hFlash->FlashState = FLASH_CHECKWRITESTATUS;
      break;
    case FLASH_SECTOR_ERASE:
        hFlash->TxBuf[0] = FLASH_SECTERASE;
        hFlash->TxBuf[1] = (hFlash->WriteAddr & 0xFF000000) >> 24;
        hFlash->TxBuf[2] = (hFlash->WriteAddr & 0xFF0000) >> 16;
        hFlash->TxBuf[3] = (hFlash->WriteAddr & 0xFF00) >> 8;
        hFlash->TxBuf[4] = (hFlash->WriteAddr & 0xFF);
        hFlash->DMA.TxLen = 5;
        hFlash->DMA.RxLen = 5;
        hFlash->DMA.pRx = hFlash->RxBuf;
        hFlash->DMA.pTx = hFlash->TxBuf;
        init_DMA(&hFlash->DMA);
        hFlash->FlashState = FLASH_SECTOR_ERASE_BUSY;
      break;
    case FLASH_SECTOR_ERASE_BUSY:
        hFlash->TxBuf[0] = FLASH_READSTATUS1;
        hFlash->DMA.pTx = hFlash->TxBuf;
        hFlash->DMA.pRx = hFlash->RxBuf;
        hFlash->DMA.TxLen = 1;
        hFlash->DMA.RxLen = 2;
        init_DMA(&hFlash->DMA);
        hFlash->FlashState = FLASH_CHKSECTORERASESTATUS;
      break;
    case FLASH_CHKSECTORERASESTATUS:
        Status1 = hFlash->RxBuf[1];
        Status2 = hFlash->RxBuf[2];
        if(CheckStatus(Status1, Status2, READSTATUS) == TM_TRUE)
        {
          //UART0QueueData((tm_u8_t *)"Flash sector Erase complete\r\n",vts_strlen("Flash sector Erase complete\r\n"));
          hFlash->FlashState = FLASH_IDLE;
          break;
        }
        hFlash->TxBuf[0] = FLASH_READSTATUS1;
        hFlash->DMA.pTx = hFlash->TxBuf;
        hFlash->DMA.pRx = hFlash->RxBuf;
        hFlash->DMA.TxLen = 1;
        hFlash->DMA.RxLen = 2;
        hFlash->DMA.pRx = hFlash->RxBuf;
        init_DMA(&hFlash->DMA);
        hFlash->FlashState = FLASH_CHKSECTORERASESTATUS;
      break;
    case FLASH_CHIP_ERASE:
      break;
    case FLASH_CHIP_ERASE_BUSY:
      break;
    case FLASH_CHKCHIPERASESTATUS:
      break;
  }
}

tm_bool_t FlashRead(tm_flash_t *hFlash, tm_u32_t Addr, tm_u8_t *pData, tm_u16_t Len)
{
  if(hFlash->FlashState != FLASH_IDLE)
  {
    return TM_FALSE;
  }

  hFlash->TxBuf[0] = FLASH_READ;
  hFlash->TxBuf[1] = (Addr & 0xFF000000) >> 24;
  hFlash->TxBuf[2] = (Addr & 0xFF0000) >> 16;
  hFlash->TxBuf[3] = (Addr & 0xFF00) >> 8;
  hFlash->TxBuf[4] = (Addr & 0xFF);

  hFlash->pData = pData;
  hFlash->ReadLen = Len;

  hFlash->DMA.pTx = hFlash->TxBuf;
  hFlash->DMA.pRx = hFlash->RxBuf;
  hFlash->DMA.TxLen = 5;
  hFlash->DMA.RxLen = 5+Len;
  init_DMA(&hFlash->DMA);

  hFlash->FlashState = FLASH_READ_BUSY;
  return TM_TRUE;
}

tm_bool_t FlashWrite(tm_flash_t *hFlash, tm_u32_t Addr, tm_u8_t *pData, tm_u16_t Len)
{
  if(hFlash->FlashState != FLASH_IDLE)
  {
    return TM_FALSE;
  }

  hFlash->pData = pData;
  hFlash->WriteLen = Len;
  hFlash->WriteAddr = Addr;

  /* Send Write Enable Command */
   hFlash->TxBuf[0] = FLASH_WRITEENABLE;

  hFlash->DMA.pTx = hFlash->TxBuf;
  hFlash->DMA.pRx = hFlash->RxBuf;
  hFlash->DMA.TxLen = 1;
  hFlash->DMA.RxLen = 1;

  init_DMA(&hFlash->DMA);

  hFlash->FlashState = FLASH_WAITFOR_WE_SENT;
  hFlash->NextFlashState = FLASH_WRITE_BUSY;

  return TM_TRUE;
}


tm_bool_t CheckStatus(tm_u8_t s1, tm_u8_t s2, tm_flashstatus_t status)
{
  if(s1 & FLASHSTATIS_WIP)
  {
    return TM_FALSE;
  }
  else
  {
    return TM_TRUE;
  }
}

tm_bool_t FlashSectorErase(tm_flash_t *hFlash, tm_u32_t SectAddr)
{
  if(hFlash->FlashState != FLASH_IDLE)
  {
    return TM_FALSE;
  }
  //UART0QueueData((tm_u8_t *)"Init Sector Erase\r\n",vts_strlen("Init sector Erase\r\n"));
  hFlash->WriteAddr = SectAddr;
  hFlash->TxBuf[0] = FLASH_WRITEENABLE;
  hFlash->DMA.TxLen = 1;
  hFlash->DMA.RxLen = 1;
  hFlash->DMA.pRx = hFlash->RxBuf;
  hFlash->DMA.pTx = hFlash->TxBuf;
  init_DMA(&hFlash->DMA);
  hFlash->FlashState = FLASH_SECTOR_ERASE;
  return TM_TRUE;
}

tm_flashstate_t FlashStateGet(tm_flash_t *hFlash)
{
  return hFlash->FlashState;
}
