#ifndef __TMFLASH_PRIVATE__
#define __TMFLASH_PRIVATE__

#include "tmflash\inc\tmflash.h"


/* Flash COmmands */

#define FLASH_READ        0x13
#define FLASH_READID      0x9F
#define FLASH_PAGEPRGM    0x12
#define FLASH_SECTERASE   0xDC
#define FLASH_READSTATUS1 0x05
#define FLASH_READSTATUS2 0x07
#define FLASH_WRITEENABLE 0x06

#define FLASHSTATIS_WIP 1
enum flash_status
{
   READSTATUS
  ,ERASESTATUS
  ,PROGRAMSTATUS
};
typedef enum flash_status tm_flashstatus_t;

tm_bool_t CheckStatus(tm_u8_t s1, tm_u8_t s2, tm_flashstatus_t status);

#endif  /* __TMFLASH_PRIVATE__ */
