#ifndef __TMSPI_H__
#define __TMSPI_H__

#include "tmtypes.h"

struct dma
{
  tm_u16_t *pTx;
  tm_u16_t *pRx;
  tm_u16_t TxLen;
  tm_u16_t RxLen;
};
typedef struct dma tm_dma_t;

void init_DMA(tm_dma_t *hDMA);
void InitSPI(void);

#endif /* __TMSPI_H__ */
