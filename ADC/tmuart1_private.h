
#ifndef _TMUART_PRIVATE_H_
#define _TMUART_PRIVATE_H_

#include "tmuart.h"


#define RXBUFLEN 32
#define TXQUEUEBUFLEN 2048
#define QUEUELEN 64

struct tm_queue
{
  tm_u8_t *pData;
  tm_u16_t Len;
  tm_bool_t bValid;
};
typedef struct tm_queue tm_queue_t;

struct tm_uart
{
  tm_u8_t   *pRxBufferWrPtr;
  tm_u8_t   *pRxBufferRdPtr;
  tm_u8_t   *pTx;
  tm_u8_t   *pTxQueueBufferWrPtr;
  tm_queue_t Queue[QUEUELEN];
  tm_u8_t   QueueRdIndex;
  tm_u8_t   QueueWrIndex;
  tm_u16_t   TxLen;
  tm_u8_t   RxBuffer[RXBUFLEN];
  tm_u8_t   TxQueueBuffer[TXQUEUEBUFLEN];
  tm_u8_t   NbrOfBlocksInQueue;
  tm_bool_t bDataSent;
};

typedef struct tm_uart tm_uart_t;

#endif /* _TMUART_PRIVATE_H_ */
