
#include "_fr.h"
#include "extern.h"

#include "tmtypes.h"
#include "tmuart8_private.h"

static tm_uart_t g_Uart8;

extern int uart8rxevent;
extern int uart8txevent;

void Asynch_UART8_Init(void)
{

  /* UART8 mode setting */
  IO_MFS8.UART.SCR.bit.UPCL = 1;	/* UART Initial */

  IO_MFS8.UART.SMR.byte = 0x00;	/* Asynchronous serial mode */
  IO_MFS8.UART.SMR.bit.SOE = 1;	/* Serial Data output enable */

  /* baund rate 115.2kbps (16MHz) */
  IO_MFS8.UART.BGR = 138;			/* (16x1000000)/(138+1) = 115107 */

  /* UART control resister setting */
  IO_MFS8.UART.SCR.byte = 0x00;

  /* UART ESCR resister setting */
  IO_MFS8.UART.ESCR.byte = 0x00;

  /* Serial Transmission enable */
  IO_MFS8.UART.SCR.bit.TIE =1;	/* Transmit int enable */
  IO_MFS8.UART.SCR.bit.TXE = 1;	/* Transmit enable */

  IO_MFS8.UART.SCR.bit.RIE = 1;	/* Receive int enable */
  IO_MFS8.UART.SCR.bit.RXE = 1;	/* Receive enable */

}

void InitGsmUART(void)
{
  /* Srilal I/F Output Enable */
  IO_KEYCDR = 0x0E9D;
  IO_KEYCDR = 0x4E9D;
  IO_KEYCDR = 0x8E9D;
  IO_KEYCDR = 0xCE9D;
  IO_EPFR61.byte = 0xFD;  /* SOT8_0,SCK8_1 */

  /* Peripheral terminal setting */
  IO_KEYCDR = 0x0E23;
  IO_KEYCDR = 0x4E23;
  IO_KEYCDR = 0x8E23;
  IO_KEYCDR = 0xCE23;
  IO_PFR03.byte = 0x20; /* ch8 SIN8_1 */

  /* Peripheral terminal setting */
  IO_KEYCDR = 0x0E2F;
  IO_KEYCDR = 0x4E2F;
  IO_KEYCDR = 0x8E2F;
  IO_KEYCDR = 0xCE2F;
  IO_PFR15.byte = 0x01; /* ch8 SOT8_2 */


  /* Configure CELL_ON_OFF output */
  KEYCDR = 0x24AE;
  KEYCDR = 0x64AE;
  KEYCDR = 0xA4AE;
  KEYCDR = 0xE4AE;
  ADERL0 = 0x0000;


  IO_KEYCDR = 0x0E29;
  IO_KEYCDR = 0x4E29;
  IO_KEYCDR = 0x8E29;
  IO_KEYCDR = 0xCE29;
  IO_PFR09.byte = 0x00;

  IO_KEYCDR = 0x0E09;
  IO_KEYCDR = 0x4E09;
  IO_KEYCDR = 0x8E09;
  IO_KEYCDR = 0xCE09;

  /* Configure as output*/
  IO_DDR09.byte = 0x80;

  /*Drive it LOW */
  //IO_PDR09.byte = 0x00;
  /*Drive it HIGH to disable GSM */
  IO_PDR09.byte = 0x80;

  /* Configure GSM_RESET output */
  IO_KEYCDR = 0x0E27;
  IO_KEYCDR = 0x4E27;
  IO_KEYCDR = 0x8E27;
  IO_KEYCDR = 0xCE27;
  IO_PFR07.byte = 0x00;

  IO_KEYCDR = 0x0E07;
  IO_KEYCDR = 0x4E07;
  IO_KEYCDR = 0x8E07;
  IO_KEYCDR = 0xCE07;

  /* Configure as output*/
  IO_DDR07.byte = 0x09;

  /*Drive it HIGH */
  IO_PDR07.byte = 0x01;


  /* Configure GSM_FAST_SHUTDOWN */
  IO_KEYCDR = 0x0E28;
  IO_KEYCDR = 0x4E28;
  IO_KEYCDR = 0x8E28;
  IO_KEYCDR = 0xCE28;
  IO_PFR08.byte = 0x00;

  IO_KEYCDR = 0x0E08;
  IO_KEYCDR = 0x4E08;
  IO_KEYCDR = 0x8E08;
  IO_KEYCDR = 0xCE08;

  /* Configure as output*/
  IO_DDR08.byte = 0x04;

  /*Drive it LOW */
  IO_PDR08.byte = 0x00;

  KEYCDR = 0x0F40;
  KEYCDR = 0x4F40;
  KEYCDR = 0x8F40;
  KEYCDR = 0xcF40;
  IO_PORTEN.byte = 0xFD;

  /* Interrupt level setting */
  IO_ICR[29].byte = 0x10;		/* Multi Func Serial RX0 */
  IO_ICR[30].byte = 0x11;		/* Multi Func Serial TX0 */

  memset((void *)&g_Uart8,0,sizeof(g_Uart8));

  g_Uart8.pRxBufferRdPtr = g_Uart8.RxBuffer;
  g_Uart8.pRxBufferWrPtr = g_Uart8.RxBuffer;

  g_Uart8.pTxQueueBufferWrPtr = g_Uart8.TxQueueBuffer;

  g_Uart8.QueueRdIndex = 0;
  g_Uart8.QueueWrIndex = 0;

  g_Uart8.pTxQueueBufferWrPtr = g_Uart8.TxQueueBuffer;
  g_Uart8.TxLen = 0;
  g_Uart8.bDataSent = TM_TRUE;
  g_Uart8.NbrOfBlocksInQueue = 0;

  Asynch_UART8_Init();

}

/* Multi Fuction Serial TX8 Int routine */
__interrupt void Multi_TX8_int(void)
{
  int tmp;
  if(g_Uart8.TxLen != 0)
    IO_MFS8.CSIO.RDR.hword.RDR0 = *g_Uart8.pTx++;   /* transmission data set */
  else
  {
    IO_MFS8.UART.SCR.bit.TIE = 0; /* Transmit int disable to stop generating tx interrupts */
    g_Uart8.bDataSent = TM_TRUE;
    uart8txevent = TM_TRUE;
  }
  tmp = IO_MFS8.UART.SSR.byte;		/* dummy read */
  g_Uart8.TxLen--;
}

/* Multi Function Serial RX8 Int routine */
__interrupt void Multi_RX8_int(void)
{
  tm_u8_t ch;
  int tmp;

  if(IO_MFS8.CSIO.SSR.bit.ORE == 1)
  {
    IO_MFS8.CSIO.SSR.bit.REC = 1;		/* error flag clear */
  }
  else
  {
    ch = IO_MFS8.CSIO.RDR.hword.RDR0; /* reception data read */
    tmp = IO_MFS8.UART.SSR.byte;      /* dummy read */
    *g_Uart8.pRxBufferWrPtr = ch;
    INCCIRCULARPTR(g_Uart8.pRxBufferWrPtr, g_Uart8.RxBuffer, RXBUFLEN);
    uart8rxevent = TM_TRUE;
  }
}

void UART8_BufferRead(tm_u8_t *pData, tm_u8_t *Len)
{

  *Len = 0;
  /* Diable interrupts */
  IO_MFS8.UART.SCR.bit.RIE = 0;

  /* Copy data */
  *Len = g_Uart8.pRxBufferWrPtr - g_Uart8.RxBuffer;
  memcpy(pData,g_Uart8.RxBuffer, *Len);
  g_Uart8.pRxBufferWrPtr = g_Uart8.RxBuffer;

  /* Enable Interrupts */
  IO_MFS8.UART.SCR.bit.RIE = 1;
}

void UART8_BufferWrite(tm_u8_t *pData, tm_u16_t Len)
{
  /* Copy data */
  g_Uart8.TxLen = Len;
  g_Uart8.pTx = pData;
  uart8txevent = TM_FALSE;

  /* Enable Interrupts */
  IO_MFS8.UART.SCR.bit.TIE = 1;
}

void UART8QueueData(tm_u8_t *pData, tm_u16_t Len)
{
  tm_u16_t tmpLen;
  /* Copy data to queue and update queue pts */

  tmpLen = TXQUEUEBUFLEN - (g_Uart8.pTxQueueBufferWrPtr - g_Uart8.TxQueueBuffer);

  if(tmpLen < Len)
  {
    /* Split data in to two queue entries */
    memcpy(g_Uart8.pTxQueueBufferWrPtr,pData,tmpLen);
    g_Uart8.Queue[g_Uart8.QueueWrIndex].pData = g_Uart8.pTxQueueBufferWrPtr;
    g_Uart8.Queue[g_Uart8.QueueWrIndex].Len = tmpLen;
    g_Uart8.Queue[g_Uart8.QueueWrIndex].bValid = TM_TRUE;
    INCCIRCULARINDEX(g_Uart8.QueueWrIndex, QUEUELEN);
    g_Uart8.NbrOfBlocksInQueue++;

    memcpy(g_Uart8.TxQueueBuffer,pData+tmpLen,Len - tmpLen);
    g_Uart8.Queue[g_Uart8.QueueWrIndex].pData = g_Uart8.TxQueueBuffer;
    g_Uart8.Queue[g_Uart8.QueueWrIndex].Len = Len - tmpLen;
    g_Uart8.Queue[g_Uart8.QueueWrIndex].bValid = TM_TRUE;
    INCCIRCULARINDEX(g_Uart8.QueueWrIndex, QUEUELEN);
    g_Uart8.pTxQueueBufferWrPtr = g_Uart8.TxQueueBuffer + Len - tmpLen;
    g_Uart8.NbrOfBlocksInQueue++;
  }
  else
  {
    memcpy(g_Uart8.pTxQueueBufferWrPtr,pData,Len);
    g_Uart8.Queue[g_Uart8.QueueWrIndex].pData = g_Uart8.pTxQueueBufferWrPtr;
    g_Uart8.Queue[g_Uart8.QueueWrIndex].Len = Len;
    g_Uart8.Queue[g_Uart8.QueueWrIndex].bValid = TM_TRUE;
    ADDCIRCULARPTR(g_Uart8.pTxQueueBufferWrPtr, Len, g_Uart8.TxQueueBuffer, TXQUEUEBUFLEN);
    INCCIRCULARINDEX(g_Uart8.QueueWrIndex, QUEUELEN);
    g_Uart8.NbrOfBlocksInQueue++;
  }
  if(g_Uart8.bDataSent == TM_TRUE)
  {
    UART8DeQueueData();
  }
}

void UART8DeQueueData(void)
{
  if(g_Uart8.NbrOfBlocksInQueue != 0)
  {
    g_Uart8.bDataSent = TM_FALSE;
    UART8_BufferWrite(g_Uart8.Queue[g_Uart8.QueueRdIndex].pData, g_Uart8.Queue[g_Uart8.QueueRdIndex].Len);
    INCCIRCULARINDEX(g_Uart8.QueueRdIndex, QUEUELEN);
    g_Uart8.NbrOfBlocksInQueue--;
  }
}



