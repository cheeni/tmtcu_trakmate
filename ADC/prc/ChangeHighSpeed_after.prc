#/*******************************************************************************
#* Copyright (C) 2013 Spansion LLC. All Rights Reserved. 
#*
#* This software is owned and published by: 
#* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
#*
#* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
#* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
#*
#* This software contains source code for use with Spansion 
#* components. This software is licensed by Spansion to be adapted only 
#* for use in systems utilizing Spansion components. Spansion shall not be 
#* responsible for misuse or illegal use of this software for devices not 
#* supported herein.  Spansion is providing this software "AS IS" and will 
#* not be responsible for issues arising from incorrect user implementation 
#* of the software.  
#*
#* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE,
#* REGARDING THE SOFTWARE (INCLUDING ANY ACOOMPANYING WRITTEN MATERIALS), 
#* ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED USE, INCLUDING, 
#* WITHOUT LIMITATION, THE IMPLIED WARRANTY OF MERCHANTABILITY, THE IMPLIED 
#* WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE OR USE, AND THE IMPLIED 
#* WARRANTY OF NONINFRINGEMENT.  
#* SPANSION SHALL HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, 
#* NEGLIGENCE OR OTHERWISE) FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT 
#* LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, 
#* LOSS OF BUSINESS INFORMATION, OR OTHER PECUNIARY LOSS) ARISING FROM USE OR 
#* INABILITY TO USE THE SOFTWARE, INCLUDING, WITHOUT LIMITATION, ANY DIRECT, 
#* INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, 
#* SAVINGS OR PROFITS, 
#* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 
#* YOU ASSUME ALL RESPONSIBILITIES FOR SELECTION OF THE SOFTWARE TO ACHIEVE YOUR
#* INTENDED RESULTS, AND FOR THE INSTALLATION OF, USE OF, AND RESULTS OBTAINED 
#* FROM, THE SOFTWARE.  
#*
#* This software may be replicated in part or whole for the licensed use, 
#* with the restriction that this Disclaimer and Copyright notice must be 
#* included with each copy of this software, whether used in part or whole, 
#* at all times.  
#*****************************************************************************/

#/** \file ChangeHighSpeed_after.prc
# **
# ** Add description here...
# **
# ** History:
# **   - 2011-12-28  0.01  MKu  Preliminary version
# **   - 2012-03-09  0.02  MKu  Preliminary version(not released)
# **   - 2012-03-16  1.01  MKu  First edition
# **   - 2012-04-13  1.02  MKu  Improvement at processing time
# **                            use command (LOAD /SYNCHRONIZE)
# **   - 2012-12-06  1.03  MKu  change command
# **                            LOAD /SYNCHRONIZE -> SYNCHRONIZE FLASH
# *****************************************************************************/
SYNCHRONIZE FLASH /FROMDEBUGGER /ALL  # FLASH memory synchronizes
CANCEL FREQUENCY /MAX                 # disable high speed mode
reset