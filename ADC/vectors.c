/*******************************************************************************
* Copyright (C) 2013 Spansion LLC. All Rights Reserved. 
*
* This software is owned and published by: 
* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software contains source code for use with Spansion 
* components. This software is licensed by Spansion to be adapted only 
* for use in systems utilizing Spansion components. Spansion shall not be 
* responsible for misuse or illegal use of this software for devices not 
* supported herein.  Spansion is providing this software "AS IS" and will 
* not be responsible for issues arising from incorrect user implementation 
* of the software.  
*
* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE,
* REGARDING THE SOFTWARE (INCLUDING ANY ACOOMPANYING WRITTEN MATERIALS), 
* ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED USE, INCLUDING, 
* WITHOUT LIMITATION, THE IMPLIED WARRANTY OF MERCHANTABILITY, THE IMPLIED 
* WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE OR USE, AND THE IMPLIED 
* WARRANTY OF NONINFRINGEMENT.  
* SPANSION SHALL HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, 
* NEGLIGENCE OR OTHERWISE) FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT 
* LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, 
* LOSS OF BUSINESS INFORMATION, OR OTHER PECUNIARY LOSS) ARISING FROM USE OR 
* INABILITY TO USE THE SOFTWARE, INCLUDING, WITHOUT LIMITATION, ANY DIRECT, 
* INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, 
* SAVINGS OR PROFITS, 
* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 
* YOU ASSUME ALL RESPONSIBILITIES FOR SELECTION OF THE SOFTWARE TO ACHIEVE YOUR
* INTENDED RESULTS, AND FOR THE INSTALLATION OF, USE OF, AND RESULTS OBTAINED 
* FROM, THE SOFTWARE.  
*
* This software may be replicated in part or whole for the licensed use, 
* with the restriction that this Disclaimer and Copyright notice must be 
* included with each copy of this software, whether used in part or whole, 
* at all times.  
*****************************************************************************/
/** \file vectors.c
 **
 ** Interrupt vector definition
 **
 ** History:
 **   - 2012-03-16  1.01  MKu  First edition
 **   - 2013-07-26  2.00  SWi  Added: initialization of IRQ levels
 **                            Added: vector table with deatailed description added
 **   - 2013-10-16  2.01  IAM  Disclaimer updated
 **                            Interrupt vector description fixed
 **                            document style changed 
 *****************************************************************************/

/*****************************************************************************/
/* Include files                                                             */
/*****************************************************************************/  
#include "_fr.h"
#include "vectors.h"
#include "extern.h"
/*****************************************************************************/
/* Local pre-processor symbols/macros ('#define')                            */
/*****************************************************************************/

/*****************************************************************************/
/* Global variable definitions (declared in header file with 'extern')       */
/*****************************************************************************/

/*****************************************************************************/
/* Local type definitions ('typedef')                                        */
/*****************************************************************************/

/*****************************************************************************/
/* Local variable definitions ('static')                                     */
/*****************************************************************************/

/*****************************************************************************/
/* Local function prototypes ('static')                                      */
/*****************************************************************************/

/*****************************************************************************/
/* Function implementation - global ('extern') and local ('static')          */
/*****************************************************************************/



/**
 ******************************************************************************
 ** InitIrqLevels()
 **
 ** \param NONE
 **
 ** \return return value NONE
  ------------------------------------------------------------------------
  This function  pre-sets all interrupt control registers. It can be used
  to set all interrupt priorities in static applications. If this file
  contains assignments to dedicated resources, verify  that the
  appropriate controller is used. Not all devices of the mb91520 Series
  offer all recources.

  NOTE: value 31 disables the interrupt and value 16 sets highest priority.

*****************************************************************************/
void Vectors_InitIrqLevels(void)
{
    /*  ICRxx */    /* MCU Resources                        */ 
    ICR0 = 31;      /* External Interrupt 0                 */
                    /* External Interrupt 1                 */
                    /* External Interrupt 2                 */
                    /* External Interrupt 3                 */
                    /* External Interrupt 4                 */
                    /* External Interrupt 5                 */
                    /* External Interrupt 6                 */
                    /* External Interrupt 7                 */
    ICR1 = 31;      /* External Interrupt 8                 */
                    /* External Interrupt 9                 */
                    /* External Interrupt 10                */
                    /* External Interrupt 11                */
                    /* External Interrupt 12                */
                    /* External Interrupt 13                */
                    /* External Interrupt 14                */
                    /* External Interrupt 15                */
					/* External Interrupt 16                */
                	/* External Interrupt 17                */
                	/* External Interrupt 18                */
                	/* External Interrupt 19                */
                	/* External Interrupt 20                */
                	/* External Interrupt 21                */
                	/* External Interrupt 22                */
                	/* External Interrupt 23                */
                    /* External Low Voltage detection       */
    ICR2 = 31;  	/* Reload Timer 0                       */
                    /* Reload Timer 1                       */
                    /* Reload Timer 4                       */
                    /* Reload Timer 5                       */
    ICR3 = 31;      /* Reload Timer 2                       */
					/* Reload Timer 3                       */
                    /* Reload Timer 6                       */
                    /* Reload Timer 7                       */
    ICR4 = 31;      /* MFS ch.0 RX completed / status       */
    ICR5 = 31;      /* MFS ch.0 TX completed                */
    ICR6 = 31;      /* MFS ch.1 RX completed / status       */
    ICR7 = 31;      /* MFS ch.1 TX completed                */
    ICR8 = 31;      /* MFS ch.2 RX completed / status       */
    ICR9 = 31;      /* MFS ch.2 TX completed                */   
    ICR10 = 31;     /* MFS ch.3 RX completed / status       */
    ICR11 = 31;     /* MFS ch.3 TX completed                */    
    ICR12 = 31;     /* MFS ch.4 RX completed / status       */
    				/* MFS ch.12 RX completed / status      */
    ICR13 = 31;     /* MFS ch.4 TX completed                */    
    				/* MFS ch.12 TX completed               */
    ICR14 = 31;     /* MFS ch.5 RX completed / status       */
    				/* MFS ch.13 RX completed / status      */
    ICR15 = 31;     /* MFS ch.5 TX completed                */
    				/* MFS ch.13 TX completed               */
    				/* FlexRay 0							*/
    ICR16 = 31;     /* MFS ch.6 RX completed / status       */
    				/* MFS ch.14 RX completed / status      */
    				/* FlexRay 1							*/    
    ICR17 = 31;     /* MFS ch.6 TX completed                */
    				/* MFS ch.14 TX completed               */
    				/* FlexRay timer 0						*/    
    ICR18 = 31;     /* CAN 0                                */
    				/* CAN 3                                */
    				/* FlexRay timer 1						*/    
    ICR19 = 31;     /* CAN 1                                */
                    /* RAM Diagnosis end                    */
                    /* RAM Initialization completion        */
                    /* Error during RAM Diagnosis           */
                    /* Backup RAM Diagnosis end             */
                    /* Backup RAM Initialization completion */
                    /* Error during Backup RAM Diagnosis    */
                	/* AHB RAM diagnosis completed 			*/
					/* AHB RAM initialization completed 	*/
					/* Error generation at AHB RAM diagnosis */
					/* CAN4 								*/
    ICR20 = 31;     /* CAN2                                 */
					/* Up/Down Counter 0                    */
                    /* Up/Down Counter 1                    */
					/* Up/Down Counter 2                    */
                	/* Up/Down Counter 3                    */
					/* CAN5 								*/
					/* FlexRay PLL gear/FlexRay PLL alarm   */
    ICR21 = 31;     /* Real Time Clock (RTC)                */
    ICR22 = 31;     /* MFS ch.7 RX completed / status       */
    				/* MFS ch.15 RX completed / status       */
    ICR23 = 31;     /* 16bit FRT0 zero detect / clear       */
					/* MFS ch.7 TX completed                */
                	/* MFS ch.15 TX completed                */
    ICR24 = 31;     /* PPG ch.0                             */
					/* PPG ch.1                             */
                    /* PPG ch.10                            */
                    /* PPG ch.11                            */
                    /* PPG ch.20                            */
					/* PPG ch.21                            */
                    /* PPG ch.30                            */
                    /* PPG ch.31                            */
					/* PPG ch.40                            */
                    /* PPG ch.41                            */
                    /* PPG ch.50                            */
                    /* PPG ch.51                            */
                    /* PPG ch.60                            */
                    /* PPG ch.61                            */
                    /* PPG ch.70                            */                                        
                    /* PPG ch.71                            */                                        
                    /* PPG ch.80                            */                                        
                    /* PPG ch.81                            */                     
                    /* 16bit FRT1 zero detect / clear       */
    ICR25 = 31;     /* PPG ch.2                             */
                    /* PPG ch.3                             */
                    /* PPG ch.12                            */
                    /* PPG ch.13                            */
					/* PPG ch.22                            */  
                    /* PPG ch.23                            */
					/* PPG ch.32                            */
					/* PPG ch.33                            */
					/* PPG ch.42                            */
                    /* PPG ch.43                            */                                       
                    /* PPG ch.52                            */
                    /* PPG ch.53                            */
                    /* PPG ch.62                            */
                    /* PPG ch.63                            */
                    /* PPG ch.72                            */                                        
                    /* PPG ch.73                            */                                        
                    /* PPG ch.82                            */                                        
                    /* PPG ch.83                            */                        					                    
                    /* 16bit FRT2 zero detect / clear       */
    ICR26 = 31;     /* PPG ch.4                             */
					/* PPG ch.5                             */
					/* PPG ch.14                            */
					/* PPG ch.15                            */
                    /* PPG ch.24                            */
					/* PPG ch.25                            */
					/* PPG ch.34                            */
                    /* PPG ch.35                            */ 
					/* PPG ch.44                            */
					/* PPG ch.45                            */
                    /* PPG ch.54                            */
					/* PPG ch.55                            */
                    /* PPG ch.64                            */ 										
                    /* PPG ch.65                            */ 										
                    /* PPG ch.74                            */ 										
                    /* PPG ch.75                            */ 										
                    /* PPG ch.84                            */ 										
                    /* PPG ch.85                            */                      					                    
    ICR27 = 31;     /* PPG ch.6                             */
					/* PPG ch.7                             */
                    /* PPG ch.16                            */
                    /* PPG ch.17                            */
					/* PPG ch.26                            */
                    /* PPG ch.27                            */
					/* PPG ch.36                            */
                    /* PPG ch.37                            */     
					/* PPG ch.46                            */
					/* PPG ch.47                            */
                    /* PPG ch.56                            */
                    /* PPG ch.57                            */
                    /* PPG ch.66                            */ 										
                    /* PPG ch.67                            */ 										
                    /* PPG ch.76                            */ 										
                    /* PPG ch.77                            */ 										
                    /* PPG ch.86                            */ 										
                    /* PPG ch.87                            */                    
    ICR28 = 31;     /* PPG ch.8                             */
					/* PPG ch.9                             */
					/* PPG ch.18                            */
					/* PPG ch.19                            */
					/* PPG ch.28                            */
					/* PPG ch.29                            */
					/* PPG ch.38                            */
					/* PPG ch.39                            */
					/* PPG ch.48                            */
					/* PPG ch.49                            */
					/* PPG ch.58                            */
					/* PPG ch.59                            */
                    /* PPG ch.68                            */ 										
                    /* PPG ch.69                            */ 										
                    /* PPG ch.78                            */ 										
                    /* PPG ch.79                            */ 	                         
    ICR29 = 31;     /* MFS ch.8 RX completed / status       */
    				/* MFS ch.16 RX completed / status      */
					/* 16bit ICU 0 (fetching)               */
                    /* 16bit ICU 1 (fetching)               */                                        
    ICR30 = 31;     /* Main/Sub/PLL timer                   */
					/* MFS ch.8 TX completed                */
					/* MFS ch.16 TX completed               */                    
                    /* 16bit ICU 2 (fetching)               */                                        
                    /* 16bit ICU 3 (fetching)               */
    ICR31 = 31;     /* Clock Calibration Unit (Sub Osc.)    */
                    /* MFS ch.9 RX completed / status       */
                	/* MFS ch.17 RX completed / status       */
    ICR32 = 31;     /* AD ch.0                              */
                    /* AD ch.1                              */
					/* AD ch.2                              */
					/* AD ch.3                              */
					/* AD ch.4                              */
					/* AD ch.5                              */
					/* AD ch.6                              */
                    /* AD ch.7                              */
					/* AD ch.8                              */
					/* AD ch.9                              */
                    /* AD ch.10                             */
                    /* AD ch.11                             */
					/* AD ch.12                             */
					/* AD ch.13                             */
                    /* AD ch.14                             */
                    /* AD ch.15                             */
                    /* AD ch.16                             */
                    /* AD ch.17                             */
					/* AD ch.18                             */
					/* AD ch.19                             */
					/* AD ch.20                             */
					/* AD ch.21                             */
                    /* AD ch.22                             */
					/* AD ch.23                             */
					/* AD ch.24                             */
					/* AD ch.25                             */
					/* AD ch.26                             */
                    /* AD ch.27                             */
                    /* AD ch.28                             */
					/* AD ch.29                             */
					/* AD ch.30                             */
                    /* AD ch.31                             */
    ICR33 = 31;     /* Clock Calibration Unit (RC Osc.)     */
                    /* MFS ch.9 TX completed                */
                	/* MFS ch.17 TX completed                */
                    /* 16bit OCU 0 (match)                  */
                    /* 16bit OCU 1 (match)                  */
    ICR34 = 31;     /* 32bit FRT4                           */
    				/* 32bit FRT6                           */
                	/* 32bit FRT8                           */
                	/* 32bit FRT10                          */
                    /* 16bit OCU 2 (match)                  */
                    /* 16bit OCU 3 (match)                  */
    ICR35 = 31;     /* 32bit FRT3                           */
					/* 32bit FRT5                           */
					/* 32bit FRT7                           */
                	/* 32bit FRT9                           */                   
					/* 16bit OCU 4 (match)                  */
                    /* 16bit OCU 5 (match)                  */
    ICR36 = 31;     /* 32bit ICU 6 (fetching/measure)       */
                    /* MFS ch.10 RX completed / status      */
                	/* MFS ch.18 RX completed / status      */
    ICR37 = 31; 	/* 32bit ICU 7 (fetching/measure)       */
					/* MFS ch.10 TX completed               */
                	/* MFS ch.18 TX completed               */
    ICR38 = 31;     /* 32bit ICU 8 (fetching/measure)       */
                    /* MFS ch.11 RX completed / status      */
                	/* MFS ch.19 RX completed / status      */
    ICR39 = 31;     /* 32bit ICU 9 (fetching/measure)       */
                    /* WFG Dead Time Timer underflow 0/1/2  */
                    /* WFG Dead Time Timer reload 0/1/2     */
                    /* WFG DTTI0                            */
    ICR40 = 31;     /* 32bit ICU4 (fetching/measure)        */
					/* 32bit ICU10 (fetching/measure)       */
                    /* MFS ch.11 TX completed               */
                	/* MFS ch.19 TX completed               */
    ICR41 = 31;     /* 32bit ICU5 (fetching/measure)        */
    				/* 32bit ICU11 (fetching/measure)       */
                    /* AD ch.32                             */
					/* AD ch.33                             */
                    /* AD ch.34                             */
                    /* AD ch.35                             */
					/* AD ch.36                             */
                    /* AD ch.37                             */
                    /* AD ch.38                             */
					/* AD ch.39                             */
                    /* AD ch.40                             */
                    /* AD ch.41                             */
                    /* AD ch.42                             */
                    /* AD ch.43                             */
                    /* AD ch.44                             */
                    /* AD ch.45                             */
                    /* AD ch.46                             */
                    /* AD ch.47                             */
                	/* AD ch.48                             */
                	/* AD ch.49                             */
                	/* AD ch.50                             */
                	/* AD ch.51                             */
                	/* AD ch.52                             */
                	/* AD ch.53                             */
                	/* AD ch.54                             */
                	/* AD ch.55                             */
                	/* AD ch.56                             */
                	/* AD ch.57                             */
                	/* AD ch.58                             */
                	/* AD ch.59                             */
                	/* AD ch.60                             */
                	/* AD ch.61                             */
                	/* AD ch.62                             */
                	/* AD ch.63                             */
    ICR42 = 31;     /* 32bit OCU 6 (match)                  */
					/* 32bit OCU 7 (match)                  */
					/* 32bit OCU 10 (match)                 */
                    /* 32bit OCU 11 (match)                 */
    ICR43 = 31;     /* 32bit OCU 8 (match)                  */
                    /* 32bit OCU 9 (match)                  */
                    /* 32bit OCU 12 (match)                 */
                    /* 32bit OCU 13 (match)                 */
	ICR44 = 31;     /* Base Timer 0 IRQ0                    */
					/* Base Timer 0 IRQ1                    */
    ICR45 = 31;     /* Base Timer 1 IRQ0                    */
					/* Base Timer 1 IRQ1                    */
    ICR46 = 31;     /* DMAC 0                               */
                    /* DMAC 1                               */
                    /* DMAC 2                               */
                    /* DMAC 3                               */
                    /* DMAC 4                               */
                    /* DMAC 5                               */
                    /* DMAC 6                               */
                    /* DMAC 7                               */
                    /* DMAC 8                               */
                    /* DMAC 9                               */
                    /* DMAC 10                              */
                    /* DMAC 11                              */
                    /* DMAC 12                              */
                    /* DMAC 13                              */
                    /* DMAC 14                              */
                    /* DMAC 15                              */
    ICR47 = 31;     /* Delayed Interrupt                    */
}


/*---------------------------------------------------------------------------
   Prototypes
   Add your own prototypes here. Each vector definition needs is proto-
   type. Either do it here or include a header file containing them.
-----------------------------------------------------------------------------*/
extern	__interrupt void _start(void);
__interrupt void Vectors_Isr_DefaultHandler (void);



/**
 ******************************************************************************
 ** Vector table definition
 **
 ------------------------------------------------------------------------
   Vector definiton

   Use following statements to define vectors. All resource related
   vectors are predefined. Remaining software interrupts can be added here
   as well.
*****************************************************************************/


#pragma intvect _start                      0    /* reset vector                         */
#pragma intvect Vectors_Isr_DefaultHandler  5    /* FPU exception                        */
#pragma intvect Vectors_Isr_DefaultHandler  6    /* MPU Instruction access violation     */
#pragma intvect Vectors_Isr_DefaultHandler  7    /* MPU Data access violation            */
#pragma intvect Vectors_Isr_DefaultHandler  8    /* MPU Data access error                */
#pragma intvect Vectors_Isr_DefaultHandler  9    /* INTE                                 */
#pragma intvect Vectors_Isr_DefaultHandler 10    /* Instruction Break                    */
#pragma intvect Vectors_Isr_DefaultHandler 12    /* Step trace trap                      */
#pragma intvect Vectors_Isr_DefaultHandler 14    /* Illegal Instruction exception        */
#pragma intvect Vectors_Isr_DefaultHandler 15    /* Non Maskable Interrupt               */
                                        /* Bus Diagnosis Error                  */
                                        /* XBS RAM ECC double bit Error         */
                                        /* Backup RAM ECC double bit Error      */
                                        /* AHB RAM double-bit error detection 	*/
										/* TPU violation                        */
#pragma intvect Vectors_Isr_DefaultHandler 16    /* External Interrupt 0                 */
                                        /* External Interrupt 1                 */
                                        /* External Interrupt 2                 */
                                        /* External Interrupt 3                 */
                                        /* External Interrupt 4                 */
                                        /* External Interrupt 5                 */
                                        /* External Interrupt 6                 */
                                        /* External Interrupt 7                 */
#pragma intvect Vectors_Isr_DefaultHandler 17    /* External Interrupt 8                 */
                                        /* External Interrupt 9                 */
                                        /* External Interrupt 10                */
                                        /* External Interrupt 11                */
                                        /* External Interrupt 12                */
                                        /* External Interrupt 13                */
                                        /* External Interrupt 14                */
                                        /* External Interrupt 15                */
										/* External Interrupt 16                */
                						/* External Interrupt 17                */
                						/* External Interrupt 18                */
                						/* External Interrupt 19                */
                						/* External Interrupt 20                */
                						/* External Interrupt 21                */
                						/* External Interrupt 22                */
                						/* External Interrupt 23                */                                        
										/* External Low Voltage detection       */
#pragma intvect reload0_1_int 18    /* Reload Timer 0                       */
                                        /* Reload Timer 1                       */
                                        /* Reload Timer 4                       */
                                        /* Reload Timer 5                       */
#pragma intvect Vectors_Isr_DefaultHandler 19    /* Reload Timer 2                       */
										/* Reload Timer 3                       */
                                        /* Reload Timer 6                       */
                                        /* Reload Timer 7                       */
#pragma intvect Vectors_Isr_DefaultHandler 20    /* MFS ch.0 RX completed / status       */
#pragma intvect Vectors_Isr_DefaultHandler 21    /* MFS ch.0 TX completed                */
#pragma intvect Multi_RX1_int 22    /* MFS ch.1 RX completed / status       */
#pragma intvect Multi_TX1_int 23    /* MFS ch.1 TX completed                */
#pragma intvect Vectors_Isr_DefaultHandler 24    /* MFS ch.2 RX completed / status       */
#pragma intvect Vectors_Isr_DefaultHandler 25    /* MFS ch.2 TX completed                */
#pragma intvect Vectors_Isr_DefaultHandler 26    /* MFS ch.3 RX completed / status       */
#pragma intvect Vectors_Isr_DefaultHandler 27    /* MFS ch.3 TX completed                */
#pragma intvect Vectors_Isr_DefaultHandler 28    /* MFS ch.4 RX completed / status       */
    									/* MFS ch.12 RX completed / status       */
#pragma intvect Vectors_Isr_DefaultHandler 29    /* MFS ch.4 TX completed                */
    									/* MFS ch.12 TX completed                */
#pragma intvect Vectors_Isr_DefaultHandler 30    /* MFS ch.5 RX completed / status       */
    									/* MFS ch.13 RX completed / status       */
#pragma intvect Vectors_Isr_DefaultHandler 31    /* MFS ch.5 TX completed                */
    									/* MFS ch.13 TX completed                */
    									/* FlexRay 0							*/
#pragma intvect Multi_RX6_int 32    /* MFS ch.6 RX completed / status       */
    									/* MFS ch.14 RX completed / status       */
    									/* FlexRay 1							*/    
#pragma intvect Multi_TX6_int 33    /* MFS ch.6 TX completed                */
    									/* MFS ch.14 TX completed               */
    									/* FlexRay timer 0						*/ 
#pragma intvect Vectors_Isr_DefaultHandler 34    /* CAN 0                                */
    									/* CAN 3                                */
    									/* FlexRay timer 1						*/   
#pragma intvect CAN_1_int 35    /* CAN 1                                */
                                        /* RAM Diagnosis end                    */
                                        /* RAM Initialization completion        */
                                        /* Error during RAM Diagnosis           */
                                        /* Backup RAM Diagnosis end             */
                                        /* Backup RAM Initialization completion */
                                        /* Error during Backup RAM Diagnosis    */                                       
                						/* AHB RAM diagnosis completed 			*/
										/* AHB RAM initialization completed 	*/
										/* Error generation at AHB RAM diagnosis */
										/* CAN4 								*/                                        
#pragma intvect Vectors_Isr_DefaultHandler 36    /* CAN2                                 */
										/* Up/Down Counter 0                    */
                                        /* Up/Down Counter 1                    */
										/* Up/Down Counter 2                    */
                						/* Up/Down Counter 3                    */                                        
										/* CAN5 								*/
										/* FlexRay PLL gear/FlexRay PLL alarm   */                                        
#pragma intvect Vectors_Isr_DefaultHandler 37    /* Real Time Clock (RTC)                */
#pragma intvect Vectors_Isr_DefaultHandler 38    /* MFS ch.7 RX completed / status       */
    									/* MFS ch.15 RX completed / status       */
#pragma intvect Vectors_Isr_DefaultHandler 39    /* 16bit FRT0 zero detect / clear       */
										/* MFS ch.7 TX completed                */
                                        /* MFS ch.15 TX completed                */
#pragma intvect Vectors_Isr_DefaultHandler 40    /* PPG ch.0                             */
										/* PPG ch.1                             */
                                        /* PPG ch.10                            */
                                        /* PPG ch.11                            */
                                        /* PPG ch.20                            */
										/* PPG ch.21                            */
                                        /* PPG ch.30                            */
                                        /* PPG ch.31                            */
										/* PPG ch.40                            */
										/* PPG ch.41                            */
                    					/* PPG ch.50                            */
                    					/* PPG ch.51                            */
                    					/* PPG ch.60                            */
                    					/* PPG ch.61                            */
                    					/* PPG ch.70                            */                                        
                    					/* PPG ch.71                            */                                        
                    					/* PPG ch.80                            */                                        
                    					/* PPG ch.81                            */                                        
                                        /* 16bit FRT1 zero detect / clear       */
#pragma intvect Vectors_Isr_DefaultHandler 41    /* PPG ch.2                             */
                                        /* PPG ch.3                             */
                                        /* PPG ch.12                            */
                                        /* PPG ch.13                            */
										/* PPG ch.22                            */
                                        /* PPG ch.23                            */
										/* PPG ch.32                            */
										/* PPG ch.33                            */
										/* PPG ch.42                            */
                                        /* PPG ch.43                            */
                    					/* PPG ch.52                            */
                    					/* PPG ch.53                            */
                    					/* PPG ch.62                            */
                    					/* PPG ch.63                            */                                          
                    					/* PPG ch.72                            */                                        
                    					/* PPG ch.73                            */                                        
                    					/* PPG ch.82                            */                                        
                    					/* PPG ch.83                            */                                        
                                        /* 16bit FRT2 zero detect / clear       */
#pragma intvect Vectors_Isr_DefaultHandler 42    /* PPG ch.4                             */
										/* PPG ch.5                             */
										/* PPG ch.14                            */
										/* PPG ch.15                            */
                                        /* PPG ch.24                            */
										/* PPG ch.25                            */
										/* PPG ch.34                            */
                                        /* PPG ch.35                            */
										/* PPG ch.44                            */
										/* PPG ch.45                            */
                    					/* PPG ch.54                            */
                    					/* PPG ch.55                            */ 	
                    					/* PPG ch.64                            */ 										
                    					/* PPG ch.65                            */ 										
                    					/* PPG ch.74                            */ 										
                    					/* PPG ch.75                            */ 										
                    					/* PPG ch.84                            */ 										
                    					/* PPG ch.85                            */ 																			
#pragma intvect Vectors_Isr_DefaultHandler 43    /* PPG ch.6                             */
										/* PPG ch.7                             */
                                        /* PPG ch.16                            */
										/* PPG ch.17                            */
                                        /* PPG ch.26                            */
										/* PPG ch.27                            */
                                        /* PPG ch.36                            */
										/* PPG ch.37                            */
                                        /* PPG ch.46                            */
										/* PPG ch.47                            */
                    					/* PPG ch.56                            */
                    					/* PPG ch.57                            */                                        
                    					/* PPG ch.66                            */ 										
                    					/* PPG ch.67                            */ 										
                    					/* PPG ch.76                            */ 										
                    					/* PPG ch.77                            */ 										
                    					/* PPG ch.86                            */ 										
                    					/* PPG ch.87                            */
#pragma intvect Vectors_Isr_DefaultHandler 44    /* PPG ch.8                             */
										/* PPG ch.9                             */
										/* PPG ch.18                            */
										/* PPG ch.19                            */
										/* PPG ch.28                            */
										/* PPG ch.29                            */
										/* PPG ch.38                            */
										/* PPG ch.39                            */
										/* PPG ch.48                            */
										/* PPG ch.49                            */
										/* PPG ch.58                            */
										/* PPG ch.59                            */
                    					/* PPG ch.68                            */
                    					/* PPG ch.69                            */
                    					/* PPG ch.78                            */ 										
                    					/* PPG ch.79                            */ 										                                        
#pragma intvect Multi_RX8_int 45    /* MFS ch.8 RX completed / status       */
    									/* MFS ch.16 RX completed / status       */
										/* 16bit ICU 0 (fetching)               */
                                        /* 16bit ICU 1 (fetching)               */                                        
#pragma intvect Multi_TX8_int 46    /* Main/Sub/PLL timer                   */
										/* MFS ch.8 TX completed                */
                                        /* MFS ch.16 TX completed                */
                                        /* 16bit ICU 2 (fetching)               */                                        
                                        /* 16bit ICU 3 (fetching)               */
#pragma intvect Vectors_Isr_DefaultHandler 47    /* Clock Calibration Unit (Sub Osc.)    */
                                        /* MFS ch.9 RX completed / status       */
                                        /* MFS ch.17 RX completed / status       */
#pragma intvect ADC_int_0_12               48    /* AD ch.0                              */
                                        /* AD ch.1                              */
										/* AD ch.2                              */
										/* AD ch.3                              */
										/* AD ch.4                              */
										/* AD ch.5                              */
										/* AD ch.6                              */
                                        /* AD ch.7                              */
										/* AD ch.8                              */
										/* AD ch.9                              */
                                        /* AD ch.10                             */
                                        /* AD ch.11                             */
										/* AD ch.12                             */
										/* AD ch.13                             */
                                        /* AD ch.14                             */
                                        /* AD ch.15                             */
                                        /* AD ch.16                             */
                                        /* AD ch.17                             */
										/* AD ch.18                             */
										/* AD ch.19                             */
										/* AD ch.20                             */
										/* AD ch.21                             */
                                        /* AD ch.22                             */
										/* AD ch.23                             */
										/* AD ch.24                             */
										/* AD ch.25                             */
										/* AD ch.26                             */
                                        /* AD ch.27                             */
                                        /* AD ch.28                             */
										/* AD ch.29                             */
										/* AD ch.30                             */
                                        /* AD ch.31                             */
#pragma intvect Vectors_Isr_DefaultHandler 49    /* Clock Calibration Unit (RC Osc.)     */
                                        /* MFS ch.9 TX completed                */
                						/* MFS ch.17 TX completed                */                                        
                                        /* 16bit OCU 0 (match)                  */
                                        /* 16bit OCU 1 (match)                  */
#pragma intvect Vectors_Isr_DefaultHandler 50    /* 32bit FRT4                           */
    									/* 32bit FRT6                           */
                						/* 32bit FRT8                           */
                						/* 32bit FRT10                          */
                                        /* 16bit OCU 2 (match)                  */
                                        /* 16bit OCU 3 (match)                  */
#pragma intvect Vectors_Isr_DefaultHandler 51    /* 32bit FRT3                           */
										/* 32bit FRT5                           */
                           				/* 32bit FRT7                           */
                						/* 32bit FRT9                           */  
										/* 16bit OCU 4 (match)                  */
                                        /* 16bit OCU 5 (match)                  */
#pragma intvect Multi_RX10_int 52    /* 32bit ICU 6 (fetching/measure)       */
                                        /* MFS ch.10 RX completed / status      */
                                        /* MFS ch.18 RX completed / status      */
#pragma intvect Multi_TX10_int 53    /* 32bit ICU 7 (fetching/measure)       */
										/* MFS ch.10 TX completed               */
										/* MFS ch.18 TX completed               */
#pragma intvect Vectors_Isr_DefaultHandler 54    /* 32bit ICU 8 (fetching/measure)       */
                                        /* MFS ch.11 RX completed / status      */
                                        /* MFS ch.19 RX completed / status      */
#pragma intvect Vectors_Isr_DefaultHandler 55    /* 32bit ICU 9 (fetching/measure)       */
                                        /* WFG Dead Time Timer underflow 0/1/2  */
                                        /* WFG Dead Time Timer reload 0/1/2     */
                                        /* WFG DTTI0                            */
#pragma intvect Vectors_Isr_DefaultHandler 56    /* 32bit ICU4 (fetching/measure)        */
										/* 32bit ICU10 (fetching/measure)        */
                                        /* MFS ch.11 TX completed               */
                                        /* MFS ch.19 TX completed               */
#pragma intvect ADC_int_32_47              57    /* 32bit ICU5 (fetching/measure)        */
										/* 32bit ICU11 (fetching/measure)        */
                                        /* AD ch.32                             */
										/* AD ch.33                             */
                                        /* AD ch.34                             */
                                        /* AD ch.35                             */
										/* AD ch.36                             */
                                        /* AD ch.37                             */
                                        /* AD ch.38                             */
										/* AD ch.39                             */
                                        /* AD ch.40                             */
                                        /* AD ch.41                             */
                                        /* AD ch.42                             */
                                        /* AD ch.43                             */
                                        /* AD ch.44                             */
                                        /* AD ch.45                             */
                                        /* AD ch.46                             */
                                        /* AD ch.47                             */
                						/* AD ch.48                             */
                						/* AD ch.49                             */
                						/* AD ch.50                             */
                						/* AD ch.51                             */
                						/* AD ch.52                             */
                						/* AD ch.53                             */
                						/* AD ch.54                             */
                						/* AD ch.55                             */
                						/* AD ch.56                             */
                						/* AD ch.57                             */
                						/* AD ch.58                             */
                						/* AD ch.59                             */
                						/* AD ch.60                             */
                						/* AD ch.61                             */
                						/* AD ch.62                             */
                						/* AD ch.63                             */                                        
#pragma intvect Vectors_Isr_DefaultHandler 58    /* 32bit OCU 6 (match)                  */
										/* 32bit OCU 7 (match)                  */
										/* 32bit OCU 10 (match)                 */
                                        /* 32bit OCU 11 (match)                 */
#pragma intvect Vectors_Isr_DefaultHandler 59    /* 32bit OCU 8 (match)                  */
										/* 32bit OCU 9 (match)                  */
										/* 32bit OCU 12 (match)                  */
										/* 32bit OCU 13 (match)                  */
#pragma intvect Vectors_Isr_DefaultHandler 60    /* Base Timer 0 IRQ0                    */
										/* Base Timer 0 IRQ1                    */
#pragma intvect Vectors_Isr_DefaultHandler 61    /* Base Timer 1 IRQ0                    */
										/* Base Timer 1 IRQ1                    */
#pragma intvect DMAC_int 62    /* DMAC 0                               */
                                        /* DMAC 1                               */
                                        /* DMAC 2                               */
                                        /* DMAC 3                               */
                                        /* DMAC 4                               */
                                        /* DMAC 5                               */
                                        /* DMAC 6                               */
                                        /* DMAC 7                               */
                                        /* DMAC 8                               */
                                        /* DMAC 9                               */
                                        /* DMAC 10                              */
                                        /* DMAC 11                              */
                                        /* DMAC 12                              */
                                        /* DMAC 13                              */
                                        /* DMAC 14                              */
                                        /* DMAC 15                              */
#pragma intvect Vectors_Isr_DefaultHandler 63    /* Delayed Interrupt                    */

#pragma defvect Vectors_Isr_DefaultHandler       /* default IRQ Vector                   */


/**
 ******************************************************************************
 ** This function is a placeholder for all vector definitions. Either use
 ** your own placeholder or add necessary code here.
 *****************************************************************************/
__interrupt void Vectors_Isr_DefaultHandler (void)
{
    // disable interrupts
    __DI();

    // halt system or wait for watchdog reset
    while(1)
    {
        __wait_nop();
    }
}
