#ifndef __TMGPS_H__
#define __TMGPS_H__

#include "_fr.h"
#include "extern.h"
#include "stdio.h"
#include "stdlib.h"
#include "ctype.h"
#include "tmcommon.h"
#include "tmuart.h"

#define MAXNMEALEN 256

#define SIZE_TIME 11
#define SIZE_DATE 7

#define MAXMVAVGBUFLEN 50

struct mvavg
{
  float SampleBuf[MAXMVAVGBUFLEN];
  int   RdPtr;
  int   WrPtr;
  int   BufLen;
};
typedef struct mvavg tm_mvavg_t;

enum gpsparsestate
{
   LOOKFOR_DOLLAR
  ,LOOKFOR_CR
  ,LOOKFOR_LF
};
typedef enum gpsparsestate tm_gpsparsestate_t;

struct gpsdata
{
  float              Speed;
  float              Latitude;
  float              Longitude;
  float              Heading;
  tm_u32_t           UTC;
  tm_s8_t            Time[SIZE_TIME];
  tm_s8_t            Date[SIZE_DATE];
  tm_bool_t          bFixValid;
};
typedef struct gpsdata tm_gpsdata_t;

struct gps
{
  tm_gpsparsestate_t GpsPraseState;
  tm_gpsdata_t       GpsData;
  tm_mvavg_t         MvAvgSpeed;
  float              AvgSpeed;
  tm_u16_t           NMEAStrIndex;
  tm_s8_t            NMEAString[MAXNMEALEN];
};

typedef struct gps tm_gps_t;

void InitGPS(tm_gps_t *hGps);
void HandleGPSData(tm_gps_t *hGps, tm_u8_t *pData, tm_u16_t Len);
void  GetGpsData(tm_gps_t *hGps, tm_gpsdata_t *hGpsData);

#endif /* __TMGPS_H__ */
