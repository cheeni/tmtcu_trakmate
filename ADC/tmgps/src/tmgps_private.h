#ifndef __TMGPS_PRIVATE_H__

#include "tmgps\inc\tmgps.h"

#define __TMGPS_PRIVATE_H__

#define GPS_UART_TX_PIN 0x40
#define GPS_UART_RX_PIN 0x02

#define GPS_WAKEUP_PIN 0x10
#define GPS_RESET_PIN  0x20
#define GPS_STDBY_PIN  0x40
#define GPS_1V8_EN_PIN 0x80

#include "tmuart.h"

#define SPEED_STOP 5.0

#define RXBUFLEN 32
#define TXQUEUEBUFLEN 512
#define QUEUELEN 64

#define NMEA_BEGINMARKER '$'

struct tm_queue
{
  tm_u8_t *pData;
  tm_u16_t Len;
  tm_bool_t bValid;
};
typedef struct tm_queue tm_queue_t;

struct tm_uart
{
  tm_u8_t   *pRxBufferWrPtr;
  tm_u8_t   *pRxBufferRdPtr;
  tm_u8_t   *pTx;
  tm_u8_t   *pTxQueueBufferWrPtr;
  tm_queue_t Queue[QUEUELEN];
  tm_u8_t   QueueRdIndex;
  tm_u8_t   QueueWrIndex;
  tm_u16_t   TxLen;
  tm_u8_t   RxBuffer[RXBUFLEN];
  tm_u8_t   TxQueueBuffer[TXQUEUEBUFLEN];
  tm_u8_t   NbrOfBlocksInQueue;
  tm_bool_t bDataSent;
};

typedef struct tm_uart tm_uart_t;

#define LATITUDE_INVALID 100.0
#define LONGITUDE_INVALID 181.0
#define SPEED_INVALID     -1.0
#define HEADING_INVALID    361.0


#define LEN_DATE           6
#define LEN_TIME  10
#define LEN_VALIDITY  1
#define LEN_LATITUDE 10
#define LEN_LATITUDEDIR 1
#define LEN_LONGITUDE 11
#define LEN_LONGITUDEDIR 1
#define LEN_COURSE 4
#define TIME_POINT 6

void ParseNMEAString(tm_gps_t *hGps);
void ProcessRMCMessage(tm_gps_t *hGps);
tm_bool_t ValidateTime(tm_gps_t *hGps, char *pChar);
tm_bool_t ValidateValidity(tm_gps_t *hGps, char *pChar);
tm_bool_t ValidateLat(tm_gps_t *hGps, char *pChar);
tm_bool_t ValidateLatDir(tm_gps_t *hGps, char *pChar);
tm_bool_t ValidateLong(tm_gps_t *hGps, char *pChar);
tm_bool_t ValidateLongDir(tm_gps_t *hGps, char *pChar);
tm_bool_t ValidateSpeed(tm_gps_t *hGps, char *pChar);
tm_bool_t ValidateCourse(tm_gps_t *hGps, char *pChar);
tm_bool_t ValidateDate(tm_gps_t *hGps, char *pChar);

static int checkforleapyear(int year);
void CalculateUTC(tm_u32_t  *utc,char *pTime, char *pDate);
void Calculatelocaltime(tm_u8_t Date[], tm_u8_t Time[], tm_u32_t utc, tm_u32_t Timeoffset);

static float average(tm_gps_t *hGps);
static void MovingAvg(tm_gps_t *hGps, float speed);
static void ResetMvAvg(tm_gps_t *hGps);
static void GetVehicleStatus(tm_gps_t *hGps);


#endif /* __TMGPS_PRIVATE_H__ */
