#include "tmgps\src\tmgps_private.h"

extern char debugstr[];

#define BEGINYEAR 2000
//#define NUMOFDAYSTILL2012 15340
#define NUMOFDAYSTILL2012 4383

#define SECSINADAY 86400
#define SECSPERHOUR 3600
#define SECSPERMIN  60
#define NUMDAYSINNORMALYEAR 365
#define NUMDAYSINLEAPYEAR 366
#define SECINMIN    3600

static int daysinmonths[12] = {31,28,31,30,31,30,31,31,30,31,30,31};

static int checkforleapyear(int year)
{
  int retval = 0;
  if(year % 400 == 0 || ((year % 100 != 0) && (year % 4) == 0))
  {
    retval = 1;
    //printf("Year %d is a leap year\n",year);
  }
  else
  {
    retval = 0;
    //printf("Year %d is not a leap year\n",year);
  }
  return retval;
}

void CalculateUTC(tm_u32_t  *utc,char *pTime, char *pDate)
{
  /*char date[] = "20120429";
  char time[] = "123210";*/
  int curyear;
  int year;
  int month;
  int days;
  int hr;
  int min;
  int secs;
  char tmp[5];
  int numberofdays;
  int i;
 // long long utc = 0;

  numberofdays = 0;


  /* Get date and time */
  strncpy(tmp,pDate+4,2);
  tmp[2] = '\0';
  year = atoi(tmp);
  if (year < 16 || year > 26)
  {
    return;
  }
  year+=2000;

  strncpy(tmp,pDate+2,2);
  tmp[2] = '\0';
  month = atoi(tmp);
  month-=1;

  strncpy(tmp,pDate,2);
  tmp[2] = '\0';
  days = atoi(tmp);
  days-=1;

  strncpy(tmp,pTime,2);
  tmp[2] = '\0';
  hr = atoi(tmp);

  strncpy(tmp,pTime+2,2);
  tmp[2] = '\0';
  min = atoi(tmp);

  strncpy(tmp,pTime+4,2);
  tmp[2] = '\0';
  secs = atoi(tmp);

  curyear = BEGINYEAR;
  numberofdays = 0;
  while(curyear < year)
  {
    if(checkforleapyear(curyear) == 1)
      numberofdays += NUMDAYSINLEAPYEAR;
    else
      numberofdays += NUMDAYSINNORMALYEAR;
    curyear++;
  }

  i = 0;
  while(i < month)
  {
    numberofdays += daysinmonths[i++];
  }
  numberofdays += days;

  if(month >= 2 && checkforleapyear(year) == 1)
    numberofdays += 1;

  *utc = numberofdays * SECSINADAY;
  *utc += (hr * SECSPERHOUR);
  *utc += (min * SECSPERMIN);
  *utc += secs;
}

void Calculatelocaltime(tm_u8_t Date[], tm_u8_t Time[], tm_u32_t utc, tm_u32_t Timeoffset)
{
  int secs;
  int mins;
  int hrs;
  int year;
  int tmpdays;
  int i;
  int DIM;
  tm_u32_t numberofdays;
  tm_u32_t secstoday;
  char Buffer[6];

  utc = utc + Timeoffset;

  /* Get number of days till today */
  numberofdays = utc / SECSINADAY;

  /* Get number of seconds that are there today */
  secstoday = utc % SECSINADAY;
  secs = secstoday % 60;
  secstoday = secstoday - secs;

  /* Get minutes */
  mins = secstoday % SECINMIN;
  secstoday -= mins;
  mins = mins / 60;

  /* Get hours */
  hrs = secstoday / SECINMIN;

  tmpdays = numberofdays;
  tmpdays++;
  year = BEGINYEAR;
  while(tmpdays > 365)
  {
    if(checkforleapyear(year) == 0)
    {
      tmpdays -= NUMDAYSINNORMALYEAR;
    }
    else
    {
      tmpdays -= NUMDAYSINLEAPYEAR;
    }
    year++;
  }

  if(tmpdays == 0)
  {
    /* Last day of the year */
    i = 12;
    tmpdays = 31;
    year--;
  }
  else
  {
    for(i = 0; i < 12; i++)
    {
      DIM=daysinmonths[i];
      if((i==1) && checkforleapyear(year) == 1)
        DIM+=1;
      if(tmpdays > DIM)
      {
        tmpdays -= DIM;
      }
      else
      {
        /* Increment month by 1*/
        i++;
        break;
      }
    }
  }
  /* secs contains seconds */
  /* mins contains minutes */
  /* hrs contains hours */
  /* tmpdays contains date */
  /* i contains month */
  /* year contains year */
  Time[0]='\0';
  Date[0]='\0';
  //vts_itoa((int)(hrs & 0x1F),&Buffer[0]);
  sprintf(Buffer,"%d",(int)(hrs & 0x1F));
  if (Buffer[1]=='\0')
    strcat((char *)Time,"0");
  strcat((char *)Time,&Buffer[0]);
  //vts_itoa((int)(mins & 0x3F),&Buffer[0]);
  sprintf(Buffer,"%d",(int)(mins & 0x3F));
  if (Buffer[1]=='\0')
    strcat((char *)Time,"0");
  strcat((char *)Time,&Buffer[0]);
  //vts_itoa((int)(secs & 0x3F),&Buffer[0]);
  sprintf(Buffer,"%d",(int)(secs & 0x3F));
  if (Buffer[1]=='\0')
    strcat((char *)Time,"0");
  strcat((char *)Time,&Buffer[0]);

  //vts_itoa((int)(tmpdays & 0x1F),&Buffer[0]);
  sprintf(Buffer,"%d",(int)(tmpdays & 0x1F));
  if (Buffer[1]=='\0')
    strcat((char *)Date,"0");
  strcat((char *)Date,&Buffer[0]);
  //vts_itoa((int)(i & 0xF),&Buffer[0]);
  sprintf(Buffer,"%d",(int)(i & 0xF));
  if (Buffer[1]=='\0')
    strcat((char *)Date,"0");
  strcat((char *)Date,&Buffer[0]);
  //vts_itoa((int)(year & 0x7FF),&Buffer[0]);
  sprintf(Buffer,"%d",(int)(year & 0x7FF));
  strcat((char *)Date,&Buffer[2]);
}


