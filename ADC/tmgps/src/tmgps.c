#include "tmgps\src\tmgps_private.h"

extern char debugstr[];

float SpdLmtMax = 80.0;
float SpdLmtMin = 60.0;

tm_uart_t g_Uart6;
extern int uart6txevent;
extern int uart6rxevent;

void Asynch_UART6_Init(void)
{

  /* UART8 mode setting */
  IO_MFS6.UART.SCR.bit.UPCL = 1;	/* UART Initial */

  IO_MFS6.UART.SMR.byte = 0x00;	/* Asynchronous serial mode */
  IO_MFS6.UART.SMR.bit.SOE = 1;	/* Serial Data output enable */

  /* baund rate 115.2kbps (16MHz) */
  IO_MFS6.UART.BGR = 138;			/* (16x1000000)/(138+1) = 115107 */

  /* UART control resister setting */
  IO_MFS6.UART.SCR.byte = 0x00;

  /* UART ESCR resister setting */
  IO_MFS6.UART.ESCR.byte = 0x00;

  /* Serial Transmission enable */
  IO_MFS6.UART.SCR.bit.TIE =1;	/* Transmit int enable */
  IO_MFS6.UART.SCR.bit.TXE = 1;	/* Transmit enable */

  IO_MFS6.UART.SCR.bit.RIE = 1;	/* Receive int enable */
  IO_MFS6.UART.SCR.bit.RXE = 1;	/* Receive enable */

}

void InitGPS(tm_gps_t *hGps)
{
  tm_u8_t byte;

  memset(hGps, 0, sizeof(tm_gps_t));

  /* Srilal I/F Output Enable */
  /* Configure SOT6_0 */
  IO_KEYCDR = 0x0E2B;
  IO_KEYCDR = 0x4E2B;
  IO_KEYCDR = 0x8E2B;
  IO_KEYCDR = 0xCE2B;
  byte = IO_PFR11.byte;
  byte |= GPS_UART_TX_PIN;
  IO_PFR11.byte = byte;

  IO_KEYCDR = 0x0E81;
  IO_KEYCDR = 0x4E81;
  IO_KEYCDR = 0x8E81;
  IO_KEYCDR = 0xCE81;
  IO_EPFR33.byte = 0xFD;  /* SOT6_0,SCK6_0 */


  /* Configure SIN6_0 pin as input */
  IO_KEYCDR = 0x0E2C;
  IO_KEYCDR = 0x4E2C;
  IO_KEYCDR = 0x8E2C;
  IO_KEYCDR = 0xCE2C;
  byte = IO_PFR12.byte;

  byte &= (~GPS_UART_RX_PIN);
  IO_PFR12.byte = byte;

  /* Configure GPS 1.8V Enable, WAKEUP,  RESET, STDBY */
  IO_KEYCDR = 0x0E26;
  IO_KEYCDR = 0x4E26;
  IO_KEYCDR = 0x8E26;
  IO_KEYCDR = 0xCE26;
  byte = IO_PFR06.byte;

  byte |= (GPS_WAKEUP_PIN | GPS_RESET_PIN | GPS_STDBY_PIN | GPS_1V8_EN_PIN );
  IO_PFR06.byte = byte;

  IO_KEYCDR = 0x0E06;
  IO_KEYCDR = 0x4E06;
  IO_KEYCDR = 0x8E06;
  IO_KEYCDR = 0xCE06;

  byte = IO_DDR06.byte;
  byte |= (GPS_WAKEUP_PIN | GPS_RESET_PIN | GPS_STDBY_PIN | GPS_1V8_EN_PIN );
  IO_DDR06.byte = byte;

  byte = IO_PDR06.byte;
  byte |= (GPS_RESET_PIN | GPS_STDBY_PIN | GPS_1V8_EN_PIN );
  byte &= (~GPS_WAKEUP_PIN);
  IO_PDR11.byte = byte;

  /* Interrupt level setting */
  IO_ICR[16].byte = 0x10;		/* Multi Func Serial RX0 */
  IO_ICR[17].byte = 0x11;		/* Multi Func Serial TX0 */

  memset((void *)&g_Uart6,0,sizeof(g_Uart6));

  g_Uart6.pRxBufferRdPtr = g_Uart6.RxBuffer;
  g_Uart6.pRxBufferWrPtr = g_Uart6.RxBuffer;

  g_Uart6.pTxQueueBufferWrPtr = g_Uart6.TxQueueBuffer;

  g_Uart6.QueueRdIndex = 0;
  g_Uart6.QueueWrIndex = 0;

  g_Uart6.pTxQueueBufferWrPtr = g_Uart6.TxQueueBuffer;
  g_Uart6.TxLen = 0;
  g_Uart6.bDataSent = TM_TRUE;
  g_Uart6.NbrOfBlocksInQueue = 0;

  Asynch_UART6_Init();
  ResetMvAvg(hGps);
  hGps->MvAvgSpeed.BufLen = 5;

}

int tmp6;
/* Multi Fuction Serial TX6 Int routine */
__interrupt void Multi_TX6_int(void)
{
  if(g_Uart6.TxLen != 0)
    IO_MFS6.CSIO.RDR.hword.RDR0 = *g_Uart6.pTx++;   /* transmission data set */
  else
  {
    IO_MFS6.UART.SCR.bit.TIE = 0; /* Transmit int disable to stop generating tx interrupts */
    g_Uart6.bDataSent = TM_TRUE;
    uart6txevent = TM_TRUE;
  }
  tmp6 = IO_MFS6.UART.SSR.byte;		/* dummy read */
  g_Uart6.TxLen--;
}

/* Multi Function Serial RX6 Int routine */
__interrupt void Multi_RX6_int(void)
{
  tm_u8_t ch;

  if(IO_MFS6.CSIO.SSR.bit.ORE == 1)
  {
    IO_MFS6.CSIO.SSR.bit.REC = 1;		/* error flag clear */
  }
  else
  {
    ch = IO_MFS6.CSIO.RDR.hword.RDR0; /* reception data read */
    tmp6 = IO_MFS6.UART.SSR.byte;      /* dummy read */
    *g_Uart6.pRxBufferWrPtr = ch;
    INCCIRCULARPTR(g_Uart6.pRxBufferWrPtr, g_Uart6.RxBuffer, RXBUFLEN);
    uart6rxevent = TM_TRUE;
  }
}

void UART6_BufferRead(tm_u8_t *pData, tm_u8_t *Len)
{

  *Len = 0;
  /* Diable interrupts */
  IO_MFS6.UART.SCR.bit.RIE = 0;

  /* Copy data */
  *Len = g_Uart6.pRxBufferWrPtr - g_Uart6.RxBuffer;
  memcpy(pData,g_Uart6.RxBuffer, *Len);
  g_Uart6.pRxBufferWrPtr = g_Uart6.RxBuffer;

  /* Enable Interrupts */
  IO_MFS6.UART.SCR.bit.RIE = 1;
}

void UART6_BufferWrite(tm_u8_t *pData, tm_u16_t Len)
{
  /* Copy data */
  g_Uart6.TxLen = Len;
  g_Uart6.pTx = pData;
  uart6txevent = TM_FALSE;

  /* Enable Interrupts */
  IO_MFS6.UART.SCR.bit.TIE = 1;
}

void UART6QueueData(tm_u8_t *pData, tm_u16_t Len)
{
  tm_u16_t tmpLen;
  /* Copy data to queue and update queue pts */

  tmpLen = TXQUEUEBUFLEN - (g_Uart6.pTxQueueBufferWrPtr - g_Uart6.TxQueueBuffer);

  if(tmpLen < Len)
  {
    /* Split data in to two queue entries */
    memcpy(g_Uart6.pTxQueueBufferWrPtr,pData,tmpLen);
    g_Uart6.Queue[g_Uart6.QueueWrIndex].pData = g_Uart6.pTxQueueBufferWrPtr;
    g_Uart6.Queue[g_Uart6.QueueWrIndex].Len = tmpLen;
    g_Uart6.Queue[g_Uart6.QueueWrIndex].bValid = TM_TRUE;
    INCCIRCULARINDEX(g_Uart6.QueueWrIndex, QUEUELEN);
    g_Uart6.NbrOfBlocksInQueue++;

    memcpy(g_Uart6.TxQueueBuffer,pData+tmpLen,Len - tmpLen);
    g_Uart6.Queue[g_Uart6.QueueWrIndex].pData = g_Uart6.TxQueueBuffer;
    g_Uart6.Queue[g_Uart6.QueueWrIndex].Len = Len - tmpLen;
    g_Uart6.Queue[g_Uart6.QueueWrIndex].bValid = TM_TRUE;
    INCCIRCULARINDEX(g_Uart6.QueueWrIndex, QUEUELEN);
    g_Uart6.pTxQueueBufferWrPtr = g_Uart6.TxQueueBuffer + Len - tmpLen;
    g_Uart6.NbrOfBlocksInQueue++;
  }
  else
  {
    memcpy(g_Uart6.pTxQueueBufferWrPtr,pData,Len);
    g_Uart6.Queue[g_Uart6.QueueWrIndex].pData = g_Uart6.pTxQueueBufferWrPtr;
    g_Uart6.Queue[g_Uart6.QueueWrIndex].Len = Len;
    g_Uart6.Queue[g_Uart6.QueueWrIndex].bValid = TM_TRUE;
    ADDCIRCULARPTR(g_Uart6.pTxQueueBufferWrPtr, Len, g_Uart6.TxQueueBuffer, TXQUEUEBUFLEN);
    INCCIRCULARINDEX(g_Uart6.QueueWrIndex, QUEUELEN);
    g_Uart6.NbrOfBlocksInQueue++;
  }
  if(g_Uart6.bDataSent == TM_TRUE)
  {
    UART6DeQueueData();
  }
}

void UART6DeQueueData(void)
{
  if(g_Uart6.NbrOfBlocksInQueue != 0)
  {
    g_Uart6.bDataSent = TM_FALSE;
    UART6_BufferWrite(g_Uart6.Queue[g_Uart6.QueueRdIndex].pData, g_Uart6.Queue[g_Uart6.QueueRdIndex].Len);
    INCCIRCULARINDEX(g_Uart6.QueueRdIndex, QUEUELEN);
    g_Uart6.NbrOfBlocksInQueue--;
  }
}


void HandleGPSData(tm_gps_t *hGps, tm_u8_t *pData, tm_u16_t Len)
{
  tm_u8_t ch;

  while(Len--)
  {
    ch = *pData++;
    switch(hGps->GpsPraseState)
    {
      case LOOKFOR_DOLLAR:
        if(ch == NMEA_BEGINMARKER)
        {
          hGps->NMEAStrIndex = 0;
          hGps->GpsPraseState = LOOKFOR_CR;
        }
        break;
      case LOOKFOR_CR:
        if(ch == CARRIAGERETURN)
        {
          hGps->GpsPraseState = LOOKFOR_LF;
          hGps->NMEAString[hGps->NMEAStrIndex++] = '\0';
        }
        else
        {
          hGps->NMEAString[hGps->NMEAStrIndex++] = (tm_s8_t)ch;
          if(hGps->NMEAStrIndex >= MAXNMEALEN)
          {
            hGps->GpsPraseState = LOOKFOR_DOLLAR;
          }
        }
        break;
      case LOOKFOR_LF:
        hGps->GpsPraseState = LOOKFOR_DOLLAR;
        if(ch == LINEFEED)
        {
          ParseNMEAString(hGps);
        }
    }
  }
}

void ParseNMEAString(tm_gps_t *hGps)
{
  if(strncmp(hGps->NMEAString,"GPRMC",5) == 0)
  {
    ProcessRMCMessage(hGps);
  }
}

void ProcessRMCMessage(tm_gps_t *hGps)
{
  int CommaCount;
  int Len;
  char *pChar;
  char *str;
  float deg;
  float min;

  str = hGps->NMEAString;

  Len = strlen(hGps->NMEAString);
  CommaCount = 0;

  pChar = str;
  while(Len)
  {
    Len--;
    if(*str == ',')
    {
      *str = '\0';
      CommaCount++;
      if(pChar == str)
      {
        pChar = str + 1;
        continue;
      }
      switch(CommaCount)
      {
        case 2:
          /* Time stamp*/
          if( ValidateTime(hGps, pChar) == TM_TRUE)
          {
            strcpy(hGps->GpsData.Time,pChar);
          }
          else
          {
            hGps->GpsData.Time[0] = '\0';
          }
          break;
        case 3:
          /* GPS Validity */
          if( ValidateValidity(hGps, pChar) == TM_TRUE)
          {
            if(pChar[0] == 'A')
              hGps->GpsData.bFixValid = TM_TRUE;
            else if(pChar[0] == 'V')
              hGps->GpsData.bFixValid = TM_FALSE;
            else
              hGps->GpsData.bFixValid = TM_FALSE;
          }
          else
            hGps->GpsData.bFixValid = TM_FALSE;
          break;
        case 4:
          /* Latitude */
          if( ValidateLat(hGps, pChar) == TM_TRUE)
          {
            min = atof(&pChar[2])/ 60.0;
            pChar[2] = '\0';
            deg = atof(pChar);
            hGps->GpsData.Latitude = deg + min;
          }
          else
          {
            hGps->GpsData.Latitude = LATITUDE_INVALID;
          }
          break;
        case 5:
          /* Latitude Direction */
          if( ValidateLatDir(hGps, pChar) == TM_TRUE)
          {
            if(pChar[0] == 'N')
            {
              hGps->GpsData.Latitude = hGps->GpsData.Latitude;
            }
            else if(pChar[0] == 'S')
            {
              hGps->GpsData.Latitude = -hGps->GpsData.Latitude;
            }
            else
            {
              hGps->GpsData.Latitude = LATITUDE_INVALID;
            }
          }
          break;
        case 6:
          /* Longitude */
          if( ValidateLong(hGps, pChar) == TM_TRUE)
          {
            hGps->GpsData.Longitude = atof(pChar);
            min = atof(&pChar[3])/ 60.0;
            pChar[3] = '\0';
            deg = atof(pChar);
            hGps->GpsData.Longitude = deg + min;
          }
          else
          {
            hGps->GpsData.Longitude = LONGITUDE_INVALID;
          }
          break;
        case 7:
          /* Longitude Direction */
          if( ValidateLongDir(hGps, pChar) == TM_TRUE)
          {
            if(pChar[0] == 'E')
            {
              hGps->GpsData.Longitude = hGps->GpsData.Longitude;
            }
            else if(pChar[0] == 'W')
            {
              hGps->GpsData.Longitude = -hGps->GpsData.Longitude;
            }
            else
            {
              hGps->GpsData.Longitude = LONGITUDE_INVALID;
            }
          }
          break;
        case 8:
          /* Speed */
          if( ValidateSpeed(hGps, pChar) == TM_TRUE)
          {
            deg = atof(pChar) * 1.852;
            /* Moving window average the speed. Window length 5 */
            MovingAvg(hGps, deg);

            GetVehicleStatus(hGps);

            hGps->GpsData.Speed = hGps->AvgSpeed;

            if(hGps->GpsData.Speed > SpdLmtMax)
            {
              /* Buzzer ON */
              IO_PDR04.byte |= 0x08;
            }
            if(hGps->GpsData.Speed < SpdLmtMin)
            {
              /* Buzzer OFF */
              IO_PDR04.byte &= (~0x08);
            }
          }
          else
          {
            hGps->GpsData.Speed = SPEED_INVALID;
          }
          break;
        case 9:
          /* Course */
          if( ValidateCourse(hGps, pChar) == TM_TRUE)
          {
            hGps->GpsData.Heading = atof(pChar);
          }
          else
          {
            hGps->GpsData.Heading = HEADING_INVALID;
          }
          break;
        case 10:
          /* Date */
          if( ValidateDate(hGps, pChar) == TM_TRUE)
          {
            strcpy(hGps->GpsData.Date,pChar);
            CalculateUTC(&hGps->GpsData.UTC, hGps->GpsData.Time, hGps->GpsData.Date);
#if 0
            sprintf(debugstr,"%d",hGps->GpsData.UTC);
            DPRINTF("UTC: ");
            DPRINTF(debugstr);
            DPRINTF("\r\n");;
            Calculatelocaltime( (tm_u8_t *)hGps->GpsData.Date,(tm_u8_t *) hGps->GpsData.Time, hGps->GpsData.UTC,0);
            DPRINTF("Locat time: ");
            DPRINTF(hGps->GpsData.Date);
            DPRINTF(" : ");
            DPRINTF(hGps->GpsData.Time);
            DPRINTF("\r\n");;
#endif
          }
          else
          {
            hGps->GpsData.Date[0] = '\0';
          }
          break;
        default:
          break;
      }
      pChar = str + 1;
    }
    str++;
  }
}

tm_bool_t ValidateTime(tm_gps_t *hGps, char *pChar)
{
  int Len;
  int i;

  Len = strlen(pChar);
  if(Len != LEN_TIME)
  {
    return TM_FALSE;
  }

  /* Parse only till seconds. Ignore fractional part */
  Len -= 4;
  i = 0;
  while(Len--)
  {
    if(!isdigit(pChar[i]))
      return TM_FALSE;
    i++;
  }

  /* Truncate fractionall part */
  pChar[TIME_POINT] = '\0';

  return TM_TRUE;
}

tm_bool_t ValidateValidity(tm_gps_t *hGps, char *pChar)
{
  if(strlen(pChar) != LEN_VALIDITY)
    return TM_FALSE;
  else
    return TM_TRUE;
}

tm_bool_t ValidateLat(tm_gps_t *hGps, char *pChar)
{
  int i;

  if(strlen(pChar) != LEN_LATITUDE)
    return TM_FALSE;

  i = 0;
  while(i < 4)
  {
    if(!isdigit(pChar[i]))
      return TM_FALSE;
    i++;
  }
  if(pChar[4] != '.')
    return TM_FALSE;

  i = 5;
  while(i < LEN_LATITUDE)
  {
    if(!isdigit(pChar[i]))
      return TM_FALSE;
    i++;
  }
  return TM_TRUE;
}

tm_bool_t ValidateLatDir(tm_gps_t *hGps, char *pChar)
{
  if(strlen(pChar) != LEN_LATITUDEDIR)
    return TM_FALSE;
  else
    return TM_TRUE;
}

tm_bool_t ValidateLong(tm_gps_t *hGps, char *pChar)
{
  int i;

  if(strlen(pChar) != LEN_LONGITUDE)
    return TM_FALSE;

  i = 0;
  while(i < 5)
  {
    if(!isdigit(pChar[i]))
      return TM_FALSE;
    i++;
  }
  if(pChar[5] != '.')
    return TM_FALSE;

  i = 6;
  while(i < LEN_LONGITUDE)
  {
    if(!isdigit(pChar[i]))
      return TM_FALSE;
    i++;
  }
  return TM_TRUE;
}

tm_bool_t ValidateLongDir(tm_gps_t *hGps, char *pChar)
{
  if(strlen(pChar) != LEN_LONGITUDEDIR)
    return TM_FALSE;
  else
    return TM_TRUE;
}

tm_bool_t ValidateSpeed(tm_gps_t *hGps, char *pChar)
{
  int Len;
  int i;

  Len = strlen(pChar);
  i = 0;
  while(Len--)
  {
    if(!isdigit(pChar[i]) && pChar[i] != '.')
      return TM_FALSE;
    i++;
  }
  return TM_TRUE;
}

tm_bool_t ValidateCourse(tm_gps_t *hGps, char *pChar)
{
  int Len;
  int i;

  Len = strlen(pChar);
  i = 0;
  while(Len--)
  {
    if(!isdigit(pChar[i]) && pChar[i] != '.')
      return TM_FALSE;
    i++;
  }
  return TM_TRUE;
}


tm_bool_t ValidateDate(tm_gps_t *hGps, char *pChar)
{
  int Len;
  int i;

  Len = strlen(pChar);
  if(Len != LEN_DATE)
  {
    return TM_FALSE;
  }

  /* Parse only till seconds. Ignore fractional part */
  i = 0;
  while(Len--)
  {
    if(!isdigit(pChar[i]))
      return TM_FALSE;
    i++;
  }

  return TM_TRUE;
}

void  GetGpsData(tm_gps_t *hGps, tm_gpsdata_t *hGpsData)
{
  hGpsData->Speed = hGps->GpsData.Speed;
  hGpsData->Latitude = hGps->GpsData.Latitude;
  hGpsData->Longitude = hGps->GpsData.Longitude;
  hGpsData->Heading = hGps->GpsData.Heading;
  hGpsData->UTC = hGps->GpsData.UTC;
  hGpsData->Speed = hGps->GpsData.Speed;
}

static float average(tm_gps_t *hGps)
{
  int i;
  float avg = 0.0;
  int rdptr;

  rdptr = hGps->MvAvgSpeed.RdPtr;
  for(i = 0; i < hGps->MvAvgSpeed.BufLen; i++)
  {
     avg += hGps->MvAvgSpeed.SampleBuf[rdptr];
     INCCIRCULARINDEX(rdptr, hGps->MvAvgSpeed.BufLen);
  }
  avg /= hGps->MvAvgSpeed.BufLen;
  return avg;
}

static void MovingAvg(tm_gps_t *hGps, float speed)
{
  hGps->MvAvgSpeed.SampleBuf[hGps->MvAvgSpeed.WrPtr] = speed;
  hGps->AvgSpeed = average(hGps);
  INCCIRCULARINDEX(hGps->MvAvgSpeed.WrPtr, hGps->MvAvgSpeed.BufLen);
  INCCIRCULARINDEX(hGps->MvAvgSpeed.RdPtr, hGps->MvAvgSpeed.BufLen);
}

static void ResetMvAvg(tm_gps_t *hGps)
{
  int i;

  for(i = 0; i < MAXMVAVGBUFLEN; i++)
  {
    hGps->MvAvgSpeed.SampleBuf[i] = 0.0;
  }
  hGps->MvAvgSpeed.RdPtr = 0;
  hGps->MvAvgSpeed.WrPtr = 0;
}

static void GetVehicleStatus(tm_gps_t *hGps)
{
  if(hGps->AvgSpeed < SPEED_STOP )
  {
    hGps->AvgSpeed = 0.0;
  }
}
