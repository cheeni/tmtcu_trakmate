/*******************************************************************************
* Copyright (C) 2013 Spansion LLC. All Rights Reserved. 
*
* This software is owned and published by: 
* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software contains source code for use with Spansion 
* components. This software is licensed by Spansion to be adapted only 
* for use in systems utilizing Spansion components. Spansion shall not be 
* responsible for misuse or illegal use of this software for devices not 
* supported herein.  Spansion is providing this software "AS IS" and will 
* not be responsible for issues arising from incorrect user implementation 
* of the software.  
*
* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE,
* REGARDING THE SOFTWARE (INCLUDING ANY ACOOMPANYING WRITTEN MATERIALS), 
* ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED USE, INCLUDING, 
* WITHOUT LIMITATION, THE IMPLIED WARRANTY OF MERCHANTABILITY, THE IMPLIED 
* WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE OR USE, AND THE IMPLIED 
* WARRANTY OF NONINFRINGEMENT.  
* SPANSION SHALL HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, 
* NEGLIGENCE OR OTHERWISE) FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT 
* LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, 
* LOSS OF BUSINESS INFORMATION, OR OTHER PECUNIARY LOSS) ARISING FROM USE OR 
* INABILITY TO USE THE SOFTWARE, INCLUDING, WITHOUT LIMITATION, ANY DIRECT, 
* INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, 
* SAVINGS OR PROFITS, 
* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 
* YOU ASSUME ALL RESPONSIBILITIES FOR SELECTION OF THE SOFTWARE TO ACHIEVE YOUR
* INTENDED RESULTS, AND FOR THE INSTALLATION OF, USE OF, AND RESULTS OBTAINED 
* FROM, THE SOFTWARE.  
*
* This software may be replicated in part or whole for the licensed use, 
* with the restriction that this Disclaimer and Copyright notice must be 
* included with each copy of this software, whether used in part or whole, 
* at all times.  
*****************************************************************************/
/**************************************************************************
 *  Date   :        2013/11/11
 *  Project:        A/D CONVERTER
 **************************************************************************/

#include "_fr.h"
#include "extern.h"

/**************************************************************************/
/* initialized ADC */
/**************************************************************************/

void init_ADC(void){

	IO_ICR[32].byte = 0x10;		/* A/D interrupt level set */
	IO_ICR[41].byte = 0x10;		/* A/D interrupt level set */

	/* compare time 1750ns */
	AD0_ADMD_CT = 0x00;
	AD1_ADMD_CT = 0x00;
	/* sampling time 750ns */
	AD0_ADMD_ST = 0x00;
	AD1_ADMD_ST = 0x00;

//	AD0_ADTSE = 0x00000001;		/* AN0 soft trigger valid */	
	AD0_ADTSE = 0x80000000;		/* AN31 soft trigger valid */
	AD1_ADTSE = 0x2000;		/* AN45 soft trigger valid */
	
//	AD0_ADTCS0 = 0x2000;	/* INT Valid,software start ADC0 */
	AD0_ADTCS31 = 0x2000;	/* INT Valid,software start ADC31 */
	AD1_ADTCS13 = 0x2000;	/* INT Valid,software start ADC45 */
	
//	AD0_ADTECS0 = 0x0000;/* AD0 */
	AD0_ADTECS31 = 0x001F;/* AD31 */
	AD1_ADTECS13 = 0x000D;/* AD45 */
	
	AD0_ADTSS_START = 0x1;	/* AN0 set A/D control start */
	AD1_ADTSS_START = 0x1;	/* AN1 set A/D control start */
}

