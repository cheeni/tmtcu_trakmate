/******************************************************************************
 * V02L02
 * 2013-07-04
 *****************************************************************************/
/*******************************************************************************
* Copyright (C) 2013 Spansion LLC. All Rights Reserved. 
*
* This software is owned and published by: 
* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software contains source code for use with Spansion 
* components. This software is licensed by Spansion to be adapted only 
* for use in systems utilizing Spansion components. Spansion shall not be 
* responsible for misuse or illegal use of this software for devices not 
* supported herein.  Spansion is providing this software "AS IS" and will 
* not be responsible for issues arising from incorrect user implementation 
* of the software.  
*
* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE,
* REGARDING THE SOFTWARE (INCLUDING ANY ACOOMPANYING WRITTEN MATERIALS), 
* ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED USE, INCLUDING, 
* WITHOUT LIMITATION, THE IMPLIED WARRANTY OF MERCHANTABILITY, THE IMPLIED 
* WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE OR USE, AND THE IMPLIED 
* WARRANTY OF NONINFRINGEMENT.  
* SPANSION SHALL HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, 
* NEGLIGENCE OR OTHERWISE) FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT 
* LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, 
* LOSS OF BUSINESS INFORMATION, OR OTHER PECUNIARY LOSS) ARISING FROM USE OR 
* INABILITY TO USE THE SOFTWARE, INCLUDING, WITHOUT LIMITATION, ANY DIRECT, 
* INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, 
* SAVINGS OR PROFITS, 
* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 
* YOU ASSUME ALL RESPONSIBILITIES FOR SELECTION OF THE SOFTWARE TO ACHIEVE YOUR
* INTENDED RESULTS, AND FOR THE INSTALLATION OF, USE OF, AND RESULTS OBTAINED 
* FROM, THE SOFTWARE.  
*
* This software may be replicated in part or whole for the licensed use, 
* with the restriction that this Disclaimer and Copyright notice must be 
* included with each copy of this software, whether used in part or whole, 
* at all times.  
*****************************************************************************/
/** \file _fr.h
 **
 ** Add description here...
 **
 ** History:
 **   See io_reg.txt.
 *****************************************************************************/

#ifndef __FR_H__
#define __FR_H__

#if defined(__CPU_MB91F522BH__) || \
    defined(__CPU_MB91F522BJ__) || \
    defined(__CPU_MB91F522BK__) || \
    defined(__CPU_MB91F522BL__) || \
    defined(__CPU_MB91F522BS__) || \
    defined(__CPU_MB91F522BU__) || \
    defined(__CPU_MB91F522BW__) || \
    defined(__CPU_MB91F522BY__) || \
    defined(__CPU_MB91F522DH__) || \
    defined(__CPU_MB91F522DJ__) || \
    defined(__CPU_MB91F522DK__) || \
    defined(__CPU_MB91F522DL__) || \
    defined(__CPU_MB91F522DS__) || \
    defined(__CPU_MB91F522DU__) || \
    defined(__CPU_MB91F522DW__) || \
    defined(__CPU_MB91F522DY__) || \
    defined(__CPU_MB91F522FH__) || \
    defined(__CPU_MB91F522FJ__) || \
    defined(__CPU_MB91F522FK__) || \
    defined(__CPU_MB91F522FL__) || \
    defined(__CPU_MB91F522FS__) || \
    defined(__CPU_MB91F522FU__) || \
    defined(__CPU_MB91F522FW__) || \
    defined(__CPU_MB91F522FY__) || \
    defined(__CPU_MB91F522JH__) || \
    defined(__CPU_MB91F522JJ__) || \
    defined(__CPU_MB91F522JK__) || \
    defined(__CPU_MB91F522JL__) || \
    defined(__CPU_MB91F522JS__) || \
    defined(__CPU_MB91F522JU__) || \
    defined(__CPU_MB91F522JW__) || \
    defined(__CPU_MB91F522JY__) || \
    defined(__CPU_MB91F522KH__) || \
    defined(__CPU_MB91F522KJ__) || \
    defined(__CPU_MB91F522KK__) || \
    defined(__CPU_MB91F522KL__) || \
    defined(__CPU_MB91F522KS__) || \
    defined(__CPU_MB91F522KU__) || \
    defined(__CPU_MB91F522KW__) || \
    defined(__CPU_MB91F522KY__) || \
    defined(__CPU_MB91F522LH__) || \
    defined(__CPU_MB91F522LJ__) || \
    defined(__CPU_MB91F522LK__) || \
    defined(__CPU_MB91F522LL__) || \
    defined(__CPU_MB91F522LS__) || \
    defined(__CPU_MB91F522LU__) || \
    defined(__CPU_MB91F522LW__) || \
    defined(__CPU_MB91F522LY__) || \
    defined(__CPU_MB91F523BH__) || \
    defined(__CPU_MB91F523BJ__) || \
    defined(__CPU_MB91F523BK__) || \
    defined(__CPU_MB91F523BL__) || \
    defined(__CPU_MB91F523BS__) || \
    defined(__CPU_MB91F523BU__) || \
    defined(__CPU_MB91F523BW__) || \
    defined(__CPU_MB91F523BY__) || \
    defined(__CPU_MB91F523DH__) || \
    defined(__CPU_MB91F523DJ__) || \
    defined(__CPU_MB91F523DK__) || \
    defined(__CPU_MB91F523DL__) || \
    defined(__CPU_MB91F523DS__) || \
    defined(__CPU_MB91F523DU__) || \
    defined(__CPU_MB91F523DW__) || \
    defined(__CPU_MB91F523DY__) || \
    defined(__CPU_MB91F523FH__) || \
    defined(__CPU_MB91F523FJ__) || \
    defined(__CPU_MB91F523FK__) || \
    defined(__CPU_MB91F523FL__) || \
    defined(__CPU_MB91F523FS__) || \
    defined(__CPU_MB91F523FU__) || \
    defined(__CPU_MB91F523FW__) || \
    defined(__CPU_MB91F523FY__) || \
    defined(__CPU_MB91F523JH__) || \
    defined(__CPU_MB91F523JJ__) || \
    defined(__CPU_MB91F523JK__) || \
    defined(__CPU_MB91F523JL__) || \
    defined(__CPU_MB91F523JS__) || \
    defined(__CPU_MB91F523JU__) || \
    defined(__CPU_MB91F523JW__) || \
    defined(__CPU_MB91F523JY__) || \
    defined(__CPU_MB91F523KH__) || \
    defined(__CPU_MB91F523KJ__) || \
    defined(__CPU_MB91F523KK__) || \
    defined(__CPU_MB91F523KL__) || \
    defined(__CPU_MB91F523KS__) || \
    defined(__CPU_MB91F523KU__) || \
    defined(__CPU_MB91F523KW__) || \
    defined(__CPU_MB91F523KY__) || \
    defined(__CPU_MB91F523LH__) || \
    defined(__CPU_MB91F523LJ__) || \
    defined(__CPU_MB91F523LK__) || \
    defined(__CPU_MB91F523LL__) || \
    defined(__CPU_MB91F523LS__) || \
    defined(__CPU_MB91F523LU__) || \
    defined(__CPU_MB91F523LW__) || \
    defined(__CPU_MB91F523LY__) || \
    defined(__CPU_MB91F524BH__) || \
    defined(__CPU_MB91F524BJ__) || \
    defined(__CPU_MB91F524BK__) || \
    defined(__CPU_MB91F524BL__) || \
    defined(__CPU_MB91F524BS__) || \
    defined(__CPU_MB91F524BU__) || \
    defined(__CPU_MB91F524BW__) || \
    defined(__CPU_MB91F524BY__) || \
    defined(__CPU_MB91F524DH__) || \
    defined(__CPU_MB91F524DJ__) || \
    defined(__CPU_MB91F524DK__) || \
    defined(__CPU_MB91F524DL__) || \
    defined(__CPU_MB91F524DS__) || \
    defined(__CPU_MB91F524DU__) || \
    defined(__CPU_MB91F524DW__) || \
    defined(__CPU_MB91F524DY__) || \
    defined(__CPU_MB91F524FH__) || \
    defined(__CPU_MB91F524FJ__) || \
    defined(__CPU_MB91F524FK__) || \
    defined(__CPU_MB91F524FL__) || \
    defined(__CPU_MB91F524FS__) || \
    defined(__CPU_MB91F524FU__) || \
    defined(__CPU_MB91F524FW__) || \
    defined(__CPU_MB91F524FY__) || \
    defined(__CPU_MB91F524JH__) || \
    defined(__CPU_MB91F524JJ__) || \
    defined(__CPU_MB91F524JK__) || \
    defined(__CPU_MB91F524JL__) || \
    defined(__CPU_MB91F524JS__) || \
    defined(__CPU_MB91F524JU__) || \
    defined(__CPU_MB91F524JW__) || \
    defined(__CPU_MB91F524JY__) || \
    defined(__CPU_MB91F524KH__) || \
    defined(__CPU_MB91F524KJ__) || \
    defined(__CPU_MB91F524KK__) || \
    defined(__CPU_MB91F524KL__) || \
    defined(__CPU_MB91F524KS__) || \
    defined(__CPU_MB91F524KU__) || \
    defined(__CPU_MB91F524KW__) || \
    defined(__CPU_MB91F524KY__) || \
    defined(__CPU_MB91F524LH__) || \
    defined(__CPU_MB91F524LJ__) || \
    defined(__CPU_MB91F524LK__) || \
    defined(__CPU_MB91F524LL__) || \
    defined(__CPU_MB91F524LS__) || \
    defined(__CPU_MB91F524LU__) || \
    defined(__CPU_MB91F524LW__) || \
    defined(__CPU_MB91F524LY__) || \
    defined(__CPU_MB91F525BH__) || \
    defined(__CPU_MB91F525BJ__) || \
    defined(__CPU_MB91F525BK__) || \
    defined(__CPU_MB91F525BL__) || \
    defined(__CPU_MB91F525BS__) || \
    defined(__CPU_MB91F525BU__) || \
    defined(__CPU_MB91F525BW__) || \
    defined(__CPU_MB91F525BY__) || \
    defined(__CPU_MB91F525DH__) || \
    defined(__CPU_MB91F525DJ__) || \
    defined(__CPU_MB91F525DK__) || \
    defined(__CPU_MB91F525DL__) || \
    defined(__CPU_MB91F525DS__) || \
    defined(__CPU_MB91F525DU__) || \
    defined(__CPU_MB91F525DW__) || \
    defined(__CPU_MB91F525DY__) || \
    defined(__CPU_MB91F525FH__) || \
    defined(__CPU_MB91F525FJ__) || \
    defined(__CPU_MB91F525FK__) || \
    defined(__CPU_MB91F525FL__) || \
    defined(__CPU_MB91F525FS__) || \
    defined(__CPU_MB91F525FU__) || \
    defined(__CPU_MB91F525FW__) || \
    defined(__CPU_MB91F525FY__) || \
    defined(__CPU_MB91F525JH__) || \
    defined(__CPU_MB91F525JJ__) || \
    defined(__CPU_MB91F525JK__) || \
    defined(__CPU_MB91F525JL__) || \
    defined(__CPU_MB91F525JS__) || \
    defined(__CPU_MB91F525JU__) || \
    defined(__CPU_MB91F525JW__) || \
    defined(__CPU_MB91F525JY__) || \
    defined(__CPU_MB91F525KH__) || \
    defined(__CPU_MB91F525KJ__) || \
    defined(__CPU_MB91F525KK__) || \
    defined(__CPU_MB91F525KL__) || \
    defined(__CPU_MB91F525KS__) || \
    defined(__CPU_MB91F525KU__) || \
    defined(__CPU_MB91F525KW__) || \
    defined(__CPU_MB91F525KY__) || \
    defined(__CPU_MB91F525LH__) || \
    defined(__CPU_MB91F525LJ__) || \
    defined(__CPU_MB91F525LK__) || \
    defined(__CPU_MB91F525LL__) || \
    defined(__CPU_MB91F525LS__) || \
    defined(__CPU_MB91F525LU__) || \
    defined(__CPU_MB91F525LW__) || \
    defined(__CPU_MB91F525LY__) || \
    defined(__CPU_MB91F526BH__) || \
    defined(__CPU_MB91F526BJ__) || \
    defined(__CPU_MB91F526BK__) || \
    defined(__CPU_MB91F526BL__) || \
    defined(__CPU_MB91F526BS__) || \
    defined(__CPU_MB91F526BU__) || \
    defined(__CPU_MB91F526BW__) || \
    defined(__CPU_MB91F526BY__) || \
    defined(__CPU_MB91F526DH__) || \
    defined(__CPU_MB91F526DJ__) || \
    defined(__CPU_MB91F526DK__) || \
    defined(__CPU_MB91F526DL__) || \
    defined(__CPU_MB91F526DS__) || \
    defined(__CPU_MB91F526DU__) || \
    defined(__CPU_MB91F526DW__) || \
    defined(__CPU_MB91F526DY__) || \
    defined(__CPU_MB91F526FH__) || \
    defined(__CPU_MB91F526FJ__) || \
    defined(__CPU_MB91F526FK__) || \
    defined(__CPU_MB91F526FL__) || \
    defined(__CPU_MB91F526FS__) || \
    defined(__CPU_MB91F526FU__) || \
    defined(__CPU_MB91F526FW__) || \
    defined(__CPU_MB91F526FY__) || \
    defined(__CPU_MB91F526JH__) || \
    defined(__CPU_MB91F526JJ__) || \
    defined(__CPU_MB91F526JK__) || \
    defined(__CPU_MB91F526JL__) || \
    defined(__CPU_MB91F526JS__) || \
    defined(__CPU_MB91F526JU__) || \
    defined(__CPU_MB91F526JW__) || \
    defined(__CPU_MB91F526JY__) || \
    defined(__CPU_MB91F526KH__) || \
    defined(__CPU_MB91F526KJ__) || \
    defined(__CPU_MB91F526KK__) || \
    defined(__CPU_MB91F526KL__) || \
    defined(__CPU_MB91F526KS__) || \
    defined(__CPU_MB91F526KU__) || \
    defined(__CPU_MB91F526KW__) || \
    defined(__CPU_MB91F526KY__) || \
    defined(__CPU_MB91F526LH__) || \
    defined(__CPU_MB91F526LJ__) || \
    defined(__CPU_MB91F526LK__) || \
    defined(__CPU_MB91F526LL__) || \
    defined(__CPU_MB91F526LS__) || \
    defined(__CPU_MB91F526LU__) || \
    defined(__CPU_MB91F526LW__) || \
    defined(__CPU_MB91F526LY__) || \
    defined(__CPU_MB91F527MH__) || \
    defined(__CPU_MB91F527MJ__) || \
    defined(__CPU_MB91F527MK__) || \
    defined(__CPU_MB91F527ML__) || \
    defined(__CPU_MB91F527MS__) || \
    defined(__CPU_MB91F527MU__) || \
    defined(__CPU_MB91F527MW__) || \
    defined(__CPU_MB91F527MY__) || \
    defined(__CPU_MB91F527RH__) || \
    defined(__CPU_MB91F527RJ__) || \
    defined(__CPU_MB91F527RK__) || \
    defined(__CPU_MB91F527RL__) || \
    defined(__CPU_MB91F527RS__) || \
    defined(__CPU_MB91F527RU__) || \
    defined(__CPU_MB91F527RW__) || \
    defined(__CPU_MB91F527RY__) || \
    defined(__CPU_MB91F527UH__) || \
    defined(__CPU_MB91F527UJ__) || \
    defined(__CPU_MB91F527UK__) || \
    defined(__CPU_MB91F527UL__) || \
    defined(__CPU_MB91F527US__) || \
    defined(__CPU_MB91F527UU__) || \
    defined(__CPU_MB91F527UW__) || \
    defined(__CPU_MB91F527UY__) || \
    defined(__CPU_MB91F527YH__) || \
    defined(__CPU_MB91F527YJ__) || \
    defined(__CPU_MB91F527YK__) || \
    defined(__CPU_MB91F527YL__) || \
    defined(__CPU_MB91F527YS__) || \
    defined(__CPU_MB91F527YU__) || \
    defined(__CPU_MB91F527YW__) || \
    defined(__CPU_MB91F527YY__) || \
    defined(__CPU_MB91F528MH__) || \
    defined(__CPU_MB91F528MJ__) || \
    defined(__CPU_MB91F528MK__) || \
    defined(__CPU_MB91F528ML__) || \
    defined(__CPU_MB91F528MS__) || \
    defined(__CPU_MB91F528MU__) || \
    defined(__CPU_MB91F528MW__) || \
    defined(__CPU_MB91F528MY__) || \
    defined(__CPU_MB91F528RH__) || \
    defined(__CPU_MB91F528RJ__) || \
    defined(__CPU_MB91F528RK__) || \
    defined(__CPU_MB91F528RL__) || \
    defined(__CPU_MB91F528RS__) || \
    defined(__CPU_MB91F528RU__) || \
    defined(__CPU_MB91F528RW__) || \
    defined(__CPU_MB91F528RY__) || \
    defined(__CPU_MB91F528UH__) || \
    defined(__CPU_MB91F528UJ__) || \
    defined(__CPU_MB91F528UK__) || \
    defined(__CPU_MB91F528UL__) || \
    defined(__CPU_MB91F528US__) || \
    defined(__CPU_MB91F528UU__) || \
    defined(__CPU_MB91F528UW__) || \
    defined(__CPU_MB91F528UY__) || \
    defined(__CPU_MB91F528YH__) || \
    defined(__CPU_MB91F528YJ__) || \
    defined(__CPU_MB91F528YK__) || \
    defined(__CPU_MB91F528YL__) || \
    defined(__CPU_MB91F528YS__) || \
    defined(__CPU_MB91F528YU__) || \
    defined(__CPU_MB91F528YW__) || \
    defined(__CPU_MB91F528YY__)
#ifdef __FASM__
#include "mb91520_a.inc"
#else
#include "mb91520.h"
#endif
#elif defined(__CPU_MB91F522BH_FR80__) || \
    defined(__CPU_MB91F522BJ_FR80__) || \
    defined(__CPU_MB91F522BK_FR80__) || \
    defined(__CPU_MB91F522BL_FR80__) || \
    defined(__CPU_MB91F522BS_FR80__) || \
    defined(__CPU_MB91F522BU_FR80__) || \
    defined(__CPU_MB91F522BW_FR80__) || \
    defined(__CPU_MB91F522BY_FR80__) || \
    defined(__CPU_MB91F522DH_FR80__) || \
    defined(__CPU_MB91F522DJ_FR80__) || \
    defined(__CPU_MB91F522DK_FR80__) || \
    defined(__CPU_MB91F522DL_FR80__) || \
    defined(__CPU_MB91F522DS_FR80__) || \
    defined(__CPU_MB91F522DU_FR80__) || \
    defined(__CPU_MB91F522DW_FR80__) || \
    defined(__CPU_MB91F522DY_FR80__) || \
    defined(__CPU_MB91F522FH_FR80__) || \
    defined(__CPU_MB91F522FJ_FR80__) || \
    defined(__CPU_MB91F522FK_FR80__) || \
    defined(__CPU_MB91F522FL_FR80__) || \
    defined(__CPU_MB91F522FS_FR80__) || \
    defined(__CPU_MB91F522FU_FR80__) || \
    defined(__CPU_MB91F522FW_FR80__) || \
    defined(__CPU_MB91F522FY_FR80__) || \
    defined(__CPU_MB91F522JH_FR80__) || \
    defined(__CPU_MB91F522JJ_FR80__) || \
    defined(__CPU_MB91F522JK_FR80__) || \
    defined(__CPU_MB91F522JL_FR80__) || \
    defined(__CPU_MB91F522JS_FR80__) || \
    defined(__CPU_MB91F522JU_FR80__) || \
    defined(__CPU_MB91F522JW_FR80__) || \
    defined(__CPU_MB91F522JY_FR80__) || \
    defined(__CPU_MB91F522KH_FR80__) || \
    defined(__CPU_MB91F522KJ_FR80__) || \
    defined(__CPU_MB91F522KK_FR80__) || \
    defined(__CPU_MB91F522KL_FR80__) || \
    defined(__CPU_MB91F522KS_FR80__) || \
    defined(__CPU_MB91F522KU_FR80__) || \
    defined(__CPU_MB91F522KW_FR80__) || \
    defined(__CPU_MB91F522KY_FR80__) || \
    defined(__CPU_MB91F522LH_FR80__) || \
    defined(__CPU_MB91F522LJ_FR80__) || \
    defined(__CPU_MB91F522LK_FR80__) || \
    defined(__CPU_MB91F522LL_FR80__) || \
    defined(__CPU_MB91F522LS_FR80__) || \
    defined(__CPU_MB91F522LU_FR80__) || \
    defined(__CPU_MB91F522LW_FR80__) || \
    defined(__CPU_MB91F522LY_FR80__) || \
    defined(__CPU_MB91F523BH_FR80__) || \
    defined(__CPU_MB91F523BJ_FR80__) || \
    defined(__CPU_MB91F523BK_FR80__) || \
    defined(__CPU_MB91F523BL_FR80__) || \
    defined(__CPU_MB91F523BS_FR80__) || \
    defined(__CPU_MB91F523BU_FR80__) || \
    defined(__CPU_MB91F523BW_FR80__) || \
    defined(__CPU_MB91F523BY_FR80__) || \
    defined(__CPU_MB91F523DH_FR80__) || \
    defined(__CPU_MB91F523DJ_FR80__) || \
    defined(__CPU_MB91F523DK_FR80__) || \
    defined(__CPU_MB91F523DL_FR80__) || \
    defined(__CPU_MB91F523DS_FR80__) || \
    defined(__CPU_MB91F523DU_FR80__) || \
    defined(__CPU_MB91F523DW_FR80__) || \
    defined(__CPU_MB91F523DY_FR80__) || \
    defined(__CPU_MB91F523FH_FR80__) || \
    defined(__CPU_MB91F523FJ_FR80__) || \
    defined(__CPU_MB91F523FK_FR80__) || \
    defined(__CPU_MB91F523FL_FR80__) || \
    defined(__CPU_MB91F523FS_FR80__) || \
    defined(__CPU_MB91F523FU_FR80__) || \
    defined(__CPU_MB91F523FW_FR80__) || \
    defined(__CPU_MB91F523FY_FR80__) || \
    defined(__CPU_MB91F523JH_FR80__) || \
    defined(__CPU_MB91F523JJ_FR80__) || \
    defined(__CPU_MB91F523JK_FR80__) || \
    defined(__CPU_MB91F523JL_FR80__) || \
    defined(__CPU_MB91F523JS_FR80__) || \
    defined(__CPU_MB91F523JU_FR80__) || \
    defined(__CPU_MB91F523JW_FR80__) || \
    defined(__CPU_MB91F523JY_FR80__) || \
    defined(__CPU_MB91F523KH_FR80__) || \
    defined(__CPU_MB91F523KJ_FR80__) || \
    defined(__CPU_MB91F523KK_FR80__) || \
    defined(__CPU_MB91F523KL_FR80__) || \
    defined(__CPU_MB91F523KS_FR80__) || \
    defined(__CPU_MB91F523KU_FR80__) || \
    defined(__CPU_MB91F523KW_FR80__) || \
    defined(__CPU_MB91F523KY_FR80__) || \
    defined(__CPU_MB91F523LH_FR80__) || \
    defined(__CPU_MB91F523LJ_FR80__) || \
    defined(__CPU_MB91F523LK_FR80__) || \
    defined(__CPU_MB91F523LL_FR80__) || \
    defined(__CPU_MB91F523LS_FR80__) || \
    defined(__CPU_MB91F523LU_FR80__) || \
    defined(__CPU_MB91F523LW_FR80__) || \
    defined(__CPU_MB91F523LY_FR80__) || \
    defined(__CPU_MB91F524BH_FR80__) || \
    defined(__CPU_MB91F524BJ_FR80__) || \
    defined(__CPU_MB91F524BK_FR80__) || \
    defined(__CPU_MB91F524BL_FR80__) || \
    defined(__CPU_MB91F524BS_FR80__) || \
    defined(__CPU_MB91F524BU_FR80__) || \
    defined(__CPU_MB91F524BW_FR80__) || \
    defined(__CPU_MB91F524BY_FR80__) || \
    defined(__CPU_MB91F524DH_FR80__) || \
    defined(__CPU_MB91F524DJ_FR80__) || \
    defined(__CPU_MB91F524DK_FR80__) || \
    defined(__CPU_MB91F524DL_FR80__) || \
    defined(__CPU_MB91F524DS_FR80__) || \
    defined(__CPU_MB91F524DU_FR80__) || \
    defined(__CPU_MB91F524DW_FR80__) || \
    defined(__CPU_MB91F524DY_FR80__) || \
    defined(__CPU_MB91F524FH_FR80__) || \
    defined(__CPU_MB91F524FJ_FR80__) || \
    defined(__CPU_MB91F524FK_FR80__) || \
    defined(__CPU_MB91F524FL_FR80__) || \
    defined(__CPU_MB91F524FS_FR80__) || \
    defined(__CPU_MB91F524FU_FR80__) || \
    defined(__CPU_MB91F524FW_FR80__) || \
    defined(__CPU_MB91F524FY_FR80__) || \
    defined(__CPU_MB91F524JH_FR80__) || \
    defined(__CPU_MB91F524JJ_FR80__) || \
    defined(__CPU_MB91F524JK_FR80__) || \
    defined(__CPU_MB91F524JL_FR80__) || \
    defined(__CPU_MB91F524JS_FR80__) || \
    defined(__CPU_MB91F524JU_FR80__) || \
    defined(__CPU_MB91F524JW_FR80__) || \
    defined(__CPU_MB91F524JY_FR80__) || \
    defined(__CPU_MB91F524KH_FR80__) || \
    defined(__CPU_MB91F524KJ_FR80__) || \
    defined(__CPU_MB91F524KK_FR80__) || \
    defined(__CPU_MB91F524KL_FR80__) || \
    defined(__CPU_MB91F524KS_FR80__) || \
    defined(__CPU_MB91F524KU_FR80__) || \
    defined(__CPU_MB91F524KW_FR80__) || \
    defined(__CPU_MB91F524KY_FR80__) || \
    defined(__CPU_MB91F524LH_FR80__) || \
    defined(__CPU_MB91F524LJ_FR80__) || \
    defined(__CPU_MB91F524LK_FR80__) || \
    defined(__CPU_MB91F524LL_FR80__) || \
    defined(__CPU_MB91F524LS_FR80__) || \
    defined(__CPU_MB91F524LU_FR80__) || \
    defined(__CPU_MB91F524LW_FR80__) || \
    defined(__CPU_MB91F524LY_FR80__) || \
    defined(__CPU_MB91F525BH_FR80__) || \
    defined(__CPU_MB91F525BJ_FR80__) || \
    defined(__CPU_MB91F525BK_FR80__) || \
    defined(__CPU_MB91F525BL_FR80__) || \
    defined(__CPU_MB91F525BS_FR80__) || \
    defined(__CPU_MB91F525BU_FR80__) || \
    defined(__CPU_MB91F525BW_FR80__) || \
    defined(__CPU_MB91F525BY_FR80__) || \
    defined(__CPU_MB91F525DH_FR80__) || \
    defined(__CPU_MB91F525DJ_FR80__) || \
    defined(__CPU_MB91F525DK_FR80__) || \
    defined(__CPU_MB91F525DL_FR80__) || \
    defined(__CPU_MB91F525DS_FR80__) || \
    defined(__CPU_MB91F525DU_FR80__) || \
    defined(__CPU_MB91F525DW_FR80__) || \
    defined(__CPU_MB91F525DY_FR80__) || \
    defined(__CPU_MB91F525FH_FR80__) || \
    defined(__CPU_MB91F525FJ_FR80__) || \
    defined(__CPU_MB91F525FK_FR80__) || \
    defined(__CPU_MB91F525FL_FR80__) || \
    defined(__CPU_MB91F525FS_FR80__) || \
    defined(__CPU_MB91F525FU_FR80__) || \
    defined(__CPU_MB91F525FW_FR80__) || \
    defined(__CPU_MB91F525FY_FR80__) || \
    defined(__CPU_MB91F525JH_FR80__) || \
    defined(__CPU_MB91F525JJ_FR80__) || \
    defined(__CPU_MB91F525JK_FR80__) || \
    defined(__CPU_MB91F525JL_FR80__) || \
    defined(__CPU_MB91F525JS_FR80__) || \
    defined(__CPU_MB91F525JU_FR80__) || \
    defined(__CPU_MB91F525JW_FR80__) || \
    defined(__CPU_MB91F525JY_FR80__) || \
    defined(__CPU_MB91F525KH_FR80__) || \
    defined(__CPU_MB91F525KJ_FR80__) || \
    defined(__CPU_MB91F525KK_FR80__) || \
    defined(__CPU_MB91F525KL_FR80__) || \
    defined(__CPU_MB91F525KS_FR80__) || \
    defined(__CPU_MB91F525KU_FR80__) || \
    defined(__CPU_MB91F525KW_FR80__) || \
    defined(__CPU_MB91F525KY_FR80__) || \
    defined(__CPU_MB91F525LH_FR80__) || \
    defined(__CPU_MB91F525LJ_FR80__) || \
    defined(__CPU_MB91F525LK_FR80__) || \
    defined(__CPU_MB91F525LL_FR80__) || \
    defined(__CPU_MB91F525LS_FR80__) || \
    defined(__CPU_MB91F525LU_FR80__) || \
    defined(__CPU_MB91F525LW_FR80__) || \
    defined(__CPU_MB91F525LY_FR80__) || \
    defined(__CPU_MB91F526BH_FR80__) || \
    defined(__CPU_MB91F526BJ_FR80__) || \
    defined(__CPU_MB91F526BK_FR80__) || \
    defined(__CPU_MB91F526BL_FR80__) || \
    defined(__CPU_MB91F526BS_FR80__) || \
    defined(__CPU_MB91F526BU_FR80__) || \
    defined(__CPU_MB91F526BW_FR80__) || \
    defined(__CPU_MB91F526BY_FR80__) || \
    defined(__CPU_MB91F526DH_FR80__) || \
    defined(__CPU_MB91F526DJ_FR80__) || \
    defined(__CPU_MB91F526DK_FR80__) || \
    defined(__CPU_MB91F526DL_FR80__) || \
    defined(__CPU_MB91F526DS_FR80__) || \
    defined(__CPU_MB91F526DU_FR80__) || \
    defined(__CPU_MB91F526DW_FR80__) || \
    defined(__CPU_MB91F526DY_FR80__) || \
    defined(__CPU_MB91F526FH_FR80__) || \
    defined(__CPU_MB91F526FJ_FR80__) || \
    defined(__CPU_MB91F526FK_FR80__) || \
    defined(__CPU_MB91F526FL_FR80__) || \
    defined(__CPU_MB91F526FS_FR80__) || \
    defined(__CPU_MB91F526FU_FR80__) || \
    defined(__CPU_MB91F526FW_FR80__) || \
    defined(__CPU_MB91F526FY_FR80__) || \
    defined(__CPU_MB91F526JH_FR80__) || \
    defined(__CPU_MB91F526JJ_FR80__) || \
    defined(__CPU_MB91F526JK_FR80__) || \
    defined(__CPU_MB91F526JL_FR80__) || \
    defined(__CPU_MB91F526JS_FR80__) || \
    defined(__CPU_MB91F526JU_FR80__) || \
    defined(__CPU_MB91F526JW_FR80__) || \
    defined(__CPU_MB91F526JY_FR80__) || \
    defined(__CPU_MB91F526KH_FR80__) || \
    defined(__CPU_MB91F526KJ_FR80__) || \
    defined(__CPU_MB91F526KK_FR80__) || \
    defined(__CPU_MB91F526KL_FR80__) || \
    defined(__CPU_MB91F526KS_FR80__) || \
    defined(__CPU_MB91F526KU_FR80__) || \
    defined(__CPU_MB91F526KW_FR80__) || \
    defined(__CPU_MB91F526KY_FR80__) || \
    defined(__CPU_MB91F526LH_FR80__) || \
    defined(__CPU_MB91F526LJ_FR80__) || \
    defined(__CPU_MB91F526LK_FR80__) || \
    defined(__CPU_MB91F526LL_FR80__) || \
    defined(__CPU_MB91F526LS_FR80__) || \
    defined(__CPU_MB91F526LU_FR80__) || \
    defined(__CPU_MB91F526LW_FR80__) || \
    defined(__CPU_MB91F526LY_FR80__) || \
    defined(__CPU_MB91F527MH_FR80__) || \
    defined(__CPU_MB91F527MJ_FR80__) || \
    defined(__CPU_MB91F527MK_FR80__) || \
    defined(__CPU_MB91F527ML_FR80__) || \
    defined(__CPU_MB91F527MS_FR80__) || \
    defined(__CPU_MB91F527MU_FR80__) || \
    defined(__CPU_MB91F527MW_FR80__) || \
    defined(__CPU_MB91F527MY_FR80__) || \
    defined(__CPU_MB91F527RH_FR80__) || \
    defined(__CPU_MB91F527RJ_FR80__) || \
    defined(__CPU_MB91F527RK_FR80__) || \
    defined(__CPU_MB91F527RL_FR80__) || \
    defined(__CPU_MB91F527RS_FR80__) || \
    defined(__CPU_MB91F527RU_FR80__) || \
    defined(__CPU_MB91F527RW_FR80__) || \
    defined(__CPU_MB91F527RY_FR80__) || \
    defined(__CPU_MB91F527UH_FR80__) || \
    defined(__CPU_MB91F527UJ_FR80__) || \
    defined(__CPU_MB91F527UK_FR80__) || \
    defined(__CPU_MB91F527UL_FR80__) || \
    defined(__CPU_MB91F527US_FR80__) || \
    defined(__CPU_MB91F527UU_FR80__) || \
    defined(__CPU_MB91F527UW_FR80__) || \
    defined(__CPU_MB91F527UY_FR80__) || \
    defined(__CPU_MB91F527YH_FR80__) || \
    defined(__CPU_MB91F527YJ_FR80__) || \
    defined(__CPU_MB91F527YK_FR80__) || \
    defined(__CPU_MB91F527YL_FR80__) || \
    defined(__CPU_MB91F527YS_FR80__) || \
    defined(__CPU_MB91F527YU_FR80__) || \
    defined(__CPU_MB91F527YW_FR80__) || \
    defined(__CPU_MB91F527YY_FR80__) || \
    defined(__CPU_MB91F528MH_FR80__) || \
    defined(__CPU_MB91F528MJ_FR80__) || \
    defined(__CPU_MB91F528MK_FR80__) || \
    defined(__CPU_MB91F528ML_FR80__) || \
    defined(__CPU_MB91F528MS_FR80__) || \
    defined(__CPU_MB91F528MU_FR80__) || \
    defined(__CPU_MB91F528MW_FR80__) || \
    defined(__CPU_MB91F528MY_FR80__) || \
    defined(__CPU_MB91F528RH_FR80__) || \
    defined(__CPU_MB91F528RJ_FR80__) || \
    defined(__CPU_MB91F528RK_FR80__) || \
    defined(__CPU_MB91F528RL_FR80__) || \
    defined(__CPU_MB91F528RS_FR80__) || \
    defined(__CPU_MB91F528RU_FR80__) || \
    defined(__CPU_MB91F528RW_FR80__) || \
    defined(__CPU_MB91F528RY_FR80__) || \
    defined(__CPU_MB91F528UH_FR80__) || \
    defined(__CPU_MB91F528UJ_FR80__) || \
    defined(__CPU_MB91F528UK_FR80__) || \
    defined(__CPU_MB91F528UL_FR80__) || \
    defined(__CPU_MB91F528US_FR80__) || \
    defined(__CPU_MB91F528UU_FR80__) || \
    defined(__CPU_MB91F528UW_FR80__) || \
    defined(__CPU_MB91F528UY_FR80__) || \
    defined(__CPU_MB91F528YH_FR80__) || \
    defined(__CPU_MB91F528YJ_FR80__) || \
    defined(__CPU_MB91F528YK_FR80__) || \
    defined(__CPU_MB91F528YL_FR80__) || \
    defined(__CPU_MB91F528YS_FR80__) || \
    defined(__CPU_MB91F528YU_FR80__) || \
    defined(__CPU_MB91F528YW_FR80__) || \
    defined(__CPU_MB91F528YY_FR80__)
#ifdef __FASM__
#include "mb91520_a.inc"
#else
#include "mb91520.h"
#endif
#else
#error "The I/O register file of the specified CPU option does not exist"
#endif

/*****************************************************************************/
/* Global type definitions ('typedef')                                       */
/*****************************************************************************/

/*****************************************************************************/
/* Global variable declarations ('extern', definition in C source)           */
/*****************************************************************************/

/*****************************************************************************/
/* Global function prototypes ('extern', definition in C source)             */
/*****************************************************************************/

#endif /* __FR_H__ */
